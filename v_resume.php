<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MPI - Discount Modifier</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->
	<script>
	function dodelete()
	{
	    job=confirm("Are you sure to delete permanently?");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i> Dashboard</a></li>
							<li><a href="datatable_basic.html">Management FDK</a></li>
							<li class="active">View FDK</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">FDK List</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-danger alert-styled-left alert-bordered"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
						</div>

						<table class="table datatable-show-all">
							<thead>
								<tr>
									<th>FDK Number</th>
									<?php foreach($resume_status as $row){ ?>
									<th><?php echo $row->nm_approval ?></th>
									<?php } ?>
									<th>Branch</th>
									<th>Outlet</th>
									<th>Date</th>
									<th>Valid From</th>
									<th>Valid Until</th>
									<th>Status</th>
									<th>Status Description</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
							 <?php foreach($fdk_list as $row){ 

							 	$valid_from = date('Y-m-d', strtotime($row->valid_from));
								$valid_until = date('Y-m-d', strtotime($row->valid_until));
								$tgl_sekarang = date('Y-m-d', strtotime($tgl_sekarang));
							 	$fdk_number = base64_encode($row->number); ?>
								<tr>
									<td>  <a href="<?php echo base_url(); ?>management_fdk/detail_fdk/<?php echo urlencode($fdk_number);?>"><?php echo $row->number;?></a></td>
									<td><?php echo $row->supplierName;?></a></td>
									<td><?php echo $row->branch;?></a></td>
									<td>
										<!--
										<?php if (($tgl_sekarang >= $valid_from) && ($tgl_sekarang <= $valid_until) && $row->id_approval==12 || $row->id_approval==5 || $row->id_approval==14) {  ?>

									<?php $no =1; foreach ($data_detail_fdk[$row->number] as $key => $value) { ?>
									<?php echo $no;?>. <a href="<?php echo base_url(); ?>management_fdk/edit_fdk_outlet/<?php echo urlencode($fdk_number);?>/<?php echo $value->outletSiteNumber;?>"> <?php echo $value->outletPelanggan;?></a>

									<a href="<?php echo base_url(); ?>management_fdk/delete_fdk_outlet/<?php echo urlencode($fdk_number);?>/<?php echo $value->outletSiteNumber;?>"><i class="icon-trash" onClick="return dodelete();"></i></a>

									<br>
									<?php $no++; } ?>
									<?php } else { ?>	

									<?php $no =1; foreach ($data_detail_fdk[$row->number] as $key => $value) { ?>
									<?php echo $no;?>. <?php echo $value->outletPelanggan;?>

									<br>
									<?php $no++; } ?>
									<?php } ?>
									-->
									<?php echo $value->outletPelanggan;?>
									</td>
									<td><?php echo $row->date;?></td>
									<td><?php echo $row->valid_from;?></td>
									<td><?php echo $row->valid_until;?></td>
									<td><span class="label label-success"><?php echo $row->nm_approval;?></span></td>
									
									<td><?php echo $row->description;?></td>
									<td class="text-center">
									<!--
									<?php if (($tgl_sekarang >= $valid_from) && ($tgl_sekarang <= $valid_until) && $row->id_approval!=4) {  ?>
										<?php if($row->id_approval!=4) { ?>
											<?php if($position_id!=1) { ?>
												 <a href="<?php echo base_url(); ?>management_fdk/validation/<?php echo urlencode($fdk_number);?>">Validasi</a>
											<?php } ?>
										<?php } ?>
									<?php } ?>
									-->
									
									<?php if (($tgl_sekarang >= $valid_from) && ($tgl_sekarang <= $valid_until) && $row->id_approval==12 || $row->id_approval==5 || $row->id_approval==14) {  ?>
									
											 <a href="<?php echo base_url(); ?>management_fdk/edit_fdk/<?php echo urlencode($fdk_number);?>">Edit</a>
											 <!--
											 <a href="<?php echo base_url(); ?>management_fdk/tambah_outlet/<?php echo urlencode($fdk_number);?>">Tambah Outlet</a>
												-->

											 <a href="<?php echo base_url(); ?>management_fdk/delete_fdk/<?php echo urlencode($fdk_number);?>" onclick="return confirm('Apakah Anda Yakin?')">Delete</a>
										<?php } ?>

										
									</td>
								</tr>
								
								<?php } ?>
								
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
