/**
 * Site : http:www.smarttutorials.net
 * @author muni
 */
	      
 $(".delete").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('.check_all').prop("checked", false); 
	check();
});
var i=$('table tr').length;

$(".addmore").on('click',function(){
	count=$('table tr').length;
	
    var data="<tr><td><input type='checkbox' class='case'/></td>";
    	data+="<td><span id='snum"+i+"'>"+count+".</span></td>";
    	data+="<td><input class='form-control autocomplete_txt' type='text' data-type='countryname' id='countryname_"+i+"' name='countryname[]'/></td>";
    	data+="<td><input class='form-control autocomplete_txt' type='text' data-type='country_no' id='country_no_"+i+"' name='country_no[]'/></td>";
    	data+="<td><input class='form-control autocomplete_txt' type='text' data-type='phone_code' id='phone_code_"+i+"' name='phone_code[]'/></td>";
    	data+="<td><input class='form-control autocomplete_txt' type='text' data-type='country_code' id='country_code_"+i+"' name='country_code[]'/></td></tr>";
	$('table').append(data);
	row = i ;
	i++;
});
				
function select_all() {
	$('input[class=case]:checkbox').each(function(){ 
		if($('input[class=check_all]:checkbox:checked').length == 0){ 
			$(this).prop("checked", false); 
		} else {
			$(this).prop("checked", true); 
		} 
	});
}

function check(){
	obj=$('table tr').find('span');
	$.each( obj, function( key, value ) {
		id=value.id;
		$('#'+id).html(key+1);
	});
}


//autocomplete script
$(document).on('focus','.autocomplete_txt',function(){
	type = $(this).data('type');
	
	if(type =='countryname' )autoTypeNo=0;
	if(type =='country_no' )autoTypeNo=1; 
	if(type =='phone_code' )autoTypeNo=2; 
	if(type =='country_code' )autoTypeNo=3; 
	
	 $(this).autocomplete({
       minLength: 0,
       source: function( request, response ) {
           var array = $.map(multiple, function (item) {
               var code = item.split("|");
               return {
                   label: code[autoTypeNo],
                   value: code[autoTypeNo],
                   data : item
               }
           });
           //call the filter here
           response($.ui.autocomplete.filter(array, request.term));
       },
       focus: function() {
      	 // prevent value inserted on focus
      	 return false;
       },
       select: function( event, ui ) {
           var names = ui.item.data.split("|");						
   		 id_arr = $(this).attr('id');
   		 id = id_arr.split("_");
   		 elementId = id[id.length-1];
   		 $('#countryname_'+elementId).val(names[0]);
   		 $('#country_no_'+elementId).val(names[1]);
   		 $('#phone_code_'+elementId).val(2);
   		 $('#country_code_'+elementId).val(names[3]);
       }
   });
	 
	 $.ui.autocomplete.filter = function (array, term) {
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
	 };
});