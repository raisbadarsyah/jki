<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Route::get('api/data','AksesLevelController@showAllAksesLevel');
$router->group(['prefix'=>'api', 'middleware' => 'BasicAuth'], function () use ($router) {

     $router->get('akses_level',  ['uses' => 'AksesLevelController@showAllAksesLevel']);

    $router->get('akses_level/{id}', ['uses' => 'AksesLevelController@showOneAksesLevel']);

    $router->post('akses_level', ['uses' => 'AksesLevelController@create']);

    $router->delete('akses_level/{id}', ['uses' => 'AksesLevelController@delete']);

    $router->put('akses_level/{id}', ['uses' => 'AksesLevelController@update']);

    $router->get('provinsi',  ['uses' => 'ProvinsiController@showAllProvinsi']);
    $router->get('kota/{id_province}',  ['uses' => 'KotaController@getKotaByProvince']);
    $router->get('kanwil',  ['uses' => 'KantorWilayahController@showAllKanwil']);
    $router->get('area/{kode_kanwil}',  ['uses' => 'KantorAreaController@getAreaByKanwil']);
    $router->get('tarif',  ['uses' => 'TarifController@showAllTarif']);
    $router->get('daya',  ['uses' => 'DayaController@showAllDaya']);
    $router->post('calon_pelanggan', ['uses' => 'CalonPelangganController@create']);
    $router->post('calon_pelanggan_pln', ['uses' => 'CalonPelangganPlnController@create']);

});

/*
$router->group(['prefix' => 'api'], function () use ($router) {
  $router->get('akses_level',  ['uses' => 'AksesLevelController@showAllAksesLevel']);

  $router->get('akses_level/{id}', ['uses' => 'AksesLevelController@showOneAksesLevel']);

  $router->post('akses_level', ['uses' => 'AksesLevelController@create']);

  $router->delete('akses_level/{id}', ['uses' => 'AksesLevelController@delete']);

  $router->put('akses_level/{id}', ['uses' => 'AksesLevelController@update']);

  $router->get('provinsi',  ['uses' => 'ProvinsiController@showAllProvinsi']);
  $router->get('kota/{id_province}',  ['uses' => 'KotaController@getKotaByProvince']);
  $router->get('kanwil',  ['uses' => 'KantorWilayahController@showAllKanwil']);
  $router->get('area/{kode_kanwil}',  ['uses' => 'KantorAreaController@getAreaByKanwil']);
  $router->get('tarif',  ['uses' => 'TarifController@showAllTarif']);
  $router->get('daya',  ['uses' => 'DayaController@showAllDaya']);
  $router->post('calon_pelanggan', ['uses' => 'CalonPelangganController@create']);
});
*/
