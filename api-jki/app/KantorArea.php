<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KantorArea extends Model
{
    protected $table = "kantor_area";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nm_area',
        'kode_kanwil',
        'alamat_area'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function scopeAreaByKanwil($query, $kode_kanwil)
    {
        return $query->where('kode_kanwil', $kode_kanwil);

    }
}