<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KantorWilayah extends Model
{
    protected $table = "kantor_wilayah";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nm_kanwil',
        'kode_kanwil'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}