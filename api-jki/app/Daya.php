<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daya extends Model
{
    protected $table = "daya";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'daya',
        'id_daya'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}