<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalonPelangganPln extends Model
{
    protected $table = "calon_pelanggan_pln";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'noagenda',
        'idpel',
        'namaplg',
        'alamatplg',
        'notelpplg',
        'tarif',
        'daya',
        'email',
        'unitup',
        'namaup',
        'kodekab',
        'namakab',
        'longitude',
        'latitude',
        'tglbayar',
        'noregpln'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}