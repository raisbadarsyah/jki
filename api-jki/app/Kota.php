<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = "kota";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nm_kota',
        'id_provinsi',
        'kd_djp_kota'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function scopeKotaByProvince($query, $id_province)
    {
        return $query->where('id_provinsi', $id_province);

    }
}