<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalonPelanggan extends Model
{
    protected $table = "calon_pelanggan";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'telepon',
        'id_tarif',
        'id_daya',
        'id_wilayah',
        'email',
        'id_area',
        'id_provinsi',
        'id_kota',
        'alamat',
        'via'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}