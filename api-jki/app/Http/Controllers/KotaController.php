<?php

namespace App\Http\Controllers;

use App\Kota;
use Illuminate\Http\Request;

class KotaController extends Controller
{

    public function showAllKota()
    {
        return response()->json(Kota::all());
    }

    public function getKotaByProvince($id_province)
    {
        
        return response()->json(Kota::kotaByProvince($id_province)->get());
       
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'nm_akses' => 'required'
        ]);

        $author = AksesLevel::create($request->all());

        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}