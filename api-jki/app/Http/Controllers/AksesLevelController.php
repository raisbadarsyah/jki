<?php

namespace App\Http\Controllers;

use App\AksesLevel;
use Illuminate\Http\Request;

class AksesLevelController extends Controller
{

    public function showAllAksesLevel()
    {
        return response()->json(AksesLevel::all());
    }

    public function showOneAksesLevel($id)
    {
        return response()->json(AksesLevel::find($id));
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'nm_akses' => 'required'
        ]);

        $author = AksesLevel::create($request->all());

        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}