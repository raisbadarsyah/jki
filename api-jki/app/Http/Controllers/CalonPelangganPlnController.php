<?php

namespace App\Http\Controllers;

use App\CalonPelangganPln;
use Illuminate\Http\Request;

class CalonPelangganPlnController extends Controller
{

    public function showAllKanwil()
    {
        return response()->json(KantorWilayah::all());
    }

    public function showOneAksesLevel($id)
    {
        return response()->json(AksesLevel::find($id));
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'noagenda' => 'required',
            'idpel' => 'required',
            'namaplg' => 'required',
            'alamatplg' => 'required',
            'tarif' => 'required',
            'daya' => 'required'
        ]);

       // $values = array('id' => 1,'name' => 'Dayle');
        //DB::table('users')->insert($values);
        # Filer & only get specific parameters.
        //$request = $request->only('code', 'name', 'status');

        $calon_pelanggan = CalonPelangganPln::create($request->all());

        if(!$calon_pelanggan)
        {
            $response = array('rc' => '99',
                              'desc' => 'Error Server');

        }else{
            $response = array('rc' => '00',
                              'desc' => 'Sukses');
        }

        return response()->json($response, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}