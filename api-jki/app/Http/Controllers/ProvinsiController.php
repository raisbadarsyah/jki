<?php

namespace App\Http\Controllers;

use App\Provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller
{

    public function showAllProvinsi()
    {
        return response()->json(Provinsi::all());
    }

    public function showOneAksesLevel($id)
    {
        return response()->json(AksesLevel::find($id));
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'nm_akses' => 'required'
        ]);

        $author = AksesLevel::create($request->all());

        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}