<?php

namespace App\Http\Controllers;

use App\CalonPelanggan;
use Illuminate\Http\Request;

class CalonPelangganController extends Controller
{

    public function showAllKanwil()
    {
        return response()->json(KantorWilayah::all());
    }

    public function showOneAksesLevel($id)
    {
        return response()->json(AksesLevel::find($id));
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'nama' => 'required',
            'telepon' => 'required',
            'id_daya' => 'required',
            'id_tarif' => 'required',
            'id_provinsi' => 'required',
            'id_kota' => 'required',
            'alamat' => 'required'
        ]);

       // $values = array('id' => 1,'name' => 'Dayle');
        //DB::table('users')->insert($values);
        # Filer & only get specific parameters.
        //$request = $request->only('code', 'name', 'status');

        $calon_pelanggan = CalonPelanggan::create($request->all());

        if(!$calon_pelanggan)
        {
            $response = array('error_code' => 500,
                              'message' => 'Failed input data');

        }else{
            $response = array('error_code' => 201,
                              'message' => 'Successfully');
        }

        return response()->json($response, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}