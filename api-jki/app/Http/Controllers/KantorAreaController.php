<?php

namespace App\Http\Controllers;

use App\KantorArea;
use Illuminate\Http\Request;

class KantorAreaController extends Controller
{

    public function showAllKota()
    {
        return response()->json(Kota::all());
    }

    public function getAreaByKanwil($kode_kanwil)
    {
        
        return response()->json(KantorArea::areaByKanwil($kode_kanwil)->get());
       
    }

    public function create(Request $request)
    {   
        $this->validate($request, [
            'nm_akses' => 'required'
        ]);

        $author = AksesLevel::create($request->all());

        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        $author = AksesLevel::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        AksesLevel::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}