<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AksesLevel extends Model
{
    protected $table = "akses_level";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nm_akses'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}