<?php 

	//helper function for auth module via uri
	function check_authority_url(){
		$CI =& get_instance();
		$CI->load->model('menu_model',true);
		$CI->load->library('session');
		
		$id_user = $CI->session->userdata('user_id');
		$id_grup = $CI->session->userdata('position_id');
	//	$position_id = $CI->session->userdata('position_id');
		
		$name    = $CI->uri->segment(1);
		$id_menu = $CI->menu_model->get_menu_id($name);
		
		$check_authority = $CI->menu_model->get_authority_grup($id_menu, $id_grup, $id_user); 
		
		if($id_menu){
			$check_authority = $CI->menu_model->get_authority_grup($id_menu, $id_grup, $id_user); 
			
			if (!$check_authority) {
				redirect(base_url().'dashboard', 'refresh');
			}
		}
		else
		{
			redirect(base_url().'dashboard', 'refresh');
		} 	
		

		//echo $id_menu;

	}
	//end of helper function for auth module via uri

	//helper function for sidebar menu
	function check_authority_menu($parentId=0){
		$CI =& get_instance();
		$CI->load->model('menu_model',true);
		$CI->load->library('session');
		
		$id_user = $CI->session->userdata('user_id');
		$id_grup = $CI->session->userdata('dep_id');
		//$position_id = $CI->session->userdata('position_id');
		$level = $CI->session->userdata('level');

		if($level == 4){
			$position_id = $CI->session->userdata('position_id');
			$menu = $CI->menu_model->get_super_user_parent_menu($parentId,$id_user,$position_id);
		}
		elseif($level == 3){
			$position_id = $CI->session->userdata('position_id');
			$menu = $CI->menu_model->get_mpi_user_parent_menu($parentId, $id_user, $position_id);
		}
		else
		{
			$menu = $CI->menu_model->get_parent_menu($parentId, $id_user);
		}
		
		foreach ($menu as $parent) {
	        if ($parent->Count > 0) {
	            echo '<li>
                        <a href="'.base_url().$parent->coreMenuUrl.'" title="'.$parent->coreMenuTitle.'">
                        	
                        	<i class="'.$parent->coreMenuIcon.'"></i><span>
                           '.$parent->coreMenuName.'
                            </span>
                        </a>
                        <ul>';
	            check_authority_menu($parent->coreMenuId);
	            echo '</ul>';
	            echo '</li>';
            }elseif (empty($parent->Count)) {
	        	echo '<li>
                        <a href="'.base_url().$parent->coreMenuUrl.'" title="'.$parent->coreMenuTitle.'">
                            <i class="'.$parent->coreMenuIcon.'"></i><span>
                            '.$parent->coreMenuName.'
                            </span>
                        </a>
                    </li>';
	        }
		}
	}

	function check_super_user_authority_menu($parentId=0){
		$CI =& get_instance();
		$CI->load->model('menu_model',true);
		$CI->load->library('session');
		
		$id_user = $CI->session->userdata('user_id');
	//	$id_grup = $CI->session->userdata('dep_id');

		$menu = $CI->menu_model->get_super_user_parent_menu($parentId);
		
		foreach ($menu as $parent) {
	        if ($parent->Count > 0) {
	            echo '<li>
                        <a href="'.base_url().$parent->coreMenuUrl.'" title="'.$parent->coreMenuTitle.'">
                        	
                        	<i class="'.$parent->coreMenuIcon.'"></i>
                           '.$parent->coreMenuName.'
                            
                        </a>
                        <ul>';
	            check_authority_menu($parent->coreMenuId);
	            echo '</ul>';
	            echo '</li>';
            }elseif (empty($parent->Count)) {
	        	echo '<li>
                        <a href="'.base_url().$parent->coreMenuUrl.'" title="'.$parent->coreMenuTitle.'">
                            <i class="'.$parent->coreMenuIcon.'"></i>
                            '.$parent->coreMenuName.'
                        </a>
                    </li>';
	        }
		}
	}
	//end of helper function for sidebar menu

	//helper function  for breadcrumb
	function get_breadcrumb($uri){
		$CI =& get_instance();
		$CI->load->model('menu_model',true);
		$breadcrumb = $CI->menu_model->get_parent_menu_breadcrumb($uri);
			foreach ($breadcrumb as $val) {
				if($val->coreMenuUrl == $uri && !empty($val->coreParentId)){
					get_all_breadcrumb($val->coreParentId, $uri);
					echo '<li class="active"><i class="fa '.$val->coreMenuIcon.'"></i> '.$val->coreMenuName.'</li>';
				}else{
					echo '<li><a href="'.base_url().$val->coreMenuUrl.'"><i class="fa '.$val->coreMenuIcon.'"></i> '.$val->coreMenuName.'</a></li>';
				}
			}
	}

	function get_all_breadcrumb($id=0, $uri=0){
		$CI =& get_instance();
		$CI->load->model('menu_model',true);
		$breadcrumb = $CI->menu_model->get_parent_menu_id_breadcrumb($id);
		//echo $CI->db->last_query();die();
		foreach ($breadcrumb as $val) {
			if(empty($val->coreParentId) && $val->coreMenuId !== '1'){
				get_all_breadcrumb('1');
				echo '<li><a href="'.base_url().$val->coreMenuUrl.'"><i class="fa '.$val->coreMenuIcon.'"></i> '.$val->coreMenuName.'</a></li>';
			}else{
				get_all_breadcrumb($val->coreParentId);
				echo '<li><a href="'.base_url().$val->coreMenuUrl.'"><i class="fa '.$val->coreMenuIcon.'"></i> '.$val->coreMenuName.'</a></li>';
			}
		}
	}
	//end of helper function breadcrumb

	//helper for list module on management group->role
	function get_list_module($status,$roles=array(),$parentId=0,$span=0){
		$CI =& get_instance();
		$CI->load->model('management_position_model',true);
		
		$menu = $CI->management_position_model->get_parent_menu($parentId);
		//echo "<pre>".$CI->db->last_query()."</pre>";die();
		foreach ($menu as $parent) {
			$html='';
			if($status=='edit'){
				$id='';
              	foreach($roles as $role3){
	              if($role3->coreRoleMenu == $parent->coreMenuId){
	                $id=$role3->coreRoleMenu;
	                $active = $role3->coreRoleActive;
	                if($active !== '1'){
	                  $id='';
	                }
	              }
            	}
			}
			
	        if ($parent->Count > 0) {
	        	$html .= '<tr id="menu_num_'.$parent->coreMenuId.'">
                      <td width="40%"><span style="padding-left:'.$span.'%">'.$parent->coreMenuName.'&nbsp;&nbsp;>></td>';
                    if($status=='edit'){
                    	if($id==$parent->coreMenuId){
                      		$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" checked ></td>';
                      	}else{
                      		$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" ></td>';
                      	}
                        
                        if($id == $parent->coreMenuId){
                          $html .= '<td style="display: none"><input name="remove[]" type="checkbox" id="remove_'.$parent->coreMenuId.'" value="'.$parent->coreMenuId.'" style="display: none"/></td>
                          <td class="menu_'.$parent->coreMenuId.'" width="4%"> >> </td>';
                          if($active == 1){
                          	$html .= '<td class="menu_'.$parent->coreMenuId.'" colspan="3"><input name="active_'.$parent->coreMenuId.'" type="checkbox" checked value="1"/> <b>Active</b></td>';
                          }else{
                          	$html .= '<td class="menu_'.$parent->coreMenuId.'" colspan="3"><input name="active_'.$parent->coreMenuId.'" type="checkbox" value="1"/> <b>Active</b></td>';
                          }
                        }
                    }else{
                    	$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" ></td>';
                    }
                    $html .= '</tr>';
                    echo $html;
                    $span=$span+5;
	            get_list_module($status,$roles,$parent->coreMenuId,$span);
	            $span=$span-5;
            }elseif (empty($parent->Count)) {
	        	$html .= '<tr id="menu_num_'.$parent->coreMenuId.'">
                      <td width="40%"><span style="padding-left:'.$span.'%">'.$parent->coreMenuName.'</td>';
                    if($status=='edit'){
                    	if($id==$parent->coreMenuId){
                      		$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" checked ></td>';
                      	}else{
                      		$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" ></td>';
                      	}
                        
                        if($id == $parent->coreMenuId){
                          $html .= '<td style="display: none"><input name="remove[]" type="checkbox" id="remove_'.$parent->coreMenuId.'" value="'.$parent->coreMenuId.'" style="display: none"/></td>
                          <td class="menu_'.$parent->coreMenuId.'" width="4%"> >> </td>';
                          if($active == 1){
                          	$html .= '<td class="menu_'.$parent->coreMenuId.'" colspan="3"><input name="active_'.$parent->coreMenuId.'" type="checkbox" checked value="1"/> <b>Active</b></td>';
                          }else{
                          	$html .= '<td class="menu_'.$parent->coreMenuId.'" colspan="3"><input name="active_'.$parent->coreMenuId.'" type="checkbox" value="1"/> <b>Active</b></td>';
                          }
                        }
                    }else{
                    	$html .= '<td><input type="checkbox" class="set_role" id="menu_'.$parent->coreMenuId.'" name="id_menu[]" value="'.$parent->coreMenuId.'" ></td>';
                    }
                    $html .= '</tr>';
                    echo $html;
	        }
		}
	}
	//end of helper for list module on management group->role
	
	
	//function veiew checkbox or delete button on view detail report title
	function status_title($id_title, $id_principal){
		$CI =& get_instance();
		$CI->load->model('ref_title_model',true);
		
		$status = $CI->ref_title_model->get_status_title_principal($id_title, $id_principal);
		
		$link = base_url().'ref_title/delete_detail/'.$id_title.'/'.$id_principal;
		if(empty($status)){
			echo"<input type='checkbox' name='title_id[]' id='checkItem' value='".$id_title."' />";
		} else {
			echo "<a href='$link' class='btn btn-primary' title='Detail'><i class='glyphicon glyphicon-remove'></i></a>";
		}
	}
	
	//function veiew button send email veirifkasi
	function cek_email_verifikasi($userId, $id_principal, $id_cabang){
		$CI =& get_instance();
		$CI->load->model('management_user_model',true);
		$email_user = $CI->management_user_model->get_email_user($userId);
		$email      = $CI->management_user_model->get_email_verifikasi($email_user);
		
		$countSetPrincipal = $CI->management_user_model->count_user_principal_on_set($userId, $id_principal);
		$countSetCabang    = $CI->management_user_model->count_user_cabang_on_set($userId, $id_cabang);
		
		//echo $CI->db->last_query();
		$link = base_url().'management_user/email_verifikasi/'.$userId.'/'.$id_principal.'/'.$id_cabang;
		if(empty($email)){
			echo"Email Not Found";
		} else {
			if(!empty($countSetPrincipal) && !empty($countSetCabang)){
				echo "-";
			} else {				
				echo "<a href='$link' class='btn btn-primary' title='Detail'><i class='glyphicon glyphicon-ok-circle'></i></a>";
			}
		}
	}
	
	function cek_status_user($userId, $id_principal, $id_cabang){
		$CI =& get_instance();
		$CI->load->model('management_user_model',true);
		
		$countSetPrincipal = $CI->management_user_model->count_user_principal_on_set($userId, $id_principal);
		$countSetCabang    = $CI->management_user_model->count_user_cabang_on_set($userId, $id_cabang);
		
		//echo $CI->db->last_query();		
		if(!empty($countSetPrincipal) && !empty($countSetCabang)){
			echo "Active";
		} else {				
			echo "Inactive";
		}
		
	}
	
	//function veiew checkbox or delete button on view detail report title stock
	function status_title_stock($id_title, $id_principal){
		$CI =& get_instance();
		$CI->load->model('ref_title_stock_model',true);
		
		$status = $CI->ref_title_stock_model->get_status_title_principal($id_title, $id_principal);
		
		$link = base_url().'ref_title_stock/delete_detail/'.$id_title.'/'.$id_principal;
		if(empty($status)){
			echo"<input type='checkbox' name='title_id[]' id='checkItem' value='".$id_title."' />";
		} else {
			echo "<a href='$link' class='btn btn-primary' title='Detail'><i class='glyphicon glyphicon-remove'></i></a>";
		}
	}
	
	function cek_set_checkbox($userId, $id_principal, $id_cabang){
		$CI =& get_instance();
		$CI->load->model('management_user_model',true);
		
		$countSetPrincipal = $CI->management_user_model->count_user_principal_on_set($userId, $id_principal);
		$countSetCabang    = $CI->management_user_model->count_user_cabang_on_set($userId, $id_cabang);
		
		if(!empty($countSetPrincipal) && !empty($countSetCabang)){			
			echo"-";
		} else {			
			echo"<input type='checkbox' name='coreUserId[]' id='checkItem' value='".$userId."' />";
		}
	}
	
?>