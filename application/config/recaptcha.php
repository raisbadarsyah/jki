<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LcA8hEUAAAAAB6pUyH6fyxNz_NR-GdfMhGxPm1f';
$config['recaptcha_secret_key'] = '6LcA8hEUAAAAACvcWuowP-KR_9Oa1U_BDGAp__4p';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
