<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarif extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Tarif_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$data['position_id'] = $this->session->position_id;
		$data['tarif'] = $this->Tarif_Model->get_all();


		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$this->load->view('add');

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$jenis_tarif = $this->input->post('jenis_tarif');
			
			try {
				
				$data_tarif = array(
					"nm_tarif"=> $jenis_tarif
				);
				
				$insert_tarif = $this->Tarif_Model->insert($data_tarif);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($insert_tarif == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('tarif','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_tarif = $this->uri->segment(3);
		$data_tarif = $this->Tarif_Model->get_tarif_by_id($id_tarif);
		$data['data_tarif'] = $data_tarif;
		$this->load->view('edit',$data);

	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_tarif = $this->input->post('id_tarif');
			$jenis_tarif = $this->input->post('jenis_tarif');
			
			try {
				
				$data_tarif = array(
					"nm_tarif"=> $jenis_tarif
				);
				
				$update_tarif = $this->Tarif_Model->update($data_tarif,$id_tarif);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($update_tarif == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('tarif','refresh');
				}


		}
	}

	function delete($id_tarif){
		$action = $this->Tarif_Model->delete($id_tarif);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'tarif', 'refresh');
	}

}
