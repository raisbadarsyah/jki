<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'ref_title/update'; ?>" enctype="multipart/form-data">
									  <?php foreach($list_title as $row): ?>
									  <input name="id" type="hidden" value="<?php echo $row->refTitleId; ?>">
									 <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Title Name</label>
										<div class="col-sm-8">
										    <select class="form-control select2" style="width: 100%;" name="refTitleName" id="group" required>
												<option value="PRINCIPAL" <?php if($row->refTitleName =='PRINCIPAL'){ echo "selected='selected'"; }?>>PRINCIPAL</option>
												<option value="BRANCH_NAME" <?php if($row->refTitleName =='BRANCH_NAME'){ echo "selected='selected'"; }?>>BRANCH_NAME</option>
												<option value="JENIS" <?php if($row->refTitleName =='BRANCH_NAME'){ echo "selected='selected'"; }?>>JENIS</option>
												<option value="NO_PERFORMA" <?php if($row->refTitleName =='NO_PERFORMA'){ echo "selected='selected'"; }?>>NO_PERFORMA</option>
												<option value="NO_INVOICE" <?php if($row->refTitleName =='NO_INVOICE'){ echo "selected='selected'"; }?>>NO_INVOICE</option>
												<option value="PELANGGAN" <?php if($row->refTitleName =='PELANGGAN'){ echo "selected='selected'"; }?>>PELANGGAN</option>
												<option value="KLSOUT" <?php if($row->refTitleName =='KLSOUT'){ echo "selected='selected'"; }?>>KLSOUT</option>
												<option value="ITEM_CODE" <?php if($row->refTitleName =='ITEM_CODE'){ echo "selected='selected'"; }?>>ITEM_CODE</option>
												<option value="NAMA_PRODUK" <?php if($row->refTitleName =='NAMA_PRODUK'){ echo "selected='selected'"; }?>>NAMA_PRODUK</option>
												<option value="QTY" <?php if($row->refTitleName =='QTY'){ echo "selected='selected'"; }?>>QTY</option>
												<option value="SALES" <?php if($row->refTitleName =='SALES'){ echo "selected='selected'"; }?>>SALES</option>
												<option value="SALESMAN" <?php if($row->refTitleName =='SALESMAN'){ echo "selected='selected'"; }?>>SALESMAN</option>
											</select>	
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Principal</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="refTitlePrincipal" id="group" required>
											<option value="">--Select--</option>
											<?php foreach($list_principal as $rows){?>
											  <option value="<?php echo $rows->supplierPrincipal;?>" <?php if($rows->supplierPrincipal == $row->refTitlePrincipal){ echo "selected='selected'"; }?>><?php echo $rows->supplierPrincipal;?></option>
											<?php };?>
										  </select>
										</div>
									  </div>
									  
									   <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Status</label>
										<div class="col-sm-8">
											<select class="form-control select2" style="width: 100%;" name="refTitleStatus" id="group" required>
												<option value="Y" <?php if($row->refTitleStatus =='Y'){ echo "selected='selected'"; }?>>Y</option>
												<option value="N" <?php if($row->refTitleStatus =='N'){ echo "selected='selected'"; }?>>N</option>						
											</select>	
										</div>
									  </div>
									  
									  
									 <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									  </div>
									<?php endforeach; ?>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>