<?php
defined('BASEPATH') OR exit('No direct script access allowed');error_reporting(E_ALL);

class Auth extends MX_Controller {
    function __construct(){
        parent::__construct();
		$this->load->model('register/Register_model');
    }

	function index()
	{
		
        if($this->session->userdata('is_login')){
            redirect('dashboard', 'refresh');
        }else{
            
            $config['title'] = "Login";
            $config['title_box'] = "Login Page";
    		$this->load->view('login',$config);
        }
	}

	function login()
	{
        if($this->session->userdata('is_login')){
            redirect('dashboard', 'refresh');
        }else{
            $user = $this->input->post('user');
            $pass = md5($this->input->post('password'));
			
			//if($string==$this->session->userdata('captchaUser')) {  
				if(isset($user)){

					//query process
					$this->db->select('*');
					$this->db->from('core_user');
					$this->db->where("coreUserName=".$this->db->escape($user));
					$this->db->where("coreUserPassword=".$this->db->escape($pass));					
					$this->db->where('coreUserActive', '1');

					$sql = $this->db->get();            
																																																			  
					if ( $sql->num_rows() > 0 ){                                                                                                                                                      
						foreach ($sql->result() as $row){ 
							$sess['is_login'] = TRUE;
							
							$sess['user_id'] = $row->coreUserId; 
                            $sess['position_id'] = $row->coreUserPositionId;
                            $sess['id_departemen'] = $row->id_departemen;	
                         						
							$sess['email'] = $row->coreUserEmail;
							$sess['uname'] = $row->coreUserName;
							$this->session->set_userdata($sess);                                                                          
						}
						if(isset($location)){
							redirect("http://".$_SERVER['HTTP_HOST'].$location);
						}else{
							//echo $this->db->last_query();die();
							redirect('dashboard');    
						}
					}
					else
					{
						$this->session->set_flashdata('message','Username/Email atau Password anda salah');
						// echo $this->db->last_query();die();
						redirect('auth','refresh');
					}
				}else{
					$this->session->set_flashdata('message','Please use login form!');
					// echo $this->db->last_query();die();
					redirect('auth','refresh');
				}
			}
			
		
	}
	
	public function create_captcha() {
        $this->load->helper('captcha');
        $random_number = substr(number_format(time() * rand(),0,'',''),0,6);
        $vals = array(
         'word' => $random_number,
         'img_path' => './captcha/',
         'img_url' => base_url().'captcha/',
         'img_width' => 140,
         'img_height' => 50,
         'expiration' => 7200
        );

        $cap    = create_captcha($vals);
        $image  = $cap['image'];
        $this->session->set_userdata('captchaUser', $cap['word']);

        print $image;
    }
	
        
    function logout()
	{
        $this->session->sess_destroy();
        redirect(base_url().'auth','refresh');
	}
}
