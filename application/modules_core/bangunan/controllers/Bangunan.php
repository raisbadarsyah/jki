<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bangunan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Bangunan_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['bangunan'] = $this->Bangunan_Model->get_all();


		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$this->load->view('add');

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$jenis_bangunan = $this->input->post('jenis_bangunan');
			$tampil = $this->input->post('tampil');
			
			try {
				
				$data_bangunan = array(
					"nm_bangunan"=> $jenis_bangunan,
					"Active"=> $tampil
				);
				
				$insert_bangunan = $this->Bangunan_Model->insert($data_bangunan);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($insert_bangunan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('bangunan','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_bangunan = $this->uri->segment(3);
		$data_bangunan = $this->Bangunan_Model->get_by_id($id_bangunan);
		$data['data_bangunan'] = $data_bangunan;
		$this->load->view('edit',$data);
	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_bangunan = $this->input->post('id_bangunan');
			$jenis_bangunan = $this->input->post('jenis_bangunan');
			$tampil = $this->input->post('tampil');
			
			try {
				
				$data_bangunan = array(
					"nm_bangunan"=> $jenis_bangunan,
					"Active"=> $tampil
				);
				
				$update_bangunan = $this->Bangunan_Model->update($data_bangunan,$id_bangunan);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($update_bangunan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('bangunan','refresh');
				}


		}
	}

	function delete($id_bangunan){
		$action = $this->Bangunan_Model->delete($id_bangunan);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'bangunan', 'refresh');
	}

}
