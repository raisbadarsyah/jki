<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daya extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Daya_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$position_id = $this->session->position_id;
		$data['daya'] = $this->Daya_Model->get_all();
		$data['position_id'] = $position_id;

		$this->load->view('index',$data);
	}

	function add()
	{	
		$data['fasa'] = $this->Daya_Model->get_fasa();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$daya = $this->input->post('daya');
			$id_fasa = $this->input->post('id_fasa');
			$hrg_per_va = $this->input->post('hrg_per_va');
			$biaya = $this->input->post('biaya');
			$ppn = $this->input->post('ppn');
			$total = $this->input->post('total');
			$terbilang = $this->input->post('terbilang');

			try {
				
				$data_daya = array(
					"id_fasa"=> $id_fasa,
					"daya"=> $daya,
					"hrg_per_va"=> $hrg_per_va,
					"biaya"=> $biaya,
					"ppn"=> $ppn,
					"total"=> $total,
					"terbilang"=> $terbilang
				);
				
				$insert_daya = $this->Daya_Model->insert($data_daya);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_daya == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('daya','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_daya = $this->uri->segment(3);
		$data['daya'] = $this->Daya_Model->get_daya($id_daya);
		$data['fasa'] = $this->Daya_Model->get_fasa();
		$this->load->view('edit',$data);
	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_daya = $this->input->post('id_daya');
			$daya = $this->input->post('daya');
			$id_fasa = $this->input->post('id_fasa');
			$hrg_per_va = $this->input->post('hrg_per_va');
			$biaya = $this->input->post('biaya');
			$ppn = $this->input->post('ppn');
			$total = $this->input->post('total');
			$terbilang = $this->input->post('terbilang');

			try {
				
				$data_daya = array(
					"id_fasa"=> $id_fasa,
					"daya"=> $daya,
					"hrg_per_va"=> $hrg_per_va,
					"biaya"=> $biaya,
					"ppn"=> $ppn,
					"total"=> $total,
					"terbilang"=> $terbilang
				);
				
				$update_daya = $this->Daya_Model->update($data_daya,$id_daya);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal edit daya');

			}


				if($update_daya == 1) {
						$this->session->set_flashdata('success', 'Data has been edited successfully');
						redirect('daya','refresh');
				}


		}
	}

	function delete($id_daya){
		$action = $this->Daya_Model->delete($id_daya);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'daya', 'refresh');
	}

}
