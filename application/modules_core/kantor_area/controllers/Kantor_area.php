<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kantor_area extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','Kota/Kota_Model','Kantor_Area_Model','Kantor_Sub_Area/Kantor_Sub_Area_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['kantor_area'] = $this->Kantor_Area_Model->get_all();


		$this->load->view('index',$data);
	}

	function add()
	{	
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_area = $this->input->post('kode_area');
			$area_lit = $this->input->post('area_lit');
			$nm_area = $this->input->post('nm_area');
			$kode_djk = $this->input->post('kode_djk');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kanwil = $this->input->post('kanwil');
			$telpon = $this->input->post('telpon');
			$manager = $this->input->post('manager');
			$pjt = $this->input->post('pjt');
			$nm_penanda_tangan = $this->input->post('nm_penanda_tangan');
			$jabatan_penanda_tangan = $this->input->post('jabatan_penanda_tangan');
			$kualifikasi_penanda_tangan = $this->input->post('kualifikasi_penanda_tangan');
			$status = $this->input->post('status');


			try {
				
				$data_kantor_area = array(
					"kode_area"=> $kode_area,
					"area_id"=> $area_lit,
					"kode_djk"=> $kode_djk,
					"kode_kanwil"=> $kanwil,
					"nm_area"=> $nm_area,
					"alamat_area"=> $alamat,
					"id_kota"=> $kota,
					"telpon"=> $telpon,
					"manager"=> $manager,
					"pjt"=> $pjt,
					"signature_name"=> $nm_penanda_tangan,
					"signature_jabatan"=> $jabatan_penanda_tangan,
					"signature_kualifikasi"=> $kualifikasi_penanda_tangan,
					"status"=> $status
				);
			
			
				$insert_kantor_area = $this->Kantor_Area_Model->insert($data_kantor_area);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input');

			}


				if($insert_kantor_area == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kantor_area','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_area = $this->uri->segment(3);

		$data['detail_area'] = $this->Kantor_Area_Model->get_by_id($kode_area);
		
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kota'] = $this->Kota_Model->kota_by_provinsi($data['detail_area']->id_provinsi);

		$cakupan_kantor_area = array();

		foreach ($data['kota'] as $key => $value) {
			$cakupan_kantor_area[$value->id_kota] = $this->Kantor_Area_Model->cakupan_kantor_area($kode_area,$value->id_kota);
		}
		
		$data['cakupan_kantor_area'] = $cakupan_kantor_area;
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$data['area'] = $this->Kantor_Sub_Area_Model->get_area($data['detail_area']->kode_kanwil);
		$data['lit_area_api'] = json_decode($this->get_api_lit_area($data['detail_area']->wilayah_id), TRUE);
		
		$this->load->view('edit',$data);

		
	}

	function proses_edit()
	{

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_area = $this->input->post('kode_area');
			$area_lit = $this->input->post('area_lit');
			$nm_area = $this->input->post('nm_area');
			$kode_djk = $this->input->post('kode_djk');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$kanwil = $this->input->post('kanwil');
			$telpon = $this->input->post('telpon');
			$manager = $this->input->post('manager');
			$pjt = $this->input->post('pjt');
			$nm_penanda_tangan = $this->input->post('nm_penanda_tangan');
			$jabatan_penanda_tangan = $this->input->post('jabatan_penanda_tangan');
			$kualifikasi_penanda_tangan = $this->input->post('kualifikasi_penanda_tangan');
			$status = $this->input->post('status');
			$id_kota = $this->input->post('id_kota');
		
			
			

			try {
				
				$delete_cakupan_kantor_area = $this->Kantor_Area_Model->delete_cakupan_area($kode_area);
				
				foreach($id_kota as $id_kota){

					$cakupan_kantor_area = array(
						"id_kota" 		=> $id_kota,
						"kode_area" 		=> $kode_area
					);

					$insert_cakupan_kantor_area = $this->Kantor_Area_Model->insert_cakupan_area($cakupan_kantor_area);
				   
				}

			

				$data_kantor_area = array(
					"area_id"=> $area_lit,
					"kode_djk"=> $kode_djk,
					"kode_kanwil"=> $kanwil,
					"nm_area"=> $nm_area,
					"alamat_area"=> $alamat,
					"id_kota"=> $kota,
					"telpon"=> $telpon,
					"manager"=> $manager,
					"pjt"=> $pjt,
					"signature_name"=> $nm_penanda_tangan,
					"signature_jabatan"=> $jabatan_penanda_tangan,
					"signature_kualifikasi"=> $kualifikasi_penanda_tangan,
					"status"=> $status
				);
			
			
				$update_kantor_area = $this->Kantor_Area_Model->update($data_kantor_area,$kode_area);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input');

			}


				if($update_kantor_area == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kantor_area','refresh');
				}


		}

	}


	function get_kota()
	{
		$provinsi = $this->input->post('provinsi');
		$kota = $this->Kantor_Area_Model->get_kota($provinsi);

		 foreach($kota as $key=>$Kota)
          {

           echo"<option value='".$Kota->id_kota."'>".$Kota->nm_kota."</option>";
          }
         // echo "</select>";
          exit();

	}

	function get_api_lit_area($wilayah_id)
	{

		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
			'wilayah_id' => $wilayah_id //optional
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/lit/area'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/lit/area'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		//echo $tmp;
		return $tmp;

	}

	function get_area_lit()
	{
		$kanwil = $this->input->post('kanwil');
		$detail_kanwil = $this->Kantor_Area_Model->get_kanwil($kanwil);
		$data_lit_area_api = json_decode($this->get_api_lit_area($detail_kanwil->wilayah_id), TRUE);
		
		 foreach($data_lit_area_api['area'] as $key=>$lit_area)
          {

           echo"<option value='".$lit_area['area_id']."'>".$lit_area['nama']."</option>";
          }
         // echo "</select>";
          exit();

	}

	function delete($kode_area){
		$action = $this->Kantor_Area_Model->delete($kode_area);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'kantor_area', 'refresh');
	}


}
