<?php

class Kantor_area_model extends CI_Model {

   function get_all(){
		$this->db->select('*');
        $this->db->from('kantor_area');
        $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function area_by_kanwil($kode_kanwil){
        $this->db->select('*');
        $this->db->from('kantor_area');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

     function sub_area_by_area($kode_area){
        $this->db->select('*');
        $this->db->from('kantor_sub_area');
        $this->db->where('kode_area',$kode_area);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota($id_provinsi) {
        $this->db->select('*');
        $this->db->from('kota');
    
        $this->db->where('id_provinsi',$id_provinsi);
        $query = $this->db->get();
     
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kanwil($kode_kanwil) {
        $this->db->select('*');
        $this->db->from('kantor_wilayah');
    
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
     
        return $query->row();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

     function cakupan_kantor_area($kode_area,$id_kota){
        $this->db->select('*');
        $this->db->from('cakupan_kantor_area');
        $this->db->join('kota','cakupan_kantor_area.id_kota=kota.id_kota');
        $this->db->where('kode_area',$kode_area);
        $this->db->where('cakupan_kantor_area.id_kota',$id_kota);
        $query = $this->db->get();
       
        return $query->row();
    }

    function cakupan_kantor_area_by_area($kode_area){
        $this->db->select('*');
        $this->db->from('cakupan_kantor_area');
        $this->db->join('kota','cakupan_kantor_area.id_kota=kota.id_kota');
        $this->db->where('kode_area',$kode_area);
       
        $query = $this->db->get();
       
        return $query->result();
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($kode_area){
		$this->db->select('*');
        $this->db->from('kantor_area');
        $this->db->join('kota','kota.id_kota=kantor_area.id_kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
        $this->db->where('kode_area', $kode_area);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_kantor_area) {
       $query = $this->db->insert('kantor_area', $data_kantor_area);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $kode_area){
        $this->db->trans_begin();
		$this->db->where('kode_area', $kode_area);
		$this->db->update('kantor_area', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

     function delete($kode_area) {
        $query = $this->db->delete('kantor_area', array('kode_area' => $kode_area));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

    function insert_cakupan_area($cakupan_area) {
       $query = $this->db->insert('cakupan_kantor_area', $cakupan_area);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

     function delete_cakupan_area($kode_area) {
        $query = $this->db->delete('cakupan_kantor_area', array('kode_area' => $kode_area));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

