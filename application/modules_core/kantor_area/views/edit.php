
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>

    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Setting</li>
							<li class="active">Edit Kantor Area</li>
						</ul>

					
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'kantor_area/proses_edit'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Edit Kantor Area</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Edit Kantor Area</legend>

											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label>Kode Area:</label>
														 <input type="text" class="form-control" name="kode_area" id="kode_area" placeholder="Kode Area" value="<?php echo $detail_area->kode_area; ?>" readonly required>
													</div>
												</div>

											
												<div class="col-md-3">
													<div class="form-group">
														<label>Kode DJK:</label>
														<input type="text" class="form-control" name="kode_djk" value="<?php echo $detail_area->kode_djk; ?>" required="">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Nama Area:</label>
														 <input type="text" class="form-control" name="nm_area" id="nm_area" placeholder="Nama Area" value="<?php echo $detail_area->nm_area; ?>" required>
													</div>
												</div>


												<div class="col-md-6">
													<div class="form-group">
														<label>Alamat:</label>
														 <input type="text" class="form-control" name="alamat" id="alamat" value="<?php echo $detail_area->alamat_area; ?>" placeholder="alamat" required>
													</div>
												</div>
												
												
												<div class="col-md-3">
													<div class="form-group">
														<label>Provinsi:</label>
														<select name="provinsi" id="provinsi" onChange="javascript:get_kota()" class="select" required="required">
														<option value=""> -- Pilih Provinsi -- </option>

														<?php foreach ($provinsi as $value) {?>
																<option <?php if($detail_area->id_provinsi == $value->id_provinsi) { echo "Selected";}?> value="<?php  echo $value->id_provinsi;?>"><?php  echo $value->nm_provinsi;?></option>
																
														<?php } ?>
															
														</select>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Kota:</label>
														<select class="select kota" name="kota" id="kota" required>
															
														<option value=""> Pilih Provinsi Terlebih Dahulu</option>
														<?php foreach ($kota as $value) {?>
																<option <?php if($detail_area->id_kota == $value->id_kota) { echo "Selected";}?> value="<?php  echo $value->id_kota;?>"><?php  echo $value->nm_kota;?></option>
																
														<?php } ?>

														</select>
													</div>
												</div>


												<div class="col-md-4">
													<div class="form-group">
														<label>Kantor Wilayah:</label>
														 <div class="multi-select-full">
															<select class="select" name="kanwil" id="kanwil" onChange="javascript:get_area_lit()">
																<?php foreach ($kanwil as $key => $value) {?>
																<option <?php if($detail_area->kode_kanwil == $value->kode_kanwil) { echo "Selected";}?>   value="<?php echo $value->kode_kanwil;?>"><?php  echo $value->nm_kanwil;?></option>
																<?php } ?>
													
															</select>
														</div>

													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label>Area Lit:</label>
														<select class="select area_lit" name="area_lit" id="area_lit" required>
															
														<option value=""> Pilih Kantor Wilayah Dahulu</option>
														<?php foreach ($lit_area_api['area'] as $value) {?>
																<option <?php if($detail_area->area_id == $value['area_id']) { echo "Selected";}?> value="<?php  echo $value['area_id'];?>"><?php  echo $value['nama'];?></option>
																
														<?php } ?>

														</select>
													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label>Telpon:</label>
														<input type="text" class="form-control" name="telpon" value="<?php echo $detail_area->telpon; ?>" required="">
													</div>
												</div>


												<div class="col-md-4">
													<div class="form-group">
														<label>Cakupan Kota:</label>
														 <div class="multi-select-full">
															<select class="multiselect-select-all-filtering" multiple="multiple" name="id_kota[]">
															<?php foreach ($kota as $value) {?>
																	<option <?php if(isset($cakupan_kantor_area[$value->id_kota]->id_kota) && $cakupan_kantor_area[$value->id_kota]->id_kota == $value->id_kota) { echo "selected"; } ?> value="<?php  echo $value->id_kota;?>"><?php  echo $value->nm_kota;?></option>
																	
															<?php } ?>
																
															</select>
														</div>

													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Manager:</label>
														 <input type="text" class="form-control" name="manager" id="manager" value="<?php echo $detail_area->manager; ?>" placeholder="Manager" required>
													</div>
												</div>
												
												
												<div class="col-md-6">
													<div class="form-group">
														<label>PJT:</label>
														<input type="text" class="form-control" name="pjt" value="<?php echo $detail_area->pjt; ?>" required="">
													</div>
												</div>


												
											</div>

										<label class="control-label col-lg-2">Penanda Tangan</label>
										<div class="col-lg-10">
											<div class="row">
												<div class="col-md-14">
													<div class="form-group has-feedback has-feedback-left">
														<input type="text" class="form-control input-lg" placeholder="Nama" name="nm_penanda_tangan" value="<?php echo $detail_area->signature_name; ?>">
														<div class="form-control-feedback">
															<i class="icon-make-group"></i>
														</div>
													</div>

													
													<div class="form-group has-feedback has-feedback-left">
														<input type="text" class="form-control input-sm" placeholder="Jabatan" name="jabatan_penanda_tangan" value="<?php echo $detail_area->signature_jabatan; ?>">
														<div class="form-control-feedback">
															<i class="icon-pin-alt"></i>
														</div>
													</div>

													<div class="form-group has-feedback has-feedback-left">
														<input type="text" class="form-control input-xs" placeholder="Kualifikasi" name="kualifikasi_penanda_tangan" value="<?php echo $detail_area->signature_kualifikasi; ?>">
														<div class="form-control-feedback">
															<i class="icon-help"></i>
														</div>
													</div>
												</div>

												
											</div>
										</div>
									
										<div class="col-md-6">
													<div class="form-group">
														<label>Status:</label>
														 <div class="multi-select-full">
															<select class="select" name="status" id="status">
																
																<option <?php if($detail_area->status == '1') { echo "Selected";}?> value="1"> Aktif</option>
														<option <?php if($detail_area->status == '2') { echo "Selected";}?> value="2"> Tidak Aktif</option>
													
															</select>
														</div>

													</div>
												</div>

										</fieldset>
									</div>
									<br>
									

								<div class="text-right">
									
									<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

  <script>
 		
 		function get_kota(){
           var provinsi = $('#provinsi :selected').val();
           var dark = $("select.kota").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "provinsi="+provinsi,
               url: '<?php echo base_url('kantor_area/get_kota/' )?>',
               success: function(result) {
                result;
                 
               
                $('#kota').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_area_lit(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area_lit").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_area/get_area_lit/' )?>',
               success: function(result) {
                result;
                 
               
                $('#area_lit').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }

  </script>
</body>
</html>
