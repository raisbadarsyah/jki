<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$this->load->view('index',$data);
	}

	function add()
	{
		$this->load->view('add');

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$nm_provinsi = $this->input->post('nm_provinsi');
			$kode_djk = $this->input->post('kode_djk');


			try {
				
				$data_provinsi = array(
					"nm_provinsi"=> $nm_provinsi,
					"kd_djk"=> $kode_djk
				);
				
				$insert = $this->Provinsi_Model->insert($data_provinsi);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('provinsi','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_provinsi = $this->uri->segment(3);

		$data['provinsi'] = $this->Provinsi_Model->get_by_id($id_provinsi);

		$this->load->view('edit',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_provinsi = $this->input->post('id_provinsi');
			$nm_provinsi = $this->input->post('nm_provinsi');
			$kode_djk = $this->input->post('kode_djk');


				try {
					
					$data_provinsi = array(
						"nm_provinsi"=> $nm_provinsi,
						"kd_djk"=> $kode_djk
					);
					
					$update = $this->Provinsi_Model->update($data_provinsi,$id_provinsi);


				} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal input provinsi');

				}


			if($update == 1) 
			{
							$this->session->set_flashdata('success', 'Sukses ubah data');
							redirect('provinsi','refresh');
			}


		}

	}



}
