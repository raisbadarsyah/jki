<?php

class Provinsi_model extends CI_Model {

   function get_all(){
		$this->db->select('*');
        $this->db->from('provinsi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

	function get_by_id($id_provinsi){
		$this->db->select('*');
        $this->db->from('provinsi');
        $this->db->where('id_provinsi', $id_provinsi);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_provinsi) {
       $query = $this->db->insert('provinsi', $data_provinsi);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $id_provinsi){
        $this->db->trans_begin();
		$this->db->where('id_provinsi', $id_provinsi);
		$this->db->update('provinsi', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

}

