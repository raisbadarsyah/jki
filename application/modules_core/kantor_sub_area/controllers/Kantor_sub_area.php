<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kantor_sub_area extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Staff/Staff_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$data['kantor_sub_area'] = $this->Kantor_Sub_Area_Model->get_all($params);
		$data['position_id'] = $position_id;

		$this->load->view('index',$data);
	}

	function add()
	{	
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_sub_area = $this->input->post('kode_sub_area');
			$nm_sub_area = $this->input->post('nm_sub_area');
			$alamat = $this->input->post('alamat');
			$area = $this->input->post('area');
			$manager = $this->input->post('manager');
			$pjt = $this->input->post('pjt');
			$status = $this->input->post('status');


			try {
				
				$data_kantor_sub_area = array(
					"kode_sub_area"=> $kode_sub_area,
					"kode_area"=> $area,
					"nm_sub_area"=> $nm_sub_area,
					"alamat_sub_area"=> $alamat,
					"nm_manager"=> $manager,
					"pjt"=> $pjt,
					"status"=> $status
				);
			
			
				$insert_kantor_sub_area = $this->Kantor_Sub_Area_Model->insert($data_kantor_sub_area);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input');

			}


				if($insert_kantor_sub_area == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kantor_sub_area','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_sub_area = $this->uri->segment(3);
		$data['kanwil'] = $this->Kanwil_Model->get_all();

		$data['detail_sub_area'] = $this->Kantor_Sub_Area_Model->detail_sub_area($kode_sub_area);
		$data['area'] = $this->Kantor_Area_Model->area_by_kanwil($data['detail_sub_area']->kode_kanwil);
		$this->load->view('edit',$data);

		
	}

	function proses_edit()
	{	
		
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_sub_area = $this->input->post('kode_sub_area');
			$nm_sub_area = $this->input->post('nm_sub_area');
			$alamat = $this->input->post('alamat');
			$area = $this->input->post('area');
			$manager = $this->input->post('manager');
			$pjt = $this->input->post('pjt');
			$status = $this->input->post('status');


			try {
				
				$data_kantor_sub_area = array(
					"kode_area"=> $area,
					"nm_sub_area"=> $nm_sub_area,
					"alamat_sub_area"=> $alamat,
					"nm_manager"=> $manager,
					"pjt"=> $pjt,
					"status"=> $status
				);
			
			
				$update_kantor_sub_area = $this->Kantor_Sub_Area_Model->update($data_kantor_sub_area,$kode_sub_area);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input');

			}


				if($update_kantor_sub_area == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kantor_sub_area','refresh');
				}


		}
	
	}

	function get_area()
	{
		$kanwil = $this->input->post('kanwil');
		$area = $this->Kantor_Sub_Area_Model->get_area($kanwil);

			echo"<option value=''>-- Pilih Area -- </option>";
		 foreach($area as $key=>$Area)
          {

           echo"<option value='".$Area->kode_area."'>".$Area->nm_area."</option>";
          }
         // echo "</select>";
          exit();

	}

	function get_sub_area()
	{
		$area = $this->input->post('area');
		$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($area);

			echo"<option value=''>-- Pilih Area -- </option>";
		 foreach($sub_area as $key=>$Sub_Area)
          {

           echo"<option value='".$Sub_Area->kode_sub_area."'>".$Sub_Area->nm_sub_area."</option>";
          }
         // echo "</select>";
          exit();

	}

	function delete($kode_sub_area){
		$action = $this->Kantor_Sub_Area_Model->delete($kode_sub_area);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'kantor_sub_area', 'refresh');
	}

}
