<?php

class Kantor_sub_area_model extends CI_Model {

   function get_all($params){
		$this->db->select('*');
        $this->db->from('kantor_sub_area');
        $this->db->join('kantor_area','kantor_area.kode_area=kantor_sub_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
  
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                
                 $this->db->where('kantor_sub_area.kode_area', $params['kode_area']);
            }
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_area($kode_kanwil) {
        $this->db->select('*');
        $this->db->from('kantor_area');
    
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
     
        return $query->result();
       // return $this->db->last_query();
    }

    function get_sub_area($kode_area) {
        $this->db->select('*');
        $this->db->from('kantor_sub_area');
    
        $this->db->where('kode_area',$kode_area);
        $query = $this->db->get();
     
        return $query->result();
       // return $this->db->last_query();
    }

    function detail_sub_area($kode_sub_area) {
        $this->db->select('*,kantor_sub_area.status as status_sub_area');
        $this->db->from('kantor_sub_area');
        $this->db->join('kantor_area','kantor_area.kode_area=kantor_sub_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
        $this->db->where('kode_sub_area',$kode_sub_area);
        $query = $this->db->get();
     
        return $query->row();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($kode_kanwil){
		$this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->where('kode_kanwil', $kode_kanwil);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_kantor_sub_area) {
       $query = $this->db->insert('kantor_sub_area', $data_kantor_sub_area);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $kode_sub_area){
        $this->db->trans_begin();
		$this->db->where('kode_sub_area', $kode_sub_area);
		$this->db->update('kantor_sub_area', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function delete($kode_sub_area) {
        $query = $this->db->delete('kantor_sub_area', array('kode_sub_area' => $kode_sub_area));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

