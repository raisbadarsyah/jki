<?php

class Pembayaran_model extends CI_Model {

    var $column_order_belum_bayar = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area'); //set column field database for datatable orderable
    var $column_search_belum_bayar = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area');  //set column field database for datatable 
    var $order_belum_bayar = array('pelanggan.no_pendaftaran' => 'desc'); // default order


    var $column_order_sudah_bayar = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area'); //set column field database for datatable orderable
    var $column_search_sudah_bayar = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area');  //set column field database for datatable 
    var $order_sudah_bayar = array('pelanggan.created_at' => 'desc'); // default order 

   function get_all(){
		$this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

     private function _get_datatables_belum_bayar_query($params)
     {
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_belum_bayar as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_belum_bayar) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_belum_bayar[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_belum_bayar))
        {
            $order = $this->order_belum_bayar;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->where('status_pembayaran =', 2);
        $this->db->group_by('pelanggan.no_pendaftaran');
    }

    function get_datatables_belum_bayar($params)
    {
        $this->_get_datatables_belum_bayar_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        //$this->db->limit('10', '1');
        $query = $this->db->get();
        return $query->result();
       // return $this->db->last_query();
    }
 
    function count_filtered_belum_bayar($params)
    {
        $this->_get_datatables_belum_bayar_query($params);
        return $this->db->count_all_results();
    }
 
    public function count_all_belum_bayar($params)
    {
        return $this->db->count_all("pembayaran");
    }


     private function _get_datatables_sudah_bayar_query($params)
     {

        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('core_user','pembayaran.diterima_oleh=core_user.coreUserId','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        

           if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->where('status_pembayaran =', 1);

        $i = 0;
     
        foreach ($this->column_search_sudah_bayar as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_sudah_bayar) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sudah_bayar[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_sudah_bayar))
        {
            $order = $this->order_sudah_bayar;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        
        
    }

    function get_datatables_sudah_bayar($params)
    {
        $this->_get_datatables_sudah_bayar_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        //$this->db->limit('10', '1');
        $query = $this->db->get();
        return $query->result();
        //return $this->db->last_query();
    }
 
    function count_filtered_sudah_bayar($params)
    {
        $this->_get_datatables_sudah_bayar_query($params);
        return $this->db->count_all_results();
    }
 
    public function count_all_sudah_bayar($params)
    {   
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        

           if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->where('status_pembayaran =', 1);

        return $this->db->count_all_results();
        //return $this->db->count_all("pembayaran");
        //$this->db->limit('10', '1');
        //return $this->db->last_query();
    }

     function get_detail_pelanggan($no_pendaftaran){
       $this->db->select('*,pelanggan.kd_area as kode_area,kantor_wilayah.kode_kanwil as kode_kanwil,pelanggan.telp as telp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('core_user','pelanggan.created_by=core_user.coreUserId','left');
        $this->db->where('no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_no_spk($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('no_spk');
        $this->db->where('no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_daya_lama($id_daya){
        $this->db->select('*');
        $this->db->from('daya');
        $this->db->where('id_daya',$id_daya);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function get_tarif_lama($id_tarif){
        $this->db->select('*');
        $this->db->from('tarif');
        $this->db->where('id_tarif',$id_tarif);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function get_cetak_pelanggan($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('provinsi','provinsi.id_provinsi=kota.id_provinsi');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pelanggan.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_pembayaran($no_pendaftaran){
       $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('core_user','pembayaran.diterima_oleh=core_user.coreUserId','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('provinsi','provinsi.id_provinsi=kota.id_provinsi');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pembayaran.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota(){
        $this->db->select('*');
        $this->db->from('kota');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl(){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl(){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($pembayaran) {
       $query = $this->db->insert('pembayaran', $pembayaran);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

  
    function update($data_pembayaran, $no_kwitansi){
        $this->db->trans_begin();
		$this->db->where('no_kwitansi', $no_kwitansi);
		$this->db->update('pembayaran', $data_pembayaran);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

      function get_max_no_kwitansi($params){
       // $this->db->select('MAX(SUBSTRING(no_kwitansi, "1","5")) AS ExtractString');
        $this->db->select('MAX(no_urut) AS ExtractString');
       // $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        
        /*
        if(isset($params['kode_area']) && $params['kode_area'] != "")
        {      
            //$this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area');
            //$this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area');
            $this->db->where('pelanggan.kd_area', $params['kode_area']);
        }
        */
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

}

