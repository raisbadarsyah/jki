<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	

	function __construct()
	{
		ini_set('memory_limit', '-1');
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan/Pelanggan_Model','Staff/Staff_model','Pembayaran_Model'));
		$this->load->helper(array('check_auth_menu','tgl_indonesia'));
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_all();


		$this->load->view('index',$data);
	}


	function detail_pelanggan()
	{
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');
		$no_pendaftaran = $this->input->post('no_pendaftaran');
		
		$config['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		if(!isset($config['detail_pelanggan']))
		{
			echo "Data tidak lengkap harap dilengkapi";
			exit();
		}
		$this->load->view('v_detail_pelanggan', $config);
	}

	function cetak()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pembayaran($no_pendaftaran);
		if($data['detail_pelanggan']->tipe_pelanggan == 2)
		{
			$tarif_lama = $this->Pembayaran_Model->get_tarif_lama($data['detail_pelanggan']->tarif_lama);
			$daya_lama = $this->Pembayaran_Model->get_daya_lama($data['detail_pelanggan']->daya_lama);

			$data['text'] = "Pindah daya dari ".$tarif_lama->nm_tarif."/".$daya_lama->daya." ke ";
		}
		else
		{
			$tarif_lama = "";
			$daya_lama = "";

			$data['text'] = "";
		}
	
		$this->load->view('cetak', $data);
	}

	function proses_bayar()
	{
		$user_id = $this->session->userdata('user_id');
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_kwitansi = date('YmdHis');
			$tgl_bayar = date('Y-m-d H:i:s');
			$diterima_oleh = $user_id;
			$harga = $this->input->post('harga');

			$detail_pelanggan = $this->Pelanggan_Model->get_detail_pelanggan_pembayaran($no_pendaftaran);

			if(!isset($detail_pelanggan)){
				$this->session->set_flashdata('error', 'Data Pelanggan Tidak Lengkap.. silahkan lengkapi data pelanggan');
				redirect('pembayaran/belum_bayar','refresh');
				exit();
			}
			

			$params_input = array_filter(array(
	        	'kode_area' => $detail_pelanggan->kode_area));
			

			$get_max_no_kwitansi = $this->Pembayaran_Model->get_max_no_kwitansi($params_input);
			
			$good_total = $get_max_no_kwitansi->ExtractString;
			
			if($good_total=='') { // bila data kosong
                $good_total ="0001";
            }else {
                $MaksID = $good_total;
                $MaksID++;
              
                if($MaksID < 1) $good_total = "0000".$MaksID; // nilai kurang dari 1000
                else if($MaksID < 10) $good_total = "000".$MaksID; // nilai kurang dari 10000
                else if($MaksID < 100) $good_total = "00".$MaksID; // nilai kurang dari 100000
                else if($MaksID < 1000) $good_total = "0".$MaksID; // nilai kurang dari 10000
          		else $good_total = $MaksID; // lebih dari 10000
                
            }


			$current_month = date("m");
			$current_year = date("Y");
			
			$no_kwitansi = "".$good_total."/BP-JKI/".$detail_pelanggan->kode_area.$current_month."/".$current_year."";


			try {
				
				$data_pembayaran = array(
					"no_kwitansi"=> $no_kwitansi,
					"no_pendaftaran"=> $no_pendaftaran,
					"tgl_bayar"=> $tgl_bayar,
					"diterima_oleh"=> $diterima_oleh,
					"harga"=> $harga,
					"no_urut"=> $good_total,
				);

				$data_pelanggan = array(
					"status_pembayaran"=> 1,
				);
				
			
				$insert_pembayaran = $this->Pembayaran_Model->insert($data_pembayaran);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pembayaran == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pembayaran/belum_bayar','refresh');
				}


		}
	}

	function querypagingbelumbayar()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Pembayaran_Model->get_datatables_belum_bayar($params);
	 	
	 	//var_dump($list);
	 	//exit();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pembayaran) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			if($id_departemen == 7)
			{
				$button = "";

			}
			elseif($id_departemen == 6)
			{
				$button = "";

			}
			else{


				 $button = "<a href='#popUp' class='btn btn-default btn-small' id='no_pendaftaran' data-toggle='modal' data-id='".$data_pembayaran->no_pendaftaran."'>Bayar</a>";
			}

			if($data_pembayaran->no_agenda == 1)
			{
				$no_agenda = $data_pembayaran->no_agenda;
			}
			else
			{
				$no_agenda = "";
			}


            $row[] = $data_pembayaran->no_pendaftaran;
            $row[] = $no_agenda;
            $row[] = $data_pembayaran->Nama;
            $row[] = $data_pembayaran->alamat;
            $row[] = $data_pembayaran->nm_kota;
            $row[] = $data_pembayaran->nm_kanwil;
            $row[] = $data_pembayaran->nm_area;
            $row[] = $data_pembayaran->nm_sub_area;
            $row[] = $data_pembayaran->nm_btl;
            $row[] = $data_pembayaran->nm_bangunan;
            $row[] = "".$data_pembayaran->nm_tarif."/".$data_pembayaran->daya."";
            $row[] = $data_pembayaran->no_gambar;
            $row[] = $data_pembayaran->nm_asosiasi;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        //"recordsTotal" => $this->Pembayaran_Model->count_all_belum_bayar($params),
                        //"recordsFiltered" => count($list),
                        "recordsTotal" => 2000000,
                        "recordsFiltered" => 2000000,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function belum_bayar()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));


		//$data['pelanggan'] = $this->Pelanggan_Model->get_belum_bayar($params);

		$this->load->view('belum_bayar');
	}

	function querypagingsudahbayar()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Pembayaran_Model->get_datatables_sudah_bayar($params);
	 	//$count_all = $this->Pembayaran_Model->count_all_sudah_bayar($params);
	 	//var_dump($list);
	 	//exit();
	 	
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pembayaran) {
            $no++;
            $row = array();
            //$row[] = $no;
          

			if($id_departemen == 7)
			{
				$button = "";

			}
			elseif($id_departemen == 6)
			{
				$button = "";

			}
			else{


				 $button = "<a href='".base_url()."pembayaran/cetak/".$data_pembayaran->no_pendaftaran."' class='btn btn-primary'>Cetak Kwitansi</a>";
			}

				 
			
			if($data_pembayaran->status_cetak == 1)
			{
				$status_cetak = "Sudah dicetak ".$data_pembayaran->total_cetak."";
			}
			else
			{
				$status_cetak = "Belum Dicetak";
			}

			if(isset($dicetak_oleh[$data_pembayaran->no_pendaftaran]->Name)) {

				$pencetak = $dicetak_oleh[$data_pembayaran->no_pendaftaran]->Name;
			}
			else
			{
				$pencetak = "";
			}

			$row[] = "<input type='checkbox' name='no_pendaftaran[]' value='".$data_pembayaran->no_pendaftaran."''>";
            $row[] = $data_pembayaran->no_kwitansi;
            $row[] = $data_pembayaran->no_pendaftaran;
            $row[] = $data_pembayaran->Nama;
            $row[] = $data_pembayaran->nm_kota;
            $row[] = $data_pembayaran->nm_kanwil;
            $row[] = $data_pembayaran->nm_area;
            $row[] = $data_pembayaran->nm_sub_area;
            $row[] = $data_pembayaran->tgl_bayar;
            $row[] = $data_pembayaran->Name;
            $row[] = $status_cetak;
            $row[] = $data_pembayaran->Name;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 		
        $output = array(
                        "draw" => $_POST['draw'],
                        //"recordsTotal" => $this->Pembayaran_Model->count_all_sudah_bayar($params),
                        //"recordsFiltered" => count($data),
                        "recordsTotal" => 2000000,
                        "recordsFiltered" => 2000000,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function sudah_bayar()
	{
		
		$this->load->view('sudah_bayar');
	}

	function add()
	{	
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$data['kota'] = $this->Pelanggan_Model->get_kota();
		$data['tarif'] = $this->Pelanggan_Model->get_tarif();
		$data['daya'] = $this->Pelanggan_Model->get_daya();
		$data['btl'] = $this->Pelanggan_Model->get_btl();
		$data['bangunan'] = $this->Pelanggan_Model->get_bangunan();
		$data['ptl'] = $this->Pelanggan_Model->get_ptl();
		$data['asosiasi'] = $this->Pelanggan_Model->get_asosiasi();

		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$no_ktp = $this->input->post('no_ktp');
			$telp = $this->input->post('telp');
			$tarif = $this->input->post('tarif');
			$daya = $this->input->post('daya');
			$btl = $this->input->post('btl');
			$bangunan = $this->input->post('bangunan');
			$ptl = $this->input->post('ptl');
			$sub_area = $this->input->post('sub_area');
			$no_gambar = $this->input->post('no_gambar');
			$asosiasi = $this->input->post('asosiasi');
			$no_reg = $this->input->post('no_reg');
			$tgl_rek = $this->input->post('tgl_rek');
			$tgl_gambar = $this->input->post('tgl_gambar');
			$nm_instalir = $this->input->post('nm_instalir');
			$telp_instalir = $this->input->post('telp_instalir');
			$lattitude = $this->input->post('lattitude');
			$longitude = $this->input->post('longitude');
			$kode_pelanggan = date('YmdHis');
			$created_at = date('Y:m:d H:i:s');

			try {
				
				$data_pelanggan = array(
					"kode_pelanggan"=> $kode_pelanggan,
					"Nama"=> $nama,
					"alamat"=> $alamat,
					"kd_kota"=> $kota,
					"no_ktp"=> $no_ktp,
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
					"telp"=> $telp,
					"id_tarif"=> $tarif,
					"id_daya"=> $daya,
					"id_btl"=> $btl,
					"id_bangunan"=> $bangunan,
					"id_ptl"=> $ptl,
					"kode_sub_area"=> $sub_area,
					"no_gambar"=> $no_gambar,
					"tgl_gambar"=> $tgl_gambar,
					"id_asosiasi"=> $asosiasi,
					"noreg_pln"=> $no_reg,
					"tgl_reg_pln"=> $tgl_rek,
					"nama_instalir"=> $nm_instalir,
					"telp_instalir"=> $telp_instalir
				);
				
			
				$insert_pelanggan = $this->Pelanggan_Model->insert($data_pelanggan);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pelanggan','refresh');
				}


		}
	
	}


	function cetak_kwitansi()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$date = date('Y-m-d'); 
			$tgl_cetak = tgl_indo($date);
		
			$user_id = $this->session->userdata('user_id');
			$position_id = $this->session->userdata('position_id');
			$id_departemen = $this->session->userdata('id_departemen');
			
			$params_staff = array_filter(array(
	            'position_id' => $position_id,
	            'id_departemen' => $id_departemen,
	      	));

			$detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);
			
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$detail_pelanggan = $this->Pembayaran_Model->get_detail_pembayaran($no_pendaftaran);

			$data_pembayaran = array('dicetak_oleh' => $user_id,
									 'total_cetak' =>$detail_pelanggan->total_cetak + 1,
									 'status_cetak' => 1 );

			if($detail_pelanggan->tipe_pelanggan == 2)
			{
				$tarif_lama = $this->Pembayaran_Model->get_tarif_lama($detail_pelanggan->tarif_lama);
				$daya_lama = $this->Pembayaran_Model->get_daya_lama($detail_pelanggan->daya_lama);

				$text = "Rubah daya dari ".$tarif_lama->nm_tarif."/".$daya_lama->daya." ke ";
			}
			else
			{
				$tarif_lama = "";
				$daya_lama = "";

				$text = "";
			}

			$update_pembayaran= $this->Pembayaran_Model->update($data_pembayaran,$detail_pelanggan->no_kwitansi);

			if(isset($detail_staff->nm_area)) 
				{	
					 $nm_area =  $detail_staff->nm_area;
				}
				else if($detail_staff->nip == '111')
				{
					$nm_area = 'Jakarta';
				}
				else if($detail_staff->nip == '222')
				{
					$nm_area = 'Jakarta';
				}
				else
				{
					$nm_area = "";
				}

			try {
				
			

			$html = "
<div style='width: 900px; height: 580px; padding: 15px; border: 1px solid #000;'>
<table border='0' width='100%' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='center' width='103'><img src='".base_url()."template/assets/images/jki_logo_small.png'/></td>
<td align='center'>
<p class='p0 ft0' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
<p class='p1 ft1' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Komplek Golden Plaza Fatmawati, Blok G11, Lantai 2</p>
<p class='p1 ft1' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Jl. R.S Fatmawati Nomor 15, Jakarta 12420</p>
<p class='p2 ft2' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Telp/Fax: 021 27650120 Email: jkijakarta1@gmail.com</p>
</td>
<td width='90'>&nbsp;</td>
</tr>
<tr>
<td colspan='3'><hr style='border-top: 1px solid #000;' /></td>
</tr>
<tr>
<td colspan='3' align='center'><span style='text-decoration: underline;'><strong>BUKTI PENERIMAAN</strong></span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan='3' align='center'>No. ".$detail_pelanggan->no_kwitansi."</td>
</tr>
<tr>
<td colspan='3'>&nbsp;</td>
</tr>
<tr>
<td colspan='3'>
<table border='0' width='100%' cellspacing='0' cellpadding='4'>
<tbody>
<tr>
<td colspan='3'>Telah terima pembayaran permintaan pemeriksaan instalasi dari : </td>
</tr>
<tr>
<td width='300'>No. Pendaftaran</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->no_pendaftaran."</td>
</tr>
<tr>
<td>Atas Nama Pemilik / Konsumen</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->Nama."</td>
</tr>
<tr>
<td>Nama BTL</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->nm_btl."</td>
</tr>
<tr>
<td>Penyedia Listrik</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->nm_ptl."</td>
</tr>
<tr>
<td>Alamat</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->alamat."</td>
</tr>
<tr>
<td>&nbsp;</td>
<td width='10'></td>
<td>".$detail_pelanggan->nm_alias.", ".$detail_pelanggan->nm_provinsi."</td>
</tr>
<tr>
<td>Tarif / Daya</td>
<td width='10'>:</td>
<td>".$text."".$detail_pelanggan->nm_tarif."/".$detail_pelanggan->daya."</td>
</tr>
<tr>
<td>Biaya Pemeriksaan</td>
<td width='10'>:</td>
<td>Rp. ".$detail_pelanggan->harga."</td>
</tr>
<tr>
<td>Terbilang</td>
<td width='10'>:</td>
<td>".$detail_pelanggan->terbilang."</td>
</tr>

</tbody>
</table>
</td>
</tr>
<tr>
<td style='padding: 0;' colspan='2'>&nbsp;</td>
</tr>
</tbody>
</table>
<br/><br/>

<table border='0' width='250' cellspacing='0' cellpadding='3' align='right'>
<tbody>
<tr>
<td align='center'>".$nm_area.",".tgl_indo(date('Y-m-d'))."</td>
</tr>
<tr>
<td align='center'>Admin</td>
</tr>
<tr>
<td align='center' height='10'>&nbsp;</td>
</tr>
<tr>
<td align='center'>(".$detail_staff->Name.")</td>
</tr>
</tbody>
</table>

</div>";
		
		//echo $html;
		
		header('Content-Type: application/pdf','charset=utf-8');

		$mpdf = new \Mpdf\Mpdf();

		$mpdf->SetDisplayMode('fullpage');

       // Write some HTML code:

       $mpdf->WriteHTML($html);

       // Output a PDF file directly to the browser
       $mpdf->Output();

		

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input verifikasi');

			}



		}
	}

	

}
