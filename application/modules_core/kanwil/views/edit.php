
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT. JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>

    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Setting</li>
							<li>Kantor Wilayah</li>
							<li class="active">Edit Kantor Wilayah</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'kanwil/edit'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Edit Kantor Wilayah</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>EDIT</legend>

											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>Kode:</label>
														 <input type="text" class="form-control" name="kode_kanwil" id="kode_kanwil" placeholder="Kode Kantor Wilayah" value="<?php echo $kanwil->kode_kanwil ?>" required readonly>
													</div>
												</div>
												
												
												<div class="col-md-6">
													<div class="form-group">
														<label>Nama Wilayah:</label>
														<input type="text" class="form-control" name="nm_wilayah" value="<?php echo $kanwil->nm_kanwil ?>" required="">
													</div>
												</div>


												<div class="col-md-6">
													<div class="form-group">
														<label>Cakupan Provinsi:</label>
														 <div class="multi-select-full">
															<select class="multiselect-select-all-filtering" multiple="multiple" name="id_provinsi[]">
																
															<?php foreach ($provinsi as $value) {?>
																	<option 
																	<?php if(isset($cakupan_provinsi[$value->id_provinsi]->id_provinsi) && $$cakupan_provinsi[$value->id_provinsi]->id_provinsi == $value->id_provinsi) { ?> 
																	value="<?php echo $value->id_provinsi;?>" selected 
																	<?php } else { ?> 
																	value="<?php  echo $value->id_provinsi;?>" 
																	<?php } ?>

																	>
																	<?php  echo $value->nm_provinsi;?></option>
																	
															<?php } ?>
																
															</select>
														</div>

													</div>
												</div>
												
												<div class="col-md-4">
													<div class="form-group">
														<label>Wilayah Lit:</label>
														 <div class="multi-select-full">
															<select class="select" name="wilayah_lit">
															<?php foreach ($lit_wilayah['wilayah'] as $value) {?>
																	<option <?php if(isset($kanwil->wilayah_id) && $kanwil->wilayah_id==$value['wilayah_id']) { echo "selected"; } ?> value="<?php  echo $value['wilayah_id'];?>"><?php  echo $value['nama'];?></option>
																	
															<?php } ?>
																
															</select>
														</div>

													</div>
												</div> 
												<div class="col-md-6">
													<div class="form-group">
														<label>General Manager:</label>
														<input type="text" class="form-control" name="general_manager" value="<?php echo $kanwil->general_manager ?>" required="">
													</div>
												</div>

												
											</div>

											


										</fieldset>
									</div>
									<br>
									

								<div class="text-right">
									<button type="submit" class="btn btn-primary">EDIT <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

  <script>
            var i = 1;
            var nilai = 0;
            function additem() {
//                menentukan target append
                var itemlist = document.getElementById('itemlist');
                
//                membuat element
                var row = document.createElement('tr');
                var item_td = document.createElement('td');
                var item_code_td = document.createElement('td');
                var aksi = document.createElement('td');
 
//                meng append element
                itemlist.appendChild(row);
                row.appendChild(item_td);
                row.appendChild(item_code_td);
                row.appendChild(aksi);
 				

 				var principal = document.createElement("div");
              	var position = document.createElement("div");
     
                var hapus = document.createElement('span');
 				var nilai = i-1;
//                meng append element input
                item_td.appendChild(principal);
                item_code_td.appendChild(position);
                aksi.appendChild(hapus);
 				

                principal.innerHTML = '<select class="select" name="principal[' + i + ']"><?php foreach ($principal_list as $value) {?><option value="<?php  echo $value->suplierId;?>"><?php  echo $value->supplierPrincipal;?></option><?php } ?></select>';

                position.innerHTML = '<select class="select" name="position[' + i + ']"><?php foreach ($position_list as $value) {?><option value="<?php  echo $value->corePositionId;?>"><?php  echo $value->corePositionName;?></option><?php } ?></select>';

                hapus.innerHTML = '<button class="btn btn-small btn-default"><i class="icon-trash"></i></button>';
//                membuat aksi delete element
                hapus.onclick = function () {
                    row.parentNode.removeChild(row);
                };
 				

                i++;
                nilai++;
            }
        </script>
</body>
</html>
