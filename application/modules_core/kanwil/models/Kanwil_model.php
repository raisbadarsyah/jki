<?php

class Kanwil_model extends CI_Model {

   function get_all(){
		$this->db->select('*');
        $this->db->from('kantor_wilayah');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_kanwil($kode_kanwil){
        $this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->where('kode_kanwil', $kode_kanwil);
        
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kanwil_by_user($params){
        $this->db->select('*');
        $this->db->from('kantor_wilayah');

         if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0 )
        {
             $this->db->where('kode_kanwil', $params['kode_kanwil']);
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

   
    function get_by_user_id($user_id,$params){
        $this->db->select('*');
        $this->db->from('core_user');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
            $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 9)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
             $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
            $this->db->join('kantor_sub_area','core_user.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        
        }
        


        $this->db->where('coreUserId',$user_id);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->join('kantor_wilayah','cakupan_kanwil.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->where('cakupan_kanwil.kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($kode_kanwil){
		$this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->where('kode_kanwil', $kode_kanwil);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_kanwil) {
       $query = $this->db->insert('kantor_wilayah', $data_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $kode_kanwil){
        $this->db->trans_begin();
		$this->db->where('kode_kanwil', $kode_kanwil);
		$this->db->update('kantor_wilayah', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function delete($kode_kanwil) {
        $query = $this->db->delete('kantor_wilayah', array('kode_kanwil' => $kode_kanwil));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

