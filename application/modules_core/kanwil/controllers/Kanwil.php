<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kanwil extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Kanwil_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}

	function get_wilayah_lit()
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/lit/wilayah'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/lit/wilayah'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		//curl_setopt ($ch, CURLOPT_POSTFIELDS, "$POSTFIELDS");
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		$data_wilayah = $tmp;

		return $data_wilayah;
	}

	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$cakupan_provinsi = array();
		foreach ($data['kanwil'] as $key => $value) {
			
			$cakupan_provinsi[$value->kode_kanwil] = $this->Kanwil_Model->cakupan_provinsi($value->kode_kanwil);
		}
		
		$data['cakupan_provinsi'] = $cakupan_provinsi;
		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$data_lit_wilayah = json_decode($this->get_wilayah_lit(), TRUE);
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['lit_wilayah'] = $data_lit_wilayah;
		
		/*
		echo "<pre>";
		print_r($data_lit_wilayah['wilayah']);
		echo "</pre>";
		*/
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_kanwil = $this->input->post('kode_kanwil');
			$wilayah_lit = $this->input->post('wilayah_lit');
			$nm_wilayah = $this->input->post('nm_wilayah');
			$id_provinsi = $this->input->post('id_provinsi');
			$general_manager = $this->input->post('general_manager');


			try {
				
				$data_kanwil = array(
					"kode_kanwil"=> $kode_kanwil,
					"wilayah_id"=> $wilayah_lit,
					"nm_kanwil"=> $nm_wilayah,
					"general_manager"=> $general_manager,
				);
				
				$insert_kanwil = $this->Kanwil_Model->insert($data_kanwil);

				foreach($id_provinsi as $provinsi){

					$cakupan_kanwil = array(
						"id_provinsi" 		=> $provinsi,
						"kode_kanwil" 		=> $kode_kanwil
					);

					$cakupan_kanwil = $this->Kanwil_Model->insert_cakupan_kanwil($cakupan_kanwil);
				   
				}

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_kanwil == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kanwil','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_kanwil = $this->uri->segment(3);
		$data_lit_wilayah = json_decode($this->get_wilayah_lit(), TRUE);
		$data['lit_wilayah'] = $data_lit_wilayah;
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_by_id($kode_kanwil);

		foreach ($data['provinsi'] as $key => $value) {
			$cakupan_provinsi[$value->id_provinsi][$kode_kanwil] = $this->Kanwil_Model->detail_cakupan_provinsi($kode_kanwil,$value->id_provinsi);

		}

		$data['cakupan_provinsi'] = $cakupan_provinsi;


		$this->load->view('edit',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_kanwil = $this->input->post('kode_kanwil');
			$wilayah_lit = $this->input->post('wilayah_lit');
			$nm_wilayah = $this->input->post('nm_wilayah');
			$id_provinsi = $this->input->post('id_provinsi');
			$general_manager = $this->input->post('general_manager');


				try {
					
					$data_kanwil = array(
					"wilayah_id"=> $wilayah_lit,
					"nm_kanwil"=> $nm_wilayah,
					"general_manager"=> $general_manager,
				);
				
				$update_kanwil = $this->Kanwil_Model->update($data_kanwil,$kode_kanwil);


				} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal update');

				}


			if($update_kanwil == 1) 
			{
							$this->session->set_flashdata('success', 'Sukses ubah data');
							redirect('kanwil','refresh');
			}


		}

	}


	function delete($kode_kanwil){
		$action = $this->Kanwil_Model->delete($kode_kanwil);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'kanwil', 'refresh');
	}

}
