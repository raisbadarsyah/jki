<?php

class Management_group_model extends CI_Model {

    function get_all() {
        $this->db->select('*');
        $this->db->from('core_department');
		$this->db->order_by('coreDepName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_menu() {
        $this->db->select('*');
        $this->db->from('core_menu');
		$this->db->order_by('coreMenuSort','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_action() {
        $this->db->select('*');
        $this->db->from($this->action);
		$this->db->order_by('actionId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert_group($data){
		$this->db->trans_begin();
        $this->db->insert('core_department', $data);
		$id = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return $id;
		}
    }
	
	function get_akses_group($id){
		$this->db->select('*');
        $this->db->from('core_role');
		$this->db->where('core_role.coreRoleGroup', $id);
        $query = $this->db->get();
        return $query->result();
	}
	
	function cek_akses_group($id){
		$this->db->select('*');
        $this->db->from('core_role');
		$this->db->where('core_role.coreRoleGroup', $id);
		$query = $this->db->get();
        return $query->num_rows();
	}
	
	function insert_roles($roles){
		$this->db->trans_begin();
        $this->db->insert('core_role', $roles);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update_roles($removed_roles,$id_group,$id_menu){
        $this->db->trans_begin();
		$this->db->where('coreRoleGroup', $id_group);
		$this->db->where('coreRoleMenu', $id_menu);
		$this->db->update('core_role', $removed_roles);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
        
    function check_roles_by_group($id_grup,$id_menu){
        $this->db->select('*');
        $this->db->from('core_role');
        $this->db->where('coreRoleGroup',$id_grup);
        $this->db->where('coreRoleMenu',$id_menu);
        return $this->db->get();
    }
	
	function delete_group($id) {
        $query = $this->db->delete('core_department', array('coreDepId' => $id));

		 if ($query) { 
			echo 1; 
	    } 
	    else { 
	    	echo 0; 
	    } 
	}

	function check_groupname($groupname){
		$this->db->select('coreDepName');
        $this->db->from('core_department');
		$this->db->where('coreDepName', $groupname);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            echo 1;
        }
        else {
            echo 0;
        }	
	}

	function get_group_by_id($id){
		$query = $this->db->get_where('core_department',array('coreDepId'=>$id));
		return $query->result();
	}

	function update_group($data, $groupId){
        $this->db->trans_begin();
		$this->db->where('coreDepId', $groupId);
		$this->db->update('core_department', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

	function get_parent_menu($parentId){
		$this->db->select('core_menu.*, Deriv1.Count');
        $this->db->from('core_menu');
        $this->db->join('(SELECT coreParentId, COUNT(*) AS COUNT FROM `core_menu` GROUP BY coreParentId) as Deriv1', 'core_menu.coreMenuId = Deriv1.coreParentId','LEFT');
		$this->db->where('core_menu.coreParentId', $parentId);
		$this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        
        return $query->result();
	}
}

