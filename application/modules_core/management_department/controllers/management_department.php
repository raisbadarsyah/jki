<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ALL);

class Management_department extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_group_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}

	function index()
	{
		$config['title'] 	 = 'Koperasi Syariah Duasatudua | Manage Department';
		$config['page_title'] = 'Management Department';
		$config['page_subtitle'] = 'Department List';
	    $config['group_list'] = $this->management_group_model->get_all();
       
		$this->load->view('v_list', $config);
	}
	
	function insert_group(){
			$group  = $this->input->post('groupname');
			$desc  = $this->input->post('desc');
			$data = array(
				"coreDepName" => $group,
				"coreDepDescription" => $desc
			);
			$last_id = $this->management_group_model->insert_group($data);

			// $insert_id = $this->db->insert_id($data);
			$menu = '1';
			$act = '1';

			$role = array(
				"coreRoleGroup" => $last_id,
				"coreRoleMenu" => $menu,
				"coreRoleActive" => $act
			);

			
			$default = $this->management_group_model->insert_roles($role); 	
			// $action = $this->management_group_model->insert_group($data);
			// echo $this->db->last_query();die();
			if($last_id != 0) {
				$this->session->set_flashdata('message', 'Department has been added successfully');
			}
			else {
				$this->session->set_flashdata('error', 'Failed to add Department');
			}	
			redirect(base_url() . 'management_department', 'refresh');	
	}

	function edit_group($id){
			$group = $this->management_group_model->get_group_by_id($id);
			echo json_encode($group);
	}

	function update_group(){	
		$coreGroupId    = $this->input->post('groupId');
		$coreGroupName  = $this->input->post('groupname');
		$desc  = $this->input->post('desc');
		
		$data = array(
			"coreDepName" 		=> $coreGroupName,
			"coreDepDescription" 		=> $desc
		);

		$action = $this->management_group_model->update_group($data, $coreGroupId);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Department has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update Department');
		}	
		redirect(base_url() . 'management_department', 'refresh');
	}
	
	function management_group_role($id){
		$config['title'] 	 = 'HMVCFW | Manage Department';	
		$config['page_title'] = 'Management Department';
		$config['page_subtitle'] = 'Manage Role';
        $config['groupId']   = $id;
		$cek_akses_group     = $this->management_group_model->cek_akses_group($config['groupId']);
		
		if($cek_akses_group > 0){
			$config['roles'] = $this->management_group_model->get_akses_group($config['groupId']);
			$config['status']='edit';
			$config['link'] ='management_department/update_roles/';
		}else{
			$config['status']='new';
			$config['link'] ='management_department/insert_roles/';
		}
		
		$this->load->view('v_manage', $config);
	}

	
	function insert_roles(){
		$id_menu       = $this->input->post('id_menu');
		$id_grup       = $this->input->post('groupId');
		$jumlah_menu   = count($id_menu);
        
		if($jumlah_menu>0){   
			for($i=0;$i<$jumlah_menu;$i++){
				$menu_id = $id_menu[$i];
			    $active = $this->input->post('active_'.$menu_id);
				
				if($active !== '1'){
					 $active = '0';
				}
				
				$roles = array(
					"coreRoleGroup" => $id_grup,
					"coreRoleMenu" => $menu_id,
					"coreRoleActive" => $active
				);
				
				$action = $this->management_group_model->insert_roles($roles);
			}  
		}
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Group roles have been updated');
		}
		else {
			$this->session->set_flashdata('error', 'Group roles have been updated');
		}	
		
        redirect(base_url() . 'management_department', 'refresh');	
	}
	
	function update_roles(){
		 $id_menu    = $this->input->post('id_menu');
		 $id_group    = $this->input->post('groupId');
		 $jumlah     = count($id_menu);
		 $id_menu_removed = $this->input->post('remove');
		 $jumlah_hilang = count($id_menu_removed);
		 
		if($jumlah_hilang>0)  {			
			for($i=0;$i<$jumlah_hilang;$i++){         
				$id_removed = $id_menu_removed[$i];
				$removed_roles = array(
					"coreRoleActive" => '0'
				);
				
				$action = $this->management_group_model->update_roles($removed_roles,$id_group,$id_removed);    
			}  
		}
		  
		if($jumlah>0){   
			for($i=0;$i<$jumlah;$i++){         
				$menu_id = $id_menu[$i];
				$active = $this->input->post('active_'.$menu_id);
				 
				if($active !== '1'){
					$active = '0';
				}
				 
				$menu = $this->management_group_model->check_roles_by_group($id_group,$menu_id);
				if($menu->num_rows()>0){
					$roles = array(
						"coreRoleActive" => $active
					 );
					 
					$action = $this->management_group_model->update_roles($roles,$id_group,$menu_id); 
				}
				else{
					
					$roles = array(
						"coreRoleGroup" => $id_group,
						"coreRoleMenu" => $menu_id,
						"coreRoleActive" => $active
					);
						
					$action = $this->management_group_model->insert_roles($roles);
				}     
			}  
		}
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Group roles have been updated');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update group roles');
		}
		
		redirect(base_url() . 'management_department', 'refresh');	
	}
	
	function delete($id){
			$this->management_group_model->delete_group($id);
	}

	function checkGroupName($gname){
		$this->management_group_model->check_groupname($gname);
	}
}