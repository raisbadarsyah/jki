<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><?php echo $page_title;?></div>
					<?php if($this->session->flashdata('message') != ""){ ?>  
					  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
				    <?php } ?>
				</div>
				<div class="panel-body">	
                <br />
  				<!--<a href="<?php //echo base_url(); ?>management_user/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>Add</button></a><hr />	-->			
				<table  class="table datatable-tools-basic">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Department Name</th>
                    <th>Department Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no=0;foreach($group_list as $row){?>
                  <tr class="rowdata_<?php echo $row->coreDepId; ?>">
                    <td><?php echo ++$no;?></td>
                    <td><?php echo $row->coreDepName;?></td>
                    <td><?php echo $row->coreDepDescription;?></td>
                    <td>
                      <a href="<?php echo base_url().'management_department/management_group_role/'.$row->coreDepId;?>" title="Manage" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                      <!--<a href="javascript:;" onclick="editUser('<?php //echo $row->coreDepId; ?>')" title="Edit" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i></a>
                      <a href="javascript:;" onclick="deletedata('<?php //echo $row->coreDepId; ?>')" title="Delete" class="btn btn-primary"><i class="glyphicon glyphicon-remove"></i></a>-->
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>