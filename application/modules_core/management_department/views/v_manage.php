<?php
$this->load->view('template/header');

?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><?php echo $page_title;?></div>
					<?php if($this->session->flashdata('message') != ""){ ?>  
					  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
				    <?php } ?>
				</div>
				<div class="panel-body">	
                <br />
					<form id="fm_hak_akses" action="<?php echo base_url().$link;?>" method="POST">
					<input type="hidden" name="groupId" value="<?php echo $groupId; ?>" />
					<table class="table table-bordered table-hover">
					  <!-- <tr>
						<th style="width: 10px">#</th>
						<th>Task</th>
						<th>Progress</th>
						<th style="width: 40px">Label</th>
					  </tr> -->
					  <?php
					  if (isset($roles)){
						get_list_module($status,$roles,0);
					  }else{
						get_list_module($status,null,0);
					  }
					  ?>
					</table>
					<button type="submit" class="btn btn-primary">Save</button> 
				  </form>      				
				</div>
  			</div>

		  </div>
		</div>
    </div>
	
<?php $this->load->view('template/footer');?>
<script type="text/javascript">

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
      
      $(function(){
        $('.set_role').on('click', function () {
          if ($(this).is(':checked'))
          {
            var menu_id = $(this).attr('id');
            var menu_num = $(this).val();
            if($('#remove_'+menu_num).length > 0){$('#remove_'+menu_num).removeAttr('checked')}
            $('#menu_num_'+menu_num).append('<td class="'+menu_id+'" width="4%">>></td>');
            $('#menu_num_'+menu_num).append('<td class="'+menu_id+'" colspan="3"><input name="active_'+menu_num+'" type="checkbox" checked="checked" value="1"/> <b>Active</b></td>');
            /*if($('.parent_menu').is(':checked')){
              $('.child_'+menu_num).prop('checked',true);
            }*/
          } else {
            var menu_id = $(this).attr('id');
              $('.'+menu_id).detach();
            var id = $(this).val();
            if($('#remove_'+id).length > 0){$('#remove_'+id).attr('checked', 'checked')}
            /*if($('.parent_menu').length > 0){
              $('.child_'+menu_num).prop('checked',false);
            }*/
          }
        });
      });
  
</script>