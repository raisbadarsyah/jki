
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/autosize.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/formatter.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/handlebars.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/passy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/maxlength.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_controls_extended.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location_edit.js"></script>


	<!-- /theme JS files -->

	<script type="text/javascript">
		
	$(document).keypress(
	    function(event){
	     if (event.which == '13') {
	        event.preventDefault();
	      }


	});

	function validate_fileupload(file)
	{
	
	
	var fileSize = file.files[0].size / 1024 / 1024;
	var fileName = file.files[0].name;

    var allowed_extensions = new Array("jpg","jpeg","png");
    var file_extension = fileName.split('.').pop().toLowerCase(); 
    // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

   
        if (fileSize > 2) {
           
		     	$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File tidak boleh lebih dari 2 MB', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
		  $('#uploadBar').attr('disabled', 'disabled');


            return false;
        } 


	    for(var i = 0; i <= allowed_extensions.length; i++)
	    {
	        if(allowed_extensions[i]==file_extension)
	        {
	        	$('#uploadBar').removeAttr('disabled');
	        	uploadFile();
	        	//alert(FileSize);
	            return true; // valid file extension

	        }
	    }

    			$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File ekstensi salah', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
	    $('#uploadBar').attr('disabled', 'disabled');
	    return false;
	}

function uploadFile() {
    // membaca data file yg akan diupload, dari komponen 'fileku'


     				$('.progress1').css('width', '0');
                    $('.msg').text('');
       
                    var myfile = $('#foto1').val();
                
                    if (myfile == '') {
                        alert('Please select file');
                        return;
                    }


                    var formData = new FormData();

                    formData.append('myfile', $('#foto1')[0].files[0]);
                 
                   
                    $('#uploadBar').attr('disabled', 'disabled');
                    $('.msg').text('Uploading in progress...');

                    $.ajax({
                        url: '<?php echo site_url('pemeriksaan/uploadFile') ?>',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.progress1').text(percentComplete + '%');
                                    $('.progress1').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSubmit: function() {
		                     $('.msg').text(data);
		                },
                        success: function (data) {
                        	
                        	$("#foto1_input").val(data['foto1']);
                         
                        }
                    });
}

function validate_fileupload2(file)
	{
	
	
	var fileSize = file.files[0].size / 1024 / 1024;
	var fileName = file.files[0].name;

    var allowed_extensions = new Array("jpg","jpeg","png");
    var file_extension = fileName.split('.').pop().toLowerCase(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

   
        if (fileSize > 2) {
            //alert('File tidak boleh lebih dari 2 MB');

            // Top center
		   
		     	$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File tidak boleh lebih dari 2 MB', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
		  $('#uploadBar').attr('disabled', 'disabled');


            return false;
        } 


	    for(var i = 0; i <= allowed_extensions.length; i++)
	    {
	        if(allowed_extensions[i]==file_extension)
	        {
	        	$('#uploadBar').removeAttr('disabled');
	        	uploadFile2();
	        	//alert(FileSize);
	            return true; // valid file extension

	        }
	    }

    			$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File ekstensi salah', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
	    $('#uploadBar').attr('disabled', 'disabled');
	    return false;
	}

function uploadFile2() {
    // membaca data file yg akan diupload, dari komponen 'fileku'


     				$('.progress2').css('width', '0');
                    $('.msg').text('');
       
                    var myfile = $('#foto2').val();
                
                    if (myfile == '') {
                        alert('Please select file');
                        return;
                    }


                    var formData = new FormData();

                    formData.append('myfile', $('#foto2')[0].files[0]);
                 
                   
                    $('#uploadBar').attr('disabled', 'disabled');
                    $('.msg').text('Uploading in progress...');

                    $.ajax({
                        url: '<?php echo site_url('pemeriksaan/uploadFile') ?>',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.progress2').text(percentComplete + '%');
                                    $('.progress2').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSubmit: function() {
		                     $('.msg').text(data);
		                },
                        success: function (data) {
                        	
                        	$("#foto2_input").val(data['foto1']);
                         
                        }
                    });
}


function validate_fileupload3(file)
	{
	
	
	var fileSize = file.files[0].size / 1024 / 1024;
	var fileName = file.files[0].name;

    var allowed_extensions = new Array("jpg","jpeg","png");
    var file_extension = fileName.split('.').pop().toLowerCase(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

   
        if (fileSize > 2) {
            //alert('File tidak boleh lebih dari 2 MB');

            // Top center
		   
		     	$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File tidak boleh lebih dari 2 MB', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
		  $('#uploadBar').attr('disabled', 'disabled');


            return false;
        } 


	    for(var i = 0; i <= allowed_extensions.length; i++)
	    {
	        if(allowed_extensions[i]==file_extension)
	        {
	        	$('#uploadBar').removeAttr('disabled');
	        	uploadFile3();
	        	//alert(FileSize);
	            return true; // valid file extension

	        }
	    }

    			$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File ekstensi salah', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
	    $('#uploadBar').attr('disabled', 'disabled');
	    return false;
	}

function uploadFile3() {
    // membaca data file yg akan diupload, dari komponen 'fileku'


     				$('.progress3').css('width', '0');
                    $('.msg').text('');
       
                    var myfile = $('#foto3').val();
                
                    if (myfile == '') {
                        alert('Please select file');
                        return;
                    }


                    var formData = new FormData();

                    formData.append('myfile', $('#foto3')[0].files[0]);
                 
                   
                    $('#uploadBar').attr('disabled', 'disabled');
                    $('.msg').text('Uploading in progress...');

                    $.ajax({
                        url: '<?php echo site_url('pemeriksaan/uploadFile') ?>',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.progress3').text(percentComplete + '%');
                                    $('.progress3').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSubmit: function() {
		                     $('.msg').text(data);
		                },
                        success: function (data) {
                        	
                        	$("#foto3_input").val(data['foto1']);
                         
                        }
                    });
}


function validate_fileupload4(file)
	{
	
	
	var fileSize = file.files[0].size / 1024 / 1024;
	var fileName = file.files[0].name;

    var allowed_extensions = new Array("jpg","jpeg","png");
    var file_extension = fileName.split('.').pop().toLowerCase(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

   
        if (fileSize > 2) {
            //alert('File tidak boleh lebih dari 2 MB');

            // Top center
		   
		     	$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File tidak boleh lebih dari 2 MB', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
		  $('#uploadBar').attr('disabled', 'disabled');


            return false;
        } 


	    for(var i = 0; i <= allowed_extensions.length; i++)
	    {
	        if(allowed_extensions[i]==file_extension)
	        {
	        	$('#uploadBar').removeAttr('disabled');
	        	uploadFile4();
	        	//alert(FileSize);
	            return true; // valid file extension

	        }
	    }

    			$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File ekstensi salah', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
	    $('#uploadBar').attr('disabled', 'disabled');
	    return false;
	}

function uploadFile4() {
    // membaca data file yg akan diupload, dari komponen 'fileku'


     				$('.progress4').css('width', '0');
                    $('.msg').text('');
       
                    var myfile = $('#foto4').val();
                
                    if (myfile == '') {
                        alert('Please select file');
                        return;
                    }


                    var formData = new FormData();

                    formData.append('myfile', $('#foto4')[0].files[0]);
                 
                   
                    $('#uploadBar').attr('disabled', 'disabled');
                    $('.msg').text('Uploading in progress...');

                    $.ajax({
                        url: '<?php echo site_url('pemeriksaan/uploadFile') ?>',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.progress4').text(percentComplete + '%');
                                    $('.progress4').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSubmit: function() {
		                     $('.msg').text(data);
		                },
                        success: function (data) {
                        	
                        	$("#foto4_input").val(data['foto1']);
                         
                        }
                    });
}

function progressHandler(event){
    // hitung prosentase
   // alert(event.loaded);
    var percent = (event.loaded / event.total) * 100;
    // menampilkan prosentase ke komponen id 'progressBar'
    document.getElementById("progressBar").value = Math.round(percent);
    // menampilkan prosentase ke komponen id 'status'
    document.getElementById("status").innerHTML = Math.round(percent)+"% telah terupload";
    // menampilkan file size yg tlh terupload dan totalnya ke komponen id 'total'
    document.getElementById("total").innerHTML = "Telah terupload "+event.loaded+" bytes dari "+event.total;
}


	</script>
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Management FDK</li>
							<li>Method</li>
							<li class="active">Create FDK</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'pemeriksaan/proses_edit'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Pemeriksaan</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								Masukkan data pemeriksaan sesuai dengan LHPP fisik yang ditandatangani pelanggan/instalatir .
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Detail Pelanggan</legend>

											<div class="row">
												

												<div class="col-md-12">
													<div class="form-group">
														<label>No Pendaftaran:</label>
														 <input type="text" class="form-control" name="no_pendaftaran" id="no_pendaftaran" required="" value="<?php echo $detail_pelanggan->no_pendaftaran ?>" readonly>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Nama:</label>
														 <input type="text" class="form-control" name="nama" id="nama" required="" value="<?php echo $detail_pelanggan->Nama ?>" readonly>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Alamat:</label>
														 <input type="text" class="form-control" name="alamat" id="alamat" required="" value="<?php echo $detail_pelanggan->alamat ?>" readonly>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Tarif/Daya:</label>
														 <input type="text" class="form-control" name="tarif_daya" id="tarif_daya" required="" value="<?php echo $detail_pelanggan->nm_tarif ?>/<?php echo $detail_pelanggan->daya ?>" readonly>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>BTL:</label>
														 <input type="text" class="form-control" name="btl" id="btl" value="<?php echo $detail_pelanggan->nm_btl ?>" required readonly>
													</div>
												</div>
												
											
												<div class="col-md-3">
													<div class="form-group">
														<label>Nama Instalir:</label>
														<input type="text" class="form-control" name="nm_instalir" id="nm_instalir" placeholder="Telp" value="<?php echo $detail_pelanggan->nama_instalir ?>" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Telp Instalir:</label>
														<input type="text" class="form-control" name="telp_instalir" id="telp_instalir" placeholder="Telp" value="<?php echo $detail_pelanggan->telp_instalir ?>" required>
													</div>
												</div>
											</div>

												<p></p>
												<legend><i class="icon-reading position-left"></i>Pemeriksa</legend>
											<div class="row">

												<div class="col-md-12">
													<div class="form-group">
														<label>No Surat Tugas:</label>
														<input type="text" class="form-control" name="no_surat_tugas" id="no_surat_tugas" value="<?php echo $detail_pelanggan->no_surat_tugas ?>" required>
													</div>
												</div>


												<div class="col-md-6">
													<div class="form-group">
														<label>Pemeriksa 1:</label>
														 <select class="select" name="pemeriksa1" id="pemeriksa1">
																<option value="">-- Pilih Pemeriksa --</option>
																<?php foreach ($pemeriksa as $key => $value) {?>

																<option <?php if($detail_pelanggan->pemeriksa1 == $value->coreUserId) { echo "selected"; } ?> value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Pemeriksa 2:</label>
														 <select class="select" name="pemeriksa2" id="pemeriksa2">
																<option value="">-- Pilih Pemeriksa --</option>
																<?php foreach ($pemeriksa as $key => $value) {?>

																<option <?php if($detail_pelanggan->pemeriksa2 == $value->coreUserId) { echo "selected"; } ?> value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>


												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>No LHPP:</label>
														<input type="text" class="form-control" name="no_lhpp" id="no_lhpp" placeholder="No LHPP" value="<?php echo $detail_pelanggan->no_lhpp;?>" readonly>
													</div>
												</div>

												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>Tanggal LHPP:</label>
														<input type="text" class="form-control daterange-single" name="tgl_lhpp" id="tgl_lhpp" placeholder="Tanggal LHPP" value="<?php echo $detail_pelanggan->tgl_lhpp;?>" required="">
													</div>
												</div>

												<div class="col-md-6" id="div_atasan1">
													<div class="form-group">
														<label>Gambar Instalasi:</label>
														<select class="select" name="gambar_instalasi" id="gambar_instalasi">
																<option <?php if($detail_pelanggan->gambar_instalasi == 1) { echo "selected"; } ?> value="1"> Gambar Sesuai</option>
																<option <?php if($detail_pelanggan->pemeriksa1 == 2) { echo "selected"; } ?> value="2"> Gambar Tidak Sesuai</option>
													
															</select>
													</div>
												</div>

												<div class="col-md-6" id="div_atasan1">
													<div class="form-group">
														<label>Diagram Garis Tunggal:</label>
														<select class="select" name="diagram_garis" id="diagram_garis">
																<option <?php if($detail_pelanggan->diagram_garis_tunggal == 1) { echo "selected"; } ?> value="1"> Diagram Sesuai</option>
																<option <?php if($detail_pelanggan->diagram_garis_tunggal == 2) { echo "selected"; } ?> value="2"> Diagram Tidak Sesuai</option>
													
															</select>
													</div>
												</div>
											</div>
												<p></p>
												<legend><i class="icon-reading position-left"></i>Penghantar</legend>
											<div class="row">
												<div class="col-md-3" id="div_atasan1">
													<div class="form-group">
														<label>Penghantar Proteksi PE (Saluran Utama):</label>
														<select class="select" name="pe_utama" id="pe_utama">
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_utama == 1) { echo "selected"; } ?> value="1"> Ada</option>
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_utama == 2) { echo "selected"; } ?> value="2"> Tidak Ada</option>
													
															</select>
													</div>
												</div>

												<div class="col-md-3" id="div_atasan1">
													<div class="form-group">
														<label>Penghantar Proteksi PE (Saluran Cabang):</label>
														<select class="select" name="pe_cabang" id="pe_cabang">
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_cabang == 1) { echo "selected"; } ?> value="1"> Ada</option>
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_cabang == 2) { echo "selected"; } ?> value="2"> Tidak Ada</option>
													
															</select>
													</div>
												</div>

												<div class="col-md-3" id="div_atasan1">
													<div class="form-group">
														<label>Penghantar Proteksi PE (Saluran Akhir):</label>
														<select class="select" name="pe_akhir" id="pe_akhir">
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_akhir == 1) { echo "selected"; } ?> value="1"> Ada</option>
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_saluran_akhir == 2) { echo "selected"; } ?> value="2"> Tidak Ada</option>
													
															</select>
													</div>
												</div>

												<div class="col-md-3" id="div_atasan1">
													<div class="form-group">
														<label>Penghantar Proteksi PE (Kotak Kontak):</label>
														<select class="select" name="pe_kotak_kontak" id="pe_kotak_kontak">
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_kotak_kontak == 1) { echo "selected"; } ?> value="1"> Ada</option>
																<option <?php if($detail_pelanggan->penghantar_proteksi_pe_kotak_kontak == 2) { echo "selected"; } ?> value="2"> Tidak Ada</option>
													
															</select>
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Jenis Penghantar Utama:</label>
														
														<input type="text" class="form-control jns_penghantar_utama" name="jns_penghantar_utama" id="jns_penghantar_utama" placeholder="Jenis Penghantar Utama" value="<?php echo $detail_pelanggan->jns_penghantar_utama?>" required="">
													
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Jenis Penghantar Cabang:</label>
														
														<input type="text" class="form-control jns_penghantar_cabang" name="jns_penghantar_cabang" id="jns_penghantar_cabang" value="<?php echo $detail_pelanggan->jns_penghantar_cabang?>" placeholder="Jenis Penghantar Cabang" required="">
														
													
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Jenis Penghantar Akhir:</label>
														
														<input type="text" class="form-control jns_penghantar_akhir" name="jns_penghantar_akhir" id="jns_penghantar_akhir" placeholder="Jenis Penghantar Akhir" value="<?php echo $detail_pelanggan->jns_penghantar_akhir?>" required="">
													
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Penghantar Pembumian (Jenis):</label>
														
														<input type="text" class="form-control jns_penghantar_bumi" name="jns_penghantar_bumi" id="jns_penghantar_bumi" placeholder="Jenis Penghantar Bumi" value="<?php echo $detail_pelanggan->jns_penghantar_bumi?>" required="">
														
														</select>
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Penghantar Pembumian (Luas Penampang):</label>
														
														<input type="text" class="form-control luas_penampang" name="luas_penampang" id="luas_penampang" placeholder="Luas Penampang Penghantar Pembumian" value="<?php echo $detail_pelanggan->luas_penampang_penghantar_bumi?>" required="">
														

													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Penghantar Pembumian (Sistem Pembumian):</label>
														<select class="select" name="sistem_pembumian" id="sistem_pembumian">
																<option <?php if($detail_pelanggan->sistem_pembumian == 'TT') { echo "selected"; } ?> value="TT"> TT</option>
																<option <?php if($detail_pelanggan->sistem_pembumian == 'TN-C-S') { echo "selected"; } ?> value="TN-C-S"> TTN-C-S</option>
																<option <?php if($detail_pelanggan->sistem_pembumian == 'TN-C') { echo "selected"; } ?> value="TN-C"> TN-C</option>
																<option <?php if($detail_pelanggan->sistem_pembumian == 'TN-S') { echo "selected"; } ?> value="TN-S"> TN-S</option>
																<option <?php if($detail_pelanggan->sistem_pembumian == 'DLL') { echo "selected"; } ?> value="DLL"> Lainnya</option>

															</select>
													</div>
												</div>
											</div>

											<p></p>
												<legend><i class="icon-reading position-left"></i>Saklar, Proteksi Sirkit & Penampang Penghantar</legend>
											<div class="row">

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Sakelar (Sakelar Utama):</label>
														
														<input type="text" class="form-control sakelar_utama" name="sakelar_utama" id="sakelar_utama" placeholder="Sakelar Utama" value="<?php echo $detail_pelanggan->sakelar_utama?>" required="">
														
														
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Sakelar (Sakelar Cabang 1):</label>
														
														<input type="text" class="form-control sakelar_cabang1" name="sakelar_cabang1" id="sakelar_cabang1" placeholder="Sakelar Cabang 1" value="<?php echo $detail_pelanggan->sakelar_cabang1?>" required="">
														
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Sakelar (Sakelar Cabang 2):</label>
														
														<input type="text" class="form-control sakelar_cabang2" name="sakelar_cabang2" id="sakelar_cabang2" value="<?php echo $detail_pelanggan->sakelar_cabang2?>" placeholder="Sakelar Cabang" required="">
														
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Proteksi Sirkit Akhir (PHBK Utama):</label>
														
														<input type="text" class="form-control phbk_utama" name="phbk_utama" id="phbk_utama" placeholder="PHBK Utama" value="<?php echo $detail_pelanggan->phbk_utama?>" required="">
														
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Proteksi Sirkit Akhir (PHBK CABANG 1):</label>
														
														<input type="text" class="form-control phbk_cabang1" name="phbk_cabang1" id="phbk_cabang1" placeholder="PHBK Cabang 1" value="<?php echo $detail_pelanggan->phbk_cabang1?>" required="">
														

													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Proteksi Sirkit Akhir (PHBK CABANG 2):</label>
														
														<input type="text" class="form-control phbk_cabang2" name="phbk_cabang2" id="phbk_cabang2" placeholder="PHBK Cabang 2" value="<?php echo $detail_pelanggan->phbk_cabang2?>" required="">
													
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Penampang Penghantar Utama:</label>
														
														<input type="text" class="form-control penampang_penghantar_utama" name="penampang_penghantar_utama" id="penampang_penghantar_utama" value="<?php echo $detail_pelanggan->penampang_penghantar_saluran_utama?>" placeholder="Penampang Penghantar Utama" required="">
													
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Penampang Penghantar Cabang:</label>
														
														<input type="text" class="form-control penampang_penghantar_cabang" name="penampang_penghantar_cabang" id="penampang_penghantar_cabang" value="<?php echo $detail_pelanggan->penampang_penghantar_saluran_cabang?>" placeholder="Penampang Penghantar Cabang" required="">
														
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Penampang Penghantar Akhir:</label>
														
														<input type="text" class="form-control penampang_penghantar_akhir" name="penampang_penghantar_akhir" id="penampang_penghantar_akhir" value="<?php echo $detail_pelanggan->penampang_penghantar_saluran_akhir?>" placeholder="Penampang Penghantar Akhir" required="">
														
													</div>
												</div>

												<div class="col-md-3" id="div_departemen">
													<div class="form-group">
														<label>Penampang Penghantar Tiga Fasa:</label>
														
														<input type="text" class="form-control penampang_penghantar_tiga_fasa" name="penampang_penghantar_tiga_fasa" id="penampang_penghantar_tiga_fasa" value="<?php echo $detail_pelanggan->penampang_penghantar_saluran_akhir_tiga_fasa?>" placeholder="Penampang Penghantar 3 Fasa" required="">
														
													</div>
												</div>
											</div>


											<p></p>
												<legend><i class="icon-reading position-left"></i>Pengujian & Pemasangan</legend>
											<div class="row">

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Pengujian Polaritas (Fitting Lampu):</label>
														<select class="select" name="polaritas_fitting_lampu" id="polaritas_fitting_lampu">
																<option <?php if($detail_pelanggan->polaritas_fitting_lampu == '1') { echo "selected"; } ?> value="1"> Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_fitting_lampu == '2') { echo "selected"; } ?> value="2"> Tidak Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_fitting_lampu == '3') { echo "selected"; } ?> value="3"> Tidak Ada</option>
																

															</select>
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Pengujian Polaritas (Kotak Kontak):</label>
														<select class="select" name="polaritas_kotak_kontak" id="polaritas_kotak_kontak">
																<option <?php if($detail_pelanggan->polaritas_kotak_kontak == '1') { echo "selected"; } ?> value="1"> Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_kotak_kontak == '2') { echo "selected"; } ?> value="2"> Tidak Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_kotak_kontak == '3') { echo "selected"; } ?> value="3"> Tidak Ada</option>
																

															</select>
													</div>
												</div>


												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Pengujian Polaritas (Sakelar):</label>
														<select class="select" name="polaritas_sakelar" id="polaritas_sakelar">
																<option <?php if($detail_pelanggan->polaritas_sakelar == '1') { echo "selected"; } ?> value="1"> Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_sakelar == '2') { echo "selected"; } ?> value="2"> Tidak Sesuai</option>
																<option <?php if($detail_pelanggan->polaritas_sakelar == '3') { echo "selected"; } ?> value="3"> Tidak Ada</option>
																

															</select>
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Pemasangan (cm):</label>
														<input type="text" class="form-control" name="tinggi_kotak_kontak" id="tinggi_kotak_kontak" placeholder="Ketinggian Kotak Kontak" value="<?php echo $detail_pelanggan->tinggi_pemasangan_kotak_kontak ?>" required="">
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Pemasangan (cm):</label>
														<input type="text" class="form-control" name="tinggi_phbk" id="tinggi_phbk" placeholder="Ketinggian PHBK" value="<?php echo $detail_pelanggan->tinggi_pemasangan_phbk ?>" required="">
													</div>
												</div>

												<div class="col-md-4" id="div_departemen">
													<div class="form-group">
														<label>Jenis Kotak Kontak:</label>
														<select class="select" name="jns_pemasangan_kotak_kontak" id="jns_pemasangan_kotak_kontak">
																<option <?php if($detail_pelanggan->jns_pemasangan_kotak_kontak == '1') { echo "selected"; } ?> value="1"> Biasa</option>
																<option <?php if($detail_pelanggan->jns_pemasangan_kotak_kontak == '2') { echo "selected"; } ?> value="2"> Putar</option>
																<option <?php if($detail_pelanggan->jns_pemasangan_kotak_kontak == '3') { echo "selected"; } ?> value="3"> Tutup</option>
																<option <?php if($detail_pelanggan->jns_pemasangan_kotak_kontak == '4') { echo "selected"; } ?> value="4"> Tidak Ada</option>
																<option <?php if($detail_pelanggan->jns_pemasangan_kotak_kontak == '5') { echo "selected"; } ?> value="5"> Lainnya</option>

															</select>
													</div>
												</div>

												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>Perlengkapan/Komponen Bertanda SNI:</label>
														<select class="select" name="perlengkapan_sni" id="pelengkapan_sni">
																<option <?php if($detail_pelanggan->perlengkapan_sni == '1') { echo "selected"; } ?> value="1"> SNI</option>
																<option <?php if($detail_pelanggan->perlengkapan_sni == '2') { echo "selected"; } ?> value="2"> Tidak SNI</option>
																
															</select>
													</div>
												</div>

												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>Pengujian Pembebanan:</label>
														<select class="select" name="pengujian_pembebanan" id="pengujian_pembebanan">
																<option <?php if($detail_pelanggan->pengujian_pembebanan == '1') { echo "selected"; } ?> value="1"> Baik</option>
																<option <?php if($detail_pelanggan->pengujian_pembebanan == '2') { echo "selected"; } ?> value="2"> Tidak Baik</option>
																
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah PHB Utama:</label>
														<input type="text" class="form-control" name="jml_phb_utama" id="jml_phb_utama" value="<?php echo $detail_pelanggan->jml_phb_utama ?>" required="">
													</div>
												</div>

												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>Jumlah PHB 1 Phasa:</label>
														<input type="text" class="form-control" name="jml_phb_satu_phasa" id="jml_phb_satu_phasa" value="<?php echo $detail_pelanggan->jumlah_phb_satu_phasa ?>" required="">
													</div>
												</div>

												<div class="col-md-6" id="div_departemen">
													<div class="form-group">
														<label>Jumlah PHB 3 Phasa:</label>
														<input type="text" class="form-control" name="jml_phb_tiga_phasa" id="jml_phb_tiga_phasa" value="<?php echo $detail_pelanggan->jumlah_phb_tiga_phasa ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah PHB Cabang:</label>
														<input type="text" class="form-control" name="jml_phb_cabang" id="jml_phb_cabang" value="<?php echo $detail_pelanggan->jml_phb_cabang ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Saluran Cabang:</label>
														<input type="text" class="form-control" name="jml_saluran_cabang" id="jml_saluran_cabang" value="<?php echo $detail_pelanggan->jml_saluran_cabang ?>" required="">
													</div>
												</div>


												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Saluran Akhir:</label>
														<input type="text" class="form-control" name="jml_saluran_akhir" id="jml_saluran_akhir" value="<?php echo $detail_pelanggan->jml_saluran_akhir ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Titik Lampu:</label>
														<input type="text" class="form-control" name="jml_titik_lampu" id="jml_titik_lampu" value="<?php echo $detail_pelanggan->jml_titik_lampu ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Sakelar:</label>
														<input type="text" class="form-control" name="jml_sakelar" id="jml_sakelar" value="<?php echo $detail_pelanggan->jml_sakelar ?>" required="">
													</div>
												</div>


												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>KKB:</label>
														<input type="text" class="form-control" name="kkb" id="kkb" value="<?php echo $detail_pelanggan->kkb ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>KKK:</label>
														<input type="text" class="form-control" name="kkk" id="kkk" value="<?php echo $detail_pelanggan->kkk ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Tahanan Isolasi Penghantar (MΩ):</label>
														<input type="text" class="form-control" name="tahanan_isolasi_penghantar" id="tahanan_isolasi_penghantar" value="<?php echo $detail_pelanggan->tahanan_isolasi_penghantar ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Resisten Pembumian(Ω):</label>
														<input type="text" class="form-control" name="resisten_pembumian" id="resisten_pembumian" value="<?php echo $detail_pelanggan->resistensi_pembumian ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Motor Listrik (Unit):</label>
														<input type="text" class="form-control" name="jml_motor_listrik" id="jml_motor_listrik" value="<?php echo $detail_pelanggan->jml_motor_listrik ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Jumlah Motor Listrik (Kw):</label>
														<input type="text" class="form-control" name="jml_daya_motor_listrik" id="jml_daya_motor_listrik" value="<?php echo $detail_pelanggan->jml_daya_motor_listrik ?>" required="">
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Catatan Pemeriksa:</label>
														<textarea name="catatan_pemeriksa" class="form-control"> <?php echo $detail_pelanggan->catatan_pemeriksa ?> </textarea>
													</div>
												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Foto 1:</label>
														<input type="file" name="foto1" class="file-styled" id="foto1" onchange="validate_fileupload(this)">
														<input type="hidden" name="foto1_input" id="foto1_input" value="<?php echo $detail_pelanggan->foto1 ?>" >
													</div>

													<div class="progress progress-rounded">
														<div class="progress-bar progress-bar-warning progress1" role="progressbar" style="width: 0%">
															<span>0% Complete</span>
														</div>
													</div>


												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Foto 2:</label>
														<input type="file" name="foto2" class="file-styled" id="foto2" onchange="validate_fileupload2(this)">
														<input type="hidden" name="foto2_input" id="foto2_input" value="<?php echo $detail_pelanggan->foto2 ?>">
													</div>
													<div class="progress progress-rounded">
														<div class="progress-bar progress-bar-warning progress2" role="progressbar" style="width: 0%">
															<span>0% Complete</span>
														</div>
													</div>

												</div>

												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Foto 3:</label>
														<input type="file" name="foto3" class="file-styled" id="foto3" onchange="validate_fileupload3(this)">
														<input type="hidden" name="foto3_input" id="foto3_input" value="<?php echo $detail_pelanggan->foto3 ?>">
													</div>
													<div class="progress progress-rounded">
														<div class="progress-bar progress-bar-warning progress3" role="progressbar" style="width: 0%">
															<span>0% Complete</span>
														</div>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Foto 4:</label>
														<input type="file" name="foto4" class="file-styled" id="foto4" onchange="validate_fileupload4(this)">
														<input type="hidden" name="foto4_input" id="foto4_input" value="<?php echo $detail_pelanggan->foto4 ?>">
													</div>
													<div class="form-group">
														<div class="progress progress-rounded">
															<div class="progress-bar progress-bar-warning progress4" role="progressbar" style="width: 0%">
																<span>0% Complete</span>
															</div>
														</div>
													</div>

												</div>

												<div class="form-group">			
													<div class="col-md-12">
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Koordinat Lokasi</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
																
																<div class="form-group">
																	<label>Location:</label>
																	<input type="text" class="form-control" id="us3-address" name="location" value="<?php echo $detail_pelanggan->location ?>">
																</div>
																<div class="form-group">
																	<div id="us3" class="map-wrapper"></div>
																</div>

																<div class="form-group">		
																	<label>Latitude:</label>
																	<input type="text" class="form-control" id="us3-lat" name="lattitude" value="<?php echo $detail_pelanggan->lattitude ?>">
																</div>

																<div class="form-group">
																	<label>Longitude:</label>
																	<input type="text" class="form-control" id="us3-lon" name="longitude" value="<?php echo $detail_pelanggan->longitude ?>">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

									
									

										</fieldset>
									</div>
									<br>
									

								<div class="text-right">
									<input type="hidden" name="no_pemeriksaan" value="<?php echo $detail_pelanggan->no_pemeriksaan?>">
									<input type="hidden" name="jml_pemeriksaan" value="<?php echo $detail_pelanggan->jumlah_pemeriksaan?>">
									<button type="submit" class="btn btn-primary">EDIT <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
