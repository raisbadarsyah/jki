<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_responsive.js"></script>
	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>

	<style type="text/css">
		
		/* Table */
		.display {
			border-collapse: collapse;
			font-size: 0.9em;
		}
		.display th, 
		.display td {
			border: 1px solid #e1edff;
			padding: 1px 7px;
		}
		.display .title {
			caption-side: bottom;
			margin-top: 12px;
		}
		
		/* Table Header */
		.display thead th {
			background-color: #508abb;
			color: #FFFFFF;
			border-color: #6ea1cc !important;
			
		}
		
		/* Table Body */
		.display tbody td {
			color: #353535;
		}
		.display tbody td:first-child,
		.display tbody td:last-child,
		.display tbody td:nth-child(4) {
			text-align: left;
		}
		.display tbody tr:nth-child(odd) td {
			background-color: #f4fbff;
		}
		.display tbody tr:hover td {
			background-color: #ffffa2;
			border-color: #ffff0f;
			transition: all .2s;
		}
		
		/* Table Footer */
		.display tfoot th {
			background-color: #e5f5ff;
		}
		.display tfoot th:first-child {
			text-align: left;
		}
		.display tbody td:empty
		{
			background-color: #ffcccc;
		}

	</style>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Dashboard</a></li>
							<li><a>Pemeriksaan</a></li>
							<li class="active">Cetak Surat Tugas</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">


						<form action="<?php echo base_url().'pemeriksaan/proses_surat_tugas'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Cetak Surat Tugas</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Cetak Surat Tugas</legend>

											<div class="row">

												<div class="col-md-12">
													<div class="form-group">
														<table class="display">
					<thead>
					<tr>
						<th>No Pendaftaran</th>						
						<th>No Agenda</th>
						<th>Nama</th>
						<th>Alamat</th>
						<th>Kota</th>
						<th>Wilayah</th>
						<th>Area</th>
						<th>Sub Area</th>
						<th>Nama BTL</th>
						<th>Jenis Bangunan</th>
						<th>Tarif/Daya</th>
						<th>Harga</th>
						
						 </tr>
				  </thead>
				  <tbody>
				
				   <?php foreach($pelanggan as $row){?>
					<tr>
						<td><?php echo $row->no_pendaftaran;?></td>
						<td><?php if(isset($row->no_agenda)) { echo $row->no_pendaftaran; } ?></td>
						<td><?php echo $row->Nama;?></td>
						<td><?php echo $row->alamat;?></td>
						<td><?php echo $row->nm_kota;?></td>
						<td><?php echo $row->nm_kanwil;?></td>
				
						<td><?php echo $row->nm_area;?></td>
						<td><?php echo $row->nm_sub_area;?></td>
						<td><?php echo $row->nm_btl;?></td>
						<td><?php echo $row->nm_bangunan;?></td>
						<td><?php echo $row->nm_tarif;?>/<?php echo $row->daya;?></td>
						<td><?php echo $row->harga;?></td>
										  
				    </tr>
				    <input type="hidden" name="no_pendaftaran[]" value="<?php echo $row->no_pendaftaran;?>">
				    <?php }?>
				 	
				  </tbody>
						</table>
													</div>
												</div>
												
					
											</div>

										

												<div class="col-md-6">
													<div class="form-group">
														<label>Pemeriksa 1:</label>
														 <select class="select" name="pemeriksa1" id="pemeriksa1" required>
																<option value="">-- Pilih Pemeriksa --</option>
																<?php foreach ($pemeriksa as $key => $value) {?>

																<option value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Pemeriksa 2:</label>
														 <select class="select" name="pemeriksa2" id="pemeriksa2">
																<option value="">-- Pilih Pemeriksa --</option>
																<?php foreach ($pemeriksa as $key => $value) {?>

																<option value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

									

										</fieldset>
									</div>
									<br>
									
								
								<div class="text-right">

									<button type="submit" class="btn btn-primary">Cetak Surat</button>
								</div>
							</div>
						</div>
					</form>

				
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>

<!-- Primary modal -->
          <div class="modal fade" id="popUp">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-primary">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">Pembayaran</h6>
                </div>
			<form id="bayar" method="post" action="<?php echo base_url().'pembayaran/proses_bayar'; ?>">
                <div class="modal-body">
                  <div class="fetched-data"></div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Bayar</button>
                
                </div>
            </form>
              </div>
            </div>
          </div>
          <!-- /primary modal -->

<script type="text/javascript">
    	
        $('#popUp').on('show.bs.modal', function (e) {
        	
            var no_pendaftaran = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
         
            $.ajax({
                type : 'post',
                url : "<?php echo site_url();?>pembayaran/detail_pelanggan",
                data :  'no_pendaftaran='+ no_pendaftaran,
                dataType: "html",
                success : function(data){
                  
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
               
            });
         });
   
  </script>

</body>
</html>
