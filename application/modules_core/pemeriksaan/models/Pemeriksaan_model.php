<?php

class Pemeriksaan_model extends CI_Model {

    var $column_order_belum_diperiksa = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat','daya','nm_kota','nm_kanwil','nm_area','nm_sub_area','nm_ptl',null,null,'tgl_bayar'); //set column field database for datatable orderable
    var $column_search_belum_diperiksa = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat','daya','nm_kota','nm_kanwil','nm_area','nm_sub_area','nm_ptl');  //set column field database for datatable 
    var $order_belum_diperiksa = array('pelanggan.created_at' => 'desc'); // default order

    var $column_order_sudah_diperiksa = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat'); //set column field database for datatable orderable
    var $column_search_sudah_diperiksa = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat');  //set column field database for datatable 
    var $order_sudah_diperiksa = array('pelanggan.created_at' => 'desc'); // default order


   function get_all(){
		$this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    private function _get_datatables_belum_diperiksa_query($params)
     {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan');
        $this->db->from('pelanggan');
       
       // $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran','left');
       // $this->db->join('no_spk','pelanggan.no_pendaftaran=no_spk.no_pendaftaran','left');
       // $this->db->join('core_user as pemeriksa1','pemeriksa1.coreUserId=no_spk.pemeriksa1','left');
       // $this->db->join('core_user as pemeriksa2','pemeriksa2.coreUserId=no_spk.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
         $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
       // $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
       // $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
       
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
       // $this->db->join('kantor_sub_area as c','pelanggan.kode_sub_area=c.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->where('status_pembayaran =', 1);
        
         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 6)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        if(isset($params['status']) && $params['status'] == 1)
          {
                $this->db->where('tgl_bayar < DATE(DATE_ADD(now(), INTERVAL -2 DAY))');
                $this->db->where('status_pemeriksaan =', 2);
          }

        if(isset($params['status']) && $params['status'] == 2)
          {
                $this->db->where('tgl_bayar > DATE(DATE_ADD(now(), INTERVAL -2 DAY))');
                $this->db->where('status_pemeriksaan =', 2);
          }

        if(isset($params['status']) && $params['status'] == 3)
          {
                $this->db->where('status_pemeriksaan =', 3);
          }
        if(!isset($params['status']))
          {
               
                $this->db->where('status_pemeriksaan =', 2);
          }

         

        $i = 0;
     
        foreach ($this->column_search_belum_diperiksa as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_belum_diperiksa) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        /*
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_belum_diperiksa[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_belum_diperiksa))
        {
            $order = $this->order_belum_diperiksa;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        */

        $this->db->group_by('pelanggan.no_pendaftaran');

    }


    private function _get_datatables_sudah_diperiksa_query($params)
     {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
       // $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
       // $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
       // $this->db->join('kantor_sub_area as c','pelanggan.kode_sub_area=c.kode_sub_area','left');
        
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
     
         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }
        
        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 6)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        if(isset($params['status']) && $params['status'] == 1)
          {
                $this->db->where('status_verifikasi =', 3);
          }

       

        $i = 0;
     
        foreach ($this->column_search_sudah_diperiksa as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_sudah_diperiksa) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
         /*
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sudah_diperiksa[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        */
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->group_by('pelanggan.no_pendaftaran');

    }

    function get_datatables_belum_diperiksa($params)
    {
        $this->_get_datatables_belum_diperiksa_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
      //$this->db->limit('5', '1');
        $query = $this->db->get();
       // return $this->db->last_query();
        return $query->result();
    }
 
    function count_filtered_belum_diperiksa($params)
    {
        $this->_get_datatables_belum_diperiksa_query($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_belum_diperiksa($params)
    {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan');
        $this->db->from('pelanggan');
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 2);
        
        return $this->db->count_all_results();
    }


    function get_datatables_sudah_diperiksa($params)
    {
        $this->_get_datatables_sudah_diperiksa_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
       // $this->db->limit('5', '1');
        $query = $this->db->get();
      //  return $this->db->last_query();
        return $query->result();
    }
 
    function count_filtered_sudah_diperiksa($params)
    {
        $this->db->select('count(*) as jumlah');
        $this->db->from('pemeriksaan'); 
        $query = $this->db->get();
        return $query->row();
    }
 
    public function count_all_sudah_diperiksa($params)
    {
        $this->db->select('count(*) as jumlah');
        $this->db->from('pemeriksaan'); 
        $query = $this->db->get();
        return $query->row();
    }

    function get_detail_pemeriksaan($no_pemeriksaan)
    {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('no_pemeriksaan =', $no_pemeriksaan);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }
    
      function get_detail_pelanggan($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pelanggan.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_pemeriksa($params){
        $this->db->select('*');
        $this->db->from('core_user');


        if(isset($params['nip']) && ($params['nip'] == 111 || $params['nip'] == 222))
        {
            $this->db->join('kantor_area','kantor_area.kode_area=core_user.kode_area');
            $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
            $this->db->where('kantor_wilayah.kode_kanwil',$params['kode_kanwil']);
            $this->db->group_by('Name');
        }
        else
        {

             if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
            {
                
                 if(isset($params['kode_kanwil']))
                {
                    $this->db->join('kantor_area','kantor_area.kode_area=core_user.kode_area');
                    $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
                    $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
                }else
                {
                     $this->db->where('kantor_wilayah.kode_kanwil', '0');
                }
                
            
            }

             if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
            {
                
                if(isset($params['kode_area']))
                {
                    $this->db->join('kantor_area','kantor_area.kode_area=core_user.kode_area');
                    $this->db->where('kantor_area.kode_area', $params['kode_area']);
                }else
                {
                     $this->db->where('kantor_area.kode_area', '0');
                }
                
            
            }

            if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
            {
                if(isset($params['kode_sub_area']))
                {
                    $this->db->join('kantor_area','kantor_area.kode_area=core_user.kode_area');
                    $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
                    $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area');
                    $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
                }else
                {
                    $this->db->join('kantor_area','kantor_area.kode_area=core_user.kode_area');
                    $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
                    $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area');
                     $this->db->where('kantor_sub_area.kode_sub_area', '0');
                }
                
            
            }

            if(isset($params['id_departemen']) && $params['id_departemen'] == 6)
            {
                
                if(isset($params['user_id']))
                {
                    $this->db->where('coreUserId', $params['user_id']);
                }
            
            }


        }

        $this->db->where('id_departemen', '6');
        $this->db->group_by('coreUserId');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota(){
        $this->db->select('*');
        $this->db->from('kota');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl(){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl(){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($pemeriksaan) {
       $query = $this->db->insert('pemeriksaan', $pemeriksaan);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_spk($data_no_spk) {
       $query = $this->db->insert('no_spk', $data_no_spk);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data_pemeriksaan, $no_pemeriksaan){
        $this->db->trans_begin();
		$this->db->where('no_pemeriksaan', $no_pemeriksaan);
		$this->db->update('pemeriksaan', $data_pemeriksaan);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }


    function update_pemeriksa($data_pemeriksaan, $no_pendaftaran){
        $this->db->trans_begin();
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->update('pemeriksaan', $data_pemeriksaan);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }       
    }

     function get_max_no_lhpp($params){
        $this->db->select('MAX(pemeriksaan.no_urut) AS ExtractString');
       // $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
      
        /*
        if(isset($params['kode_area']) && $params['kode_area'] != "")
        {      
           // $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area');
           // $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area');
            $this->db->where('pelanggan.kd_area', $params['kode_area']);
        }
        */
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_max_no_spk(){
        $this->db->select('MAX(id) as id');
        $this->db->from('no_spk');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_detail_pemeriksa($id_pemeriksa){
        $this->db->select('*');
        $this->db->from('core_user');
        $this->db->where('coreUserId', $id_pemeriksa);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_no_spk($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('no_spk');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->order_by('no_spk desc');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_pemeriksa1($no_pendaftaran)
    {
       
        $this->db->select('*');
        $this->db->from('no_spk');
        $this->db->join('core_user','no_spk.pemeriksa1=core_user.coreUserId');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->group_by('no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

    function get_pemeriksa2($no_pendaftaran)
    {
       
        $this->db->select('*');
        $this->db->from('no_spk');
        $this->db->join('core_user','no_spk.pemeriksa2=core_user.coreUserId');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->group_by('no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

    function get_pembayaran($no_pendaftaran)
    {
         $this->db->select('*');
        $this->db->from('pembayaran');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

}

