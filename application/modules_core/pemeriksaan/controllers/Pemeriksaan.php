<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends CI_Controller {

	function __construct()
	{
		ini_set('memory_limit', '-1');
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan/Pelanggan_Model','Staff/Staff_Model','Pembayaran/Pembayaran_Model','Pemeriksaan_Model','Laporan/Laporan_Model','Verifikasi/Verifikasi_Model'));
		$this->load->helper(array('check_auth_menu','tgl_indonesia'));
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_all();


		$this->load->view('index',$data);
	}

	function detail_pelanggan()
	{
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');
		$no_pendaftaran = $this->input->post('no_pendaftaran');
		
		$config['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		
		$this->load->view('v_detail_pelanggan', $config);
	}

	function detail_lhpp()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		
		$this->load->view('detail_lhpp',$data);
	}

	function no_urut_update()
	{
		$this->load->database();
   		$sql = $this->db->query('SELECT kd_area FROM pelanggan,pemeriksaan WHERE pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran AND no_urut="NULL"');
   		//$sql->result_array();

   		$no = 0;
   		foreach ($sql->result_array() as $value) {

   			$this->load->database();
   			$sql = $this->db->query('SET @rank:=0;');
   			$sql = $this->db->query('update pelanggan JOIN pemeriksaan ON pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran set no_urut=@rank:=@rank+1 WHERE kd_area="'.$value['kd_area'].'"');

   			echo $value['kd_area'];
   		}
   		var_dump($sql->result_array());
   		exit();
	}

	function cetak_lhpp()
	{
		
						$no_pendaftaran = $this->uri->segment(3);
						$detail_pelanggan = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
						if($detail_pelanggan->tipe_pelanggan == 1)
						{
							$tipe_pelanggan = "Baru";
						}
						if($detail_pelanggan->tipe_pelanggan == 2)
						{
							$tipe_pelanggan = "Rubah Daya";
						}
						if($detail_pelanggan->tipe_pelanggan == 3)
						{
							$tipe_pelanggan = "Instalasi Lama";
						}
						else
						{
							$tipe_pelanggan = "";
						}
							$html = "
						
				
				<div style='width:900px;padding:15px;border:1px solid #000;'>
				<table border='0' cellpadding='0' cellspacing='0' width='100%'>
					<tbody>
						<tr>
							<td align='center' width='103' rowspan='2'><img src='".base_url()."template/assets/images/jki_logo_small.png' width='78' height='78'/></td>
							<td align='center'>
							<h6 style='font-size:9px;'>LAPORAN HASIL PEMERIKSAAN DAN PENGUJIAN INSTALASI PEMANFAATAN TENAGA LISTRIK TEGANGAN RENDAH<br />
							PT JASA KELISTRIKAN INDONESIA<br />
							NOMOR LHPP :.................</h6>

							<hr />
							</tr>
							<tr>
							<td>
							<div style='padding:15px;'>
							<table border='0' cellpadding='0' cellspacing='0' style='width:600px;' align='center'>
								<tbody>
									<tr>
										<td><span style='font-size:9px;'>Pemilik</span></td>
										<td width='9'><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->Nama."</span></td>
										<td>&nbsp;</td>
										<td><span style='font-size:9px;'>Instalasi</span></td>
										<td width='9'><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$tipe_pelanggan."</span></td>
									</tr>
									<tr>
										<td><span style='font-size:9px;'>Alamat Instalasi</span></td>
										<td width='9'><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'><span style='font-weight: 700;'>".$detail_pelanggan->alamat."</span></span></td>
										<td>&nbsp;</td>
										<td><span style='font-size:9px;'>Instalatur</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->nm_btl."</span></td>
									</tr>
									<tr>
										<td><span style='font-size:9px;'>Kantor PLN</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->nm_ptl."</span></td>
										<td>&nbsp;</td>
										<td><span style='font-size:9px;'>No. Gambar</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->no_gambar."</span></td>
									</tr>
									<tr>
										<td><span style='font-size:9px;'>No. SIP</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->no_sip."</span></td>
										<td>&nbsp;</td>
										<td><span style='font-size:9px;'>Tarif/Daya</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>".$detail_pelanggan->nm_tarif."/".$detail_pelanggan->daya."</span></td>
									</tr>
									<tr>
										<td><span style='font-size:9px;'>Gardu</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td><span style='font-size:9px;'>Pemeriksaan ke</span></td>
										<td><span style='font-size:9px;'>:</span></td>
										<td><span style='font-size:9px;'>......... tanggal ........</span></td>
									</tr>
								</tbody>
							</table>
							</div>
							</tr>
							</td>
							</td>
						</tr>
						<tr>
							<td colspan='3'>
							<hr />
							<p><img src='".base_url()."template/assets/images/img.jpg' width='900px' /></p>
							</td>
						</tr>
					</tbody>
				</table>
				</div>";


					   $mpdf = new \Mpdf\Mpdf();

				       // Write some HTML code:

				       $mpdf->WriteHTML($html);

				       // Output a PDF file directly to the browser
				       $mpdf->Output();

					exit();	
			

	}

	function proses_pemeriksaan()
	{
		$user_id = $this->session->userdata('user_id');
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_kwitansi = date('YmdHis');
			$tgl_bayar = date('Y-m-d H:i:s');
			$diterima_oleh = $user_id;
			$harga = $this->input->post('harga');

			try {
				
				$data_pembayaran = array(
					"no_kwitansi"=> $no_kwitansi,
					"no_pendaftaran"=> $no_pendaftaran,
					"tgl_bayar"=> $tgl_bayar,
					"diterima_oleh"=> $diterima_oleh,
					"harga"=> $harga,
				);

				$data_pelanggan = array(
					"status_pembayaran"=> 1,
				);
				
			
				$insert_pembayaran = $this->Pembayaran_Model->insert($data_pembayaran);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pembayaran == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pembayaran/belum_bayar','refresh');
				}


		}
	}

	function instalasi_belum_siap()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$reject_reason = $this->input->post('reject_reason');

			try {
				
				$data_pelanggan = array(
					"no_pendaftaran"=> $no_pendaftaran,
					"status_pemeriksaan"=> 3,
					"alasan_instalasi_belum_siap"=> $reject_reason,
				);

				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}

			if($update_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pemeriksaan/belum_diperiksa','refresh');
				}

		}
	}


	function querypagingbelumdiperiksa()
	{
		$status = "";
		//$status = $this->uri->segment(3);
		$status = $this->input->post('status');
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
            'status' => $status
      	));

		$data['status'] = $status;

	 	$list = $this->Pemeriksaan_Model->get_datatables_belum_diperiksa($params);
	 	/*
	 	echo "<pre>";
	 	print_r($list);
	 	echo "</pre>";
	 	exit();
	 	*/
	 	$pemeriksa1 = array();
	 	$pemeriksa2 = array();
	 	$pembayaran = array();

	 	foreach ($list as $key => $value) {
				
				$pemeriksa1[$value->no_pendaftaran] = $this->Pemeriksaan_Model->get_pemeriksa1($value->no_pendaftaran);
				$pemeriksa2[$value->no_pendaftaran] = $this->Pemeriksaan_Model->get_pemeriksa2($value->no_pendaftaran);
				$pembayaran[$value->no_pendaftaran] = $this->Pemeriksaan_Model->get_pembayaran($value->no_pendaftaran);
		}


        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pemeriksaan) {


        	 			  $limit = strtotime(date('H:i:s',mktime(48,0,0)));
                          $limit_jam = date('H', $limit);
                          $limit_menit = date('i', $limit);
                          $datetime1 = new DateTime($pembayaran[$data_pemeriksaan->no_pendaftaran]->tgl_bayar);
                        
                          $now = date("Y-m-d H:i:s");
                          $tgl_sekarang = new DateTime($now);
                          $new_date = date("Y-m-d H:i:s", strtotime(''.$pembayaran[$data_pemeriksaan->no_pendaftaran]->tgl_bayar.', +'.$limit_jam.' hours +'.$limit_menit.' minute'));

                     
							$datetime1->add(new DateInterval('PT48H'));
							$batas_waktu = $datetime1->format('Y-m-d H:i:s');


							$tgl_bayar = new DateTime($pembayaran[$data_pemeriksaan->no_pendaftaran]->tgl_bayar);

							$datetime2 = new DateTime($batas_waktu);

							$difference = $tgl_bayar->diff($datetime2);

							if($tgl_sekarang <= $datetime1)
							{
								$sisa_waktu_penerbitan = "".$difference->d." Days ".$difference->h." Hours ".$difference->i." Minute";
							}
							elseif ($tgl_sekarang > $datetime1) 
							{
								$sisa_waktu_penerbitan = "Lewat dari ".$difference->d." Hari ".$difference->h." Jam ".$difference->i." Menit";
							}

            $no++;
            $row = array();
            //$row[] = $no;
          

			if($id_departemen == 7)
			{
				$button = "";

			}else{


				 $button = "<a href='#popUp' class='btn btn-default btn-small' id='no_pendaftaran' data-toggle='modal' data-id='".$data_pemeriksaan->no_pendaftaran_pelanggan."'>Periksa</a>

						 <a href='#popUpReject' class='btn btn-primary; id='no_pendaftaran' data-toggle='modal' data-id='".$data_pemeriksaan->no_pendaftaran_pelanggan."'>Belum Siap</a>

						<a href='".base_url()."pemeriksaan/cetak_lhpp/".$data_pemeriksaan->no_pendaftaran_pelanggan."' class='btn btn-primary'>Cetak LHPP</a>";

			}
			
            $row[] = $pembayaran[$data_pemeriksaan->no_pendaftaran]->no_kwitansi;
            $row[] = $data_pemeriksaan->no_pendaftaran_pelanggan;
            $row[] = $data_pemeriksaan->Nama;
            $row[] = $data_pemeriksaan->alamat;
            $row[] = $data_pemeriksaan->daya;
            $row[] = $data_pemeriksaan->nm_kota;
            $row[] = $data_pemeriksaan->nm_kanwil;
            $row[] = $data_pemeriksaan->nm_area;
            $row[] = $data_pemeriksaan->nm_sub_area;
            $row[] = $data_pemeriksaan->nm_ptl;
            $row[] = $sisa_waktu_penerbitan;
            $row[] = date('d F Y H:i:s', strtotime($pembayaran[$data_pemeriksaan->no_pendaftaran]->tgl_bayar));
            $row[] = $button;
           
 
            $data[] = $row;
        }
 		

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pemeriksaan_Model->count_all_belum_diperiksa($params),
                        "recordsFiltered" => count($list),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function querypagingsudahdiperiksa()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		$status = $this->input->post('status');

		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
            'status' => $status
      	));

	 	$list = $this->Pemeriksaan_Model->get_datatables_sudah_diperiksa($params);
	 	/*
	 	echo "<pre>";
	 	print_r($list);
	 	echo "</pre>";
	 	exit();
	 	*/
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pemeriksaan) {

        	 			 

            $no++;
            $row = array();
            //$row[] = $no;
          

			if($id_departemen == 7)
			{
				$button = "";

			}
			else
			{

				if($position_id == 1) 
				{
				 $button = "<a class='btn bg-slate btn-icon' href='".base_url()."pemeriksaan/edit/".$data_pemeriksaan->no_pendaftaran."' title='Edit'><i class='fa fa-edit'></i></a>";
				}
				else
				{	
					if($status == 1)
					{
						$button = "<a class='btn bg-slate btn-icon' href='".base_url()."pemeriksaan/edit/".$data_pemeriksaan->no_pendaftaran."' title='Edit'><i class='fa fa-edit'></i></a>";
					}
					else
					{
						$button = "";
					}
				}
			}
			
			
		
            $row[] = $data_pemeriksaan->no_kwitansi;
            $row[] = $data_pemeriksaan->no_pendaftaran_pelanggan;
            $row[] = $data_pemeriksaan->Nama;
            $row[] = $data_pemeriksaan->alamat;
            $row[] = $data_pemeriksaan->daya;
            $row[] = $data_pemeriksaan->nm_kota;
            $row[] = $data_pemeriksaan->nm_kanwil;
            $row[] = $data_pemeriksaan->nm_area;
            $row[] = $data_pemeriksaan->nm_sub_area;
            $row[] = $data_pemeriksaan->no_lhpp;
            $row[] = date('d F Y', strtotime($data_pemeriksaan->tgl_lhpp));
            $row[] = "".$data_pemeriksaan->pemeriksa1."  ".$data_pemeriksaan->pemeriksa2."";
            $row[] = date('d F Y', strtotime($data_pemeriksaan->tgl_bayar));
            $row[] = date('d F Y', strtotime($data_pemeriksaan->created_at));
            $row[] = $data_pemeriksaan->jumlah_pemeriksaan;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 		
 		$count = $this->Pemeriksaan_Model->count_filtered_sudah_diperiksa($params);
 		$count_all = $this->Pemeriksaan_Model->count_all_sudah_diperiksa($params);

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $count_all->jumlah,
                        "recordsFiltered" => $count->jumlah,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function belum_diperiksa()
	{	
		$status = "";
		$status = $this->uri->segment(3);
		$data['status'] = $status;

		$this->load->view('belum_diperiksa',$data);
	}

	function sudah_diperiksa()
	{
		
		$status = "";
		$status = $this->uri->segment(3);
		$data['status'] = $status;

		$this->load->view('sudah_diperiksa',$data);
	}

	function sudah_bayar()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_sudah_bayar();

		$this->load->view('sudah_bayar',$data);
	}

	function uploadFile()
	{	
			header('Content-type: application/json');
			$created_at = Date("YmdHis");
			$path = "./uploads/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("jpg", "jpeg", "png"); //add the formats you want to upload

		    $name = $_FILES['myfile']['name']; //get the name of the file
		    
		    $size = $_FILES['myfile']['size']; //get the size of the file

		  
		    if (strlen($name)) { //check if the file is selected or cancelled after pressing the browse button.
		        list($txt, $ext) = explode(".", $name); //extract the name and extension of the file
		        if (in_array($ext, $valid_formats)) { //if the file is valid go on.
		            if ($size < 2098888) { // check if the file size is more than 2 mb
		                $file_name = $created_at; //get the file name
		                $file = $file_name.'.'.$ext;
		                $tmp = $_FILES['myfile']['tmp_name'];
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.

		                	$arr = array('foto1' => $file,'status' => 'Sukses');
		                	
		                	//$this->checkFile($file,$supplier);
		                    
		                } else {
		                    $arr = array('status' => 'Failed','message'=>'Upload Failed');
		                    
		                }
		            } else {
		                $arr = array('status' => 'Failed','message'=>'Maksimum File Upload 2MB');
		            }
		        } else {
		            $arr = array('status' => 'Failed','message'=>'Format File Salah');
		        }
		    } else {
		        $arr = array('status' => 'Failed','message'=>'Silahkan Upload File Anda');
		    }

		    
		    $upload = json_encode($arr);
		    echo $upload;
		    exit;
		
	}

	function edit()
	{
		
		$no_pendaftaran = $this->uri->segment(3);
			

		// -- batas -- //


		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));


		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);
	
		$data['detail_pelanggan'] = $this->Pemeriksaan_Model->get_detail_pelanggan($no_pendaftaran);
		$data['user_id'] = $user_id;
		$data_spk = $this->Pembayaran_Model->get_no_spk($no_pendaftaran);
		if(!isset($data_spk))
		{

						$this->session->set_flashdata('error', 'Silahkan Buat Surat Tugas Terlebih Dahulu');
						redirect('pemeriksaan/belum_diperiksa','refresh');
						exit();
				
		}
	
		$data['spk'] = $data_spk;
		//$kode_area = $get_detail_staff->kd_area;
		$kode_area = $data['detail_pelanggan']->kd_area;
		$kode_kanwil = $data['detail_pelanggan']->kode_kanwil;
		$nip = $get_detail_staff->nip;


		$params = array_filter(array(
		            'kode_area' => $kode_area,
		            'id_departemen' =>$id_departemen,
		            'user_id' =>$user_id,
		        	'kode_kanwil' => $kode_kanwil,
		        	'nip' => $nip));

		

		$data['pemeriksa'] = $this->Pemeriksaan_Model->get_pemeriksa($params);
		
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$data['kota'] = $this->Pelanggan_Model->get_kota($params);
		$data['tarif'] = $this->Pelanggan_Model->get_tarif();
		$data['daya'] = $this->Pelanggan_Model->get_daya();
		$data['btl'] = $this->Pelanggan_Model->get_btl($params);
		$data['bangunan'] = $this->Pelanggan_Model->get_bangunan();
		$data['ptl'] = $this->Pelanggan_Model->get_ptl($params);
		$data['asosiasi'] = $this->Pelanggan_Model->get_asosiasi();

		$this->load->view('edit',$data);

		


	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$pemeriksa1 = $this->input->post('pemeriksa1');
			$pemeriksa2 = $this->input->post('pemeriksa2');
			$no_lhpp = $this->input->post('no_lhpp');
			$no_surat_tugas = $this->input->post('no_surat_tugas');
			$tgl_lhpp = $this->input->post('tgl_lhpp');
			$gambar_instalasi = $this->input->post('gambar_instalasi');
			$diagram_garis = $this->input->post('diagram_garis');
			$pe_utama = $this->input->post('pe_utama');
			$pe_cabang = $this->input->post('pe_cabang');
			$pe_akhir = $this->input->post('pe_akhir');
			$pe_kotak_kontak = $this->input->post('pe_kotak_kontak');
			$jns_penghantar_utama = $this->input->post('jns_penghantar_utama');
			$jns_penghantar_cabang = $this->input->post('jns_penghantar_cabang');
			$jns_penghantar_akhir = $this->input->post('jns_penghantar_akhir');

			$jns_penghantar_bumi = $this->input->post('jns_penghantar_bumi');
			$luas_penampang = $this->input->post('luas_penampang');
			$sistem_pembumian = $this->input->post('sistem_pembumian');
			$sakelar_utama = $this->input->post('sakelar_utama');
			$sakelar_cabang1 = $this->input->post('sakelar_cabang1');
			$sakelar_cabang2 = $this->input->post('sakelar_cabang2');
			$telp_instalir = $this->input->post('telp_instalir');
			$phbk_utama = $this->input->post('phbk_utama');
			$phbk_cabang1 = $this->input->post('phbk_cabang1');
			$phbk_cabang2 = $this->input->post('phbk_cabang2');
			$penampang_penghantar_utama = $this->input->post('penampang_penghantar_utama');
			$penampang_penghantar_cabang = $this->input->post('penampang_penghantar_cabang');
			$penampang_penghantar_akhir = $this->input->post('penampang_penghantar_akhir');
			$penampang_penghantar_tiga_fasa = $this->input->post('penampang_penghantar_tiga_fasa');
			$polaritas_fitting_lampu = $this->input->post('polaritas_fitting_lampu');
			$polaritas_kotak_kontak = $this->input->post('polaritas_kotak_kontak');
			$polaritas_sakelar = $this->input->post('polaritas_sakelar');
			$tinggi_kotak_kontak = $this->input->post('tinggi_kotak_kontak');
			$tinggi_phbk = $this->input->post('tinggi_phbk');
			$jns_pemasangan_kotak_kontak = $this->input->post('jns_pemasangan_kotak_kontak');


			$perlengkapan_sni = $this->input->post('perlengkapan_sni');
			$pengujian_pembebanan = $this->input->post('pengujian_pembebanan');
			$jml_phb_utama = $this->input->post('jml_phb_utama');
			$jml_phb_satu_phasa = $this->input->post('jml_phb_satu_phasa');
			$jml_phb_tiga_phasa = $this->input->post('jml_phb_tiga_phasa');
			$jml_phb_cabang = $this->input->post('jml_phb_cabang');
			$jml_saluran_cabang = $this->input->post('jml_saluran_cabang');
			$jml_saluran_akhir = $this->input->post('jml_saluran_akhir');
			$jml_titik_lampu = $this->input->post('jml_titik_lampu');
			$jml_sakelar = $this->input->post('jml_sakelar');
			$kkb = $this->input->post('kkb');
			$kkk = $this->input->post('kkk');
			$tahanan_isolasi_penghantar = $this->input->post('tahanan_isolasi_penghantar');
			$resisten_pembumian = $this->input->post('resisten_pembumian');
			$jml_motor_listrik = $this->input->post('jml_motor_listrik');
			$jml_daya_motor_listrik = $this->input->post('jml_daya_motor_listrik');


			$catatan_pemeriksa = $this->input->post('catatan_pemeriksa');
			$foto1_input = $this->input->post('foto1_input');
			$foto2_input = $this->input->post('foto2_input');
			$foto3_input = $this->input->post('foto3_input');
			$foto4_input = $this->input->post('foto4_input');

			$location = $this->input->post('location');
			$lattitude = $this->input->post('lattitude');
			$longitude = $this->input->post('longitude');
			$nomor_pemeriksaan = $this->input->post('no_pemeriksaan');
			$jml_pemeriksaan = $this->input->post('jml_pemeriksaan');
			
			try {
				
				$data_pemeriksaan = array(
					"no_pendaftaran"=> $no_pendaftaran,
					"no_surat_tugas"=> $no_surat_tugas,
					"pemeriksa1"=> $pemeriksa1,
					"pemeriksa2"=> $pemeriksa2,
					"no_lhpp"=> $no_lhpp,
					"tgl_lhpp"=> $tgl_lhpp,
					"gambar_instalasi"=> $gambar_instalasi,
					"diagram_garis_tunggal"=> $diagram_garis,
					"penghantar_proteksi_pe_saluran_utama"=> $pe_utama,
					"penghantar_proteksi_pe_saluran_cabang"=> $pe_cabang,
					"penghantar_proteksi_pe_saluran_akhir"=> $pe_akhir,
					"penghantar_proteksi_pe_kotak_kontak"=> $pe_kotak_kontak,
					"jns_penghantar_utama"=> $jns_penghantar_utama,
					"jns_penghantar_cabang"=> $jns_penghantar_cabang,
					"jns_penghantar_akhir"=> $jns_penghantar_akhir,
					"jns_penghantar_bumi"=> $jns_penghantar_bumi,
					"luas_penampang_penghantar_bumi"=> $luas_penampang,
					"sistem_pembumian"=> $sistem_pembumian,
					"sakelar_utama"=> $sakelar_utama,

					"sakelar_cabang1"=> $sakelar_cabang1,
					"sakelar_cabang2"=> $sakelar_cabang2,
					"phbk_utama"=> $phbk_utama,
					"phbk_cabang1"=> $phbk_cabang1,
					"phbk_cabang2"=> $phbk_cabang2,
					"penampang_penghantar_saluran_utama"=> $penampang_penghantar_utama,
					"penampang_penghantar_saluran_cabang"=> $penampang_penghantar_cabang,
					"penampang_penghantar_saluran_akhir"=> $penampang_penghantar_akhir,
					"penampang_penghantar_saluran_akhir_tiga_fasa"=> $penampang_penghantar_tiga_fasa,
					"polaritas_fitting_lampu"=> $polaritas_fitting_lampu,
					"polaritas_kotak_kontak"=> $polaritas_kotak_kontak,
					"polaritas_sakelar"=> $polaritas_sakelar,
					"tinggi_pemasangan_kotak_kontak"=> $tinggi_kotak_kontak,
					"tinggi_pemasangan_phbk"=> $tinggi_phbk,
					"jns_pemasangan_kotak_kontak"=> $jns_pemasangan_kotak_kontak,
					"perlengkapan_sni"=> $perlengkapan_sni,
					"pengujian_pembebanan"=> $pengujian_pembebanan,
					"jml_phb_utama"=> $jml_phb_utama,
					"jumlah_phb_satu_phasa"=> $jml_phb_satu_phasa,
					"jumlah_phb_tiga_phasa"=> $jml_phb_tiga_phasa,
					"jml_phb_cabang"=> $jml_phb_cabang,
					"jml_saluran_cabang"=> $jml_saluran_cabang,

					"jml_saluran_akhir"=> $jml_saluran_akhir,
					"jml_titik_lampu"=> $jml_titik_lampu,
					"jml_sakelar"=> $jml_sakelar,
					"kkk"=> $kkk,
					"kkb"=> $kkb,
					"tahanan_isolasi_penghantar"=> $tahanan_isolasi_penghantar,
					"resistensi_pembumian"=> $resisten_pembumian,
					"jml_motor_listrik"=> $jml_motor_listrik,
					"jml_daya_motor_listrik"=> $jml_daya_motor_listrik,
					"catatan_pemeriksa"=> $catatan_pemeriksa,
					"foto1"=> $foto1_input,
					"foto2"=> $foto2_input,
					"foto3"=> $foto3_input,
					"foto4"=> $foto4_input,
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
					"jumlah_pemeriksaan"=> $jml_pemeriksaan + 1
				);
				
				
				$data_pelanggan = array(
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
					"location"=>$location,
					"status_verifikasi"=>'2',
				);

				/*
				echo "<pre>";
				print_r($nomor_pemeriksaan);
				echo "</pre>";
				exit();
				*/

				$update_pemeriksaan = $this->Pemeriksaan_Model->update($data_pemeriksaan,$nomor_pemeriksaan);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pemeriksaan');

			}


				if($update_pemeriksaan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pemeriksaan/sudah_diperiksa','refresh');
				}


		}
	}


	function cetak_surat_tugas()
	{
		$no_pendaftaran = $this->input->post('no_pendaftaran');

		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));


		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		//$pemeriksa = array();
		$data_pelanggan = array();
		if(isset($no_pendaftaran))
		{

			foreach ($no_pendaftaran as $nilai) {
				
				$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($nilai);

				//$kode_area = $get_detail_staff->kd_area;
				$kode_area = $data['detail_pelanggan']->kd_area;
				$kode_kanwil = $data['detail_pelanggan']->kode_kanwil;
				$nip = $get_detail_staff->nip;

				$params = array_filter(array(
		            'kode_area' => $kode_area,
		            'id_departemen' =>$id_departemen,
		            'user_id' =>$user_id,
		        	'kode_kanwil' => $kode_kanwil,
		        	'nip' => $nip));

				
				//$pemeriksa= $this->Pemeriksaan_Model->get_pemeriksa($params);

				//$pemeriksa[] = array('id_pemeriksa' => $pemeriksa1->id_pemeriksa,
				//				    'nm_pemeriksa' => $pemeriksa1->nm_pemeriksa);

				$data_pelanggan[] = $this->Pembayaran_Model->get_cetak_pelanggan($nilai);

			}
		}

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$pemeriksa= $this->Pemeriksaan_Model->get_pemeriksa($params);

		$data['pemeriksa'] = $pemeriksa;
	
		//var_dump($data['pemeriksa']);
		//exit();
		$data['pelanggan'] = $data_pelanggan;
		$this->load->view('cetak_surat_tugas',$data);

	}

	function proses_surat_tugas()
	{
		$no_pendaftaran = $this->input->post('no_pendaftaran');
		$pemeriksa1 = $this->input->post('pemeriksa1');
		$pemeriksa2 = $this->input->post('pemeriksa2');


		$detail_pemeriksa1 = $this->Pemeriksaan_Model->get_detail_pemeriksa($pemeriksa1);
		$detail_pemeriksa2 = $this->Pemeriksaan_Model->get_detail_pemeriksa($pemeriksa2);



		if(isset($detail_pemeriksaan2->Name)) 
			{ 
				$nm_pemeriksa2 = $detail_pemeriksa2->Name; 
			}
		else
		{
			$nm_pemeriksa2 = "";
		}
		$data_pelanggan = array();


			$date = date('Y-m-d'); 
			$tgl_cetak = tgl_indo($date);
		
			$user_id = $this->session->userdata('user_id');
			$position_id = $this->session->userdata('position_id');
			$id_departemen = $this->session->userdata('id_departemen');
			
			$params_staff = array_filter(array(
	            'position_id' => $position_id,
	            'id_departemen' => $id_departemen,
	      	));

			$detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);
				
				if(isset($detail_staff->nm_area)) 
				{	
					 $nm_area =  $detail_staff->nm_area;
				}
				else if($detail_staff->nip == '111')
				{
					$nm_area = 'Jakarta';
				}
				else if($detail_staff->nip == '222')
				{
					$nm_area = 'Jakarta';
				}
				else
				{
					$nm_area = "";
				}

		$get_max_no_spk = $this->Pemeriksaan_Model->get_max_no_spk();

		if($get_max_no_spk->id == NULL)
		{
			$no_urut = 1;
		}
		else
		{
			$no_urut = 1 + $get_max_no_spk->id;
		}

		$current_month = date("m");
		$current_year = date("y");


			if(!isset($no_pendaftaran))
			{
				$this->session->set_flashdata('error', 'Tidak ada no pendaftaran');
				redirect('pemeriksaan/belum_diperiksa','refresh');
				exit();
			}

		foreach ($no_pendaftaran as $nilai) {

			$pelanggan = $this->Pembayaran_Model->get_cetak_pelanggan($nilai);
			$data_pelanggan[] = $this->Pembayaran_Model->get_cetak_pelanggan($nilai);
			
		}

		$no_spk = "".$no_urut."/".$current_month.$current_year."/".$pelanggan->kd_area."";

		foreach ($no_pendaftaran as $nilai) {

			$get_no_spk = $this->Pemeriksaan_Model->get_no_spk($nilai);
			if(isset($get_no_spk))
			{
				$this->session->set_flashdata('error', 'No Pendaftaran '.$nilai.' Sudah Memiliki Surat Tugas');
				redirect('pembayaran/sudah_bayar','refresh');
				exit();
			}

			$data_no_spk = array(
					"no_spk"=> $no_spk,
					"pemeriksa1"=> $pemeriksa1,
					"pemeriksa2"=> $pemeriksa2,
					"no_pendaftaran"=> $nilai,
				);

			$insert_pemeriksaan = $this->Pemeriksaan_Model->insert_spk($data_no_spk);

		}
		
		
		header('Content-Type: application/pdf');
		$html = "
				<div style='width: 900px; height: 650px; padding: 15px; border: 1px solid #000;'>
				<table border='0' width='100%' cellspacing='0' cellpadding='0'>
				<tbody>
				<tr>
				<td align='center' width='103'><img src='".base_url()."template/assets/images/jki_logo_small.png' /></td>
				<td align='center'>
				<p class='p0 ft0' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
				<p class='p1 ft1' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Komplek Golden Plaza Fatmawati, Blok G11, Lantai 2</p>
				<p class='p1 ft1' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Jl. R.S Fatmawati Nomor 15, Jakarta 12420</p>
				<p class='p2 ft2' style='text-align: center; margin-bottom: 1px; margin-top: 1px;'>Telp/Fax: 021 27650120 Email: jkijakarta1@gmail.com</p>
				</td>
				<td width='90'>&nbsp;</td>
				</tr>
				<tr>
				<td colspan='3'><hr style='border-top: 1px solid #000;' /></td>
				</tr>
				<tr>
				<td colspan='3' align='center'><span style='text-decoration: underline;'><strong>SURAT TUGAS</strong></span></td>
				<td>&nbsp;</td>
				</tr>
				<tr>
				<td colspan='3' align='center'>No. ".$no_spk."</td>
				</tr>
				<tr>
				<td colspan='3'>&nbsp;</td>
				</tr>
				<tr>
				<td colspan='3'>Surat Tugas diberikan kepada :</td>
				</tr>
				<tr>
				<td colspan='3'>
				<ol>
				<li>".$detail_pemeriksa1->Name."</li>
				
				</ol>
				<p>&nbsp;</p>
				<p>Sebagai petugas pemeriksa PT.JKI, untuk melakukan pemeriksaan instalasi listrik pada bangunan :</p>
				<p>&nbsp;</p>
				</td>
				</tr>
				<tr>
				<td colspan='3'>
				<table border='1' width='100%' cellspacing='0' cellpadding='4'>
				<tbody>
				<tr>
				<th>No</th>
				<th>No Pendaftaran</th>
				<th>No Agenda</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>Tarif/Daya</th>
				<th>Instalatir</th>
				<th>No Telepon</th>
				</tr>";

				$no =1; 
				foreach($data_pelanggan as $row) { 
			$html .="
				<tr>
				<td>".$no."</td>
				<td>".$row->no_pendaftaran."</td>
				<td>".$row->no_agenda."</td>
				<td>".$row->Nama."</td>
				<td>".$row->alamat."</td>
				<td>".$row->nm_tarif."/".$row->daya."</td>
				<td>".$row->nama_instalir."</td>
				<td>".$row->telp."</td>
				
				</tr>";
				$no++; }

			$html .="

				</tbody>
				</table>
				<br /><br />
				<table border='0' width='250' cellspacing='0' cellpadding='3' align='right'>
				<tbody>
				<tr>
				<td align='center'>".$nm_area.",".tgl_indo(date('Y-m-d'))."</td>
				</tr>
				<tr>
				<td align='center'></td>
				</tr>
				<tr>
				<td align='center' height='80'>&nbsp;</td>
				</tr>
				<tr>
				<td align='center'>
				(".$detail_staff->general_manager.")<br>
				(Koordinator Pemeriksa)
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr>
				</tbody>
				</table>
				</div>";

				
				$mpdf = new \Mpdf\Mpdf();

		       // Write some HTML code:

		       $mpdf->WriteHTML($html);

		       // Output a PDF file directly to the browser
		       
		        
						/*
						if($mpdf->WriteHTML($html))
				       	{
				       		$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);
				       	}
						*/

				       // Output a PDF file directly to the browser
				       $mpdf->Output(''.$no_spk.'.pdf', 'I');

		       

		       exit();

	}

	function add()
	{	

		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));


		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);
		
		if(isset($_GET['no_pendaftaran']))
		{
			$no_pendaftaran = $_GET['no_pendaftaran'];
			//$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);

		}else
		{
			$no_pendaftaran = $this->input->post('no_pendaftaran');
		}

		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		$data['user_id'] = $user_id;
		$data_spk = $this->Pembayaran_Model->get_no_spk($no_pendaftaran);
		if(!isset($data_spk))
		{

						$this->session->set_flashdata('error', 'Silahkan Buat Surat Tugas Terlebih Dahulu');
						redirect('pemeriksaan/belum_diperiksa','refresh');
						exit();
				
		}
	
		$data['spk'] = $data_spk;
		//$kode_area = $get_detail_staff->kd_area;
		$kode_area = $data['detail_pelanggan']->kd_area;
		$kode_kanwil = $data['detail_pelanggan']->kode_kanwil;
		$nip = $get_detail_staff->nip;


		$params = array_filter(array(
		            'kode_area' => $kode_area,
		            'id_departemen' =>$id_departemen,
		            'user_id' =>$user_id,
		        	'kode_kanwil' => $kode_kanwil,
		        	'nip' => $nip));

		$current_month = date("m");
		$current_year = date("y");

			
			$get_max_no_lhpp = $this->Pemeriksaan_Model->get_max_no_lhpp($params);
			//var_dump($get_max_no_lhpp);
			//exit();
			/*
			$good_total = $get_max_no_lhpp->ExtractString;
			
		
			if($good_total=='NULL') { // bila data kosong
                $good_total ="0001";
            }else {
                $MaksID = $good_total;
                $MaksID++;
              
                if($MaksID < 1) $good_total = "0000".$MaksID; // nilai kurang dari 1000
                else if($MaksID < 10) $good_total = "000".$MaksID; // nilai kurang dari 10000
                else if($MaksID < 100) $good_total = "00".$MaksID; // nilai kurang dari 100000
                else if($MaksID < 1000) $good_total = "0".$MaksID; // nilai kurang dari 10000
                else $good_total = $MaksID; // lebih dari 10000
            }
			*/
			//var_dump($good_total);
        $good_total = substr($no_pendaftaran, -4);

		$no_lhpp = "LHI-".$current_month.$current_year."-".$data['detail_pelanggan']->kd_kota."-".$good_total."-".$data['detail_pelanggan']->id_provinsi."-".$data['detail_pelanggan']->kd_area."";

		$data['no_lhpp'] = $no_lhpp;
		$data['pemeriksa'] = $this->Pemeriksaan_Model->get_pemeriksa($params);
		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$data['kota'] = $this->Pelanggan_Model->get_kota($params);
		$data['tarif'] = $this->Pelanggan_Model->get_tarif();
		$data['daya'] = $this->Pelanggan_Model->get_daya();
		$data['btl'] = $this->Pelanggan_Model->get_btl($params);
		$data['bangunan'] = $this->Pelanggan_Model->get_bangunan();
		$data['ptl'] = $this->Pelanggan_Model->get_ptl($params);
		$data['asosiasi'] = $this->Pelanggan_Model->get_asosiasi();

		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$pemeriksa1 = $this->input->post('pemeriksa1');
			$pemeriksa2 = $this->input->post('pemeriksa2');
			$verifikator = $this->input->post('verifikator');
			$no_lhpp = $this->input->post('no_lhpp');
			$no_surat_tugas = $this->input->post('no_surat_tugas');
			$tgl_lhpp = $this->input->post('tgl_lhpp');
			$gambar_instalasi = $this->input->post('gambar_instalasi');
			$diagram_garis = $this->input->post('diagram_garis');
			$pe_utama = $this->input->post('pe_utama');
			$pe_cabang = $this->input->post('pe_cabang');
			$pe_akhir = $this->input->post('pe_akhir');
			$pe_kotak_kontak = $this->input->post('pe_kotak_kontak');
			$jns_penghantar_utama = $this->input->post('jns_penghantar_utama');
			$jns_penghantar_cabang = $this->input->post('jns_penghantar_cabang');
			$jns_penghantar_akhir = $this->input->post('jns_penghantar_akhir');

			$jns_penghantar_bumi = $this->input->post('jns_penghantar_bumi');
			$luas_penampang = $this->input->post('luas_penampang');
			$sistem_pembumian = $this->input->post('sistem_pembumian');
			$sakelar_utama = $this->input->post('sakelar_utama');
			$sakelar_cabang1 = $this->input->post('sakelar_cabang1');
			$sakelar_cabang2 = $this->input->post('sakelar_cabang2');
			$telp_instalir = $this->input->post('telp_instalir');
			$phbk_utama = $this->input->post('phbk_utama');
			$phbk_cabang1 = $this->input->post('phbk_cabang1');
			$phbk_cabang2 = $this->input->post('phbk_cabang2');
			$penampang_penghantar_utama = $this->input->post('penampang_penghantar_utama');
			$penampang_penghantar_cabang = $this->input->post('penampang_penghantar_cabang');
			$penampang_penghantar_akhir = $this->input->post('penampang_penghantar_akhir');
			$penampang_penghantar_tiga_fasa = $this->input->post('penampang_penghantar_tiga_fasa');
			$polaritas_fitting_lampu = $this->input->post('polaritas_fitting_lampu');
			$polaritas_kotak_kontak = $this->input->post('polaritas_kotak_kontak');
			$polaritas_sakelar = $this->input->post('polaritas_sakelar');
			$tinggi_kotak_kontak = $this->input->post('tinggi_kotak_kontak');
			$tinggi_phbk = $this->input->post('tinggi_phbk');
			$jns_pemasangan_kotak_kontak = $this->input->post('jns_pemasangan_kotak_kontak');


			$perlengkapan_sni = $this->input->post('perlengkapan_sni');
			$pengujian_pembebanan = $this->input->post('pengujian_pembebanan');
			$jml_phb_utama = $this->input->post('jml_phb_utama');
			$jml_phb_satu_phasa = $this->input->post('jml_phb_satu_phasa');
			$jml_phb_tiga_phasa = $this->input->post('jml_phb_tiga_phasa');
			$jml_phb_cabang = $this->input->post('jml_phb_cabang');
			$jml_saluran_cabang = $this->input->post('jml_saluran_cabang');
			$jml_saluran_akhir = $this->input->post('jml_saluran_akhir');
			$jml_titik_lampu = $this->input->post('jml_titik_lampu');
			$jml_sakelar = $this->input->post('jml_sakelar');
			$kkb = $this->input->post('kkb');
			$kkk = $this->input->post('kkk');
			$tahanan_isolasi_penghantar = $this->input->post('tahanan_isolasi_penghantar');
			$resisten_pembumian = $this->input->post('resisten_pembumian');
			$jml_motor_listrik = $this->input->post('jml_motor_listrik');
			$jml_daya_motor_listrik = $this->input->post('jml_daya_motor_listrik');


			$catatan_pemeriksa = $this->input->post('catatan_pemeriksa');
			$foto1_input = $this->input->post('foto1_input');
			$foto2_input = $this->input->post('foto2_input');
			$foto3_input = $this->input->post('foto3_input');
			$foto4_input = $this->input->post('foto4_input');
			$foto5_input = $this->input->post('foto5_input');

			$lattitude = $this->input->post('lattitude');
			$longitude = $this->input->post('longitude');
			$nomor_pemeriksaan = date('YmdHis');
			$created_at = date('Y:m:d H:i:s');
			
			try {
				
				$data_pemeriksaan = array(
					"no_pemeriksaan"=> $nomor_pemeriksaan,
					"no_pendaftaran"=> $no_pendaftaran,
					"no_surat_tugas"=> $no_surat_tugas,
					"pemeriksa1"=> $pemeriksa1,
					"pemeriksa2"=> $pemeriksa2,
					//"verifikator"=> $verifikator,
					"no_lhpp"=> $no_lhpp,
					"tgl_lhpp"=> $tgl_lhpp,
					"gambar_instalasi"=> $gambar_instalasi,
					"diagram_garis_tunggal"=> $diagram_garis,
					"penghantar_proteksi_pe_saluran_utama"=> $pe_utama,
					"penghantar_proteksi_pe_saluran_cabang"=> $pe_cabang,
					"penghantar_proteksi_pe_saluran_akhir"=> $pe_akhir,
					"penghantar_proteksi_pe_kotak_kontak"=> $pe_kotak_kontak,
					"jns_penghantar_utama"=> $jns_penghantar_utama,
					"jns_penghantar_cabang"=> $jns_penghantar_cabang,
					"jns_penghantar_akhir"=> $jns_penghantar_akhir,
					"jns_penghantar_bumi"=> $jns_penghantar_bumi,
					"luas_penampang_penghantar_bumi"=> $luas_penampang,
					"sistem_pembumian"=> $sistem_pembumian,
					"sakelar_utama"=> $sakelar_utama,

					"sakelar_cabang1"=> $sakelar_cabang1,
					"sakelar_cabang2"=> $sakelar_cabang2,
					"phbk_utama"=> $phbk_utama,
					"phbk_cabang1"=> $phbk_cabang1,
					"phbk_cabang2"=> $phbk_cabang2,
					"penampang_penghantar_saluran_utama"=> $penampang_penghantar_utama,
					"penampang_penghantar_saluran_cabang"=> $penampang_penghantar_cabang,
					"penampang_penghantar_saluran_akhir"=> $penampang_penghantar_akhir,
					"penampang_penghantar_saluran_akhir_tiga_fasa"=> $penampang_penghantar_tiga_fasa,
					"polaritas_fitting_lampu"=> $polaritas_fitting_lampu,
					"polaritas_kotak_kontak"=> $polaritas_kotak_kontak,
					"polaritas_sakelar"=> $polaritas_sakelar,
					"tinggi_pemasangan_kotak_kontak"=> $tinggi_kotak_kontak,
					"tinggi_pemasangan_phbk"=> $tinggi_phbk,
					"jns_pemasangan_kotak_kontak"=> $jns_pemasangan_kotak_kontak,
					"perlengkapan_sni"=> $perlengkapan_sni,
					"pengujian_pembebanan"=> $pengujian_pembebanan,
					"jml_phb_utama"=> $jml_phb_utama,
					"jumlah_phb_satu_phasa"=> $jml_phb_satu_phasa,
					"jumlah_phb_tiga_phasa"=> $jml_phb_tiga_phasa,
					"jml_phb_cabang"=> $jml_phb_cabang,
					"jml_saluran_cabang"=> $jml_saluran_cabang,

					"jml_saluran_akhir"=> $jml_saluran_akhir,
					"jml_titik_lampu"=> $jml_titik_lampu,
					"jml_sakelar"=> $jml_sakelar,
					"kkk"=> $kkk,
					"kkb"=> $kkb,
					"tahanan_isolasi_penghantar"=> $tahanan_isolasi_penghantar,
					"resistensi_pembumian"=> $resisten_pembumian,
					"jml_motor_listrik"=> $jml_motor_listrik,
					"jml_daya_motor_listrik"=> $jml_daya_motor_listrik,
					"catatan_pemeriksa"=> $catatan_pemeriksa,
					"foto1"=> $foto1_input,
					"foto2"=> $foto2_input,
					"foto3"=> $foto3_input,
					"foto4"=> $foto4_input,
					"foto5"=> $foto5_input,
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
					"created_at"=> $created_at
				);
				
				$data_pelanggan = array(
					"status_pemeriksaan"=> 1,
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
				);
				

				/*
				echo "<pre>";
				print_r($data_pemeriksaan);
				echo "</pre>";
				exit();
				*/

				$insert_pemeriksaan = $this->Pemeriksaan_Model->insert($data_pemeriksaan);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pemeriksaan');

			}


				if($insert_pemeriksaan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pemeriksaan/sudah_diperiksa','refresh');
				}


		}
	
	}

	

}
