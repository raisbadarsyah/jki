<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$principal_id=$this->session->userdata('principal_id');
		//$config['principal_name'] = $this->menu_model->get_nama_principal($principal_id);
		$this->load->view('help',$config);
	}

	

}
