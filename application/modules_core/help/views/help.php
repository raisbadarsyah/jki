<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Support</span> - FAQ's</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="support_faq.html">Support</a></li>
							<li class="active">FAQ's</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Info blocks -->
					<!--
					<div class="row">
						<div class="col-md-4">
							<div class="panel">
								<div class="panel-body text-center">
									<div class="icon-object border-success-400 text-success"><i class="icon-book"></i></div>
									<h5 class="text-semibold">Knowledge Base</h5>
									<p class="mb-15">Ouch found swore much dear conductively hid submissively hatchet vexed far inanimately alongside candidly much and jeez</p>
									<a href="#" class="btn bg-success-400">Browse articles</a>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="panel">
								<div class="panel-body text-center">
									<div class="icon-object border-warning-400 text-warning"><i class="icon-lifebuoy"></i></div>
									<h5 class="text-semibold">Support center</h5>
									<p class="mb-15">Dear spryly growled much far jeepers vigilantly less and far hideous and some mannishly less jeepers less and and crud</p>
									<a href="#" class="btn bg-warning-400">Open a ticket</a>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<div class="panel">
								<div class="panel-body text-center">
									<div class="icon-object border-blue text-blue"><i class="icon-reading"></i></div>
									<h5 class="text-semibold">Articles and news</h5>
									<p class="mb-15">Diabolically somberly astride crass one endearingly blatant depending peculiar antelope piquantly popularly adept much</p>
									<a href="#" class="btn bg-blue">Browse articles</a>
								</div>
							</div>
						</div>
					</div>
				-->
					<!-- /info blocks -->

					<!-- /questions area -->


					<!-- Latest articles -->
				
					<!-- /latest articles -->


					<!-- Featured articles -->
					<h4 class="text-center content-group">
						FAQ dan TUTORIAL
						
					</h4>

					<div class="row">
						
						<div class="col-lg-3 col-sm-6">
							<div class="thumbnail">
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/6PXwtSeolI0" frameborder="0" allowfullscreen></iframe>
					            </div>

								<div class="caption">
									<h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Login</a></h6>
									Video ini menjelaskan tentang bagaimana proses login ke dalam sistem
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
							<div class="thumbnail">
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/6PXwtSeolI0" frameborder="0" allowfullscreen></iframe>
					            </div>

								<div class="caption">
									<h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Melihat DPL</a></h6>
									Video ini menjelaskan tentang bagaimana proses melihat dpl ke dalam sistem
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
							<div class="thumbnail">
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/6PXwtSeolI0" frameborder="0" allowfullscreen></iframe>
					            </div>

								<div class="caption">
									<h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Mencari DPL</a></h6>
									Video ini menjelaskan tentang bagaimana proses pencarian dpl ke dalam sistem
								</div>
							</div>
						</div>

						<div class="col-lg-3 col-sm-6">
							<div class="thumbnail">
								<div class="video-container">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/VLQtZzgp-pQ" frameborder="0" allowfullscreen></iframe>
					            </div>

								<div class="caption">
									<h6 class="text-semibold no-margin-top"><a href="#" class="text-default">Mengganti Username dan Password</a></h6>
									Video ini menjelaskan tentang bagaimana proses edit username password ke dalam sistem
								</div>
							</div>
						</div>
					<!-- /featured articles -->


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; 2017. <a href="#">WEB DISKON MODIFIER</a> by <a href="mpi-indonesia.co.id" target="_blank">MPI</a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
