<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); //error_reporting(0);

class Inventaris_email extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','inventaris_email_model'));
		$this->load->helper(array('check_auth_menu'));
		$this->load->library('upload');
		//check_authority_url();		
	}

	function index()
	{
		$config['title'] = 'Daftar Email Aktif';
		$config['page_title'] = 'Daftar Email Aktif';
		$config['page_subtitle'] = 'Email List';

        $config['cabang']    = $this->inventaris_email_model->get_branch();
        $config['principal'] = $this->inventaris_email_model->get_principal();

		$this->load->view('v_list', $config);
	}
	
	function ajax_list($set_cabang, $set_principal){
		$report = $this->inventaris_email_model->get_all($set_cabang, $set_principal);
		//echo $this->db->last_query(); die();
		if(!empty($report)){
			$no = 1;
			foreach($report as $rows) {
				$action ="<a href='".base_url()."inventaris_email/edit/$rows->refEmailId' class='btn btn-primary' title='Edit'><i class='glyphicon glyphicon-pencil'></i></a>
				          <a href='".base_url()."inventaris_email/delete/$rows->refEmailId' class='btn btn-danger'  title='Delete'><i class='glyphicon glyphicon-remove'></i></a>";
						  
				$data[] = array(
					'no'                => $no,
					'refEmail'		    => $rows->refEmail, 
					'branchName'	    => $rows->branchName, 
					'supplierPrincipal'	=> $rows->supplierPrincipal,
					'action'	        => $action
				); 
				$no++;
			}
				
			$results = array(
				"sEcho" => 1,
				"iTotalRecords" => count($data),
				"iTotalDisplayRecords" => count($data),
				"aaData"=>$data
			);
		} 
		else {
			$results = array(
				"sEcho" => 1,
				"iTotalRecords" => count($data),
				"iTotalDisplayRecords" => count($data),
				"aaData"=>''
			);				
		}
		  
		echo json_encode($results); 		
	}
	
	function add(){
		$config['title'] = 'Daftar Email Aktif';
		$config['page_title'] = 'Daftar Email Aktif';
		$config['page_subtitle'] = 'Email List';

        $config['cabang']    = $this->inventaris_email_model->get_branch();
        $config['principal'] = $this->inventaris_email_model->get_principal();

		$this->load->view('v_add', $config);
	}

	function insert(){
		$cabang         = $this->input->post('cabang');
		$principal_set  = $this->input->post('principal_set');
		$email = $this->input->post('email');
		
		$data = array(
			"refEmail" 		 => $email,
			"refEmailStatus" => 1,
			"refPrincipalId" => $principal_set,
			"refBranchId"    => $cabang
		);

		$action = $this->inventaris_email_model->insert($data);
			
		
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Email has been added successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to add user');
		}

		redirect('inventaris_email','refresh');
	}
	
	function edit($id){
		$config['title'] = 'Daftar Email Aktif';
		$config['page_title'] = 'Daftar Email Aktif';
		$config['page_subtitle'] = 'Email List';

        $config['cabang']    = $this->inventaris_email_model->get_branch();
        $config['principal'] = $this->inventaris_email_model->get_principal();
		
	    $id = $this->uri->segment(3);
        $config['email_list'] = $this->inventaris_email_model->get_email_by_id($id);
       
		$this->load->view('v_edit', $config);
	}
	
	function update(){	
		$refEmailId     = $this->input->post('refEmailId');
		$cabang         = $this->input->post('cabang');
		$principal_set  = $this->input->post('principal_set');
		$email          = $this->input->post('email');
		
		$data = array(
			"refEmail" 		 => $email,
			"refEmailStatus" => 1,
			"refPrincipalId" => $principal_set,
			"refBranchId"    => $cabang
		);
		
		$action = $this->inventaris_email_model->update_email($data, $refEmailId);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Email has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update email');
		}	
		redirect(base_url() . 'inventaris_email', 'refresh');
	}
	
	function delete($id){
		$id     = $this->uri->segment(3);
		$action = $this->inventaris_email_model->delete_email($id);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Email has been delete successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to delete Email');
		}	
		redirect(base_url() . 'inventaris_email', 'refresh');
	}
	
}