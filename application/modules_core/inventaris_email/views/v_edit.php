<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
							     <a href="<?php echo base_url(); ?>inventaris_email"><button class="btn btn-warning"><i class="glyphicon glyphicon-arrow-left"></i> Back</button></a><hr />
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'inventaris_email/update'; ?>" enctype="multipart/form-data">
									  <?php foreach($email_list as $row): ?>
									  <input name="refEmailId" type="hidden" value="<?php echo $row->refEmailId; ?>">
									 								  
									 <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Cabang</label>
										<div class="col-sm-8">
										<select id="cabang" class="form-control" name="cabang" data-filter="true" style="width: 340px;">
										  <?php foreach($cabang as $rows): ?>
											 <option value="<?php echo $rows->branchId; ?>" <?php if($row->refBranchId == $rows->branchId){ echo"selected='selected'"; } ?>><?php echo $rows->branchName; ?></option>
										  <?php endforeach; ?>
										</select>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Principal</label>
										<div class="col-sm-8">
										    <select id="principal_set" class="form-control" name="principal_set" data-filter="true" style="width: 340px;">
											  <div data-value="0">Principal</div>
											  <?php foreach($principal as $rows): ?>
												 <option value="<?php echo $rows->suplierId; ?>" <?php if($row->refPrincipalId == $rows->suplierId){ echo"selected='selected'"; } ?>><?php echo $rows->supplierPrincipal; ?></option>
											  <?php endforeach; ?>
											</select>
										</div>
									  </div>
									 
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-8">
										  <input type="email" class="form-control" id="refEmail" name="email" value="<?php echo $row->refEmail; ?>" required>
										</div>
									  </div>
									 
									 <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									  </div>
									<?php endforeach; ?>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>