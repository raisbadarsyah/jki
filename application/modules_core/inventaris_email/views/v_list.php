<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><h4><i class="glyphicon glyphicon-th-list"></i> <?php echo $page_title;?></h4></div>	
					<?php if($this->session->flashdata('message') != ""){ ?> 
						<br /><br />				
						<div class="alert alert-info" role="alert">
						  <i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?>
						</div>
					<?php } ?>
				</div>
				
				<div class="panel-body">	
  				<a href="<?php echo base_url(); ?>inventaris_email/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
				<hr />
				<div>
					<table  width="100%">
						<tr>
							<td width="60%">
							    <div class="col-md-3">							
									<p>
										<div id="cabang" class="bfh-selectbox" data-name="cabang" data-value="0" data-filter="true" style="width: 140px;">
										  <div data-value="0">Cabang</div>
										  <?php foreach($cabang as $row): ?>
											 <div data-value="<?php echo $row->branchId; ?>"><?php echo $row->branchName; ?></div>
										  <?php endforeach; ?>
										</div>
									</p>
								</div>
								
								<div class="col-md-3">
									<p>
									    <div id="principal_set" class="bfh-selectbox" data-name="principal_set" data-value="0" data-filter="true" style="width: 140px;">
										  <div data-value="0">Principal</div>
										  <?php foreach($principal as $row): ?>
											 <div data-value="<?php echo $row->suplierId; ?>"><?php echo $row->supplierPrincipal; ?></div>
										  <?php endforeach; ?>
										</div>
									</p>
								</div>								
							</td>
							<td style="text-align:left;">
								<p>
									<button class="btn btn-primary" onclick="email_view()">Load</button>
								</p>
							</td>
						</tr>
					</table> 	
				</div>
				<hr />				
			
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="table responsive table-striped table-bordered" id="email_list">
				  <thead>
					<tr>
						<th width="5%">No</th>						
						<th>Email</th>
						<th>Cabang</th>
						<th>Principal</th>
						<th width="10%">Action</th>
				    </tr>
				  </thead>
				  <tbody>
					<tr>
						<th colspan="5">No Data Available</th>									
					</tr>
				  </tbody>
				</table>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>

<script type="text/javascript">
    function email_view(){
	      var cabangId = $("#cabang").val();
		  var principalId = $("#principal_set").val();
		
	     $('#email_list').dataTable({  
                 "bProcessing": true,
                 "sAjaxSource": "<?php echo base_url() . 'inventaris_email/ajax_list/'; ?>" + cabangId +"/"+ principalId,
                 "bserverSide": true,
                 "bDestroy": true,
                 "aoColumns": [
                    { mData: 'no' },						
				    { mData: 'refEmail' },
					{ mData: 'branchName' },
					{ mData: 'supplierPrincipal' },
					{ mData: 'action' }
                ]				
        });
	}
</script>