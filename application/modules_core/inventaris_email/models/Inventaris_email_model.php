<?php

class Inventaris_email_model extends CI_Model {

    function get_all($cabang, $principal) {
        $this->db->select('*');
        $this->db->from('ref_email');
		$this->db->join('m_branch','ref_email.refBranchId=m_branch.branchId','left');
		$this->db->join('m_supplier','ref_email.refPrincipalId=m_supplier.suplierId','left');
		if($cabang !=0){
			$this->db->where('ref_email.refBranchId', $cabang);
		}
		
		if($principal !=0){
			$this->db->where('ref_email.refPrincipalId', $principal);
		}
		$this->db->order_by('ref_email.refBranchId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_principal() {
        $this->db->select('*');
        $this->db->from('m_supplier');
		$this->db->order_by('supplierPrincipal','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_branch() {
        $this->db->select('*');
        $this->db->from('m_branch');
		$this->db->order_by('branchId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_email_by_id($id) {
        $this->db->select('*');
        $this->db->from('ref_email');
		$this->db->where('refEmailId', $id);
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert($data){
		$this->db->trans_begin();
        $this->db->insert('ref_email', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update_email($data, $refEmailId){
        $this->db->trans_begin();
		$this->db->where('refEmailId', $refEmailId);
		$this->db->update('ref_email', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function delete_email($id) {
        $query = $this->db->delete('ref_email', array('refEmailId' => $id));

		 if ($query) { 
			return 1;
	    } 
	    else { 
	    	return 0; 
	    } 
	}
	
}

