<?php

class Management_module_model extends CI_Model {

    function get_all() {
        $this->db->select('*');
        $this->db->from('core_menu');
		$this->db->order_by('coreParentId','asc');
		$this->db->order_by('coreMenuName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_module_by_id($id) {
        $this->db->select('*');
        $this->db->from('core_menu');
		$this->db->where('coreMenuId', $id);
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert_module($data){
		$this->db->trans_begin();
        $this->db->insert('core_menu', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update_module($data, $menuId){
        $this->db->trans_begin();
		$this->db->where('coreMenuId', $menuId);
		$this->db->update('core_menu', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function delete_module($id) {
        $query = $this->db->delete('core_menu', array('coreMenuId' => $id));

		 if ($query) { 
			echo 1; 
	    } 
	    else { 
	    	echo 0; 
	    } 
	}
	
	function check_menuUrl($url){
		$this->db->select('coreMenuUrl');
        $this->db->from('core_menu');
		$this->db->where('coreMenuUrl', $url);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
        	//echo 1;
           // echo $url;
        }
        else {
            echo 0;
        }	
	}

	function get_parent_menu() {
        $this->db->select('*');
        $this->db->from('core_menu');
        //$this->db->where('coreParentId',0);
		$this->db->order_by('coreParentId','asc');
		$this->db->order_by('coreMenuName','asc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_parent_menu_name($id_parent){
		$this->db->select('coreMenuName');
		$this->db->where('coreMenuId',$id_parent);
		$query = $this->db->get('core_menu');

		foreach ($query->result() as $row) {
			return $row->coreMenuName;
		}
	}
}

