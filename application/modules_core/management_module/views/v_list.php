
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MPI - Discount Modifier</title>

    <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
  <!-- /theme JS files -->

  <!-- /main navbar -->

  <!-- Memanggil file .js untuk proses autocomplete -->
  
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>

    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/management_fdk/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#outletSiteNumber').val(''+suggestion.outletSiteNumber); // membuat id 'v_nim' untuk ditampilkan
                     $('#outletAlamat').val(''+suggestion.outletAlamat); // membuat id 'v_nim' untuk ditampilkan
                      $('#outletPostalCode').val(''+suggestion.outletPostalCode); // membuat id 'v_nim' untuk ditampilkan
                                    
                }
            });
        });
    </script>
       <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('.autocompleteItem0').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'/management_fdk/searchItem',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#itemCode0').val(''+suggestion.itemCode); // membuat id 'v_nim' untuk ditampilkan
                  
                }
            });
        });
    </script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/dashboard.js"></script>
  <!-- /theme JS files -->

  

</head>

<body>

  <!-- Main navbar -->
  <?php
  $this->load->view('template/main_navbar');
  ?>
  <!-- /main navbar -->


  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main sidebar -->
      <?php $this->load->view('template/sidebar'); ?>
      <!-- /main sidebar -->


      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
          

          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><i class="icon-home2 position-left"></i>Admin</li>
              <li>Module Management</li>
              
            </ul>

            <ul class="breadcrumb-elements">
              <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear position-left"></i>
                  Settings
                  <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                  <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                  <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                  <li class="divider"></li>
                  <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

          <!-- Vertical form options -->
      
          <!-- /vertical form options -->


          <!-- Centered forms -->
        
          <!-- /form centered -->


          <!-- Fieldset legend -->
          
          <!-- /fieldset legend -->


          <!-- 2 columns form -->
          <form action="<?php echo base_url().'Management_user/insert_user'; ?>" enctype="multipart/form-data" method="post">
            <div class="panel panel-flat">
              <div class="panel-heading">
                <h5 class="panel-title">Module Management</h5>
                <div class="heading-elements">
                  <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                          </ul>
                        </div>
              </div>
              <?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

              <div class="panel-body">
               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popUp">
                Add New
              </button>
                <div class="row">
                  <table  class="table datatable-tools-basic">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Menu Name</th>
                    <th>Menu Url</th>
                    <th>Parent</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no=0;foreach($module_list as $row){?>
                  <tr class="rowdata_<?php echo $row->coreMenuId; ?>">
                    <td><?php echo ++$no;?></td>
                    <td><?php echo $row->coreMenuName;?></td>
                    <td><?php echo $row->coreMenuUrl;?></td>
                    <td><?php if($row->coreParentId=='0'){echo "-";}else{echo getParentName($row->coreParentId);}?></td>
                    <td>
                    
                      <button type="button" class="btn btn-primary" onclick="editModule('<?php echo $row->coreMenuId; ?>')" data-toggle="modal" data-target="#popUp">
                        Edit
                      </button>


                      <a href="javascript:;" onclick="deletedata('<?php echo $row->coreMenuId; ?>')" title="Delete"><i class="fa fa-trash-o"></i>Delete</a>
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
                  </div>
                  <br>
                  

                <div class="text-right">
                 
                </div>
              </div>
            </div>
          </form>
          <!-- /2 columns form -->


          <!-- Footer -->
          <?php $this->load->view('template/footer'); ?>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->


</body>
</html>

<?php if(!empty($this->session->flashdata('message'))){ 
                  echo "<script type='text/javascript'>
                  var base_url = window.location.origin;
                    PNotify.desktop.permission();
        (new PNotify({
            title: 'Success notification',
            type: 'success',
            text: 'Your Data has been Added Successfully.',
            desktop: {
                desktop: true,
                icon: '". base_url()."assets/images/pnotify/success.png'
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
            alert('Hey! You clicked the desktop notification!');
        });  </script>";}elseif(!empty($this->session->flashdata('error'))){ 
                  echo "<script type='text/javascript'>
                  var base_url = window.location.origin;
                    PNotify.desktop.permission();
        (new PNotify({
            title: 'Failed notification',
            type: 'danger',
            text: 'Failed to add data',
            desktop: {
                desktop: true,
                icon: '". base_url()."assets/images/pnotify/danger.png'
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
            alert('Hey! You clicked the desktop notification!');
        });  </script>";}  ?>

<div class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" id="popUp" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="close_popup()" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title_popup">Add Module</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
          
            <form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'management_module/insert_module'; ?>">
              <input name="menuId" type="hidden">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Menu Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="menuName" id="menuName" placeholder="Menu Name" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Menu Title</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="menuTitle" name="menuTitle" placeholder="Menu Title" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Menu URL</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="menuUrl" name="menuUrl" placeholder="Menu URL" onkeyup="check()" required>
                  <span id="urlAvailResult"></span>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Menu FA Icon</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="menuIcon" name="menuIcon" placeholder="fa-icon" required>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Parent</label>
                <div class="col-sm-8">
                  <select class="form-control select2" style="width: 100%;" name="parent" id="parent" required>
                    <option value="">--Select--</option>
                    <option value="0">Root</option>
                    <?php foreach($parent as $row){?>
                      <option value="<?php echo $row->coreMenuId;?>"><?php echo $row->coreMenuName;?></option>
                    <?php };?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Menu Order</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" id="menuOrder" name="menuOrder" placeholder="Menu Order" onkeypress="return isNumberKey(event)">
                  <span id="orderAvailResult">Smaller is higher position</span>
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="inputPassword3" class="col-sm-4 control-label">Confirm Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" id="password2" name="password2" placeholder="Password">
                </div>
              </div> -->
            </form>
          
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="button_save" disabled="" onclick="$('#form-add').submit()">Save</button>
      </div>
      
   
<script type="text/javascript">
 $(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});

    function editModule(id){

      $.getJSON("<?php echo base_url().'management_module/edit_module/';?>"+id, function(json){
        $('input[name="menuName"]').val(json['0'].coreMenuName);
        $('input[name="menuUrl"]').val(json['0'].coreMenuUrl);
        $('input[name="menuId"]').val(json['0'].coreMenuId);
        $('input[name="menuTitle"]').val(json['0'].coreMenuTitle);
        $('input[name="menuIcon"]').val(json['0'].coreMenuIcon);
        $('input[name="menuOrder"]').val(json['0'].coreMenuSort);
        $('#parent option[value="'+json['0'].coreParentId+'"]').prop('selected',true);
        $('input[name=hidden][value="'+json['0'].coreMenuHidden+'"]').prop("checked",true);
        $('#parent option[value="'+id+'"]').css('display','none');
        $('#title_popup').text(' Edit Module');
        $('#form-add').attr('action', "<?php echo base_url().'management_module/update_module'; ?>");
        $("#button_save").removeAttr("disabled");
      });
    };

    function close_popup(){
      $('#title_popup').text(' Add Module');
      var hidden = $('input[name="menuId"]').val();
      $('#parent option[value="'+hidden+'"]').css('display','block');
      $('#form-add')[0].reset();
      $('#form-add').attr('action', "<?php echo base_url().'management_module/insert_module'; ?>");
      $("#button_save").attr("disabled",'disabled');
      $('input[name=hidden][value="0"]').prop("checked",true);
    }

    function check(){      
      var menuUrl = $('#menuUrl').val(); 
      var urlAvailResult = $("#urlAvailResult");

      if(menuUrl.length != "") { 

        urlAvailResult.html('Loading..');
        $.ajax({ 
          type : 'GET',
          //data : username,
          url  : '<?php echo base_url(); ?>management_module/checkURL/'+menuUrl,

          success: function(response){ 
              if(response == 0){
                urlAvailResult.html('');
                $("#button_save").removeAttr("disabled");

              }
              else if(response > 0){
               // alert(url);
                urlAvailResult.html(response);
                $("#button_save").attr("disabled","disabled");
              }
              else{
                urlAvailResult.html('Error Query');
              }
            }
          });
      }else{
        urlAvailResult.html('');
        $("#button_save").removeAttr("disabled");
        $("#button_save").attr("disabled","disabled");
      }
    }

    function deletedata(rowdata)
    {
      var okcancel = confirm("Are you sure to delete the data?");

      if (okcancel) {
        $.ajax({
          type: "post",
          url: "<?php echo base_url().'management_module/delete/'; ?>"+rowdata,
          success: function(response) {
            if (response) { 
              $(".rowdata_"+rowdata).fadeOut("fast");
              alert("Success delete data");
            } else {
              alert("Sory, failed delete your data.");
            };
          }
        });
      };
    };

    function isNumberKey(evt)
    {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
      return true;
    };
</script>