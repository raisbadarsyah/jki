<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ALL);

class Management_module extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_module_model'));
		$this->load->helper(array('check_auth_menu','management_module'));
		check_authority_url();		
	}

	function index()
	{
		$config['title'] 	 = 'Koperasi Syariah Duasatudua | Manage Module';
		$config['page_title'] = 'Management Module';
		$config['page_subtitle'] = 'Module List';
	    $config['module_list'] = $this->management_module_model->get_all();
	    $config['parent'] = $this->management_module_model->get_parent_menu();
       
		$this->load->view('v_list', $config);
	}
	
	function insert_module(){
		$menuName  = $this->input->post('menuName');
		$menuUrl  = $this->input->post('menuUrl');
		$menuParent = $this->input->post('parent');
		$menuTitle  = $this->input->post('menuTitle');
		$menuIcon  = $this->input->post('menuIcon');
		$menuOrder  = $this->input->post('menuOrder');
		
		$data = array(
			"coreMenuName" => $menuName,
			"coreMenuUrl" => $menuUrl,
			"coreParentId" => $menuParent,
			"coreMenuTitle" => $menuTitle,
			"coreMenuIcon" => $menuIcon,
			"coreMenuSort" => $menuOrder
		);	
		$action = $this->management_module_model->insert_module($data);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Module has been added successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to add module');
		}	
		redirect(base_url() . 'management_module', 'refresh');	
	}

	function edit_module($id){
			$module = $this->management_module_model->get_module_by_id($id);			
			echo json_encode($module);
	}

	function update_module(){	
		$menuName  = $this->input->post('menuName');
		$menuUrl  = $this->input->post('menuUrl');
		$menuParent = $this->input->post('parent');
		$menuTitle  = $this->input->post('menuTitle');
		$menuIcon  = $this->input->post('menuIcon');
		$menuId    = $this->input->post('menuId');
		$menuOrder  = $this->input->post('menuOrder');
		
		
		$data = array(
			"coreMenuName" => $menuName,
			"coreMenuUrl" => $menuUrl,
			"coreParentId" => $menuParent,
			"coreMenuTitle" => $menuTitle,
			"coreMenuIcon" => $menuIcon,
			"coreMenuSort" => $menuOrder
			// "coreMenuHidden"=> $menuHidden
		);

		$action = $this->management_module_model->update_module($data, $menuId);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Module has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update Module');
		}	
		redirect(base_url() . 'management_module', 'refresh');
	}
	
	function delete($id){
		$this->management_module_model->delete_module($id);
	}

	function checkURL($url){
		$this->management_module_model->check_menuUrl($url);
	}
}