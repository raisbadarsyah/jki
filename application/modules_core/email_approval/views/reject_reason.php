<!DOCTYPE html>
<html>

<head>

  <meta charset="UTF-8">

  <title>MPI Diskon Modifier</title>

    <link rel="stylesheet" href="<?php echo base_url();?>template/reject_form/css/style.css" media="screen" type="text/css" />

</head>

<body>

  <span href="#" class="button" id="toggle-login">MPI Diskon Modifier</span>
<div id="login">
  <div id="triangle"></div>
  <h1>Reject Reason</h1>
  <?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-danger alert-styled-left alert-bordered"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

  <form method="post">
    <input type="text" name="no_fdk" id="no_fdk" class="form-control" value="<?php echo $data_fdk['fdk_number'];?>" readonly>
    <input type="hidden" class="form-control" placeholder="" name="id_principal" value="<?php echo $data_fdk['id_principal'];?>" readonly>
    <textarea name="reject_reason" placeholder="Reject Reason"></textarea>

    <input type="submit" value="Submit" />
  </form>
</div>

  <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>

  <script src="js/index.js"></script>

</body>

</html>