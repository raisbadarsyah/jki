<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); //error_reporting(0);

class Email_approval extends CI_Controller
{
	var $API ="";

	function __construct()
	{
		parent::__construct();
		$this->API="http://localhost/oracle-api/city/d2ba5ac651d985a7fad886044d92b5cd/";
		$this->load->model(array('menu_model','management_fdk/management_fdk_model'));
		$this->load->model('management_fdk/fdk_principal_model');
		$this->load->model('management_fdk/fdk_row_model');
		$this->load->model('management_fdk/fdk_model');
		$this->load->model('management_fdk/fdk_type_model');
		$this->load->model('menu_model');
		$this->load->model('management_fdk/approval_model');
		$this->load->model('master_outlet/Master_outlet_model');
		$this->load->model('management_user/Management_user_model');
		$this->load->model('management_user_mpi/Management_user_mpi_model');
		$this->load->model('management_principal_position/Management_principal_position_model');
		$this->load->helper(array('check_auth_menu','tgl_indonesia','email_content'));
	}

	public function tes_email()
	{
		$this->load->library('PHPMailerAutoload');
		        	

					$mail = new PHPMailer();

					//$mail->SMTPDebug = 2;
					
					$mail->CharSet = 'UTF-8';
					$mail->isSMTP();
					$mail->SMTPDebug = 2;  // debugging: 1 = errors and messages, 2 = messages only
				    $mail->SMTPAuth = true;  // authentication enabled
				    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
					$mail->Host = 'smtp.googlemail.com';
					$mail->Port = 465; 
					$mail->Username = 'mpi17sppt@gmail.com';
					$mail->Password = 'support2017';
					$mail->WordWrap = 50;  
					$mail->setFrom('mpi17sppt@gmail.com', 'FDK Created');
					$mail->addAddress('muhammadraisb@gmail.com');
					$mail->Subject = 'Pengajuan Form Diskon Khusus';
					$message = "tes";
					$mail->Body = $message;
					$mail->IsHTML(true);
					$mail->send();
					
				if(!$mail->send())
				{
				   echo "Message could not be sent. <p>";
				   echo "Mailer Error: " . $mail->ErrorInfo;
				   exit;
				}
	}

	public function approval($fdk_number,$kode,$coreUserId,$id_principal)
	{
		$fdk_number = base64_decode(urldecode($this->uri->segment(3)));
		echo $coreUserId;

		$position = $this->management_fdk_model->get_user_position($coreUserId,$id_principal);
		$id_position = $position->corePositionId;

		$cek_fdk = $this->fdk_model->get_fdk($fdk_number);
		$approval_status = $this->management_fdk_model->get_approval_status($id_principal,$id_position);
		$list_item =$this->fdk_row_model->get_detail_fdk($fdk_number);

		
		if(isset($approval_status->approval_sort) && $approval_status->approval_sort < $cek_fdk->approval_sort  )
		{
			echo "FDK Sudah di approve oleh atasan anda";
			exit();
		}

		if(!isset($approval_status))
		{
			echo "There is no your position in approval role, Contact your principal administrator to set approval role, make sure your position is involved in the approval role";
			exit();
		}

	
		$cek_role = $this->Management_principal_position_model->cek_role($id_principal);
		
		if(!isset($cek_role))
		{
			echo "Approval Role Has Not Been Set. Cannot validation fdk without approval role, Contact your principal administrator to set approval role";
			exit();
			
		}

		try {
			

				$id_approval = $approval_status->id_approval;

				$data_log = array(
					'fdk_number' => $fdk_number,
					'created_by' => $coreUserId,
					'created_at' => date("Y-m-d h:i:s"),
					'description' => "Pengguna dengan nomor id ".$coreUserId." melakukan approval"
				);


				$data_approval = array(
					'no_fdk' => $fdk_number,
					'coreUserUserId' => $coreUserId,
					'id_position' => $id_position,
					'status' => 'Approved'
				);

				$cek_approval = $this->approval_model->get_approval($fdk_number,$coreUserId);
				
	   				if(isset($cek_approval->no_fdk)) 
	   				{
	   					$delete_approval = $this->approval_model->delete_approval($fdk_number,$coreUserId);
	   				}

				$action_approve = $this->approval_model->insert($data_approval);

				$data = array(
					'number' => $fdk_number,
					'id_approval' => $id_approval,
					'fdk_position' => $id_position,
					'approval_sort' => $approval_status->approval_sort
				);

				$data_fdk_row = array(
						'fdk_item_status' 	  => 1,
					);

				$action = $this->fdk_row_model->update_fdk_item_reject($fdk_number,$data_fdk_row);
				$action = $this->fdk_row_model->approve_fdk($fdk_number,$data);
				$action_log = $this->management_fdk_model->insert_fdk_log($data_log);

				$this->session->set_flashdata('success', 'Data Berhasil di Approve');
				redirect('email_approval/approval_notifikasi','refresh');

		} catch (Exception $e) {

			$this->session->set_flashdata('error', 'Data Gagal di Approve');
				redirect('email_approval/approval_notifikasi','refresh');
		
		}


		exit();
	}

	public function reject($fdk_number,$kode,$coreUserId,$id_principal)
	{
	
		$fdk_number = base64_decode(urldecode($this->uri->segment(3)));
		$fdk_number_encrypt = base64_encode($fdk_number);
		$data_fdk = array('id_principal' => $id_principal,
					  'fdk_number'	 => $fdk_number,
					  'kode'		 => $kode,
					  'coreUserId'	 => $coreUserId,
		 );

		$position = $this->management_fdk_model->get_user_position($coreUserId,$id_principal);
		$id_position = $position->corePositionId;

		$data['data_fdk']=$data_fdk;

		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			
			try {
				
				$id_approval = 5; // rejected
				$reject_reason  = $this->input->post('reject_reason');

				$data_log = array(
					'fdk_number' => $fdk_number,
					'created_by' => $coreUserId,
					'created_at' => date("Y-m-d h:i:s"),
					'description' => "Pengguna dengan nomor id ".$coreUserId." melakukan rejected"
				);

				$data = array(
					'number' => $fdk_number,
					'id_approval' => $id_approval,
					'fdk_position' => $id_position,
					'reject_reason' => $reject_reason
				);

				$data_fdk_row = array(
							'fdk_item_status' 	  => 2,
						);

				$action = $this->fdk_row_model->update_fdk_item_reject($fdk_number,$data_fdk_row);
				$action = $this->fdk_row_model->approve_fdk($fdk_number,$data);
				$action_log = $this->management_fdk_model->insert_fdk_log($data_log);

				$this->session->set_flashdata('success', 'Data Berhasil di Reject');
				redirect('email_approval/approval_notifikasi','refresh');

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Data Gagal di Reject');
				redirect('email_approval/approval_notifikasi','refresh');

			}
			

			exit();
		}

		$this->load->view('reject_reason', $data);
	}

	public function approval_notifikasi()
	{
		$this->load->view('approval_notifikasi');
	}
}	

