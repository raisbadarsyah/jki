<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><?php echo $page_title;?></div>
					<?php if($this->session->flashdata('message') != ""){ ?>  
					<br /><br />	
					  <div class="alert alert-info" role="alert">
						  <i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?>
					  </div>
				    <?php } ?>
				</div>
				<div class="panel-body">
                <a href="<?php echo base_url(); ?>ref_title_stock"><button class="btn btn-warning"><i class="glyphicon glyphicon-arrow-left"></i> Back</button></a>				
                <br /><br />
				Principal : <?php  echo $principal_name; ?><hr />
				
				<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'ref_title_stock/insert_detail'; ?>" enctype="multipart/form-data">
					<input type="hidden" name="id_principal" value="<?php echo $id_principal; ?>">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped">
					  <thead>
						<tr>
							<th width="5%">No</th>
							<th width="8%" align="center"><input type="checkbox" id="checkAll"></th>
							<th>Title Name</th>						
						</tr>
					  </thead>
					  <tbody>
					   <?php $no=0;foreach($list_title as $row){?>
						<tr>
							<td><?php echo ++$no;?></td>
							<td><?php status_title_stock($row->refTitleId, $id_principal); ?></td>	
							<td><?php echo $row->refTitleName;?></td>				
						</tr>
						<?php }?>
						<tr>
							<td colspan="3" align="right">
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <button type="submit" class="btn btn-primary">Save</button>
									</div>
								</div>
							</td>
						</tr>
					  </tbody>
					</table>				
				</form>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>
<script type="text/javascript">
	$("#checkAll").click(function () {
		 $('input:checkbox').not(this).prop('checked', this.checked);
	});
</script>