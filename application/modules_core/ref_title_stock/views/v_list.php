<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><?php echo $page_title;?></div>
					<?php if($this->session->flashdata('message') != ""){ ?> 
						<br /><br />				
						<div class="alert alert-info" role="alert">
						  <i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?>
						</div>
					<?php } ?>
				</div>
				<div class="panel-body">	
                <br />
				<?php /*
  				<a href="<?php echo base_url(); ?>ref_title/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
				<a href="<?php echo base_url(); ?>ref_title/import"><button class="btn btn-warning"><i class="glyphicon glyphicon-upload"></i> Import Excel</button></a>
				<a href="<?php echo base_url(); ?>ref_title/download"><button class="btn btn-secondary"><i class="glyphicon glyphicon-download"></i> Download Excel</button></a>
				<hr /> */ ?>				
				
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				  <thead>
					<tr>
						<th width="5%">No</th>
						<th>Principal</th>
						<th width="8%">Action</th>
						 </tr>
				  </thead>
				  <tbody>
				   <?php $no=0;foreach($list_principal as $row){?>
					<tr>
						<td><?php echo ++$no;?></td>
						<td><?php echo $row->supplierPrincipal;?></td>						
						<td>
						  <a href="<?php echo base_url(); ?>ref_title_stock/detail/<?php echo $row->suplierId; ?>" class="btn btn-primary" title="Detail"><i class="glyphicon glyphicon-pencil"></i></a>						  
						</td>					  
				    </tr>
				    <?php }?>
				  </tbody>
				</table>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>