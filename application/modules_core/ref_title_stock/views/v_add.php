<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'ref_title/insert'; ?>" enctype="multipart/form-data">
									  <input name="userId" type="hidden">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Title Name</label>
										<div class="col-sm-8">
										    <select class="form-control select2" style="width: 100%;" name="refTitleName" id="group" required>
												<option value="">--Select--</option>
												<option value="PRINCIPAL">PRINCIPAL</option>
												<option value="BRANCH_NAME">BRANCH_NAME</option>
												<option value="JENIS">JENIS</option>
												<option value="NO_PERFORMA">NO_PERFORMA</option>
												<option value="NO_INVOICE">NO_INVOICE</option>
												<option value="PELANGGAN">PELANGGAN</option>
												<option value="KLSOUT">KLSOUT</option>
												<option value="ITEM_CODE">ITEM_CODE</option>
												<option value="NAMA_PRODUK">NAMA_PRODUK</option>
												<option value="QTY">QTY</option>
												<option value="SALES">SALES</option>
												<option value="SALESMAN">SALESMAN</option>
											</select>	
										</div>
									  </div>
									 
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Principal</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="refTitlePrincipal" id="refTitlePrincipal" required>
											<option value="">--Select--</option>
											<?php foreach($list_principal as $row){?>
											  <option value="<?php echo $row->supplierPrincipal;?>"><?php echo $row->supplierPrincipal;?></option>
											<?php };?>
										  </select>
										</div>
									  </div>									  
								
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Status</label>
										<div class="col-sm-8">
											<select class="form-control select2" style="width: 100%;" name="refTitleStatus" id="group" required>
												<option value="">--Select--</option>
												<option value="Y">Y</option>
												<option value="N">N</option>						
											</select>	
										</div>
									  </div>
									 <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									  </div>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>