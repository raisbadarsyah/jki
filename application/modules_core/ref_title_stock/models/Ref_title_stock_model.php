<?php

class Ref_title_stock_model extends CI_Model {

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from('ref_title');
		$this->db->where('refTitleId', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_all() {
        $this->db->select('*');
        $this->db->from('ref_title');
		$this->db->where('refTitleType', 2);
		$this->db->order_by('refTitleId','asc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_principal() {
        $this->db->select('*');
        $this->db->from('m_supplier');
		$this->db->order_by('supplierPrincipal','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert_detail($data){
		$this->db->trans_begin();
        $this->db->insert('ref_title_principal_stock', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function get_name_principal($id_principal) {
        $this->db->select('supplierPrincipal');
        $this->db->from('m_supplier');
        $this->db->where('suplierId', $id_principal);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $supplierPrincipal = $row->supplierPrincipal;
            }

            return $supplierPrincipal;
        } else {
            return FALSE;
        }
    }
	
	function get_status_title_principal($id_title, $id_principal) {
        $this->db->select('refTitleId');
        $this->db->from('ref_title_principal_stock');
        $this->db->where('refTitleId', $id_title);
		$this->db->where('refPrincipalId', $id_principal);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $refTitleId = $row->refTitleId;
            }

            return $refTitleId;
        } else {
            return FALSE;
        }
    }

	
	function delete_detail($id_title, $id_principal) {
        $query = $this->db->delete('ref_title_principal_stock', array('refTitleId' => $id_title, 'refPrincipalId' => $id_principal));

		 if ($query) { 
			return TRUE; 
	    } 
	    else { 
	    	return FALSE; 
	    } 
	}
	
}

