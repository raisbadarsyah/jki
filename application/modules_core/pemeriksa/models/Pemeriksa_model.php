<?php

class Pemeriksa_model extends CI_Model {

   function get_all($params){
		$this->db->select('*,pemeriksa.status as status_pemeriksa');
        $this->db->from('pemeriksa');
        $this->db->join('kantor_area','pemeriksa.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
  
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                 
                $this->db->where('kantor_wilayah.kode_kanwil',$params['kode_kanwil']);
                 //$this->db->where('pemeriksa.kode_area', $params['kode_area']);
            }
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_fasa(){
        $this->db->select('*');
        $this->db->from('fasa');
       
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($id_pemeriksa){
		$this->db->select('*');
        $this->db->from('pemeriksa');
        $this->db->join('kantor_area','pemeriksa.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->where('id_pemeriksa',$id_pemeriksa);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_pemeriksa) {
       $query = $this->db->insert('pemeriksa', $data_pemeriksa);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update_pemeriksa($data, $id_pemeriksa){
        $this->db->trans_begin();
		$this->db->where('id_pemeriksa', $id_pemeriksa);
		$this->db->update('pemeriksa', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function delete($id_pemeriksa) {
        $query = $this->db->delete('pemeriksa', array('id_pemeriksa' => $id_pemeriksa));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

