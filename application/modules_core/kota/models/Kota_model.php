<?php

class Kota_model extends CI_Model {

   function get_all(){
		$this->db->select('*');
        $this->db->from('kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_detail_kodefikasi($kode_djp_kota){
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->where('kode_djp_kota',$kode_djp_kota);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function get_detail_kota_prov($kode_djp_kota){
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_djp_kota',$kode_djp_kota);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_kota($kode_kota){
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->where('id_kota',$kode_kota);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

    function kota_by_provinsi($id_provinsi){
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->where('id_provinsi', $id_provinsi);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function get_by_id($kode_kanwil){
		$this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->where('kode_kanwil', $kode_kanwil);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_kota) {
       $query = $this->db->insert('kota', $data_kota);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $id_kota){
        $this->db->trans_begin();
		$this->db->where('id_kota', $id_kota);
		$this->db->update('kota', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function delete($id_kota) {
        $query = $this->db->delete('kota', array('id_kota' => $id_kota));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

