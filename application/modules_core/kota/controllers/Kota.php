<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Kota_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['kota'] = $this->Kota_Model->get_all();

		$this->load->view('index',$data);
	}

	function add()
	{	
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_kota = $this->input->post('id_kota');
			$kode_djp_kota = $this->input->post('kode_djp_kota');
			$nm_kota = $this->input->post('nm_kota');
			$nm_alias = $this->input->post('nm_alias');
			$id_provinsi = $this->input->post('id_provinsi');


			try {
				
				$kota = $this->Kota_Model->get_detail_kota($id_kota);

				if(isset($kota->id_kota))
				{
					$this->session->set_flashdata('error', 'Kode kota tidak boleh sama');
					redirect('kota','refresh');
					die();
				}
				

				$data_kota = array(
					"id_kota"=> $id_kota,
					"kode_djp_kota"=> $kode_djp_kota,
					"nm_kota"=> $nm_kota,
					"nm_alias"=> $nm_alias,
					"id_provinsi"=> $id_provinsi,
				);
				
				$insert_kota = $this->Kota_Model->insert($data_kota);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_kota == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kota','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_kota = $this->uri->segment(3);
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kota'] = $this->Kota_Model->get_detail_kota($kode_kota);
		$this->load->view('edit',$data);
		
	}

	function proses_edit()
	{

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_kota = $this->input->post('id_kota');
			$kode_djp_kota = $this->input->post('kode_djp_kota');
			$nm_kota = $this->input->post('nm_kota');
			$nm_alias = $this->input->post('nm_alias');
			$id_provinsi = $this->input->post('id_provinsi');


			try {
				
				$data_kota = array(
					"id_kota"=> $id_kota,
					"kode_djp_kota"=> $kode_djp_kota,
					"nm_kota"=> $nm_kota,
					"nm_alias"=> $nm_alias,
					"id_provinsi"=> $id_provinsi,
				);
				
				$update_kota = $this->Kota_Model->update($data_kota,$id_kota);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal edit kota');

			}


				if($update_kota == 1) {
						$this->session->set_flashdata('success', 'Data berhasil diubah');
						redirect('kota','refresh');
				}


		}

	}

	function delete($id_kota){
		$action = $this->Kota_Model->delete($id_kota);

		if($action == 1) {
			$this->session->set_flashdata('success', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'kota', 'refresh');
	}

}
