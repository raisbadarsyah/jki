<?php

class Djk_model extends CI_Model {

   function get_all(){
		$this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_detail_pemeriksaan($no_pemeriksaan)
    {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('no_pemeriksaan =', $no_pemeriksaan);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }
    
      function get_detail_pelanggan($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pelanggan.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_pemeriksa($params){
        $this->db->select('*');
        $this->db->from('pemeriksa');

        if(isset($params['nip']) && ($params['nip'] == 111 || $params['nip'] == 222))
        {
            $this->db->join('kantor_area','kantor_area.kode_area=pemeriksa.kode_area');
            $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
            $this->db->where('kantor_wilayah.kode_kanwil',$params['kode_kanwil']);
            $this->db->group_by('nm_pemeriksa');
        }
        else
        {

            if(isset($params['kode_area']))
            {
                
                $this->db->join('kantor_area','kantor_area.kode_area=pemeriksa.kode_area');
                $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
                $this->db->where('kantor_wilayah.kode_kanwil',$params['kode_kanwil']);
                $this->db->group_by('nm_pemeriksa');
            
                //$this->db->where('kode_area',$params['kode_area']);
            }
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota(){
        $this->db->select('*');
        $this->db->from('kota');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl(){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl(){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($pemeriksaan) {
       $query = $this->db->insert('pemeriksaan', $pemeriksaan);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_spk($data_no_spk) {
       $query = $this->db->insert('no_spk', $data_no_spk);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data_pemeriksaan, $no_pemeriksaan){
        $this->db->trans_begin();
		$this->db->where('no_pemeriksaan', $no_pemeriksaan);
		$this->db->update('pemeriksaan', $data_pemeriksaan);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function update_pemeriksa($data_pemeriksaan, $no_pendaftaran){
        $this->db->trans_begin();
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->update('pemeriksaan', $data_pemeriksaan);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }       
    }

     function get_max_no_lhpp($params){
        $this->db->select('MAX(SUBSTRING(no_pemeriksaan, "1","4")) AS ExtractString');
       // $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
      

        if(isset($params['kode_area']) && $params['kode_area'] != "")
        {      
            $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area');
            $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area');
            $this->db->where('kantor_area.kode_area', $params['kode_area']);
        }
        
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_max_no_spk(){
        $this->db->select('MAX(id) as id');
        $this->db->from('no_spk');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_detail_pemeriksa($id_pemeriksa){
        $this->db->select('*');
        $this->db->from('pemeriksa');
        $this->db->where('id_pemeriksa', $id_pemeriksa);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_no_spk($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('no_spk');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $this->db->order_by('no_spk desc');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

}

