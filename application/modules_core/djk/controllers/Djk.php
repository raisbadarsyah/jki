<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Djk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan/Pelanggan_Model','Staff/Staff_Model','Pembayaran/Pembayaran_Model','Djk_Model','Laporan/Laporan_Model','ptl/Ptl_model','kota/Kota_model','daya/Daya_model'));
		$this->load->helper(array('check_auth_menu','tgl_indonesia'));
		
	}


	function detail_lhpp()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		
		$this->load->view('detail_lhpp',$data);
	}

	function lokasi()
	{

		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
		$this->load->view('view_map',$data);
	
	}
	
	function foto()
	{
		$no_pendaftaran = $this->uri->segment(3);
		
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		$this->load->view('view_foto_pemeriksaan',$data);
	}

	function get_permohonan()
	{
		
		//$data_pelanggan_djk = '{"metadata":{"code":1,"trx_id":2017953,"message":"Sukses"},"permohonan":[{"no_agenda":"H18R610962","pemilik":{"nama":"Fulan","alamat":"Kebayoran Lama","kota_id":3174,"daya":1300,"unit_pelayanan_id":"54360"},"pemohon":{"nama":"Fulan","nik":"3174069483918212","no_telpon":"087884150458","email":"raisbadarsyah@yahoo.com"}}]}';//$this->api_djk();
		
		$data_pelanggan_djk = $this->api_djk();
		$decode_pelanggan_djk = json_decode($data_pelanggan_djk,TRUE);
		$tgl_daftar = date("Y-m-d H:i:s");

		foreach ($decode_pelanggan_djk['permohonan'] as $key => $value) {
			
			$get_detail_ptl = $this->Ptl_model->get_by_kode_unit_pln($value['pemilik']['unit_pelayanan_id']);
			$get_prov = $this->Kota_model->get_detail_kota_prov($value['pemilik']['kota_id']);
			$get_daya = $this->Daya_model->get_daya_by_name($value['pemilik']['daya']);

			$data_calon_pelanggan = array('no_permohonan' => '',
										  'id_wilayah' => $get_detail_ptl->kode_kanwil,
										  'id_area' => $get_detail_ptl->kode_area,
										  'nama' => $value['pemilik']['nama'],
										  'alamat' => $value['pemilik']['alamat'],
										  'email' => $value['pemohon']['email'],
										  'id_provinsi' => $get_prov->id_provinsi,
										  'id_kota' => $get_prov->id_kota,
										  'telepon' => $value['pemohon']['no_telpon'],
										  'id_daya' => $get_daya->id_daya,
										  'tgl_daftar' => $tgl_daftar,
										  'status' => '1',
										  'via' => '2',
										  'no_agenda'=> $value['no_agenda']
									);
			$insert_pelanggan = $this->Pelanggan_Model->insert_calon_pelanggan($data_calon_pelanggan);

			
			echo "<pre>";
			print_r($data_calon_pelanggan);
			echo "</pre>";
			
		}
	}
	
	function api_djk()
	{

		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/tr/permohonan/get'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/tr/permohonan/get'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;

	}


}
