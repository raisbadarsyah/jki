<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_responsive.js"></script>
	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>
	<style type="text/css">
	
	.grid-view-loading
	{
		background:url(loading.gif) no-repeat;
	}

	.grid-view
	{
		padding: 40px 0;
	}

	.datatable-header>div:first-child, .datatable-footer>div:first-child {
    margin-left: 20px;
	}

	.dataTables_length {
    	float: right;
	    display: inline-block;
	    margin: 0 0 20px 20px;
	    margin-top: 0px;
	    margin-right: 20px;
	    margin-bottom: 20px;
	    margin-left: 20px;
	}

	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}

</style>
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Dashboard</a></li>
							<li><a>Pemeriksaan</a></li>
							<li class="active">Belum diperiksa</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Belum Diperiksa 
							<?php if(isset($status) && $status == 1) { ?>
							(Lebih Dari 2 Hari)
							<?php } elseif(isset($status) && $status == 2) { ?>
							(Kurang Dari 2 Hari)
							<?php } elseif(isset($status) && $status == 3) { ?>
							(Instalasi Belum Siap)
							<?php } ?>
							</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
						<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

							<?php 
					$position_id = $this->session->userdata('position_id');
					//if($position_id == 9){
				?>
  				
  						<a href="<?php echo base_url(); ?>pemeriksaan/belum_diperiksa/1"><button class="btn btn-primary">Data Belum Diperiksa Lebih Dari 2 Hari</button></a>

  						<a href="<?php echo base_url(); ?>pemeriksaan/belum_diperiksa/2"><button class="btn btn-primary">Data Belum Diperiksa Kurang Dari 2 Hari</button></a>

  						<a href="<?php echo base_url(); ?>pemeriksaan/belum_diperiksa/3"><button class="btn btn-primary">Data Instalasi Belum Siap</button></a>
						</div>
						<form action="<?php echo base_url().'pemeriksaan/cetak_surat_tugas'; ?>" enctype="multipart/form-data" method="post">
						<div id="pelanggan-grid" class="grid-view">
																
						<table class="datatable-responsive table display items">
							 <thead>
					<tr>
						<th></th>
						<th>No Kwitansi</th>
						<th>No Pendaftaran</th>						
						<th>Nama</th>
						<th>Alamat</th>
						<th>Daya</th>
						<th>Kota</th>
						<th>Wilayah</th>
						<th>Area</th>
						<th>Sub Area</th>
						<th>Kantor PLN</th>
						<th>Petugas Pemeriksa</th>
						<th>Sisa Waktu Penerbitan</th>
						<th>Tgl Pembayaran</th>
						
						<th width="15%" align="center">Action</th>
						 </tr>
				  </thead>
				  <tbody>	
				
				   <?php foreach($pelanggan as $row){?>
					<tr>
						<td><input type="checkbox" name="no_pendaftaran[]" 
value="<?php echo $row->no_pendaftaran_pelanggan;?>"></td>
						<td><?php echo $row->no_kwitansi;?></td>
						<td><?php echo $row->no_pendaftaran_pelanggan;?></td>
						<td><?php echo $row->Nama;?></td>
						<td><?php echo $row->alamat;?></td>
						<td><?php echo $row->daya;?></td>
						<td><?php echo $row->nm_kota;?></td>
						<td><?php echo $row->nm_kanwil;?></td>
				
						<td><?php echo $row->nm_area;?></td>
						<td><?php echo $row->nm_sub_area;?></td>
						<td><?php echo $row->nm_ptl;?></td>
						<td>
							<?php 

								if (empty($pemeriksa1[$row->no_pendaftaran_pelanggan]->nm_pemeriksa)) {
								    $nm_pemeriksa1 = "";
								    $nm_pemeriksa2 = "";
								  }else
								  {
								  	$nm_pemeriksa1 = $pemeriksa1[$row->no_pendaftaran_pelanggan]->nm_pemeriksa;
								  	$nm_pemeriksa2 = $pemeriksa2[$row->no_pendaftaran_pelanggan]->nm_pemeriksa;
								  }

							?>


							<?php echo $nm_pemeriksa1;?> <?php echo $nm_pemeriksa2;?></td>
						
						<td>
							
							<?php 

                       	  $limit = strtotime($limit_pemeriksaan);
                          $limit_jam = date('H', $limit);
                          $limit_menit = date('i', $limit);
                          $datetime1 = new DateTime($row->tgl_bayar);
                        
                          $now = date("Y-m-d H:i:s");
                          $tgl_sekarang = new DateTime($now);
                          $new_date = date("Y-m-d H:i:s", strtotime(''.$row->tgl_bayar.', +'.$limit_jam.' hours +'.$limit_menit.' minute'));

                     
							$datetime1->add(new DateInterval('PT48H'));
							$batas_waktu = $datetime1->format('Y-m-d H:i:s');


							$tgl_bayar = new DateTime($row->tgl_bayar);

							$datetime2 = new DateTime($batas_waktu);

							$difference = $tgl_bayar->diff($datetime2);

							if($tgl_sekarang <= $datetime1)
							{
								echo "".$difference->d." Days ".$difference->h." Hours ".$difference->i." Minute";
							}
							elseif ($tgl_sekarang > $datetime1) 
							{
								echo "Lewat dari ".$difference->d." Hari ".$difference->h." Jam ".$difference->i." Menit";
							}
							
                          ?>
                  			<!--
                          <?php echo date('d F Y H:i:s', strtotime($batas_waktu));?>
                			-->
						</td>
						<td><?php echo date('d F Y H:i:s', strtotime($row->tgl_bayar));?></td>
						
						
						<td>
						
						 <a href='#popUp' class='btn btn-default btn-small' id='no_pendaftaran' data-toggle='modal' data-id="<?php echo $row->no_pendaftaran_pelanggan;?>">Periksa</a>

						 <a href='#popUpReject' class="btn btn-primary" id='no_pendaftaran' data-toggle='modal' data-id="<?php echo $row->no_pendaftaran_pelanggan;?>">Belum Siap</a>

						<a href='<?php echo base_url()?>pemeriksaan/cetak_lhpp/<?php echo $row->no_pendaftaran_pelanggan;?>' class='btn btn-primary'">Cetak LHPP</a>

						</td>					  
				    </tr>
				    <?php }?>
				 	
				  </tbody>

						</table>
						</div>
						<div class="text-left">
											<input type="submit" value="Cetak Surat Tugas" class="btn btn-primary">
										</div>

					</form>
					</div>
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>

<!-- Primary modal -->
          <div class="modal fade" id="popUp">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-primary">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">Konfirmasi Kesiapan Instalasi</h6>
                </div>
			<form id="bayar" method="get" action="<?php echo base_url().'pemeriksaan/add'; ?>">
                <div class="modal-body">
                  <div class="fetched-data"></div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Lakukan Pemeriksaan</button>
                
                </div>


            </form>
              </div>
            </div>
          </div>
          <!-- /primary modal -->

<script type="text/javascript">
    	
        $('#popUp').on('show.bs.modal', function (e) {
        	
            var no_pendaftaran = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
         
            $.ajax({
                type : 'post',
                url : "<?php echo site_url();?>pemeriksaan/detail_pelanggan",
                data :  'no_pendaftaran='+ no_pendaftaran,
                dataType: "html",
                success : function(data){
                  
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
               
            });
         });
   
  </script>

  <!-- Horizontal form modal -->
					<div class="modal fade" id="popUpReject">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Alasan Belum Siap</h5>
								</div>

								<form action="<?php echo site_url();?>pemeriksaan/instalasi_belum_siap" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Nomor Pendaftaran</label>
											<div class="col-sm-9">
												<input type="text" name="no_pendaftaran" id="showid" class="form-control" readonly>
												
												
											</div>
										</div>
										

										<div class="form-group">
											<label class="control-label col-sm-3">Alasan Belum Siap</label>
											<div class="col-sm-9">
												<textarea class="form-control" placeholder="Sudah kadaluarsa" name="reject_reason"></textarea>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							
										<input type="submit" class="btn btn-primary" name="reject" id="reject" value="Submit">
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

					<script type="text/javascript">
  
        $('#popUpReject').on('show.bs.modal', function (e) {
            var no_pendaftaran = $(e.relatedTarget).data('id');
            $("#showid").val(no_pendaftaran);
           
         });
  
  </script>

</body>
</html>
