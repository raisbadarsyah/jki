<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_responsive.js"></script>
	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>
		<style type="text/css">
		
		/* Table */
		.display {
			border-collapse: collapse;
			font-size: 0.9em;
		}
		.display th, 
		.display td {
			border: 1px solid #e1edff;
			padding: 1px 7px;
		}
		.display .title {
			caption-side: bottom;
			margin-top: 12px;
		}
		
		/* Table Header */
		.display thead th {
			background-color: #508abb;
			color: #FFFFFF;
			border-color: #6ea1cc !important;
			
		}
		
		/* Table Body */
		.display tbody td {
			color: #353535;
		}
		.display tbody td:first-child,
		.display tbody td:last-child,
		.display tbody td:nth-child(4) {
			text-align: left;
		}
		.display tbody tr:nth-child(odd) td {
			background-color: #f4fbff;
		}
		.display tbody tr:hover td {
			background-color: #ffffa2;
			border-color: #ffff0f;
			transition: all .2s;
		}
		
		/* Table Footer */
		.display tfoot th {
			background-color: #e5f5ff;
		}
		.display tfoot th:first-child {
			text-align: left;
		}
		
	</style>


	<style type="text/css">
	
	.grid-view-loading
	{
		background:url(loading.gif) no-repeat;
	}

	

	.datatable-header>div:first-child, .datatable-footer>div:first-child {
    margin-left: 20px;
	}

	.dataTables_length {
    	float: right;
	    display: inline-block;
	    margin: 0 0 20px 20px;
	    margin-top: 0px;
	    margin-right: 20px;
	    margin-bottom: 20px;
	    margin-left: 20px;
	}

	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}

</style>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Dashboard</a></li>
							<li><a>Pemeriksaan</a></li>
							<li class="active">Sudah diperiksa</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Sudah Diperiksa</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
						<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

							<?php 
					$position_id = $this->session->userdata('position_id');
					//if($position_id == 9){
				?>
  				
					
				
						</div>
						<div id="pelanggan-grid" class="grid-view">
						<table class="table datatable-responsive display">
							 <thead>
					<tr>
						<th>No Kwitansi</th>
						<th>No Pendaftaran</th>						
						<th>Nama</th>
						<th>Alamat</th>
						<th>Daya</th>
						<th>Kota</th>
						<th>Wilayah</th>
						<th>Area</th>
						<th>Sub Area</th>
						<th>NO LHPP</th>
						<th>Tgl LHPP</th>
						<th>Petugas Pemeriksa</th>
						<th>Tgl Pembayaran</th>
						<th>Tgl Pemeriksaan</th>
						<th>Diperiksa</th>
						<th width="15%" align="center">Action</th>
						 </tr>
				  </thead>
				  <tbody>
				
				   <?php foreach($pelanggan as $row){?>
					<tr>
						<td><?php echo $row->no_kwitansi;?></td>
						<td><?php echo $row->no_pendaftaran_pelanggan;?></td>
						<td><?php echo $row->Nama;?></td>
						<td><?php echo $row->alamat;?></td>
						<td><?php echo $row->daya;?></td>
						<td><?php echo $row->nm_kota;?></td>
						<td><?php echo $row->nm_kanwil;?></td>
				
						<td><?php echo $row->nm_area;?></td>
						<td><?php echo $row->nm_sub_area;?></td>
						<td><?php echo $row->no_lhpp;?></td>
						<td><?php echo date('d F Y', strtotime($row->tgl_lhpp));?></td>
						<td><?php echo $row->nm_pemeriksa1;?> <?php echo $row->nm_pemeriksa2;?></td>
						
						<td>
						<?php echo date('d F Y', strtotime($row->tgl_bayar));?>
						</td>
						<td><?php echo date('d F Y', strtotime($row->created_at));?></td>
						<td><?php echo $row->jumlah_pemeriksaan;?></td>
						
						<td>
						 <?php if($position_id == 1) { ?>
						 <a class="btn bg-slate btn-icon" href="<?php echo base_url()?>pemeriksaan/edit/<?php echo $row->no_pendaftaran;?>" title="Buat No Agenda"><i class="fa fa-edit"></i></a>
						<?php } ?>
						</td>					  
				    </tr>
				    <?php }?>
				 
				  </tbody>
						</table>
					</div>
					</div>
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>

<!-- Primary modal -->
          <div class="modal fade" id="popUp">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-primary">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">Konfirmasi Kesiapan Instalasi</h6>
                </div>
			<form id="bayar" method="get" action="<?php echo base_url().'pemeriksaan/add'; ?>">
                <div class="modal-body">
                  <div class="fetched-data"></div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Lakukan Pemeriksaan</button>
                
                </div>


            </form>
              </div>
            </div>
          </div>
          <!-- /primary modal -->

<script type="text/javascript">
    	
        $('#popUp').on('show.bs.modal', function (e) {
        	
            var no_pendaftaran = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
         
            $.ajax({
                type : 'post',
                url : "<?php echo site_url();?>pemeriksaan/detail_pelanggan",
                data :  'no_pendaftaran='+ no_pendaftaran,
                dataType: "html",
                success : function(data){
                  
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
               
            });
         });
   
  </script>

  <!-- Horizontal form modal -->
					<div class="modal fade" id="popUpReject">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Alasan Belum Siap</h5>
								</div>

								<form action="<?php echo site_url();?>management_fdk/send" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Nomor Pendaftaran</label>
											<div class="col-sm-9">
												<input type="text" name="no_pendaftaran" id="showid" class="form-control" readonly>
												
												
											</div>
										</div>
										

										<div class="form-group">
											<label class="control-label col-sm-3">Alasan Belum Siap</label>
											<div class="col-sm-9">
												<textarea class="form-control" placeholder="Sudah kadaluarsa" name="reject_reason"></textarea>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							
										<input type="submit" class="btn btn-primary" name="reject" id="reject" value="Submit">
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

					<script type="text/javascript">
  
        $('#popUpReject').on('show.bs.modal', function (e) {
            var no_pendaftaran = $(e.relatedTarget).data('id');
            $("#showid").val(no_pendaftaran);
           
         });
  
  </script>

</body>
</html>
