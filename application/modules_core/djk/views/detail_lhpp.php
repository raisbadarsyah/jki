
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT. JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<!-- /theme JS files -->

	
    
    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/gallery_library.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location.js"></script>

	

	
</head>

<body>

	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<div class="row">

					<!-- 2 columns form -->
					<div class="col-md-12">
					<!-- Basic layout-->
							<form action="#" class="form-horizontal">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Hasil Pemeriksaan</h5>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<label class="col-lg-3 control-label">No Pendaftaran:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->no_pendaftaran; } ?></label>
												
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Pelanggan:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->Nama; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Alamat:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->alamat; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Tarif/Daya:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->nm_tarif; } ?>/<?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->daya; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">BTL:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->nm_btl; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Instalir:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->nama_instalir; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Telp Instalir:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->telp_instalir; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No Surat Tugas:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->no_surat_tugas; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Pemeriksa:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->nm_pemeriksa1; }?>,<?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->nm_pemeriksa2; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No LHPP:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->no_lhpp; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">TGL LHPP:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php if(isset($detail_pemeriksaan))  { echo $detail_pemeriksaan->tgl_lhpp; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Gambar Instalasi:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->gambar_instalasi == 1) {  ?>

												<label class="col-lg-6 control-label">Sesuai</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->gambar_instalasi == 2){?>

												<label class="col-lg-6 control-label">Tidak Sesuai</label>

												<?php } ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Diagram Garis Tunggal:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->diagram_garis_tunggal == 1) {  ?>

												<label class="col-lg-6 control-label">Sesuai</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->diagram_garis_tunggal == 2){?>

												<label class="col-lg-6 control-label">Tidak Sesuai</label>

												<?php } ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Proteksi PE:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_utama == 1) {  ?>

												<label class="col-lg-4 control-label">Utama : Ada</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_utama == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>

												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_cabang == 1) {  ?>

												<label class="col-lg-4 control-label">Cabang : Ada</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_cabang == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>

												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_akhir == 1) {  ?>

												<label class="col-lg-4 control-label">Utama : Ada</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->penghantar_proteksi_pe_saluran_akhir == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>


											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jenis Penghantar:</label>
											<div class="col-lg-9">
												
												<label class="col-lg-4 control-label">Utama : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jns_penghantar_utama; }?></label>

												<label class="col-lg-4 control-label">Cabang : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jns_penghantar_cabang; } ?></label>

												<label class="col-lg-4 control-label">Akhir : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jns_penghantar_akhir; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penampang Penghantar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label">Utama : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->penampang_penghantar_saluran_utama; } ?></label>

												<label class="col-lg-4 control-label">Cabang : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->penampang_penghantar_saluran_cabang; } ?></label>

												<label class="col-lg-4 control-label">Akhir : <?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->penampang_penghantar_saluran_akhir; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penampang Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->luas_penampang_penghantar_bumi;} ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Sakelar Utama:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->sakelar_utama; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Ketinggian Kotak Kontak:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->tinggi_pemasangan_kotak_kontak; }?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Ketinggia PHBK:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->tinggi_pemasangan_phbk;} ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jenis Kotak Kontak:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan) && $detail_pemeriksaan->jns_pemasangan_kotak_kontak == 1) {  ?>

												<label class="col-lg-4 control-label">Biasa</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->jns_pemasangan_kotak_kontak == 2){?>

												<label class="col-lg-4 control-label">Putar</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->jns_pemasangan_kotak_kontak == 3){?>

												<label class="col-lg-4 control-label">Tutup</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->jns_pemasangan_kotak_kontak == 4){?>

												<label class="col-lg-4 control-label">Tidak Ada</label>

												<?php }elseif(isset($detail_pemeriksaan) && $detail_pemeriksaan->jns_pemasangan_kotak_kontak == 5){?>

												<label class="col-lg-4 control-label">Lainnya</label>

												<?php } ?>

											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah PHB Cabang:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jml_phb_cabang;} ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Saluran Cabang:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jml_saluran_cabang;}?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Saluran Akhir:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jml_saluran_akhir;} ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Titik Lampu:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jml_titik_lampu;} ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Sakelar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->jml_sakelar;} ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKB:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->kkb; } ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKK:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->kkk; } ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Tahanan Isolasi Penghantar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->tahanan_isolasi_penghantar; } ?> MΩ</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Sistem Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->sistem_pembumian; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Resisten Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->resistensi_pembumian; } ?> Ω</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Catatan Pemeriksa:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php if(isset($detail_pemeriksaan)) { echo $detail_pemeriksaan->catatan_pemeriksa; } ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 1:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan)) { ?>
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto1 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto1 ?>" alt="" class="img-rounded img-preview">
					                    <?php } ?>
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 2:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan)) { ?>
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto2 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto2 ?>" alt="" class="img-rounded img-preview">
					                    <?php } ?> 
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 3:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan)) { ?>
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto3 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto3 ?>" alt="" class="img-rounded img-preview">
					                    <?php } ?>
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 4:</label>
											<div class="col-lg-9">
												<?php if(isset($detail_pemeriksaan)) { ?>
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto4 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto4 ?>" alt="" class="img-rounded img-preview">
					                    <?php } ?>
				                        		</a>
											</div>
										</div>



										
									</div>
								</div>
							</form>
							<!-- /basic layout -->
						</div>
					
				</div>
		

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
