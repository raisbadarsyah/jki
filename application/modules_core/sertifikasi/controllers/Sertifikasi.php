<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikasi extends CI_Controller {

	function __construct()
	{
		ini_set('memory_limit', '-1');
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan/Pelanggan_Model','Staff/Staff_Model','Pembayaran/Pembayaran_Model','Pemeriksaan/Pemeriksaan_Model','Verifikasi/Verifikasi_Model','Sertifikasi_Model'));
		$this->load->helper(array('check_auth_menu','tgl_indonesia'));
		
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_all();


		$this->load->view('index',$data);
	}

	function detail_pelanggan()
	{
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');
		$no_pendaftaran = $this->input->post('no_pendaftaran');
		
		$config['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		
		$this->load->view('v_detail_pelanggan', $config);
	}

	function proses_pemeriksaan()
	{
		$user_id = $this->session->userdata('user_id');
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_kwitansi = date('YmdHis');
			$tgl_bayar = date('Y-m-d H:i:s');
			$diterima_oleh = $user_id;
			$harga = $this->input->post('harga');

			try {
				
				$data_pembayaran = array(
					"no_kwitansi"=> $no_kwitansi,
					"no_pendaftaran"=> $no_pendaftaran,
					"tgl_bayar"=> $tgl_bayar,
					"diterima_oleh"=> $diterima_oleh,
					"harga"=> $harga,
				);

				$data_pelanggan = array(
					"status_pembayaran"=> 1,
				);
				
			
				$insert_pembayaran = $this->Pembayaran_Model->insert($data_pembayaran);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pembayaran == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pembayaran/belum_bayar','refresh');
				}


		}
	}

	function belum_sertifikasi()
	{
		/*
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));
		*/
		//$data['pelanggan'] = $this->Pelanggan_Model->get_sertifikasi_belum_terbit($params);

		$this->load->view('belum_sertifikasi');
	}


	function querypagingbelumterbit()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Sertifikasi_Model->get_datatables_belum_terbit($params);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_sertifikasi) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			
			$button = "<a href='".base_url()."sertifikasi/add/".$data_sertifikasi->no_verifikasi."/".$data_sertifikasi->no_pemeriksaan."/".$data_sertifikasi->no_pendaftaran."' class='btn btn-primary'>Permohonan Registrasi</a>";

			
            $row[] = $data_sertifikasi->no_pendaftaran;
            $row[] = $data_sertifikasi->Nama;
            $row[] = $data_sertifikasi->nm_kota;
            $row[] = $data_sertifikasi->nm_kanwil;
            $row[] = $data_sertifikasi->nm_area;
            $row[] = $data_sertifikasi->nm_sub_area;
            $row[] = $data_sertifikasi->nomor_lhpp;
            $row[] = $data_sertifikasi->nm_btl;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $count = $this->Sertifikasi_Model->count_filtered_belum_terbit($params);
 		$count_all = $this->Sertifikasi_Model->count_all_belum_terbit($params);

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $count_all->jumlah,
                        "recordsFiltered" => $count->jumlah,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function querypagingsudahterbit()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Sertifikasi_Model->get_datatables_sudah_terbit($params);

	 	/*
	 	echo "<pre>";
	 	print_r($list);
	 	echo "</pre>";
	 	exit();
	 	*/
        $data = array();
        $no = $_POST['start'];

        $data_sertifikat = array();

		foreach ($list as $key => $value) {
			$data_sertifikat[$value->no_pendaftaran] = $this->Sertifikasi_Model->data_sertifikasi_last($value->no_pendaftaran);
		}

        foreach ($list as $data_sertifikasi) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			
			$button = "
			<a href='".base_url()."sertifikasi/cetak_ulang/".$data_sertifikasi->no_verifikasi."/".$data_sertifikasi->no_pemeriksaan."/".$data_sertifikasi->no_pendaftaran."/' class='btn btn-info btn-xs'><span class='ladda-label'>Cetak Ulang</span></a> <br>

						 <a href='".base_url()."sertifikasi/copy/".$data_sertifikasi->no_verifikasi."/".$data_sertifikasi->no_pemeriksaan."/".$data_sertifikasi->no_pendaftaran."/' class='btn btn-info btn-xs'>Copy</a>";

			
            $row[] = $data_sertifikasi->no_pendaftaran;
            $row[] = $data_sertifikasi->Nama;
            $row[] = $data_sertifikasi->nm_kota;
            $row[] = $data_sertifikasi->nm_kanwil;
            $row[] = $data_sertifikasi->nm_area;
            $row[] = $data_sertifikasi->nm_sub_area;
            $row[] = $data_sertifikasi->nomor_lhpp;
            $row[] = $data_sertifikasi->nm_btl;
            $row[] = "".$data_sertifikat[$data_sertifikasi->no_pendaftaran]->no_seri."";
            $row[] = "".date('d F Y H:i:s', strtotime($data_sertifikat[$data_sertifikasi->no_pendaftaran]->dtcreate))."";
            $row[] = "".$data_sertifikat[$data_sertifikasi->no_pendaftaran]->Name."";
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $count = $this->Sertifikasi_Model->count_filtered_sudah_terbit($params);
 		$count_all = $this->Sertifikasi_Model->count_all_sudah_terbit($params);

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $count_all->jumlah,
                        "recordsFiltered" => $count->jumlah,
                        "data" => $data,
                );

        //output to json format
        echo json_encode($output);

	}


	function sudah_sertifikasi()
	{
		/*
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		/*
		$data['pelanggan'] = $this->Sertifikasi_Model->get_sertifikasi_sudah_terbit($params);

		$data_sertifikat = array();

		foreach ($data['pelanggan'] as $key => $value) {
			$data_sertifikat[$value->no_pendaftaran] = $this->Sertifikasi_Model->data_sertifikasi_last($value->no_pendaftaran);
		}

		
		$data['limit_pemeriksaan'] = date('H:i:s',mktime(48,0,0));
		$data['last_sertifikasi'] = $data_sertifikat;
		*/

		$this->load->view('sudah_sertifikasi');
	}

	function sudah_bayar()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_sudah_bayar();

		$this->load->view('sudah_bayar',$data);
	}

	function uploadFile()
	{	
			header('Content-type: application/json');
			$created_at = Date("YmdHis");
			$path = "./uploads/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("jpg", "jpeg", "png"); //add the formats you want to upload

		    $name = $_FILES['myfile']['name']; //get the name of the file
		    
		    $size = $_FILES['myfile']['size']; //get the size of the file

		  
		    if (strlen($name)) { //check if the file is selected or cancelled after pressing the browse button.
		        list($txt, $ext) = explode(".", $name); //extract the name and extension of the file
		        if (in_array($ext, $valid_formats)) { //if the file is valid go on.
		            if ($size < 2098888) { // check if the file size is more than 2 mb
		                $file_name = $created_at; //get the file name
		                $file = $file_name.'.'.$ext;
		                $tmp = $_FILES['myfile']['tmp_name'];
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.

		                	$arr = array('foto1' => $file,'status' => 'Sukses');
		                	
		                	//$this->checkFile($file,$supplier);
		                    
		                } else {
		                    $arr = array('status' => 'Failed','message'=>'Upload Failed');
		                    
		                }
		            } else {
		                $arr = array('status' => 'Failed','message'=>'Maksimum File Upload 2MB');
		            }
		        } else {
		            $arr = array('status' => 'Failed','message'=>'Format File Salah');
		        }
		    } else {
		        $arr = array('status' => 'Failed','message'=>'Silahkan Upload File Anda');
		    }

		    
		    $upload = json_encode($arr);
		    echo $upload;
		    exit;
		
	}

	function get_pjt_lit($params_pjt)
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		if(isset($params_pjt['area_id']))
		{
			$postData = [
			'area_id' => $params_pjt['area_id']
			];
		}
		else
		{
			$postData = [
			'wilayah_id' => $params_pjt['wilayah_id']
			];
		}
		

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/lit/pjt'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/lit/pjt'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;
	}

	function get_tt_lit($wilayah_id,$area_id)
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
			'wilayah_id' => $wilayah_id,
			'area_id' => $area_id //optional
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/lit/tt'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/lit/tt'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;
	}

	function add()
	{	

		$no_pendaftaran = $this->uri->segment(5);
		$no_pemeriksaan = $this->uri->segment(4);
		$no_verifikasi = $this->uri->segment(3);
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		
		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);

		$params_pjt = array_filter(array(
            'area_id' => $data['detail_pemeriksaan']->area_id,
            'wilayah_id' =>$data['detail_pemeriksaan']->wilayah_id
      	));

		$data_pjt_lit = $this->get_pjt_lit($params_pjt);
		$data_tt_lit = $this->get_tt_lit($data['detail_pemeriksaan']->wilayah_id,$data['detail_pemeriksaan']->area_id);

		$decode_pjt_lit = json_decode($data_pjt_lit,TRUE);

		if(empty($decode_pjt_lit['pjt']))
		{
			$params_pjt1 = array_filter(array(
            'wilayah_id' => $data['detail_pemeriksaan']->wilayah_id,
	      	));

			$data_pjt_lit1 = $this->get_pjt_lit($params_pjt1);

			$data['pjt_lit'] = json_decode($data_pjt_lit1,TRUE);
		}
		else
		{
			$data['pjt_lit'] = json_decode($data_pjt_lit,TRUE);
		}
		
		$data['tt_lit'] = json_decode($data_tt_lit,TRUE);
		
		//$data['verifikator'] = $this->Verifikasi_Model->get_detail_verifikator($no_pendaftaran);
		//$data['pemeriksa'] = $this->Verifikasi_Model->get_detail_pemeriksa($no_pendaftaran);
		
		$this->load->view('add',$data);

		
	
	}

	function cetak_ulang()
	{	

		$no_pendaftaran = $this->uri->segment(5);
		$no_pemeriksaan = $this->uri->segment(4);
		$no_verifikasi = $this->uri->segment(3);
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		
		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);

		$data['detail_sertifikasi'] = $this->Sertifikasi_Model->get_no_sertifikat($no_pendaftaran);

		$params_pjt = array_filter(array(
            'area_id' => $data['detail_pemeriksaan']->area_id,
      	));

		$data_pjt_lit = $this->get_pjt_lit($params_pjt);
		$data_tt_lit = $this->get_tt_lit($data['detail_pemeriksaan']->wilayah_id,$data['detail_pemeriksaan']->area_id);

		$decode_pjt_lit = json_decode($data_pjt_lit,TRUE);

		if(empty($decode_pjt_lit['pjt']))
		{
			$params_pjt1 = array_filter(array(
            'wilayah_id' => $data['detail_pemeriksaan']->wilayah_id,
	      	));

			$data_pjt_lit1 = $this->get_pjt_lit($params_pjt1);

			$data['pjt_lit'] = json_decode($data_pjt_lit1,TRUE);
		}
		else
		{
			$data['pjt_lit'] = json_decode($data_pjt_lit,TRUE);
		}

		$data['tt_lit'] = json_decode($data_tt_lit,TRUE);
		
		$this->load->view('cetak_ulang',$data);

		
	
	}

	function copy()
	{	

		$no_pendaftaran = $this->uri->segment(5);
		$no_pemeriksaan = $this->uri->segment(4);
		$no_verifikasi = $this->uri->segment(3);
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');

		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		
		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);

		$data['detail_sertifikasi'] = $this->Sertifikasi_Model->get_no_sertifikat($no_pendaftaran);

		$params_pjt = array_filter(array(
            'area_id' => $data['detail_pemeriksaan']->area_id,
      	));

		$data_pjt_lit = $this->get_pjt_lit($params_pjt);
		$data_tt_lit = $this->get_tt_lit($data['detail_pemeriksaan']->wilayah_id,$data['detail_pemeriksaan']->area_id);

		$decode_pjt_lit = json_decode($data_pjt_lit,TRUE);

		if(empty($decode_pjt_lit['pjt']))
		{
			$params_pjt1 = array_filter(array(
            'wilayah_id' => $data['detail_pemeriksaan']->wilayah_id,
	      	));

			$data_pjt_lit1 = $this->get_pjt_lit($params_pjt1);

			$data['pjt_lit'] = json_decode($data_pjt_lit1,TRUE);
		}
		else
		{
			$data['pjt_lit'] = json_decode($data_pjt_lit,TRUE);
		}

		$data['tt_lit'] = json_decode($data_tt_lit,TRUE);
		
		$this->load->view('copy',$data);

		
	
	}

	function cetak()
	{	
		
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$user_id = $this->session->userdata('user_id');
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$date = date('Y-m-d H:i:s');
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_pemeriksaan= $this->input->post('no_pemeriksaan');
			$no_lhpp = $this->input->post('no_lhpp');
			$no_seri = $this->input->post('no_seri');
			$pjt_lit1 = $this->input->post('pjt_lit');
			$tt_lit1 = $this->input->post('tt_lit');

			$pjt_explode = explode('|', $pjt_lit1);
			$tt_explode = explode('|', $tt_lit1);

			$nik_pjt = $pjt_explode['0'];
			$name_pjt = $pjt_explode['1'];
			$nik_tt = $tt_explode['0'];
			$name_tt = $tt_explode['1'];

			$detail_pelanggan = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
			$detail_pemeriksaan = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);
			$data_sertifikasi = $this->Sertifikasi_Model->get_all();

			$cek_sertifikat = $this->Sertifikasi_Model->get_by_no_sertifikat($no_pendaftaran);
			if(isset($cek_sertifikat->nomor_sertifikat) && $cek_sertifikat->nomor_sertifikat != "")
			{
				$this->session->set_flashdata('error', 'No Pendaftaran '.$no_pendaftaran.' sudah dicetak, silahkan masuk menu pencetakan ulang apabila ingin mencetak ulang');
				redirect('sertifikasi/belum_sertifikasi','refresh');
			}
		
			$random_huruf = $this->create_random(2);
			$random_angka = rand(1,9);
			$year = date('y');
			$no_urut = count($data_sertifikasi) + 1;

			/*
			echo "<pre>";
			print_r($detail_pelanggan);
			echo "</pre>";
			exit();
			*/
			$nomor_sertifikat = "".$random_angka.$random_huruf.$random_angka.".6".$detail_pelanggan->kode_djk.".".$detail_pelanggan->id_bangunan.".".$detail_pelanggan->kode_djp_kota.".".$detail_pelanggan->kodefikasi.".".$year."";
			//$nomor_sertifikat = '7BL7.618.7.7371.UE88.18';
			//var_dump($nomor_sertifikat);
			$cek_sertifikat = $this->Sertifikasi_Model->cek_sertifikat($no_pendaftaran,$nomor_sertifikat);

			if(isset($cek_sertifikat))
			{
					$this->session->set_flashdata('error', 'Nmor sertifikat tidak boleh duplikat');
					redirect('sertifikasi/belum_sertifikasi','refresh');
					exit();
			}
			
			

			//var_dump($registrasi_slo);

			try {

			$data_registrasi = array(
				  'no_agenda' => $detail_pelanggan->no_agenda,
				  'nama' => $detail_pelanggan->Nama,
				  'alamat' => $detail_pelanggan->alamat,
				  'kota_id' => $detail_pelanggan->kode_djp_kota,
				  'area_id' => $detail_pelanggan->area_id,
				  'no_telpon' => $detail_pelanggan->telp,
				  'email' => $detail_pelanggan->email_pelanggan,
				  'tarif' => $detail_pelanggan->nm_tarif,
				  'daya' => $detail_pelanggan->daya,
				  'instalatir' => $detail_pelanggan->nm_btl,
				  //'nik_pjt' => ''.$name_pjt.'/'.$nik_pjt.'',
				  //'nik_tt' => ''.$name_tt.'/'.$nik_tt.'',
				  'nik_pjt' => $nik_pjt,
				  'nik_tt' => $nik_tt,
				  'nomor_slo' => $nomor_sertifikat,
				  'url_detail_lhpp' => ''.base_url().'djk/detail_lhpp/'.$no_pendaftaran.'',
				  'url_koordinat_lokasi' => ''.base_url().'djk/lokasi/'.$no_pendaftaran.'',
				  'url_foto_pelaksanaan' => ''.base_url().'djk/foto/'.$no_pendaftaran.'',
				  'url_gil' => '',
				  'latitude' => $detail_pelanggan->lattitude,
				  'longitude' => $detail_pelanggan->longitude,
				  'no_agenda_pln' => $detail_pelanggan->noreg_pln,
				  'id_pel_pln' => '',
				  'unit_pelayanan_id' => $detail_pelanggan->kode_unit_pln
			);
			
			
			/*
			echo "<pre>";
			print_r($data_registrasi);
			echo "</pre>";
			exit();
			*/

			$registrasi_slo = json_decode($this->registrasi_slo($data_registrasi));

				if(isset($registrasi_slo->metadata->code) && $registrasi_slo->metadata->code == 0)
				{
					$error_message = $registrasi_slo->metadata->message;
					$this->session->set_flashdata('error', ''.$error_message.'');
					redirect('sertifikasi/belum_sertifikasi','refresh');
					exit();
					
				}

				if(isset($registrasi_slo->metadata->code) && $registrasi_slo->metadata->code == 1)
				{

					$data_sertifikasi = array(
						"no_pendaftaran"=> $no_pendaftaran,
						"nomor_sertifikat"=> $data_registrasi['nomor_slo'],
						"no_registrasi_slo"=> $registrasi_slo->response->no_registrasi,
						"user_id" => $user_id,
						"no_seri"=> $no_seri,
						"dtcreate" => $date,
						"nik_pjt"=> $nik_pjt,
						"nik_tt"=> $nik_tt,
						"nm_pjt"=> $name_pjt,
						"nm_tt"=> $name_tt,
						"tgl_terbit"=>$date
					);
			

					$data_pelanggan = array(
						"status_sertifikasi"=> 1,
					);

					$dt = new DateTime($date);
					$tgl_terbit_masa_berlaku = $dt->format('Y-m-d');

					$masa_berlaku = date('Y-m-d',strtotime('+15 years', strtotime($tgl_terbit_masa_berlaku)));
					$tgl_masa_berlaku = tgl_indo($masa_berlaku);

					$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);

					$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

						$html = "
						<style type='text/css'>
							<link href='".base_url()."template/cleandream/css/style_text.css'>
							
						
						</style>
						<div style='padding-top: 0px; padding-left: 90px; padding-bottom: 1px; font-family: Arial; overflow:hidden;'>
						<div>
							<p style='text-align: center; line-height:1%; font-size: 20px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'><strong>Kantor Wilayah ".$detail_pelanggan->nm_kanwil.", Area Pelayanan ".$detail_pelanggan->nm_area."</strong></p>
							<p style='text-align: center; line-height: 1; font-size: 12px;' padding-left:1px;>".$detail_pelanggan->alamat_area." Telp : ".$detail_pelanggan->telpon."</p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'>Penetapan Menteri Energi dan Sumber Daya Mineral Nomor 182 K/24/DJL.4/2017 Tahun 2017</p>
						</div>

					<p style='text-align: center; font-size: 20px;'><strong>SERTIFIKAT LAIK OPERASI</strong></p>
					<table style='height: 45px; width: 409px; margin-left: auto; margin-right: auto;'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Sertifikat</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$nomor_sertifikat."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Registrasi</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$registrasi_slo->response->no_registrasi."</td>
					</tr>
					</tbody>
					</table>
				
					<span style='font-size: 12px;'>Dengan ini menerangkan bahwa instalasi pemanfaatan tenaga listrik tegangan rendah:</span>
					<table style='font-size: 12px;'>
					<tbody>
					<tr>
					<td style='width: 300px;'>Nama Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->Nama."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Alamat Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->alamat.", ".$detail_pemeriksaan->nm_kota."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Titik Koordinat</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->lattitude.",".$detail_pemeriksaan->longitude."</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Daya Tersambung</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->daya." VA</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel&nbsp;Hubung Bagi Utama</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_phb_utama." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel Hubung Bagi 3 Phasa</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jumlah_phb_tiga_phasa." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Kotak Kontak</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->kkb." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Lampu</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_titik_lampu." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Sakelar</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_sakelar." buah</td>
					</tr>
					<tr>
					<tr>
					<td style='width: 300px;'>Penyedia Tenaga Listrik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->nm_ptl."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Penanggung Jawab Teknik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$name_pjt."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Nomor LHPP</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;' align='left'>".$detail_pemeriksaan->no_lhpp."</td>
					</tr>
					</tbody>
					</table>
					<div>
					<p style='font-size: 12px; line-height:1%;'>Telah sesuai dengan ketentuan keselamatan ketenagalistrikan sehingga dinyatakan :</p>
					<p style='text-align: center; font-size: 20px; line-height: 0.1em;'><strong>LAIK OPERASI</strong></p>
					<p style='font-size: 12px; line-height: 0.1em;'>Sertifikat Laik Operasi ini berlaku sampai dengan tanggal ".$tgl_masa_berlaku." sepanjang tidak ada perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</p>
					</div>
					
					<table align='right'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Ditetapkan di</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_pemeriksaan->nm_alias."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Pada Tanggal</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".tgl_indo(date('Y-m-d'))."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;' colspan='3'>
						<p style='text-align: right; font-size: 12px;'>General Manager / Manager</p>
						<br><br><br>
						<p style='text-align: right; font-size: 12px;'>".$detail_pemeriksaan->signature_name."</p></font>
					</td>
					</tr>
					</tbody>

					</table>
					</div>
					</body>";

					
						$mpdf = new \Mpdf\Mpdf();
						$mpdf->useFixedNormalLineHeight = false;
						$mpdf->useFixedTextBaseline = false;
						$mpdf->adjustFontDescLineheight = 1.14;
				       // Write some HTML code:
						$mpdf->AddPage('L');
						
				        $mpdf->WriteHTML($html);
				       
				        $file_name = "".date('Ymdhis')."-".$nomor_sertifikat."";
						/*
						if($mpdf->WriteHTML($html))
				       	{
				       		$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);
				       	}
						*/

				       // Output a PDF file directly to the browser
				       $mpdf->Output(''.$file_name.'.pdf', 'I');

				        $this->session->set_flashdata('success', 'Registrasi Slo Berhasil');
						redirect('sertifikasi/sudah_sertifikasi','refresh');
		       
					}
					else
					{

						$this->session->set_flashdata('error', 'Tidak dapat terhubung ke API DJK');
						redirect('sertifikasi/belum_sertifikasi','refresh');
						exit();
					}

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal cetak sertifikat');
				redirect('sertifikasi/belum_sertifikasi','refresh');
			}

				
			
		}
	}

	function cetak_ulang_sertifikat()
	{	
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$user_id = $this->session->userdata('user_id');
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$date = date('Y-m-d H:i:s');
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$nomor_sertifikat = $this->input->post('nomor_sertifikat');
			$no_pemeriksaan= $this->input->post('no_pemeriksaan');
			$no_lhpp = $this->input->post('no_lhpp');
			$no_seri = $this->input->post('no_seri');
			$pjt_lit = $this->input->post('pjt_lit');
			$tt_lit = $this->input->post('tt_lit');

			$detail_pelanggan = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
			$detail_pemeriksaan = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);
			$detail_sertifikasi = $this->Sertifikasi_Model->get_no_sertifikat($no_pendaftaran);
			$tgl_terbit = strtotime($detail_sertifikasi->tgl_terbit);

			$dt = new DateTime($detail_sertifikasi->tgl_terbit);
			$tgl_terbit_masa_berlaku = $dt->format('Y-m-d');

			$masa_berlaku = date('Y-m-d',strtotime('+15 years', strtotime($tgl_terbit_masa_berlaku)));
			$tgl_masa_berlaku = tgl_indo($masa_berlaku);
			
			try {

					$data_sertifikasi = array(
					"no_pendaftaran"=> $no_pendaftaran,
					"nomor_sertifikat"=> $nomor_sertifikat,
					"no_registrasi_slo"=> $detail_sertifikasi->no_registrasi_slo,
					"user_id" => $user_id,
					"no_seri"=> $no_seri,
					"dtcreate" => $date,
					"nik_pjt"=> $pjt_lit,
					"nik_tt"=> $tt_lit,
					"tgl_terbit"=>$detail_sertifikasi->tgl_terbit
					);
			
		

					$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);


						$html = "
						<style type='text/css'>
							<link href='".base_url()."template/cleandream/css/style_text.css'>
							
						
						</style>
						<div style='padding-top: 0px; padding-left: 90px; padding-bottom: 1px; font-family: Arial; overflow:hidden;'>
						<div>
							<p style='text-align: center; line-height:1%; font-size: 20px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'><strong>Kantor Wilayah ".$detail_pelanggan->nm_kanwil.", Area Pelayanan ".$detail_pelanggan->nm_area."</strong></p>
							<p style='text-align: center; line-height: 1; font-size: 12px;' padding-left:1px;>".$detail_pelanggan->alamat_area." Telp : ".$detail_pelanggan->telpon."</p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'>Penetapan Menteri Energi dan Sumber Daya Mineral Nomor 182 K/24/DJL.4/2017 Tahun 2017</p>
						</div>

					<p style='text-align: center; font-size: 20px;'><strong>SERTIFIKAT LAIK OPERASI</strong></p>
					<table style='height: 45px; width: 409px; margin-left: auto; margin-right: auto;'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Sertifikat</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$nomor_sertifikat."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Registrasi</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_sertifikasi->no_registrasi_slo."</td>
					</tr>
					</tbody>
					</table>
				
					<span style='font-size: 12px;'>Dengan ini menerangkan bahwa instalasi pemanfaatan tenaga listrik tegangan rendah:</span>
					<table style='font-size: 12px;'>
					<tbody>
					<tr>
					<td style='width: 300px;'>Nama Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->Nama."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Alamat Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->alamat.", ".$detail_pemeriksaan->nm_kota."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Titik Koordinat</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->lattitude.",".$detail_pemeriksaan->longitude."</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Daya Tersambung</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->daya." VA</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel&nbsp;Hubung Bagi Utama</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_phb_utama." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel Hubung Bagi 3 Phasa</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jumlah_phb_tiga_phasa." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Kotak Kontak</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->kkb." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Lampu</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_titik_lampu." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Sakelar</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_sakelar." buah</td>
					</tr>
					<tr>
					<tr>
					<td style='width: 300px;'>Penyedia Tenaga Listrik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->nm_ptl."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Penanggung Jawab Teknik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_sertifikasi->nm_pjt."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Nomor LHPP</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;' align='left'>".$detail_pemeriksaan->no_lhpp."</td>
					</tr>
					</tbody>
					</table>
					<div>
					<p style='font-size: 12px; line-height:1%;'>Telah sesuai dengan ketentuan keselamatan ketenagalistrikan sehingga dinyatakan :</p>
					<p style='text-align: center; font-size: 20px; line-height: 0.1em;'><strong>LAIK OPERASI</strong></p>
					<p style='font-size: 12px; line-height: 0.1em;'>Sertifikat Laik Operasi ini berlaku sampai dengan tanggal ".$tgl_masa_berlaku." sepanjang tidak ada perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</p>
					</div>
					<table align='right'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Ditetapkan di</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_pemeriksaan->nm_alias."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Pada Tanggal</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".tgl_indo(date('Y-m-d',$tgl_terbit))."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;' colspan='3'>
						<div>
						<p style='text-align: right; font-size: 12px; line-height: 5em;'>General Manager / Manager</p>
						<br><br><br>
						<p style='text-align: right; font-size: 12px; line-height: 5em;'>".$detail_pemeriksaan->signature_name."</p></font>
						</div>
					</td>
					</tr>
					</tbody>

					</table>
					</div>
					</body>
					";
						/*
						print_r($html);
						exit();
						*/

						$mpdf = new \Mpdf\Mpdf();
						$mpdf->useFixedNormalLineHeight = false;
						$mpdf->useFixedTextBaseline = false;
						$mpdf->adjustFontDescLineheight = 1.14;
				       // Write some HTML code:
						$mpdf->AddPage('L');
						
				        $mpdf->WriteHTML($html);
				       
				        $file_name = "".date('Ymdhis')."-".$nomor_sertifikat."";
						/*
						if($mpdf->WriteHTML($html))
				       	{
				       		$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);
				       	}
						*/

				       // Output a PDF file directly to the browser
				       $mpdf->Output(''.$file_name.'.pdf', 'I');

				        $this->session->set_flashdata('success', 'Cetak Ulang Berhasil');
						redirect('sertifikasi/sudah_sertifikasi','refresh');
		       


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal cetak sertifikat');
				redirect('sertifikasi/belum_sertifikasi','refresh');
			}

				
			
		}
	}
	
	function copy_sertifikat()
	{	
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$user_id = $this->session->userdata('user_id');
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$date = date('Y-m-d H:i:s');
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$nomor_sertifikat = $this->input->post('nomor_sertifikat');
			$no_pemeriksaan= $this->input->post('no_pemeriksaan');
			$no_lhpp = $this->input->post('no_lhpp');
			$no_seri = $this->input->post('no_seri');
			$pjt_lit = $this->input->post('pjt_lit');
			$tt_lit = $this->input->post('tt_lit');

			$detail_pelanggan = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
			$detail_pemeriksaan = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);
			$detail_sertifikasi = $this->Sertifikasi_Model->get_no_sertifikat($no_pendaftaran);
			$tgl_terbit = strtotime($detail_sertifikasi->tgl_terbit);

			$dt = new DateTime($detail_sertifikasi->tgl_terbit);
			$tgl_terbit_masa_berlaku = $dt->format('Y-m-d');

			$masa_berlaku = date('Y-m-d',strtotime('+15 years', strtotime($tgl_terbit_masa_berlaku)));
			$tgl_masa_berlaku = tgl_indo($masa_berlaku);

			try {

					$data_sertifikasi = array(
					"no_pendaftaran"=> $no_pendaftaran,
					"nomor_sertifikat"=> $nomor_sertifikat,
					"no_registrasi_slo"=> $detail_sertifikasi->no_registrasi_slo,
					"user_id" => $user_id,
					"no_seri"=> $no_seri,
					"dtcreate" => $date,
					"nik_pjt"=> $pjt_lit,
					"nik_tt"=> $tt_lit,
					"tgl_terbit"=>$detail_sertifikasi->tgl_terbit
					);
			
		

						$html = "
						<style type='text/css'>
							<link href='".base_url()."template/cleandream/css/style_text.css'>
							
						
						</style>
						<div style='padding-top: 0px; padding-left: 90px; padding-bottom: 1px; font-family: Arial; overflow:hidden;'>
						<div>
							<p style='text-align: center; line-height:1%; font-size: 20px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'><strong>Kantor Wilayah ".$detail_pelanggan->nm_kanwil.", Area Pelayanan ".$detail_pelanggan->nm_area."</strong></p>
							<p style='text-align: center; line-height: 1; font-size: 12px;' padding-left:1px;>".$detail_pelanggan->alamat_area." Telp : ".$detail_pelanggan->telpon."</p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'>Penetapan Menteri Energi dan Sumber Daya Mineral Nomor 182 K/24/DJL.4/2017 Tahun 2017</p>
						</div>


					
					<p style='text-align: center; font-size: 20px;'><strong>SERTIFIKAT LAIK OPERASI</strong></p>
					<table style='height: 45px; width: 409px; margin-left: auto; margin-right: auto;'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Sertifikat</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$nomor_sertifikat."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Registrasi</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_sertifikasi->no_registrasi_slo."</td>
					</tr>
					</tbody>
					</table>
				
					<span style='font-size: 12px;'>Dengan ini menerangkan bahwa instalasi pemanfaatan tenaga listrik tegangan rendah:</span>
					<table style='font-size: 12px;'>
					<tbody>
					<tr>
					<td style='width: 300px;'>Nama Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->Nama."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Alamat Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->alamat.", ".$detail_pemeriksaan->nm_kota."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Titik Koordinat</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->lattitude.",".$detail_pemeriksaan->longitude."</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Daya Tersambung</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->daya." VA</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel&nbsp;Hubung Bagi Utama</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_phb_utama." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel Hubung Bagi 3 Phasa</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jumlah_phb_tiga_phasa." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Kotak Kontak</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->kkb." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Lampu</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_titik_lampu." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Sakelar</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->jml_sakelar." buah</td>
					</tr>
					<tr>
					<tr>
					<td style='width: 300px;'>Penyedia Tenaga Listrik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_pemeriksaan->nm_ptl."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Penanggung Jawab Teknik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;'>".$detail_sertifikasi->nm_pjt."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Nomor LHPP</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 500px;' align='left'>".$detail_pemeriksaan->no_lhpp."</td>
					</tr>
					</tbody>
					</table>
					<div>
					<p style='font-size: 12px; line-height:1%;'>Telah sesuai dengan ketentuan keselamatan ketenagalistrikan sehingga dinyatakan :</p>
					<p style='text-align: center; font-size: 20px; line-height: 0.1em;'><strong>LAIK OPERASI</strong></p>
					<p style='font-size: 12px; line-height: 0.1em;'>Sertifikat Laik Operasi ini berlaku sampai dengan tanggal ".$tgl_masa_berlaku." sepanjang tidak ada perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</p>
					</div>
					<table align='right'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Ditetapkan di</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_pemeriksaan->nm_alias."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Pada Tanggal</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".tgl_indo(date('Y-m-d',$tgl_terbit))."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;' colspan='3'>
						<p style='text-align: right; font-size: 12px;'>General Manager / Manager</p>
						<br><br><br>
						<p style='text-align: right; font-size: 12px;'>".$detail_pemeriksaan->signature_name."</p></font>
					</td>
					</tr>
					</tbody>

					</table>
					</div>
					</body>
					";
						/*
						print_r($html);
						exit();
						*/

						$mpdf = new \Mpdf\Mpdf();
						$mpdf->useFixedNormalLineHeight = false;
						$mpdf->useFixedTextBaseline = false;
						$mpdf->adjustFontDescLineheight = 1.14;
				       // Write some HTML code:
						$mpdf->AddPage('L');
						
				        $mpdf->WriteHTML($html);
				       
				        $file_name = "".date('Ymdhis')."-".$nomor_sertifikat."";
						/*
						if($mpdf->WriteHTML($html))
				       	{
				       		$insert_sertifikasi = $this->Sertifikasi_Model->insert($data_sertifikasi);
				       	}
						*/

				       // Output a PDF file directly to the browser
				       $mpdf->Output(''.$file_name.'.pdf', 'I');

				        $this->session->set_flashdata('success', 'Copy Sertifikat Berhasil');
						redirect('sertifikasi/sudah_sertifikasi','refresh');
		       


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal cetak sertifikat');
				redirect('sertifikasi/belum_sertifikasi','refresh');
			}

				
			
		}
	}

	function create_random($length)
	{
	    $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $string = '';
	    for($i = 0; $i < $length; $i++) {
	        $pos = rand(0, strlen($data)-1);
	        $string .= $data{$pos};
	    }
	    return $string;
	}

	function create_random_angka($length)
	{
	    $data = '123456789';
	    $string = '';
	    for($i = 0; $i < $length; $i++) {
	        $pos = rand(0, strlen($data)-1);
	        $string .= $data{$pos};
	    }
	    return $string;
	}

	function registrasi_slo($data_registrasi)
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
		  'no_agenda' => $data_registrasi['no_agenda'],
		  'nama' => $data_registrasi['nama'],
		  'alamat' => $data_registrasi['alamat'],
		  'kota_id' => $data_registrasi['kota_id'],
		  'area_id' => $data_registrasi['area_id'],
		  'no_telpon' => $data_registrasi['no_telpon'],
		  'email' => $data_registrasi['email'],
		  'tarif' => $data_registrasi['tarif'],
		  'daya' => $data_registrasi['daya'],
		  'instalatir' => $data_registrasi['instalatir'],
		  'nik_pjt' => $data_registrasi['nik_pjt'],
		  'nik_tt' => $data_registrasi['nik_tt'],
		  'nomor_slo' => $data_registrasi['nomor_slo'],
		  'url_detail_lhpp' => $data_registrasi['url_detail_lhpp'],
		  'url_koordinat_lokasi' => $data_registrasi['url_koordinat_lokasi'],
		  'url_foto_pelaksanaan' => $data_registrasi['url_foto_pelaksanaan'],
		  'url_gil' => $data_registrasi['url_gil'],
		  'latitude' => $data_registrasi['latitude'],
		  'longitude' => $data_registrasi['longitude'],
		  'no_agenda_pln' => $data_registrasi['no_agenda_pln'],
		  'id_pel_pln' => $data_registrasi['id_pel_pln'],
		  'unit_pelayanan_id' => $data_registrasi['unit_pelayanan_id']
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/tr/slo/register'); 
		
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/tr/permohonan/create'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;
	}

	function sertifikat_template()
	{
		$this->load->view('sertifikat_template');
	}
}
