<?php

class Sertifikasi_model extends CI_Model {

    var $column_order_belum_terbit = array('pelanggan.no_pendaftaran','pelanggan.Nama','nm_kanwil','nm_kota','nm_area','nm_sub_area','pemeriksaan.no_lhpp','nm_btl'); //set column field database for datatable orderable
    var $column_search_belum_terbit = array('pelanggan.no_pendaftaran','pelanggan.Nama','nm_kanwil','nm_kota','nm_area','nm_sub_area','pemeriksaan.no_lhpp','nm_btl');  //set column field database for datatable 
    var $order_belum_terbit = array('pelanggan.created_at' => 'desc'); // default order

    var $column_order_sudah_terbit = array('pelanggan.no_pendaftaran','pelanggan.Nama','nm_kanwil','nm_kota','nm_area','nm_sub_area','pemeriksaan.no_lhpp','nm_btl','no_seri','tgl_terbit','Name'); //set column field database for datatable orderable
    var $column_search_sudah_terbit = array('pelanggan.no_pendaftaran','pelanggan.Nama','nm_kanwil','nm_kota','nm_area','nm_sub_area','pemeriksaan.no_lhpp','nm_btl','no_seri','tgl_terbit','Name');  //set column field database for datatable 
    var $order_sudah_terbit = array('pelanggan.created_at' => 'desc');  // default order

    private function _get_datatables_belum_terbit_query($params)
     {
        /*
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
      
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        */

        $this->db->select('*,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        /*
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        */
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        /*
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        */
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }
        
        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 2);

        $i = 0;
     
        foreach ($this->column_search_belum_terbit as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_belum_terbit) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_belum_terbit[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_belum_terbit))
        {
            $order = $this->order_belum_terbit;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        //$this->db->group_by('pelanggan.no_pendaftaran');
    }

    function get_datatables_belum_terbit($params)
    {
        $this->_get_datatables_belum_terbit_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
       // $this->db->limit('5', '1');
        $query = $this->db->get();
       // return $this->db->last_query();
       return $query->result();
    }
 
    function count_filtered_belum_terbit($params)
    {
        /*
        $this->_get_datatables_belum_terbit_query($params);
        $query = $this->db->get();
        return $query->num_rows();
        */

        $this->db->select('count(*) jumlah');
        $this->db->from('pelanggan');
       
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 2); 

        $query = $this->db->get();
        return $query->row();

    }
 
    public function count_all_belum_terbit($params)
    {
        $this->db->select('count(*) jumlah');
        $this->db->from('pelanggan');
       
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 2); 

        $query = $this->db->get();
        return $query->row();
    }



    private function _get_datatables_sudah_terbit_query($params)
     {
        $this->db->select('*,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
  
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('sertifikasi','pelanggan.no_pendaftaran=sertifikasi.no_pendaftaran');
        $this->db->join('core_user','sertifikasi.user_id=core_user.coreUserId','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);

        $i = 0;
     
        foreach ($this->column_search_sudah_terbit as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_sudah_terbit) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sudah_terbit[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_sudah_terbit))
        {
            $order = $this->order_sudah_terbit;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    
       
    }

    function get_datatables_sudah_terbit($params)
    {
        $this->_get_datatables_sudah_terbit_query($params);
        if($_POST['length'] != -1)
       $this->db->limit($_POST['length'], $_POST['start']);
        //$this->db->limit('5', '1');
        $query = $this->db->get();
        //return $this->db->last_query();
        return $query->result();
    }
 
    function count_filtered_sudah_terbit($params)
    {
        /*
        $this->_get_datatables_sudah_terbit_query($params);
        $query = $this->db->get();
        return $query->num_rows();
        */

        $this->db->select('count(*) jumlah');
        $this->db->from('pelanggan');
       
        
        $this->db->join('sertifikasi','pelanggan.no_pendaftaran=sertifikasi.no_pendaftaran');

        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);

        $query = $this->db->get();
        return $query->row();
    }
 
    public function count_all_sudah_terbit($params)
    {
        $this->db->select('count(*) jumlah');
        $this->db->from('pelanggan');
       
        
        $this->db->join('sertifikasi','pelanggan.no_pendaftaran=sertifikasi.no_pendaftaran');

        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);

        $query = $this->db->get();
        return $query->row();
    }


   function get_last_no_sertifikat(){
		$this->db->select('nomor_sertifikat');
        $this->db->from('sertifikasi');
        $this->db->order_by('id_sertifikasi');
        $this->db->limit(1);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

     function get_by_no_sertifikat($no_pendaftaran){
        $this->db->select('nomor_sertifikat');
        $this->db->from('sertifikasi');
        $this->db->where('no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function get_all(){
        $this->db->select('*');
        $this->db->from('sertifikasi');
     
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

     function get_detail_pelanggan($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_sertifikasi_sudah_terbit($params){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);
  

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

     function get_laporan_sertifikasi_sudah_terbit($params){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);
       // $this->db->limit('30');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function data_sertifikasi_last($no_pendaftaran){
        $this->db->select('nomor_sertifikat,no_seri,max(id_sertifikasi) as id_sertifikasi,dtcreate,user_id,nik_pjt,nik_tt,Name');
        $this->db->from('sertifikasi');
        $this->db->join('core_user','sertifikasi.user_id=core_user.coreUserId','left');
        $this->db->where('no_pendaftaran =', $no_pendaftaran);

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_sertifikat($no_pendaftaran,$nomor_sertifikat){
        $this->db->select('*');
        $this->db->from('sertifikasi');
        $this->db->where('no_pendaftaran !=', $no_pendaftaran);
        $this->db->where('nomor_sertifikat =', $nomor_sertifikat);

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function data_sertifikasi_sebelumnya($no_pendaftaran,$nomor_sertifikat){
        $this->db->select('nomor_sertifikat');
        $this->db->from('sertifikasi');
        $this->db->join('core_user','sertifikasi.user_id=core_user.coreUserId');
        $this->db->where('no_pendaftaran =', $no_pendaftaran);
        $this->db->where('nomor_sertifikat !=', $nomor_sertifikat);

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_no_sertifikat($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('sertifikasi');
        $this->db->where('no_pendaftaran =', $no_pendaftaran);

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_verifikator($params){
        $this->db->select('*');
        $this->db->from('verifikator');

        if(isset($params['kode_area']))
        {
        
        $this->db->where('kode_area',$params['kode_area']);
        }

        
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota(){
        $this->db->select('*');
        $this->db->from('kota');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl(){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl(){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($data_sertifikasi) {
       $query = $this->db->insert('sertifikasi', $data_sertifikasi);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

  
    function update($data, $id_provinsi){
        $this->db->trans_begin();
		$this->db->where('id_provinsi', $id_provinsi);
		$this->db->update('provinsi', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

}

