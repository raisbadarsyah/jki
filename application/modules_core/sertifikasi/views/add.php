
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT. JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<!-- /theme JS files -->

	
    
    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/gallery_library.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location.js"></script>

	

	
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Sertifikasi</li>
							<li>Belum Terbit</li>
							<li class="active">Cetak Sertifikat</li>
						</ul>

					
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<div class="row">

							<div class="col-md-12">

							<!-- Basic layout-->
							<form action="<?php echo base_url().'sertifikasi/cetak'; ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Sertifikat</h5>
										Durasi waktu penerbitan SLO dihitung mulai dari tanggal bayar atau tanggal siap, Dimohon penerbitan SLO diselesaikan maksimal 3 hari kerja
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">
										
										<?php if ($detail_pemeriksaan->no_agenda == ''): ?>
							                <div class="alert alert-warning alert-styled-left">Sertifikat ini belum dapat dicetak karena belum mendapatkan No. Agenda dari DJK. Klik <a href="<?php echo base_url()?>pelanggan/add_agenda/<?php echo $detail_pemeriksaan->no_pendaftaran;?>">di sini</a> untuk mendapatkan No. Agenda</div>
							            <?php endif; ?>
							            <input type="hidden" name="no_pemeriksaan" value="<?php echo $detail_pemeriksaan->no_pemeriksaan ?>">
										<div class="form-group">
											<label class="col-lg-3 control-label">No Pendaftaran:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_pendaftaran" value="<?php echo $detail_pemeriksaan->no_pendaftaran ?>" readonly>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No Agenda:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_agenda" value="<?php echo $detail_pemeriksaan->no_agenda ?>" readonly>
											</div>
										</div>


										<div class="form-group">
											<label class="col-lg-3 control-label">No LHPP:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_lhpp" value="<?php echo $detail_pemeriksaan->no_lhpp ?>" readonly>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Kantor Wilayah:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_kanwil ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Kantor Area:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_area ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Kantor Sub Area:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_sub_area ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jenis Bangunan:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_bangunan ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Pelanggan:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->Nama ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Alamat:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->alamat ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nomor Agenda:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->noreg_pln ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Daya Tersambung:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->daya ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penyedia Tenaga Listrik:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_ptl ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Biro Teknik Listrik:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->nm_btl ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah PHB:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_phb_utama ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Saluran Cabang:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_saluran_cabang ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Titik Lampu:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_titik_lampu ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Sakelar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_sakelar ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKB:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->kkb ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKK:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->kkk ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penanda Tangan:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->signature_name ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">PJT:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->pjt ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nomor Seri:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_seri" required>
											</div>
										</div>
										<!--
										<div class="form-group">
											<label class="col-lg-3 control-label">PJT Untuk Cetak:</label>
											<div class="col-lg-9">
												<select class="select" name="pjt_lit" id="pjt_lit" required="">
																<option value="">-- Pilih PJT --</option>
																<?php foreach ($verifikator as $key => $value) {?>

																<option value="<?php echo $value->nip;?>|<?php echo $value->Name;?>"><?php  echo $value->nip;?>: <?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">TT Untuk Cetak:</label>
											<div class="col-lg-9">
												<select class="select" name="tt_lit" id="tt_lit" required="">
																<option value="">-- Pilih TT --</option>
																<?php foreach ($pemeriksa as $key => $value) {?>

																<option value="<?php echo $value->nip;?>|<?php echo $value->Name;?>"><?php  echo $value->nip;?>: <?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>
										-->
									
										<div class="form-group">
											<label class="col-lg-3 control-label">PJT Untuk Cetak:</label>
											<div class="col-lg-9">
												<select class="select" name="pjt_lit" id="pjt_lit" required="">
																<option value="">-- Pilih PJT --</option>
																<?php foreach ($pjt_lit['pjt'] as $key => $value) {?>

																<option value="<?php echo $value['nik'];?>|<?php echo $value['nama'];?>"><?php  echo $value['nik'];?>: <?php  echo $value['nama'];?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">TT Untuk Cetak:</label>
											<div class="col-lg-9">
												<select class="select" name="tt_lit" id="tt_lit" required="">
																<option value="">-- Pilih TT --</option>
																<?php foreach ($tt_lit['tt'] as $key => $value) {?>

																<option value="<?php echo $value['nik'];?>|<?php echo $value['nama'];?>"><?php  echo $value['nik'];?>: <?php  echo $value['nama'];?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>
										
										<strong>Setelah dicetak data tidak dapat dihapus dan diedit. Pastikan data pelanggan tersebut sudah benar.</strong>
										<div class="text-right">
											<?php if($detail_pemeriksaan->no_agenda == '') { ?>
											
											<button type="submit" id="mySubmit" class="btn btn-primary">Cetak <i class="icon-arrow-right14 position-right" disabled></i></button>

											<script type="text/javascript">
												document.getElementById("mySubmit").disabled = true;
											</script>
											<?php } else {?>
												<button type="submit" id="mySubmit" class="btn btn-primary">Permohonan Registrasi <i class="icon-arrow-right14 position-right" disabled></i></button>
											<?php } ?>
										</div>
									</div>
								</div>
							</form>
							<!-- /basic layout -->

						</div>

					<!-- 2 columns form -->
			
				

							<!-- /basic layout -->
				</div>
			</div>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
