<style type='text/css'>
							<link href='".base_url()."template/cleandream/css/style_text.css'>
							
						
						</style>
						<div style='padding-top: 0px; padding-left: 90px; padding-bottom: 1px; font-family: Arial; overflow:hidden;'>
						<div>
							<p style='text-align: center; line-height:1%; font-size: 20px;'><strong>PT JASA KELISTRIKAN INDONESIA</strong></p>
							<p style='text-align: center; line-height: 0.1em; font-size: 12px;'><strong>Kantor Wilayah ".$detail_pelanggan->nm_kanwil.", Area Pelayanan ".$detail_pelanggan->nm_area."</strong></p>
							<p style='text-align: center; line-height: 1; font-size: 12px;' padding-left:1px;>".$detail_pelanggan->alamat_area." Telp : ".$detail_pelanggan->telpon."</p>
						</div>


					<p style='text-align: center; line-height: 0.5em; font-size: 12px;'>Penetapan Menteri Energi dan Sumber Daya Mineral Nomor 182 K/24/DJL.4/2017 Tahun 2017</p>
					<p style='text-align: center; font-size: 20px;'><strong>SERTIFIKAT LAIK OPERASI</strong></p>
					<table style='height: 45px; width: 409px; margin-left: auto; margin-right: auto;'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Sertifikat</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$nomor_sertifikat."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Nomor Registrasi</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_sertifikasi->no_registrasi_slo."</td>
					</tr>
					</tbody>
					</table>
				
					<span style='font-size: 12px;'>Dengan ini menerangkan bahwa instalasi pemanfaatan tenaga listrik tegangan rendah:</span>
					<table style='font-size: 12px;'>
					<tbody>
					<tr>
					<td style='width: 300px;'>Nama Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->Nama."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Alamat Pemilik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->alamat."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Titik Koordinat</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->lattitude.",".$detail_pemeriksaan->longitude."</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Daya Tersambung</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->daya." VA</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel&nbsp;Hubung Bagi Utama</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->jml_phb_utama." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Panel Hubung Bagi 3 Phasa</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->jml_saluran_cabang." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Kotak Kontak</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->kkk." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Titik Lampu</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->jml_titik_lampu." buah</td>
					</tr>
					<tr>
					<td style='width: 152px;'>Jumlah Sakelar</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->jml_sakelar." 5 buah</td>
					</tr>
					<tr>
					<tr>
					<td style='width: 300px;'>Penyedia Tenaga Listrik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->nm_ptl."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Penanggung Jawab Teknik</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;'>".$detail_pemeriksaan->pjt."</td>
					</tr>
					<tr>
					<td style='width: 300px;'>Nomor LHPP</td>
					<td style='width: 10px;'>:</td>
					<td style='width: 400px;' align='left'>".$detail_pemeriksaan->no_lhpp."</td>
					</tr>
					</tbody>
					</table>
					<p style='font-size: 12px;'>Telah sesuai dengan ketentuan keselamatan ketenagalistrikan sehingga dinyatakan :</p>
					<p style='text-align: center; font-size: 20px;'><strong>LAIK OPERASI</strong></p>
					<p style='font-size: 12px;'>Sertifikat Laik Operasi ini berlaku sampai dengan tanggal 20 April 2033 sepanjang tidak ada perubahan kapasitas, perubahan instalasi, direkondisi atau direlokasi</p>
					
					<table align='right'>
					<tbody>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Ditetapkan di</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".$detail_pemeriksaan->nm_alias."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;'>Pada Tanggal</td>
					<td style='width: 10px; font-size: 12px;'>:</td>
					<td style='width: 225px; font-size: 12px;'>".tgl_indo(date('Y-m-d',$tgl_terbit))."</td>
					</tr>
					<tr>
					<td style='width: 152px; font-size: 12px;' colspan='3'>
						<p style='text-align: right; font-size: 12px;'>General Manager / Manager</p>
						<br>
						<p style='text-align: right; font-size: 12px;'>".$detail_pemeriksaan->signature_name."</p></font>
					</td>
					</tr>
					</tbody>

					</table>
					</div>
					</body>