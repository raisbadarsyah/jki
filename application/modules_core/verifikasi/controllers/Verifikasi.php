<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi extends CI_Controller {

	function __construct()
	{
		ini_set('memory_limit', '-1');
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan/Pelanggan_Model','Staff/Staff_Model','Pembayaran/Pembayaran_Model','Pemeriksaan/Pemeriksaan_Model','Verifikasi_Model'));
		$this->load->helper(array('check_auth_menu','tgl_indonesia'));
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_all();


		$this->load->view('index',$data);
	}

	function detail_pelanggan()
	{
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');
		$no_pendaftaran = $this->input->post('no_pendaftaran');
		
		$config['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		
		$this->load->view('v_detail_pelanggan', $config);
	}

	function proses_pemeriksaan()
	{
		$user_id = $this->session->userdata('user_id');
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			

			exit();
			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_kwitansi = date('YmdHis');
			$tgl_bayar = date('Y-m-d H:i:s');
			$diterima_oleh = $user_id;
			$harga = $this->input->post('harga');

			try {
				
				$data_pembayaran = array(
					"no_kwitansi"=> $no_kwitansi,
					"no_pendaftaran"=> $no_pendaftaran,
					"tgl_bayar"=> $tgl_bayar,
					"diterima_oleh"=> $diterima_oleh,
					"harga"=> $harga,
				);

				$data_pelanggan = array(
					"status_pembayaran"=> 1,
				);
				
			
				$insert_pembayaran = $this->Pembayaran_Model->insert($data_pembayaran);
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pembayaran == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pembayaran/belum_bayar','refresh');
				}


		}
	}

	function belum_diverifikasi()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));
		
		$data['id_departemen'] = $id_departemen;
		//$data['pelanggan'] = $this->Pelanggan_Model->get_belum_diverifikasi($params);

		//$data['limit_pemeriksaan'] = date('H:i:s',mktime(48,0,0));

		$this->load->view('belum_diverifikasi',$data);
	}

	function querypagingbelumverifikasi()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Verifikasi_Model->get_datatables_belum_verifikasi($params);
	 	
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_verifikasi) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			if($id_departemen == 7)
			{
				$button = "<a href='".base_url()."verifikasi/add/".$data_verifikasi->no_pemeriksaan."/".$data_verifikasi->no_pendaftaran."' class='btn btn-default btn-small'>Verifikasi</a>";

			}else{


				 $button = "";
			}

			
            $row[] = $data_verifikasi->no_pendaftaran;
            $row[] = $data_verifikasi->Nama;
            $row[] = $data_verifikasi->nm_kota;
            $row[] = $data_verifikasi->nm_kanwil;
            $row[] = $data_verifikasi->nm_area;
            $row[] = $data_verifikasi->nm_sub_area;
            $row[] = $data_verifikasi->no_lhpp;
            $row[] = $data_verifikasi->nm_btl;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        //$count = $this->Verifikasi_Model->count_filtered_belum_verifikasi($params);
 		$count_all = $this->Verifikasi_Model->count_all_belum_verifikasi($params);

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $count_all->jumlah,
                        "recordsFiltered" => count($list),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function querypagingsudahverifikasi()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Verifikasi_Model->get_datatables_sudah_verifikasi($params);
	 	/*
	 	echo "<pre>";
	 	print_r($list);
	 	echo "</pre>";
	 	exit();
	 	*/
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_verifikasi) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			if($id_departemen == 7)
			{
				
				$button = "<a class='btn bg-slate btn-icon' href='".base_url()."verifikasi/view/".$data_verifikasi->no_verifikasi."' title='Buat No Agenda'><i class='fa fa-search'></i></a>

						  <a class='btn bg-slate btn-icon' href='".base_url()."verifikasi/printverifikasi/".$data_verifikasi->no_verifikasi."' title='Buat No Agenda' target='_blank'><i class='fa fa-print'></i></a>";

			}else{


				 $button = "";
			}

			
            $row[] = $data_verifikasi->no_pendaftaran;
            $row[] = $data_verifikasi->Nama;
            $row[] = $data_verifikasi->nm_kota;
            $row[] = $data_verifikasi->nm_kanwil;
            $row[] = $data_verifikasi->nm_area;
            $row[] = $data_verifikasi->nm_sub_area;
            $row[] = $data_verifikasi->nomor_lhpp;
            $row[] = date('d F Y', strtotime($data_verifikasi->verifikasi_at));
            $row[] = $data_verifikasi->hasil_pemeriksaan;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $count = $this->Verifikasi_Model->count_filtered_sudah_verifikasi($params);
 		$count_all = $this->Verifikasi_Model->count_all_sudah_verifikasi($params);

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $count_all->jumlah,
                        "recordsFiltered" => $count->jumlah,
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function sudah_diverifikasi()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));


	//	$data['pelanggan'] = $this->Pelanggan_Model->get_sudah_diverifikasi($params);

		$data['limit_pemeriksaan'] = date('H:i:s',mktime(48,0,0));

		$this->load->view('sudah_diverifikasi',$data);
	}

	function sudah_bayar()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['pelanggan'] = $this->Pelanggan_Model->get_sudah_bayar();

		$this->load->view('sudah_bayar',$data);
	}

	function uploadFile()
	{	
			header('Content-type: application/json');
			$created_at = Date("YmdHis");
			$path = "./uploads/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("jpg", "jpeg", "png"); //add the formats you want to upload

		    $name = $_FILES['myfile']['name']; //get the name of the file
		    
		    $size = $_FILES['myfile']['size']; //get the size of the file

		  
		    if (strlen($name)) { //check if the file is selected or cancelled after pressing the browse button.
		        list($txt, $ext) = explode(".", $name); //extract the name and extension of the file
		        if (in_array($ext, $valid_formats)) { //if the file is valid go on.
		            if ($size < 2098888) { // check if the file size is more than 2 mb
		                $file_name = $created_at; //get the file name
		                $file = $file_name.'.'.$ext;
		                $tmp = $_FILES['myfile']['tmp_name'];
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.

		                	$arr = array('foto1' => $file,'status' => 'Sukses');
		                	
		                	//$this->checkFile($file,$supplier);
		                    
		                } else {
		                    $arr = array('status' => 'Failed','message'=>'Upload Failed');
		                    
		                }
		            } else {
		                $arr = array('status' => 'Failed','message'=>'Maksimum File Upload 2MB');
		            }
		        } else {
		            $arr = array('status' => 'Failed','message'=>'Format File Salah');
		        }
		    } else {
		        $arr = array('status' => 'Failed','message'=>'Silahkan Upload File Anda');
		    }

		    
		    $upload = json_encode($arr);
		    echo $upload;
		    exit;
		
	}

	function view()
	{
		$no_verifikasi = $this->uri->segment(3);
		$user_id = $this->session->userdata('user_id');
		$id_departemen = $this->session->userdata('id_departemen');
		$position_id = $this->session->userdata('position_id');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);
		$kode_area = $get_detail_staff->kode_area;

		$params = array_filter(array(
            'kode_area' => $kode_area));

		
		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);

		$data['detail_verifikasi'] = $this->Verifikasi_Model->get_detail_verifikasi($no_verifikasi);
		$this->load->view('view',$data);
	}

	function printverifikasi()
	{
		$no_verifikasi = $this->uri->segment(3);
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$now = date('Y-m-d');
		$data['tgl_sekarang'] = tgl_indo($now);

		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);
		
		$kode_area = $get_detail_staff->kode_area;

		$params = array_filter(array(
            'kode_area' => $kode_area));

		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);
		$data['detail_staff'] = $get_detail_staff;
		
		$data['detail_verifikasi'] = $this->Verifikasi_Model->get_detail_verifikasi($no_verifikasi);
		$data['detail_pelanggan'] = $this->Verifikasi_Model->get_detail_pelanggan($data['detail_verifikasi']->no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Verifikasi_Model->get_detail_pemeriksaan($data['detail_verifikasi']->no_pendaftaran);
		$data_perbaikan = json_decode($data['detail_verifikasi']->ceklist_ppu);
	
		$data['data_perbaikan'] = $data_perbaikan;
		
		$this->load->view('print',$data);
	}

	function add()
	{	

		$no_pendaftaran = $this->uri->segment(4);
		$no_pemeriksaan = $this->uri->segment(3);
		$position_id = $this->session->userdata('position_id');
		$user_id = $this->session->userdata('user_id');
		$approve  = $this->input->post('approve');
		$reject  = $this->input->post('reject');

		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));


		$get_detail_staff = $this->Staff_Model->get_detail_staff($user_id,$params_staff);

		
		$data['detail_pelanggan'] = $this->Pembayaran_Model->get_detail_pelanggan($no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Pemeriksaan_Model->get_detail_pemeriksaan($no_pemeriksaan);
		

		//var_dump($data['detail_pemeriksaan']);
		$kode_area = $get_detail_staff->kode_area;

		$params = array_filter(array(
            'kode_area' => $kode_area));

		$data['user'] = $get_detail_staff;
		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);
		$data['ceklist_ppu'] = $this->Verifikasi_Model->get_ceklist_ppu();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$no_lhpp = $this->input->post('no_lhpp');
			$pemeriksaan_dokumen = $this->input->post('pemeriksaan_dokumen');
			$proteksi_sentuh_langsung = $this->input->post('proteksi_sentuh_langsung');
			$pemeriksaan_penghantar = $this->input->post('pemeriksaan_penghantar');
			$pemeriksaan_phb = $this->input->post('pemeriksaan_phb');
			$pemeriksaan_elektroda = $this->input->post('pemeriksaan_elektroda');
			$pemeriksaan_polaritas = $this->input->post('pemeriksaan_polaritas');
			$pemeriksaan_pemasangan = $this->input->post('pemeriksaan_pemasangan');
			$pemeriksaan_sni = $this->input->post('pemeriksaan_sni');
			$pemeriksaan_kamar_mandi = $this->input->post('pemeriksaan_kamar_mandi');
			$catatan_verifikasi = $this->input->post('catatan_verifikasi');
			$hasil_pemeriksaan = $this->input->post('hasil_pemeriksaan');
			$reject_reason = $this->input->post('reject_reason');

			if($hasil_pemeriksaan == "PPU")
			{

				$ceklist_ppu = $this->input->post('ceklist_ppu');
				$json_ceklist_ppu = json_encode($ceklist_ppu);
			}
			else
			{
				$ceklist_ppu = "";
				$json_ceklist_ppu = "";
			}
			
			
		
			$verifikator1 = $this->input->post('verifikator1');
			$verifikator2 = $this->input->post('verifikator2');
			$verifikator3 = $this->input->post('verifikator3');
			$verifikator4 = $this->input->post('verifikator4');
			
			$nomor_verifikasi = date('YmdHis');
			$created_at = date('Y:m:d H:i:s');
			
			try {
				
				$data_verifikasi = array(
					"no_verifikasi"=> $nomor_verifikasi,
					"no_pendaftaran"=> $no_pendaftaran,
					"no_lhpp"=> $no_lhpp,
					"pemeriksaan_dokumen"=> $pemeriksaan_dokumen,
					"proteksi_sentuh_langsung"=> $proteksi_sentuh_langsung,
					"pemeriksaan_penghantar"=> $pemeriksaan_penghantar,
					"pemeriksaan_phb"=> $pemeriksaan_phb,
					"pemeriksaan_polaritas"=> $pemeriksaan_polaritas,
					"pemeriksaan_elektroda"=> $pemeriksaan_elektroda,
					"pemeriksaan_pemasangan"=> $pemeriksaan_pemasangan,
					"pemeriksaan_sni"=> $pemeriksaan_sni,
					"pemeriksaan_kamar_mandi"=> $pemeriksaan_kamar_mandi,
					"catatan_verifikasi"=> $catatan_verifikasi,
					"hasil_pemeriksaan"=> $hasil_pemeriksaan,
					"verifikator1"=> $verifikator1,
					//"verifikator2"=> $verifikator2,
					//"verifikator3"=> $verifikator3,
					//"verifikator4"=> $verifikator4,
					"verifikasi_at"=> $created_at,
					"ceklist_ppu" => $json_ceklist_ppu
				);
				
				
				if($approve)
				{
					$data_pelanggan = array(
						"status_verifikasi"=> 1,
					);

					$insert_verifikasi = $this->Verifikasi_Model->insert($data_verifikasi);
					$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

					if($insert_verifikasi == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('verifikasi/sudah_diverifikasi','refresh');
					}elseif ($insert_verifikasi == 0) {
							$this->session->set_flashdata('error', 'Data has been submitted successfully');
							redirect('verifikasi/sudah_diverifikasi','refresh');
					}

				}
				if($reject)
				{
					$data_pelanggan = array(
						'reject_reason' => $reject_reason,
						"status_verifikasi"=> 3,
					);
					
					$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

					if($update_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('verifikasi/sudah_diverifikasi','refresh');
					}elseif ($update_pelanggan == 0) {
							$this->session->set_flashdata('error', 'Data has been submitted successfully');
							redirect('verifikasi/sudah_diverifikasi','refresh');
					}
				}
				

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input verifikasi');

			}


		}
	
	}

	

}
