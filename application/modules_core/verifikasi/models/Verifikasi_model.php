<?php

class Verifikasi_model extends CI_Model {

    var $column_order_belum_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','no_lhpp','nm_btl'); //set column field database for datatable orderable
    var $column_search_belum_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','no_lhpp','nm_btl');  //set column field database for datatable 
    var $order_belum_verifikasi = array('pelanggan.created_at' => 'desc'); // default order


    var $column_order_sudah_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','nm_btl'); //set column field database for datatable orderable
    var $column_search_sudah_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','nm_btl');  //set column field database for datatable 
    var $order_sudah_verifikasi = array('pelanggan.created_at' => 'desc'); // default order


   function get_all(){
		$this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_detail_verifikator($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('verifikasi');
        $this->db->join('core_user','verifikasi.verifikator1=core_user.coreUserId');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_detail_pemeriksa($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pemeriksaan');
        $this->db->join('core_user','pemeriksaan.pemeriksa1=core_user.coreUserId');
        $this->db->where('no_pendaftaran', $no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    private function _get_datatables_belum_verifikasi_query($params)
     {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_area.kode_area=pelanggan.kd_area','left');
        /*
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
         */
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 2);

        $i = 0;
     
        foreach ($this->column_search_belum_verifikasi as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_belum_verifikasi) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_belum_verifikasi[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pelanggan.no_pendaftaran');
    }


     private function _get_datatables_sudah_verifikasi_query($params)
     {
        /*
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
      
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        */

        $this->db->select('*,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        /*
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        */
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        /*
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        */
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');


          if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);

        $i = 0;
     
        foreach ($this->column_search_sudah_verifikasi as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_sudah_verifikasi) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sudah_verifikasi[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->group_by('pelanggan.no_pendaftaran');
    }

    function get_datatables_belum_verifikasi($params)
    {
        $this->_get_datatables_belum_verifikasi_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
       // $this->db->limit('5', '1');
        $query = $this->db->get();
       // return $this->db->last_query();
        return $query->result();
    }
 
    function count_filtered_belum_verifikasi($params)
    {
        $this->db->select('count(*) as jumlah');
        $this->db->from('verifikasi');
        $this->db->join('pelanggan','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_area.kode_area=pelanggan.kd_area','left');
        /*
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
         */
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }


        $query = $this->db->get();
        return $query->row();

        /*
        $this->_get_datatables_belum_verifikasi_query($params);
        $query = $this->db->get();
        return $query->num_rows();
        */
    }
 
    public function count_all_belum_verifikasi($params)
    {
        
        $this->db->select('count(*) as jumlah');
        $this->db->from('verifikasi');
        $this->db->join('pelanggan','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_area.kode_area=pelanggan.kd_area','left');
        /*
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
         */
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        } 
        $query = $this->db->get();
        return $query->row();
    }


    function get_datatables_sudah_verifikasi($params)
    {
        $this->_get_datatables_sudah_verifikasi_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
       // $this->db->limit('5', '1');
        $query = $this->db->get();
       // return $this->db->last_query();
        return $query->result();
    }
 
    function count_filtered_sudah_verifikasi($params)
    {
        $this->db->select('count(*) as jumlah');
        $this->db->from('verifikasi'); 
        $query = $this->db->get();
        return $query->row();

        /*
        $this->_get_datatables_sudah_verifikasi_query($params);
        $query = $this->db->get();
        return $query->num_rows();
        */
    }
 
    public function count_all_sudah_verifikasi($params)
    {
        $this->db->select('count(*) as jumlah');
        $this->db->from('verifikasi'); 
        $query = $this->db->get();
        return $query->row();
    }

     function get_detail_pelanggan($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_pemeriksaan($no_pendaftaran)
    {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pemeriksaan.no_pendaftaran =', $no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

    function get_detail_verifikasi($no_verifikasi){
        $this->db->select('*');
        $this->db->from('verifikasi');
        $this->db->join('core_user','verifikasi.verifikator1=core_user.coreUserId');
        $this->db->where('no_verifikasi',$no_verifikasi);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_verifikator($params){
        $this->db->select('*');
        $this->db->from('verifikator');

        if(isset($params['kode_area']))
        {
                /*
                $this->db->join('kantor_area','kantor_area.kode_area=verifikator.kode_area');
                $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
                $this->db->where('kantor_wilayah.kode_kanwil',$params['kode_kanwil']);
                */

                $this->db->where('kode_area',$params['kode_area']);
        }

        
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ceklist_ppu(){
        $this->db->select('*');
        $this->db->from('ceklist_ppu');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota(){
        $this->db->select('*');
        $this->db->from('kota');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl(){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl(){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($verifikasi) {
       $query = $this->db->insert('verifikasi', $verifikasi);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

  
    function update($data, $id_provinsi){
        $this->db->trans_begin();
		$this->db->where('id_provinsi', $id_provinsi);
		$this->db->update('provinsi', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

}

