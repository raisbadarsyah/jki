
<html>
<head>
	<title>Cetak daftar perbaikan</title>
	<style>
		table td { font-size:14pt; }
		ol { padding:0;margin:0 0 0 30px; }
		ol li { margin:5px 0; }
	</style>
</head>
<body style="padding:0;margin:10px">
<!--<div style="width:900px;padding:15px;border:1px solid #000;background:url(images/latihan.jpg) center center;">-->
<div style="width:900px;padding:15px;border:1px solid #000;">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="103" align="center">
			<img src="<?php echo base_url();?>template/assets/images/jki_logo_small.png" />
			</td>
			<td align="center">
			<strong>PT JASA KELISTRIKAN INDONESIA<br />
			KANTOR PElAYANAN WILAYAH <?php echo $detail_pelanggan->nm_kanwil?></strong> <br /> 
			AREA <?php echo $detail_pelanggan->nm_area?><br />
			<?php echo $detail_pelanggan->alamat_area?> Telp: <?php echo $detail_pelanggan->telpon?>			</td>
			<td width="90">&nbsp;
			
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<hr style="border-top:1px solid #000"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<strong>DATA HASIL VERIFIKASI</strong>
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;
				
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" border="0" cellpadding="4" cellspacing="0">
					<tr>
						<td width="300">
							No. Pendaftaran
						</td>
						<td>
							: <?php echo $detail_pelanggan->no_pendaftaran?>						</td>
					</tr>
					<tr>
						<td>
							Atas Nama Pemilik / Konsumen
						</td>
						<td>
							: <?php echo $detail_pelanggan->Nama?>						</td>
					</tr>
					<tr>
						<td>
							Nama BTL
						</td>
						<td>
							: <?php echo $detail_pelanggan->nm_btl?>						</td>
					</tr>
					<tr>
						<td>
							Alamat
						</td>
						<td>
							: <?php echo $detail_pelanggan->alamat?>						</td>
					</tr>
					<tr>
						<td>&nbsp;
							
						</td>
						<td>
							&nbsp; <?php echo $detail_pelanggan->nm_alias?>, <?php echo $detail_pelanggan->nm_provinsi?>						</td>
					</tr>
					<tr>
						<td>
							Tarif / Daya
						</td>
						<td>
							: <?php echo $detail_pelanggan->nm_tarif?> / <?php echo $detail_pelanggan->daya?>						</td>
					</tr>
					<tr>
						<td colspan="2">
							<br />
							Hasil Pemeriksaan
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="95%" border="0" cellpadding="4" cellspacing="0" align="center" style="border:1px solid #000">
							<tr>
								<td width="300">
									Jumlah PHB
								</td>
								<td>
									: <?php echo $jml_phb = $detail_pemeriksaan->jml_phb_utama?> BH
								</td>
							</tr>
							<tr>
								<td>
									Jumlah Saluran Cabang
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->jml_saluran_cabang?> BH
								</td>
							</tr>
							<tr>
								<td>
									Jumlah Saluran Akhir
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->jml_saluran_akhir?> BH
								</td>
							</tr>
							<tr>
								<td>
									Jumlah Titik Lampu
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->jml_titik_lampu?> BH
								</td>
							</tr>
							<tr>
								<td>
									Jumlah KKB
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->kkb?> BH
								</td>
							</tr>
							<tr>
								<td>
									Jumlah KKK
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->kkk?> BH
								</td>
							</tr>
							<tr>
								<td>
									Tahanan Isolasi Penghantar
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->tahanan_isolasi_penghantar?> &#8486;
								</td>
							</tr>
							<tr>
								<td>
									Sistem Pembumian
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->sistem_pembumian?>								</td>
							</tr>
							<tr>
								<td>
									Jumlah Resisten Pembumian
								</td>
								<td>
									: <?php echo $detail_pemeriksaan->resistensi_pembumian?> &#8486;
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding:0">&nbsp;</td>
					</tr>
					<tr>
						<td>
							Hasil Verifikasi
						</td>
						<td>
							: <?php if($detail_verifikasi->hasil_pemeriksaan == 'LO') { ?> 

													Laik Operasi (LO)

												<?php }elseif ($detail_verifikasi->hasil_pemeriksaan == 'LOM') {?>
													Laik operasi dengan perbaikan minor(LOM)

												<?php }elseif ($detail_verifikasi->hasil_pemeriksaan == 'PPU') {?>
													Perlu Perbaikan Ulang(PPU)

												<?php }elseif ($detail_verifikasi->hasil_pemeriksaan == 'IBT') {?>
													Instalasi Belum Terpasang(IBT)
												<?php } ?>					
					</tr>
					<tr>
						<td colspan="2">
							Daftar Perbaikan
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width="95%" border="0" cellpadding="8" cellspacing="0" align="center" style="border:1px solid #000">
							<tr>
								<td>
									<ol><?php if($detail_verifikasi->hasil_pemeriksaan == "PPU") { ?>
											<?php foreach ($data_perbaikan as $key => $value) { ?>
											<li><?php echo $value ?></li>
											<?php } ?>
										<?php } ?>
									</ul>								
								</td>
							</tr>
							</table>
						</td>
					</tr>
                      	<tr>
						<td>
							Catatan
						</td>
						<td>
							: <?php echo $detail_verifikasi->catatan_verifikasi?>						</td>
					</tr>
				</table>
				<br /><br />
<table width="250" border="0" align="right" cellpadding="3" cellspacing="0">
  <tr>
    <td align="center"><?php 

   				
    			if(isset($detail_staff->nm_area) && $detail_staff->nm_area == "") 
				{	
					 $nm_area =  $detail_staff->nm_area;
				}
				else if($detail_staff->nip = '111')
				{
					$nm_area = 'Jakarta';
				}
				else if($detail_staff->nip = '222')
				{
					$nm_area = 'Jakarta';
				}
				else
				{
					$nm_area = "";
				}

    ?>

    	<?php echo $nm_area; ?>, <?php echo $tgl_sekarang?></td>
  </tr>
  <tr>
    <td align="center">Verifikator</td>
  </tr>
  <tr>
    <td height="80" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><?php echo $detail_staff->Name?></td>
  </tr>
</table>

			</td>
		</tr>
	</table>

</div>
</body>
</html>