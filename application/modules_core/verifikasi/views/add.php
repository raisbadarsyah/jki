
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<!-- /theme JS files -->

	
    
    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<!--
	<script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>
-->
	<script type="text/javascript" src='http://maps-api-ssl.google.com/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/gallery_library.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location_edit.js"></script>

	

	
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Verifikasi</li>
							<li>Belum Diverifikasi</li>
							<li class="active">Lakukan Verifikasi</li>
						</ul>

					
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<div class="row">

							<div class="col-md-6">

							<!-- Basic layout-->
							<form action="<?php echo base_url().'verifikasi/add'; ?>" enctype="multipart/form-data" method="post" class="form-horizontal">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Form Verifikasi</h5>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<label class="col-lg-3 control-label">No Pendaftaran:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_pendaftaran" value="<?php echo $detail_pemeriksaan->no_pendaftaran ?>" readonly>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No LHPP:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_lhpp" value="<?php echo $detail_pemeriksaan->no_lhpp ?>" readonly>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Dokumen:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_dokumen">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Proteksi Terhadap Sentuh Langsung:</label>
											<div class="col-lg-9">
												<select class="select" name="proteksi_sentuh_langsung">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Penghantar:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_penghantar">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>


										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan PHB:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_phb">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Elektroda:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_elektroda">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Polaritas:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_polaritas">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Pemasangan:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_pemasangan">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan SNI:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_sni">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Pemeriksaan Kamar Mandi:</label>
											<div class="col-lg-9">
												<select class="select" name="pemeriksaan_kamar_mandi">
													<option value="1">Laik</option>
													<option value="2">Tidak Laik</option>
													<option value="3">Tidak Perlu</option>
												</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Catatan:</label>
											<div class="col-lg-9">
												<textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here" name="catatan_verifikasi"></textarea>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Hasil Pemeriksaan:</label>
											<div class="col-lg-9">
												<select class="select" name="hasil_pemeriksaan" id="hasil_pemeriksaan" onchange="showDiv()">
													<option value="LO">Laik Operasi (LO)</option>
													<option value="LOM">Laik operasi dengan perbaikan minor(LOM)</option>
													<option value="PPU">Perlu Perbaikan Ulang(PPU)</option>
													<option value="IBT">Instalasi Belum Terpasang(IBT)</option>
												</select>
											</div>
										</div>

										<div id="div_ceklist_ppu" style="display: none">
										<div class="form-group">
											<label class="col-lg-3 control-label">Ceklist PPU:</label>
											<div class="col-lg-9">
												<select class="multiselect-select-all-filtering" multiple="multiple" name="ceklist_ppu[]" id="ceklist_ppu">
																
																<?php foreach ($ceklist_ppu as $key => $value) {?>

																<option value="<?php echo $value->keterangan;?>"><?php  echo $value->keterangan;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Verifikator:</label>
											<div class="col-lg-9">
												<input type="hidden" class="form-control" name="verifikator1" value="<?php echo $user->coreUserId ?>" readonly>
												<input type="text" class="form-control" name="nm_verifikator" value="<?php echo $user->Name ?>" readonly>
											</div>
										</div>

										<!--
										<div class="form-group">
											<label class="col-lg-3 control-label">Verifikator 1:</label>
											<div class="col-lg-9">
												<select class="select" name="verifikator1" id="verifikator1" required="required">
																<option value="">-- Pilih Verifikator --</option>
																<?php foreach ($verifikator as $key => $value) {?>

																<option value="<?php echo $value->id_verifikator;?>"><?php  echo $value->nm_verifikator;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Verifikator 2:</label>
											<div class="col-lg-9">
												<select class="select" name="verifikator2" id="verifikator2" required="required">
																<option value="">-- Pilih Verifikator --</option>
																<?php foreach ($verifikator as $key => $value) {?>

																<option value="<?php echo $value->id_verifikator;?>"><?php  echo $value->nm_verifikator;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Verifikator 3:</label>
											<div class="col-lg-9">
												<select class="select" name="verifikator3" id="verifikator3" required="required">
																<option value="">-- Pilih Verifikator --</option>
																<?php foreach ($verifikator as $key => $value) {?>

																<option value="<?php echo $value->id_verifikator;?>"><?php  echo $value->nm_verifikator;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-lg-3 control-label">Verifikator 4:</label>
											<div class="col-lg-9">
												<select class="select" name="verifikator4" id="verifikator4" required="required">
																<option value="">-- Pilih Verifikator --</option>
																<?php foreach ($verifikator as $key => $value) {?>

																<option value="<?php echo $value->id_verifikator;?>"><?php  echo $value->nm_verifikator;?></option>
																<?php } ?>
													
															</select>
											</div>
										</div>
										-->
										

										<div class="text-right">
											<a href='#popUpReject' class="btn btn-primary" id='no_pendaftaran' data-toggle='modal' data-id="<?php echo $detail_pemeriksaan->no_pendaftaran ?>">Reject</a>
											<input type="submit" class="btn btn-primary" name="approve" id="approve" value="Approve">
										</div>
									</div>
								</div>
							</form>
							<!-- /basic layout -->

						</div>

					<!-- 2 columns form -->
			
				<div class="col-md-6">
					<!-- Basic layout-->
							<form action="#" class="form-horizontal">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Hasil Pemeriksaan</h5>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">
										<div class="form-group">
											<label class="col-lg-3 control-label">No Pendaftaran:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->no_pendaftaran ?></label>
												
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Pelanggan:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->Nama ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Alamat:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->alamat ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Tarif/Daya:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->nm_tarif ?>/<?php echo $detail_pemeriksaan->daya ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">BTL:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->nm_btl ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Instalir:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->nama_instalir ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Telp Instalir:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->telp_instalir ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No Surat Tugas:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->no_surat_tugas ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Nama Pemeriksa:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->nm_pemeriksa1 ?>,<?php echo $detail_pemeriksaan->nm_pemeriksa2 ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">No LHPP:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->no_lhpp ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">TGL LHPP:</label>
											<div class="col-lg-9">
												<label class="col-lg-6 control-label"><?php echo $detail_pemeriksaan->tgl_lhpp ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Gambar Instalasi:</label>
											<div class="col-lg-9">
												<?php if($detail_pemeriksaan->gambar_instalasi == 1) {  ?>

												<label class="col-lg-6 control-label">Sesuai</label>

												<?php }elseif($detail_pemeriksaan->gambar_instalasi == 2){?>

												<label class="col-lg-6 control-label">Tidak Sesuai</label>

												<?php } ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Diagram Garis Tunggal:</label>
											<div class="col-lg-9">
												<?php if($detail_pemeriksaan->diagram_garis_tunggal == 1) {  ?>

												<label class="col-lg-6 control-label">Sesuai</label>

												<?php }elseif($detail_pemeriksaan->diagram_garis_tunggal == 2){?>

												<label class="col-lg-6 control-label">Tidak Sesuai</label>

												<?php } ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Proteksi PE:</label>
											<div class="col-lg-9">
												<?php if($detail_pemeriksaan->penghantar_proteksi_pe_saluran_utama == 1) {  ?>

												<label class="col-lg-4 control-label">Utama : Ada</label>

												<?php }elseif($detail_pemeriksaan->penghantar_proteksi_pe_saluran_utama == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>

												<?php if($detail_pemeriksaan->penghantar_proteksi_pe_saluran_cabang == 1) {  ?>

												<label class="col-lg-4 control-label">Cabang : Ada</label>

												<?php }elseif($detail_pemeriksaan->penghantar_proteksi_pe_saluran_cabang == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>

												<?php if($detail_pemeriksaan->penghantar_proteksi_pe_saluran_akhir == 1) {  ?>

												<label class="col-lg-4 control-label">Utama : Ada</label>

												<?php }elseif($detail_pemeriksaan->penghantar_proteksi_pe_saluran_akhir == 2){?>

												<label class="col-lg-4 control-label">Utama : Tidak ada</label>

												<?php } ?>


											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jenis Penghantar:</label>
											<div class="col-lg-9">
												
												<label class="col-lg-4 control-label">Utama : <?php echo $detail_pemeriksaan->jns_penghantar_utama ?></label>

												<label class="col-lg-4 control-label">Cabang : <?php echo $detail_pemeriksaan->jns_penghantar_cabang ?></label>

												<label class="col-lg-4 control-label">Akhir : <?php echo $detail_pemeriksaan->jns_penghantar_akhir ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penampang Penghantar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label">Utama : <?php echo $detail_pemeriksaan->penampang_penghantar_saluran_utama ?></label>

												<label class="col-lg-4 control-label">Cabang : <?php echo $detail_pemeriksaan->penampang_penghantar_saluran_cabang ?></label>

												<label class="col-lg-4 control-label">Akhir : <?php echo $detail_pemeriksaan->penampang_penghantar_saluran_akhir ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Penampang Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->luas_penampang_penghantar_bumi ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Sakelar Utama:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->sakelar_utama ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Ketinggian Kotak Kontak:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->tinggi_pemasangan_kotak_kontak ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Ketinggia PHBK:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->tinggi_pemasangan_phbk ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jenis Kotak Kontak:</label>
											<div class="col-lg-9">
												<?php if($detail_pemeriksaan->jns_pemasangan_kotak_kontak == 1) {  ?>

												<label class="col-lg-4 control-label">Biasa</label>

												<?php }elseif($detail_pemeriksaan->jns_pemasangan_kotak_kontak == 2){?>

												<label class="col-lg-4 control-label">Putar</label>

												<?php }elseif($detail_pemeriksaan->jns_pemasangan_kotak_kontak == 3){?>

												<label class="col-lg-4 control-label">Tutup</label>

												<?php }elseif($detail_pemeriksaan->jns_pemasangan_kotak_kontak == 4){?>

												<label class="col-lg-4 control-label">Tidak Ada</label>

												<?php }elseif($detail_pemeriksaan->jns_pemasangan_kotak_kontak == 5){?>

												<label class="col-lg-4 control-label">Lainnya</label>

												<?php } ?>

											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah PHB Cabang:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_phb_utama ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Saluran Cabang:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_saluran_cabang?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Saluran Akhir:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_saluran_akhir ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Titik Lampu:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_titik_lampu ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Jumlah Sakelar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->jml_sakelar ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKB:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->kkb ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">KKK:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->kkk ?> Buah</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Tahanan Isolasi Penghantar:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->tahanan_isolasi_penghantar ?> MΩ</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Sistem Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->sistem_pembumian ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Resisten Pembumian:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->resistensi_pembumian ?> Ω</label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Catatan Pemeriksa:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->catatan_pemeriksa ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 1:</label>
											<div class="col-lg-9">
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto1 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto1 ?>" alt="" class="img-rounded img-preview">
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 2:</label>
											<div class="col-lg-9">
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto2 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto2 ?>" alt="" class="img-rounded img-preview">
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 3:</label>
											<div class="col-lg-9">
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto3 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto3 ?>" alt="" class="img-rounded img-preview">
				                        		</a>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Foto 4:</label>
											<div class="col-lg-9">
												<a href="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto4 ?>" data-popup="lightbox">
					                        <img src="<?php echo base_url();?>uploads/<?php echo $detail_pemeriksaan->foto4 ?>" alt="" class="img-rounded img-preview">
				                        		</a>
											</div>
										</div>

										<div class="form-group">			
													<div class="col-md-12">
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Koordinat Lokasi</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
																
																<div class="form-group">
																	<label>Location:</label>
																	<input type="text" class="form-control" id="us3-address" name="location" value="<?php echo $detail_pelanggan->location ?>" readonly>
																</div>
																<div class="form-group">
																	<div id="us3" class="map-wrapper"></div>
																</div>

																<div class="form-group">		
																	<label>Latitude:</label>
																	<input type="text" class="form-control" id="us3-lat" name="lattitude" value="<?php echo $detail_pelanggan->lattitude ?>" readonly>
																</div>

																<div class="form-group">
																	<label>Longitude:</label>
																	<input type="text" class="form-control" id="us3-lon" name="longitude" value="<?php echo $detail_pelanggan->longitude ?>" readonly>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>


										
									</div>
								</div>
							</form>
							<!-- /basic layout -->
				</div>
			</div>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpReject">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Reject Reason / Noted</h5>
								</div>

								<form action="<?php echo base_url().'verifikasi/add'; ?>" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">No Pendaftaran</label>
											<div class="col-sm-9">
												<input type="text" name="no_pendaftaran" id="no_pendaftaran" class="form-control" value="<?php echo $detail_pemeriksaan->no_pendaftaran ?>" readonly>
											
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Reject Reason</label>
											<div class="col-sm-9">
												<textarea class="form-control" placeholder="Sudah kadaluarsa" name="reject_reason"></textarea>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							
										<input type="submit" class="btn btn-primary" name="reject" id="reject" value="Submit">
									</div>
								</form>
							</div>
						</div>
					</div>
<script type="text/javascript">
  
        $('#popUpReject').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
           
         });
  
  </script>

<script type="text/javascript">
		function showDiv(){

		var hasil_pemeriksaan = $('#hasil_pemeriksaan :selected').val();

			if(hasil_pemeriksaan=='PPU'){
			    document.getElementById('div_ceklist_ppu').style.display = "block";
		    	event.preventDefault();
			}else{
				document.getElementById('div_ceklist_ppu').style.display = "none";
		    	event.preventDefault();
			}


		}
</script>
<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
