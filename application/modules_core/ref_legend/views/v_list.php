<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><?php echo $page_title;?></div>
					<?php if($this->session->flashdata('message') != ""){ ?> 
						<br /><br />				
						<div class="alert alert-info" role="alert">
						  <i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?>
						</div>
					<?php } ?>
				</div>
				<div class="panel-body">	
                <br />
				
  				<a href="<?php echo base_url(); ?>ref_legend/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
				<hr />				
				
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				  <thead>
					<tr>
						<th>No</th>
						<th>Principal</th>
						<th>Field Name</th>
						<th>Keterangan</th>
						<th width="8%">Action</th>
					</tr>
				  </thead>
				  <tbody>
				   <?php $no=0;foreach($list_title as $row){?>
					<tr>
						<td><?php echo ++$no;?></td>
						<td><?php echo $row->supplierPrincipal;?></td>
						<td><?php echo $row->refTitleName;?></td>
						<td><?php echo $row->refTitleKeterangan;?></td>					
						<td>
						  <a href="<?php echo base_url(); ?>ref_legend/edit/<?php echo $row->refTitleLegendId; ?>" class="btn btn-primary" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>						  
						</td>					  
				    </tr>
				    <?php }?>
				  </tbody>
				</table>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>