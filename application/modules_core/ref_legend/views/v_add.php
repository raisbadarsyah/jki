<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
								<a href="<?php echo base_url(); ?>ref_legend"><button class="btn btn-warning"><i class="glyphicon glyphicon-arrow-left"></i> Back</button></a><hr />
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'ref_legend/insert'; ?>" enctype="multipart/form-data">
									
									   <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Principal</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="refTitlePrincipalId" id="refTitlePrincipalId" required>
											<option value="">--Select--</option>
											<?php foreach($list_principal as $row){?>
											  <option value="<?php echo $row->suplierId;?>"><?php echo $row->supplierPrincipal;?></option>
											<?php };?>
										  </select>
										</div>
									  </div>
									  
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Field Name</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="refTitleNameId" id="refTitleNameId" required>
											<option value="">--Select--</option>
											<?php foreach($list_title as $row){?>
											  <option value="<?php echo $row->refTitleId;?>"><?php echo $row->refTitleName;?></option>
											<?php };?>
										  </select>
										</div>
									  </div>

									   <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
										<div class="col-sm-8" id="unameInput">
										  <input type="text" class="form-control" id="refTitleKeterangan" name="refTitleKeterangan" placeholder="Keterangan" required>
										  <span id="refTitleKeterangan"></span>
										</div>
									  </div>									  
								
									 
									 <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									  </div>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>