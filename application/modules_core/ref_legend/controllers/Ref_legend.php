<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); //error_reporting(0);

class Ref_legend extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','ref_legend_model'));
		$this->load->helper('check_auth_menu');
		$this->load->library('upload');
		ini_set('max_execution_time', 0);	
		//check_authority_url();		
	}

	function index()
	{
		$config['title'] = 'Management Legend Report';
		$config['page_title'] = 'Management Legend Report';
		$config['page_subtitle'] = 'Legend Report';
		
        $config['list_title']    = $this->ref_legend_model->get_all();  
		$this->load->view('v_list', $config);
	}
	
	function add(){
		$config['title'] = 'Management Legend Report';
		$config['page_title'] = 'Management Legend Report';
		$config['page_subtitle'] = 'Legend Report';
	   
	    $config['list_title']     = $this->ref_legend_model->get_title();
		$config['list_principal'] = $this->ref_legend_model->get_principal();
		$this->load->view('v_add', $config);
	}

	function insert(){
		$refTitleNameId      = $this->input->post('refTitleNameId');
		$refTitleKeterangan  = $this->input->post('refTitleKeterangan');
		$refTitlePrincipalId = $this->input->post('refTitlePrincipalId');
		
		$data = array(
			"refTitleNameId" 	=> $refTitleNameId,
			"refTitlePrincipalId" => $refTitlePrincipalId, 
			"refTitleKeterangan" => $refTitleKeterangan
		);

		$action = $this->ref_legend_model->insert($data);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Data has been added successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to add data');
		}

		redirect('ref_legend','refresh');
	}
	
	function edit($id){
		$config['title'] = 'Management Legend Report';
		$config['page_title'] = 'Management Legend Report';
		$config['page_subtitle'] = 'Legend Report';		
	    $id = $this->uri->segment(3);
		
		$config['list_title']     = $this->ref_legend_model->get_title();
		$config['list_principal'] = $this->ref_legend_model->get_principal();
        $config['list_legend']    = $this->ref_legend_model->get_by_id($id);
       
		$this->load->view('v_edit', $config);
	}
	
	function update(){	
		$id            		= $this->input->post('id');
		$refTitleNameId     = $this->input->post('refTitleNameId');
		$refTitleKeterangan = $this->input->post('refTitleKeterangan');	
		$refTitlePrincipalId = $this->input->post('refTitlePrincipalId');
		
		$data = array(
			"refTitleNameId" 	 => $refTitleNameId,
			"refTitlePrincipalId" => $refTitlePrincipalId, 
			"refTitleKeterangan" => $refTitleKeterangan
		);
		
		$action = $this->ref_legend_model->update($data, $id);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Data has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update Data');
		}	
		redirect(base_url() . 'ref_legend', 'refresh');
	}
	
	function delete($id){
		$id     = $this->uri->segment(3);
		$action = $this->management_user_model->delete_user($id);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been delete successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to delete user');
		}	
		redirect(base_url() . 'ref_legend', 'refresh');
	}
	
	function download(){			
		$data = $this->ref_legend_model->get_all();  
		
		$this->load->library("PHPExcel");
		$PHPExcel = new PHPExcel();
		$PHPExcel->getProperties()->setTitle('title')->setDescription('description');
		$PHPExcel->setActiveSheetIndex(0);
		
		
		$kolom = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O');
		$header = array('No','Title Name','Principal Name','Keterangan','Status');
						
		$jml_kolom = count($header); 
					
		//Creating Border -------------------------------------------------------------------
		$styleArray = array(
			   'borders' => array(
					 'outline' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => '000000'),
					 ),
					 'inside' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => '000000'),
					 ),
			   ),
			   'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'size'  => 12,
					'name'  => 'Calibri'
				),
		);		
		
		//Column Header
		for($i=0;$i<$jml_kolom;$i++){
			$PHPExcel->getActiveSheet()->setCellValue("$kolom[$i]1", $header[$i]);
		}
		
		//width colum
		$PHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(35);
		$PHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(25);
		
		//Header Style
		$tmp = $jml_kolom-1;
		$PHPExcel->getActiveSheet()->getStyle("A1:$kolom[$tmp]1")->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle("A1:$kolom[$tmp]1")->getAlignment()->setHorizontal('center');
		$PHPExcel->getActiveSheet()->getStyle("A1:$kolom[$tmp]1")->getFill()->setFillType('solid')->getStartColor()->setRGB('E0E0E0');
		$PHPExcel->getActiveSheet()->getStyle("A1:$kolom[$tmp]1")->applyFromArray($styleArray);
	
		
		$set_kolom = 2;
		$total_row = count($data);
		
		$no=0;
		$ar=0;			
		$grand_total =0;

		
		foreach ($data as $val) { 
			$PHPExcel->getActiveSheet()->setCellValue($kolom[0].$set_kolom, ++$no);			
			$PHPExcel->getActiveSheet()->setCellValue($kolom[1].$set_kolom, $val->refTitleName);
			$PHPExcel->getActiveSheet()->setCellValue($kolom[2].$set_kolom, $val->refTitlePrincipal);
			$PHPExcel->getActiveSheet()->setCellValue($kolom[3].$set_kolom, $val->refTitleKeterangan);
			$PHPExcel->getActiveSheet()->setCellValue($kolom[4].$set_kolom, $val->refTitleStatus);
		
				
			$set_kolom++;									
		}
		
	
		
		for($box=1; $box<=$total_row+1; $box++){
			$PHPExcel->getActiveSheet()->getStyle($kolom[0].$box.':'.$kolom[3].$box)->applyFromArray($styleArray);				
		}
				
		// Save it as file ------------------------------------------------------------------
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="legend_report.xls"');
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
		$objWriter->save('php://output');
		//-----------------------------------------------------------------------------------
	}
	
	function import()
	{

		$config['title'] = 'Management Legend Report';
		$config['page_title'] = 'Management Legend Report';
		$config['page_subtitle'] = 'Legend Report';		
        $config['file']   = base_url().'uploads/format/import_legend_report.xls';

		$this->load->view('v_import', $config);
	}

	function import_excel(){
		$PHPExcel = $this->load->library("PHPExcel"); 
		$fileName = $_FILES['file_excel']['name'];
		$inputFileName = ($_FILES['file_excel']['tmp_name']);
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$ObjReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
		$ObjPHPExcel = $ObjReader->load($inputFileName);
		$ObjWorksheet = $ObjPHPExcel->setActiveSheetIndex(0);

		$data = array();

		foreach ($ObjWorksheet->getRowIterator() as $row) {
			$row_data = array();
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			foreach ($cellIterator as $cell) {
				if (!is_null($cell)) {
					if(strstr($cell,'=')==true)
					{
						$row_data[] = trim($cell->getOldCalculatedValue());
					} else {
						$row_data[] = trim($cell->getCalculatedValue());
					}
				}  
			}
			
			$data[] = $row_data;
		}
		
		if ($data){
                unset($data[0]);
		}
		
		//loop data from excel
        foreach ($data as $key => $val) {
			$refTitleName  	   = $val['1'];
			$refTitlePrincipal = $val['2'];
			$refTitleKeterangan = $val['3'];
			$refTitleStatus    = $val['4'];
			
			
			$data = array(
				"refTitleName" 		=> $refTitleName,
				"refTitlePrincipal" => $refTitlePrincipal,
				"refTitleKeterangan" => $refTitleKeterangan,
				"refTitleStatus"    => $refTitleStatus
			);

		    $action = $this->ref_legend_model->insert($data);
		}

		if($action) {
			$this->session->set_flashdata('message', 'data has been added successfully');
		}else{
			$this->session->set_flashdata('error', 'Failed to add');
		    redirect('ref_legend/import','refresh');
		}
		
		redirect(base_url() . 'ref_title', 'refresh');
	}
}