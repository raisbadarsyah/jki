<?php

class Ref_legend_model extends CI_Model {

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from('ref_legend');
		$this->db->where('refTitleLegendId', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_all() {
        $this->db->select('*');
        $this->db->from('ref_legend');
		$this->db->join('ref_title','ref_legend.refTitleNameId=ref_title.refTitleId','left');
		$this->db->join('m_supplier','ref_legend.refTitlePrincipalId=m_supplier.suplierId','left');
		$this->db->order_by('refTitleLegendId','asc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_principal() {
        $this->db->select('*');
        $this->db->from('m_supplier');
		$this->db->order_by('supplierPrincipal','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_title() {
        $this->db->select('*');
        $this->db->from('ref_title');
		$this->db->order_by('refTitleId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert($data){
		$this->db->trans_begin();
        $this->db->insert('ref_legend', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update($data, $id){
        $this->db->trans_begin();
		$this->db->where('refTitleLegendId', $id);
		$this->db->update('ref_legend', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function delete_user($id) {
        $query = $this->db->delete('ref_legend', array('refTitleLegendId' => $id));

		 if ($query) { 
			echo 1; 
	    } 
	    else { 
	    	echo 0; 
	    } 
	}
	
}

