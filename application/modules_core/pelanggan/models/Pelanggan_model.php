<?php

class Pelanggan_model extends CI_Model {

    var $column_order = array('no_pendaftaran', 'no_agenda','Nama','alamat','nm_kota','nm_kanwil','nm_area','nm_sub_area','nm_btl','nm_bangunan','nm_tarif','nm_asosiasi','pelanggan.created_at','tipe_pelanggan','Name'); 

    var $column_search = array('no_pendaftaran','pelanggan.created_at');
   
    var $order = array('pelanggan.created_at' => 'desc'); 

    /* Calon Pelanggan */

    var $column_order_calon_web = array('nm_kanwil', 'nm_area','nama','alamat','nm_provinsi','nm_kota','telepon','nm_tarif','daya','tgl_daftar','via','calon_pelanggan.status'); //set column field database for datatable orderable
    var $column_search_calon_web = array('nm_kanwil', 'nm_area','nama','alamat','nm_provinsi','nm_kota','telepon','nm_tarif','daya','tgl_daftar','via','calon_pelanggan.status'); //set column field database for datatable searchable 
    var $order_calon_web = array('id' => 'asc'); // default order 

    var $column_order_calon_pln = array('nm_kanwil', 'nm_area','nama','alamat','nm_provinsi','nm_kota','telepon','nm_tarif','daya','tgl_daftar','via','calon_pelanggan.status'); //set column field database for datatable orderable
    var $column_search_calon_pln = array('nm_kanwil', 'nm_area','nama','alamat','nm_provinsi','nm_kota','telepon','nm_tarif','daya','tgl_daftar','via','calon_pelanggan.status'); //set column field database for datatable searchable 
    var $order_calon_pln = array('idpel' => 'asc'); // default order 


     private function _get_datatables_query($params)
     {
        $this->db->select('no_pendaftaran,no_agenda,Nama,alamat,nm_kota,nm_kanwil,nm_area,nm_sub_area,nm_btl,nm_bangunan,nm_tarif,daya,nm_asosiasi,pelanggan.created_at as created_at,tipe_pelanggan,Name');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        /*
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        */
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('core_user','pelanggan.created_by=core_user.coreUserId','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
            if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value'], 'after');
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value'], 'after');
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_query_calon_pelanggan_web($params)
     {
        $this->db->select('*,calon_pelanggan.status as status_calon_pelanggan');
        $this->db->from('calon_pelanggan');
        $this->db->join('kota','calon_pelanggan.id_kota=kota.id_kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->join('tarif','calon_pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','calon_pelanggan.id_daya=daya.id_daya');
        $this->db->join('kantor_area','calon_pelanggan.id_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_calon_web as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_calon_web) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_calon_web[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
         else if(isset($this->order_calon_web))
        {
            $order = $this->order_calon_web;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->where('via','1');
        $this->db->group_by('id');
    }

    private function _get_datatables_query_calon_pelanggan_pln($params)
     {
        $this->db->select('*');
        $this->db->from('calon_pelanggan_pln');
        $this->db->join('kota','calon_pelanggan_pln.kodekab=kota.kode_djp_kota');
        $this->db->join('kantor_area','kota.id_kota=kantor_area.id_kota');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_calon_pln as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_calon_pln) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_calon_pln[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
         else if(isset($this->order_calon_pln))
        {
            $order = $this->order_calon_pln;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('idpel');
    }


    private function _get_datatables_query_calon_pelanggan_djk($params)
     {
        $this->db->select('*,calon_pelanggan.status as status_calon_pelanggan');
        $this->db->from('calon_pelanggan');
        $this->db->join('kota','calon_pelanggan.id_kota=kota.id_kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->join('tarif','calon_pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','calon_pelanggan.id_daya=daya.id_daya');
        $this->db->join('kantor_area','calon_pelanggan.id_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_calon_web as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_calon_web) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_calon_web[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
         else if(isset($this->order_calon_web))
        {
            $order = $this->order_calon_web;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->where('via','2');
        $this->db->group_by('id');
    }

    function get_datatables($params)
    {
        $this->_get_datatables_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        //$this->db->limit('5', '1');

        $query = $this->db->get();
        return $query->result();
      //  return $this->db->last_query();

    }
 
    function count_filtered($params)
    {
        $this->_get_datatables_query($params);
       return $this->db->count_all_results();
       
    }
 
    public function count_all($params)
    {
        return $this->db->count_all("pelanggan");
        //return $this->db->last_query();
    }

    function get_datatables_calon_pelanggan_pln($params)
    {
        $this->_get_datatables_query_calon_pelanggan_pln($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_calon_pelanggan_pln($params)
    {
        $this->_get_datatables_query_calon_pelanggan_pln($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_calon_pelanggan_pln()
    {
        $this->db->select('*');
        $this->db->from('calon_pelanggan_pln');
        return $this->db->count_all_results();
    }

    function get_datatables_calon_pelanggan_web($params)
    {
        $this->_get_datatables_query_calon_pelanggan_web($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_calon_pelanggan_web($params)
    {
        $this->_get_datatables_query_calon_pelanggan_web($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_calon_pelanggan_web()
    {
        $this->db->select('*');
        $this->db->from('calon_pelanggan');
        $this->db->where('via','1');
        return $this->db->count_all_results();
    }


    function get_datatables_calon_pelanggan_djk($params)
    {
        $this->_get_datatables_query_calon_pelanggan_djk($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_calon_pelanggan_djk($params)
    {
        $this->_get_datatables_query_calon_pelanggan_djk($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_calon_pelanggan_djk()
    {
        $this->db->select('*');
        $this->db->from('calon_pelanggan');
        $this->db->where('via','2');
        return $this->db->count_all_results();
    }

    function get_detail_pelanggan($no_pendaftaran){
        $this->db->select('*,pelanggan.kd_area as kode_area,kantor_wilayah.kode_kanwil as kode_kanwil,pelanggan.telp as telp');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('core_user','pelanggan.created_by=core_user.coreUserId','left');
        $this->db->where('no_pendaftaran',$no_pendaftaran);


        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_pelanggan_pembayaran($no_pendaftaran){
        $this->db->select('*,pelanggan.kd_area as kode_area');
        $this->db->from('pelanggan');
        $this->db->where('no_pendaftaran',$no_pendaftaran);


        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }
    
    function get_belum_bayar($params){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }


        $this->db->where('status_pembayaran =', 2);
        $this->db->group_by('pelanggan.no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    
    function get_belum_diperiksa($params){
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran','left');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
       // $this->db->join('kantor_sub_area as c','pelanggan.kode_sub_area=c.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('status_pembayaran =', 1);
        
         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        if(isset($params['status']) && $params['status'] == 1)
          {
                $this->db->where('tgl_bayar < DATE(DATE_ADD(now(), INTERVAL -2 DAY))');
                $this->db->where('status_pemeriksaan =', 2);
          }

        if(isset($params['status']) && $params['status'] == 2)
          {
                $this->db->where('tgl_bayar > DATE(DATE_ADD(now(), INTERVAL -2 DAY))');
                $this->db->where('status_pemeriksaan =', 2);
          }

        if(isset($params['status']) && $params['status'] == 3)
          {
                $this->db->where('status_pemeriksaan =', 3);
          }
        if(!isset($params['status']))
          {
               
                $this->db->where('status_pemeriksaan =', 2);
          }

        $query = $this->db->get();
       // echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
        //return $this->db->last_query();
    }


    function get_sudah_diperiksa($params){
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }
        
        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->group_by('pelanggan.no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_belum_diverifikasi($params){
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

          if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 2);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_sertifikasi_belum_terbit($params){
        $this->db->select('*,pemeriksaan.no_lhpp as nomor_lhpp');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }
        
        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 2);
         $this->db->group_by('pelanggan.no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_sertifikasi_sudah_terbit(){
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('sertifikasi','pelanggan.no_pendaftaran=sertifikasi.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_sudah_diverifikasi($params){
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

          if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->group_by('pelanggan.no_pendaftaran');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_sudah_bayar($params){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('core_user','pembayaran.diterima_oleh=core_user.coreUserId','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
       // $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        
           if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

      function get_dicetak_oleh($coreUserId){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserId', $coreUserId);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_kota($params){

        $this->db->select('*');
        $this->db->from('kota');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
  
            if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0 and !isset($params['kode_area']))
            {    
                 $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
                // $this->db->join('cakupan_kantor_area','kota.id_kota=cakupan_kantor_area.id_kota');
                 $this->db->join('cakupan_kanwil','provinsi.id_provinsi=cakupan_kanwil.id_provinsi');
                 $this->db->where('cakupan_kanwil.kode_kanwil', $params['kode_kanwil']);

            }
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                 $this->db->join('cakupan_kantor_area','kota.id_kota=cakupan_kantor_area.id_kota');
                 $this->db->where('cakupan_kantor_area.kode_area', $params['kode_area']);
            }
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_tarif(){
        $this->db->select('*');
        $this->db->from('tarif');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_daya(){
        $this->db->select('*');
        $this->db->from('daya');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_btl($params){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        { 

            if(isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0 && $params['kode_kanwil'] == 030 || $params['kode_kanwil'] == 033)
            {

                 $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
                 $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
                 $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }
            else
            {

                    if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0)
                    {    
                         $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
                         $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);

                    }

                    /*
                    if (isset($params['kode_area']) and $params['kode_area'] !=0)
                    {
                       //  $this->db->join('cakupan_kantor_area','kota.id_kota=cakupan_kantor_area.id_kota');
                         $this->db->where('kode_area', $params['kode_area']);
                    }
                    */
            }
        }
        $this->db->where('biro_teknik_listrik.status', '1');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_asosiasi(){
        $this->db->select('*');
        $this->db->from('asosiasi');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_ptl($params){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        { 

            if(isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0 && $params['kode_kanwil'] == 30 || $params['kode_kanwil'] == 33)
            {

                 $this->db->join('kantor_area','penyedia_listrik.kode_area=kantor_area.kode_area');
                         $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }
            else
            {
                
                    if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0 and !isset($params['kode_area']))
                    {    
                         $this->db->join('kantor_area','penyedia_listrik.kode_area=kantor_area.kode_area');
                         $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);

                    }

                    if (isset($params['kode_area']) and $params['kode_area'] !=0)
                    {
                       //  $this->db->join('cakupan_kantor_area','kota.id_kota=cakupan_kantor_area.id_kota');
                         $this->db->where('kode_area', $params['kode_area']);
                    }
            }
        }
        $this->db->where('penyedia_listrik.status', '1');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_bangunan(){
        $this->db->select('*');
        $this->db->from('bangunan');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function insert($pelanggan) {
       $query = $this->db->insert('pelanggan', $pelanggan);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_calon_pelanggan($calon_pelanggan) {
       $query = $this->db->insert('calon_pelanggan', $calon_pelanggan);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function upload_pelanggan($upload_pelanggan) {
       $query = $this->db->insert('upload_pelanggan', $upload_pelanggan);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

  
    function update($data, $no_pendaftaran){
        $this->db->trans_begin();
		$this->db->where('no_pendaftaran', $no_pendaftaran);
		$this->db->update('pelanggan', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function update_calon_pelanggan($data, $id){
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update('calon_pelanggan', $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }       
    }

     function delete($no_pendaftaran) {
        $query = $this->db->delete('pelanggan', array('no_pendaftaran' => $no_pendaftaran));
         if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
        
    }

    function delete_calon_pelanggan($calon_pelanggan_id) {
        $query = $this->db->delete('calon_pelanggan', array('id' => $calon_pelanggan_id));
         if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
        
    }

    function cek_btl_nama($nama_btl){
        $this->db->select('*');
        $this->db->from('biro_teknik_listrik');
        $this->db->where('nm_btl', $nama_btl);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

     function cek_area($nama_area){
        $this->db->select('*');
        $this->db->from('kantor_area');
        $this->db->where('nm_area', $nama_area);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_sub_area($nama_sub_area){
        $this->db->select('*');
        $this->db->from('kantor_sub_area');
        $this->db->where('nm_sub_area', $nama_sub_area);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_kota_nama($nama_kota){
        $this->db->select('*');
        $this->db->from('kota');
        $this->db->where('nm_kota', $nama_kota);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_tarif_nama($nama_tarif){
        $this->db->select('*');
        $this->db->from('tarif');
        $this->db->where('nm_tarif', $nama_tarif);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_daya_nama($nama_daya){
        $this->db->select('*');
        $this->db->from('daya');
        $this->db->where('daya', $nama_daya);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_bangunan_nama($nama_bangunan){
        $this->db->select('*');
        $this->db->from('bangunan');
        $this->db->where('nm_bangunan', $nama_bangunan);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function cek_pjt_nama($nama_penyedia){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $this->db->where('nm_ptl', $nama_penyedia);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_kanwil_by_sub($kode_sub_area){
        $this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->join('kantor_area','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
        $this->db->join('kantor_sub_area','kantor_sub_area.kode_area=kantor_area.kode_area'); 
        $this->db->where('kode_sub_area', $kode_sub_area);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_max_kode_pelanggan($kota){
        //$this->db->select('MAX(SUBSTRING(no_pendaftaran, "-5")) AS ExtractString');
        $this->db->select('MAX(no_urut_pelanggan) AS ExtractString');
        $this->db->from('pelanggan');
       // $this->db->where('kd_kota', $kota);

        /*
        if(isset($params['kode_kanwil']) && $params['kode_kanwil'] != "")
        {      
             //$this->db->join('kantor_sub_area','kantor_sub_area.kode_sub_area=pelanggan.kode_sub_area'); 
             $this->db->join('kantor_area','kantor_area.kode_area=pelanggan.kd_area');
             $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
             $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
        }
        */
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_max_kode_pelanggan1($kota){
        $this->db->select('MAX(SUBSTRING(no_pendaftaran, "-6")) AS ExtractString');
        $this->db->from('pelanggan');
       // $this->db->where('kd_kota', $kota);

        /*
        if(isset($params['kode_kanwil']) && $params['kode_kanwil'] != "")
        {      
             //$this->db->join('kantor_sub_area','kantor_sub_area.kode_sub_area=pelanggan.kode_sub_area'); 
             $this->db->join('kantor_area','kantor_area.kode_area=pelanggan.kd_area');
             $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
             $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
        }
        */
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

     function get_detail_calon_pelanggan($calon_pelanggan_id){
        $this->db->select('*,calon_pelanggan.status as status_calon_pelanggan');
        $this->db->from('calon_pelanggan');
        $this->db->join('kota','calon_pelanggan.id_kota=kota.id_kota');
        $this->db->join('provinsi','kota.id_provinsi=provinsi.id_provinsi');
        $this->db->join('tarif','calon_pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','calon_pelanggan.id_daya=daya.id_daya');
        $this->db->join('kantor_area','calon_pelanggan.id_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->where('id', $calon_pelanggan_id);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }

    function get_detail_calon_pelanggan_pln($calon_pelanggan_id){
        $this->db->select('namaplg as nama,kodekab,unitup,alamatplg as alamat,id_kota,id_tarif,id_daya,noagenda as noreg_pln,created_at');
        $this->db->from('calon_pelanggan_pln');
        $this->db->join('kota','calon_pelanggan_pln.kodekab=kota.kode_djp_kota','left');
        $this->db->join('tarif','calon_pelanggan_pln.tarif=tarif.nm_tarif','left');
        $this->db->join('daya','calon_pelanggan_pln.daya=daya.daya','left');
        $this->db->where('idpel', $calon_pelanggan_id);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
      //  return $this->db->last_query();
    }



}

