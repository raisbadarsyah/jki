<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_responsive.js"></script>

	<!--
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	-->
	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>
	<style type="text/css">
	
	.grid-view-loading
	{
		background:url(loading.gif) no-repeat;
	}

	.grid-view
	{
		padding: 40px 0;
	}

	.datatable-header>div:first-child, .datatable-footer>div:first-child {
    margin-left: 20px;
	}

	.dataTables_length {
    	float: right;
	    display: inline-block;
	    margin: 0 0 20px 20px;
	    margin-top: 0px;
	    margin-right: 20px;
	    margin-bottom: 20px;
	    margin-left: 20px;
	}

	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}



</style>

	
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Admin</a></li>
							<li><a>Pelanggan</a></li>
							<li class="active">Pelanggan</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Left icons -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Data Pelanggan</h6>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-highlight">
											<li class="active"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-menu7 position-left"></i> Data Pelanggan</a></li>
											<li><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-menu7 position-left"></i>Calon Pelanggan Web</a></li>
											
											<li><a href="#left-icon-tab3" data-toggle="tab"><i class="icon-menu7 position-left"></i>Calon Pelanggan DJK</a></li>
										
											<li><a href="#left-icon-tab4" data-toggle="tab"><i class="icon-menu7 position-left"></i>Calon Pelanggan PLN</a></li>
									

										</ul>

										<div class="tab-content">
											<div class="tab-pane active" id="left-icon-tab1">
												
																<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Pelanggan List</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
																<div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
											<?php if ($this->session->flashdata('error') == TRUE): ?>
									                <div class="alert alert-danger alert-styled-left alert-bordered">
									                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
									                    <span class="text-semibold">Oh snap!</span><?php echo $this->session->flashdata('error'); ?>.
								                    </div>
									            <?php endif; ?>
									            <?php if ($this->session->flashdata('success') == TRUE): ?>
									                 <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
									                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
									                    <span class="text-semibold">Well done!</span> <?php echo $this->session->flashdata('success'); ?>.
									                  </div>
									            <?php endif; ?>

																<?php 
														$position_id = $this->session->userdata('position_id');
														//if($position_id == 9){
													?>
													<!--
									  				<a href="<?php echo base_url(); ?>pelanggan/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
													-->

									  			
																<a href='#popUpAdd' class="btn btn-primary" id='add' data-toggle='modal'>Tambah Data Pelanggan</a>
						
									  				<a href="<?php echo base_url(); ?>pelanggan/upload"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Import Data Pelanggan</button></a>
									  				
															</div>
													
															<div id="pelanggan-grid" class="grid-view">
																 <table id="pelanggan" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
															               <th><?php echo wordwrap('No Pendaftaran',9,"<br>\n") ?></th>						
																			<th><?php echo wordwrap('No Agenda',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Nama',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Alamat',9,"<br>\n") ?></th>
																			<th>Kota</th>
																			<th>Wilayah</th>
																			<th>Area</th>
																			<th><?php echo wordwrap('Sub Area',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Nama BTL',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Jenis Bangunan',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Tarif/Daya',9,"<br>\n") ?></th>
																			
																			<th><?php echo wordwrap('Asoisasi No Gambar',9,"<br>\n") ?></th>
																			<th><?php echo wordwrap('Tgl Daftar',9,"<br>\n") ?></th>
																			<th>Tipe</th>
																			<th><?php echo wordwrap('Diinputkan Oleh',9,"<br>\n") ?></th>

																			<th>Action</th>
															            </tr>
															        </thead>
															 
															    </table>
																</div>
															</div>
														
														<!-- /basic datatable -->

											</div>

											<div class="tab-pane" id="left-icon-tab2">

														<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Calon Pelanggan Web</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															<?php if ($this->session->flashdata('error') == TRUE): ?>
									                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
									            <?php endif; ?>
									            <?php if ($this->session->flashdata('success') == TRUE): ?>
									                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
									            <?php endif; ?>

																<?php 
														$position_id = $this->session->userdata('position_id');
														//if($position_id == 9){
													?>
													<!--
									  				<a href="<?php echo base_url(); ?>pelanggan/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
													-->

									  			
																
															</div>
															<div id="pelanggan-grid" class="grid-view">
																 <table id="calon_pelanggan" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
															               <th>Wilayah</th>						
																			<th>Area</th>
																			<th>Nama</th>
																			<th>Alamat</th>
																			<th>Provinsi</th>
																			<th>Kota</th>
																			<th>Telepon</th>
																			<th>Tarif</th>
																			<th>Daya</th>
																			<th>Tgl. Daftar</th>
																			<th>Daftar Via</th>
																			<th>Status</th>
																			<th>Action</th>
															            </tr>
															        </thead>
															 
															    </table>
															</div>
														</div>
														<!-- /basic datatable -->

											</div>

											<div class="tab-pane" id="left-icon-tab3">

														<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Calon Pelanggan DJK</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															<?php if ($this->session->flashdata('error') == TRUE): ?>
									                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
									            <?php endif; ?>
									            <?php if ($this->session->flashdata('success') == TRUE): ?>
									                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
									            <?php endif; ?>

																<?php 
														$position_id = $this->session->userdata('position_id');
														//if($position_id == 9){
													?>
													<!--
									  				<a href="<?php echo base_url(); ?>pelanggan/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
													-->

									  			
																
															</div>
															<div id="pelanggan-grid-djk" class="grid-view">
																 <table id="calon_pelanggan_djk" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
															               <th>Wilayah</th>						
																			<th>Area</th>
																			<th>Nama</th>
																			<th>Alamat</th>
																			<th>Provinsi</th>
																			<th>Kota</th>
																			<th>Telepon</th>
																			<th>Tarif</th>
																			<th>Daya</th>
																			<th>Tgl. Daftar</th>
																			<th>Daftar Via</th>
																			<th>Status</th>
																			<th>Action</th>
															            </tr>
															        </thead>
															 
															    </table>
															</div>
														</div>
														<!-- /basic datatable -->

											</div>


											<!--
											<div class="tab-pane" id="left-icon-tab3">
												DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
											</div>

											<div class="tab-pane" id="left-icon-tab4">
												Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
											</div>
										-->
										</div>
									</div>
								</div>
							</div>
						</div>

						
					</div>
					<!-- /left icons -->

				

					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpAdd">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Input Data Pelanggan</h5>
								</div>

								<form action="<?php echo site_url();?>pelanggan/add" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Tipe Pelanggan</label>
											<div class="col-sm-9">
												<select class="form-control select" name="tipe_pelanggan" id="tipe_pelanggan" onchange="showDiv()">
													
													<option value="1">Baru</option>
													<option value="2">Rubah Daya</option>
													<option value="3">Instalasi Lama</option>
												</select>
												
											</div>
										</div>
										
										<div class="form-group" id="div_no_slo" style="display: none">
											<label class="control-label col-sm-3">No SLO</label>
											<div class="col-sm-9">
												<input type="text" name="no_slo" id="no_slo" class="form-control" disabled>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->


					<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpJadikanPelanggan">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Input Data Pelanggan</h5>
								</div>

								<form action="<?php echo site_url();?>pelanggan/add" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Tipe Pelanggan</label>
											<div class="col-sm-9">
												<select class="form-control select" name="tipe_pelanggan" id="tipe_pelanggan" onchange="showDiv()">
													
													<option value="1">Pelanggan Baru</option>
													<option value="2">Tambah Daya</option>
													
												</select>
												
											</div>
										</div>
										
										<input type="hidden" name="calon_pelanggan_id" id="calon_pelanggan_id">
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

<script type="text/javascript">
	
	function showDiv()
	{
		var tipe_pelanggan = $('#tipe_pelanggan :selected').val();

		 if(tipe_pelanggan==2){
		 	
		    document.getElementById('div_no_slo').style.display = "block";
		   
			$("#no_slo").prop('disabled',false);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==1){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==3){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

	

	}
</script>

<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>

<script type="text/javascript">
 
var calon_pelanggan;
var calon_pelanggan_djk;
var pelanggan;
 
$(document).ready(function() {
 	
 	var dark = $("table.items").parent();

           $(dark).block({
            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.85,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none',
                color: '#1B2024'
            }
        });

    //datatables
    calon_pelanggan = $('#calon_pelanggan').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print'
					],

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('pelanggan/querypagingcalonpelangganweb')?>",
            "type": "POST"
        },
 		
 		"initComplete": function( settings, json ) {
		    $(dark).unblock();
		  },
		  
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });

     //datatables
    calon_pelanggan_djk = $('#calon_pelanggan_djk').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print'
					],

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('pelanggan/querypagingcalonpelanggandjk')?>",
            "type": "POST"
        },
 		
 		"initComplete": function( settings, json ) {
		    $(dark).unblock();
		  },
		  
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });

     pelanggan = $('#pelanggan').DataTable({ 
 		
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 		"pageLength": 5,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('pelanggan/querypaging')?>",
            "type": "POST"
        },
 		
 		"initComplete": function( settings, json ) {
		    $(dark).unblock();
		  },

        //Set column definition initialisation properties.
        "columnDefs": [

        { 
            "targets": [ 15 ], //first column / numbering column
            "orderable": false, //set not orderable
            "autoWidth": false,

        },
        ],
 
    });
 
});

</script>


<script>
	

	$('#popUpJadikanPelanggan').on('show.bs.modal', function(e) {
	    var id_calon_pelanggan = $(e.relatedTarget).data('calon-pelanggan-id');
	  
	    $(e.currentTarget).find('input[name="calon_pelanggan_id"]').val(id_calon_pelanggan);
	});
</script>

</body>
</html>
