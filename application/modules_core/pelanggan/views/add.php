<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='http://https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location.js"></script>


	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Pelanggan</li>
							<li class="active">Buat Pelanggan Baru</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'pelanggan/save_pelanggan'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Membuat Data Pelanggan Baru</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Membuat Pelanggan</legend>

											<div class="row">
												
												<input type="hidden" name="calon_pelanggan_id" value="<?php if(isset($calon_pelanggan_id)) { echo $calon_pelanggan_id;} ?>">
												<div class="col-md-12">
													<div class="form-group">
														<label>Nama Lengkap:</label>
														 <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->Nama; } if(isset($calon_pelanggan)) { echo $calon_pelanggan->nama; } ?>" required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Alamat:</label>
														 <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->alamat; } if(isset($calon_pelanggan)) { echo $calon_pelanggan->alamat; } ?>" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Kota:</label>
														 <select class="select" name="kota" id="kota" required>
																<option value="">-- Pilih Kota --</option>
																<?php foreach ($kota as $key => $value) {?>

																<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->kd_kota == $value->id_kota) { echo "selected"; } 

																	if(isset($calon_pelanggan) && $calon_pelanggan->id_kota == $value->id_kota) { echo "selected"; } ?> value="<?php echo $value->id_kota;?>"><?php  echo $value->nm_kota;?></option>
																<?php } ?>
																
															</select>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>No KTP:</label>
														 <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="Nomor Ktp" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->no_ktp; } ?>" required>
													</div>
												</div>
												
											
												<div class="col-md-6">
													<div class="form-group">
														<label>Telp:</label>
														<input type="text" class="form-control" name="telp" id="telp" placeholder="Telp" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->telp; } ?>" required>
													</div>
												</div>

												<div class="col-md-6" <?php if(!isset($detail_pelanggan) && $tipe_pelanggan=="2") { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
													<div class="form-group">
														<label>Tarif(Lama):</label>
														 <select class="select" name="tarif_lama" id="tarif_lama">
																<option value="">-- Pilih Tarif --</option>
																<?php foreach ($tarif as $key => $value) {?>

																<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->id_tarif == $value->id_tarif) { echo "selected"; }?> value="<?php echo $value->id_tarif;?>" selected><?php  echo $value->nm_tarif;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6" <?php if(!isset($detail_pelanggan) && $tipe_pelanggan=="2") { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
													<div class="form-group">
														<label>Daya (Lama)</label>
														 <select class="select" name="daya_lama" id="daya_lama">
																<option value="">-- Pilih Daya --</option>
																<?php foreach ($daya as $key => $value) {?>

																<option value="<?php if(isset($detail_pelanggan) && $detail_pelanggan->id_daya == $value->id_daya) { echo $value->id_daya ?>" selected <?php } ?> <?php echo $value->id_daya;?>"><?php  echo $value->daya;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Tarif(Baru):</label>
														 <select class="select" name="tarif" id="tarif" required>
																<option value="">-- Pilih Tarif --</option>
																<?php foreach ($tarif as $key => $value) {?>

																<option <?php if(isset($calon_pelanggan) && $calon_pelanggan->id_tarif == $value->id_tarif) { echo "selected"; } ?> value="<?php echo $value->id_tarif;?>"><?php  echo $value->nm_tarif;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Daya Baru</label>
														 <select class="select" name="daya" id="daya" required>
																<option value="">-- Pilih Daya --</option>
																<?php foreach ($daya as $key => $value) {?>

																<option <?php if(isset($calon_pelanggan) && $calon_pelanggan->id_daya == $value->id_daya) { echo "selected"; } ?> value="<?php echo $value->id_daya;?>"><?php  echo $value->daya;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>


												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Nama BTL:</label>
														<select class="select" name="btl" id="btl" required>
																<option> -- Pilih BTL --</option>
																<?php foreach ($btl as $key => $value) {?>
																<option value="<?php if(isset($detail_pelanggan) && $detail_pelanggan->btl_id == $value->btl_id) { echo $value->btl_id?>" selected <?php } ?> <?php echo $value->btl_id;?>"><?php  echo $value->nm_btl;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_atasan1">
													<div class="form-group">
														<label>Bangunan:</label>
														<select class="select" name="bangunan" id="bangunan" required>
																<option> -- Pilih Bangunan --</option>

																<?php foreach ($bangunan as $key => $value) {?>


																<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->id_bangunan == $value->id_bangunan) { echo "selected";} ?> value="<?php echo $value->id_bangunan;?>"><?php  echo $value->nm_bangunan;?>
																	
																</option>
																<?php } ?>
													
															</select>
													</div>
												</div>
												
												<div class="col-md-12" id="div_atasan2">
													<div class="form-group">
														<label>Penyedia Tenaga Listrik:</label>
														 <div class="multi-select-full">
															<select class="select" name="ptl" id="ptl" required>
																<option value=""> -- Pilih Penyedia Tenaga Listrik --</option>
																<?php foreach ($ptl as $key => $value) {?>

																	<?php if(isset($detail_pelanggan)) { ?>

																		<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->id_ptl == $value->id_ptl) { echo "selected"; }?> value="<?php echo $value->id_ptl;?>"><?php echo $value->nm_ptl;?></option>
																		
																	<?php } else {  ?>
																	
																		<option <?php if(isset($cek_ptl) && $cek_ptl->id_ptl == $value->id_ptl) { echo "selected"; }?> value="<?php echo $value->id_ptl;?>"><?php  echo $value->nm_ptl;?></option>
																	

																	<?php } ?>
																<?php } ?>
															</select>
														</div>

													</div>
												</div>

												<div class="col-md-4" id="div_kanwil">
													<div class="form-group">
														<label>Kantor Wilayah:</label>
														<?php if(isset($position_id) && $position_id == 2) { ?>
														<input type="hidden" name="kanwil" value="<?php  echo $kanwil_staff->kode_kanwil;?>">

														<?php  echo $kanwil_staff->nm_kanwil;?>

														<?php } else { ?>

														 <select name="kanwil" id="kanwil" onChange="javascript:get_area()" class="select" required="required">
														<option value="1" selected> -- Pilih Kantor Wilayah -- </option>

														<?php foreach ($kanwil as $value) {?>
																<option 

																<?php if(isset($detail_pelanggan) && $detail_pelanggan->kode_kanwil == $value->kode_kanwil) {?>

																value="<?php echo $value->kode_kanwil;?>" selected
																
																<?php } else { ?>

																<?php if(isset($calon_pelanggan->id_wilayah) && $calon_pelanggan->id_wilayah == $value->kode_kanwil) { echo "selected"; } ?>

																value="<?php  echo $value->kode_kanwil;?>"
																<?php } ?>
																>
																<?php  echo $value->nm_kanwil;?> - <?php  echo $value->alias;?></option>
																
														<?php } ?>
															
														</select>
														<?php } ?>
													</div>
												</div>

											<div id="div_area">
												<div class="col-md-4">
													<div class="form-group">
														<label>Area:</label>
														<?php if(isset($position_id) && $position_id == 2) { ?>
														

															<?php if($kanwil_staff->kode_area == "") { ?>

																<select name="area" id="area" onChange="javascript:get_sub_area()" class="select area" required="required">
																<option value=""> Pilih Area </option>
																<?php foreach ($area_staff as $value) { ?>
																<option value="<?php  echo $value->kode_area;?>"><?php  echo $value->nm_area;?></option>
																
																<?php } ?>
																</select>
																
															<?php } else { ?>

															<input type="hidden" name="area" value="<?php  echo $kanwil_staff->kode_area;?>">

																<?php  echo $kanwil_staff->nm_area;?>
															
															<?php } ?>

														<?php } else { ?>

														<select name="area" id="area" onChange="javascript:get_sub_area()" class="select area" required="required">

														<?php 
														

														foreach ($area as $value) {?>

																<option 

																<?php if(isset($detail_pelanggan) && $detail_pelanggan->kode_area == $value->kode_area) {?>

																value="<?php echo $value->kode_area;?>" selected
																
																<?php } else { ?>

																<?php if(isset($calon_pelanggan->id_area) && $calon_pelanggan->id_area == $value->kode_area) { echo "selected"; } ?>

																value="<?php  echo $value->kode_area;?>"
																<?php } ?>
																>
																<?php  echo $value->nm_area;?></option>


														<?php } ?>

														</select>

														<?php } ?>
													</div>
												</div>
											</div>

											
												<div class="col-md-4" id="div_sub_area">
													<div class="form-group">
														<label>Sub Area:</label>
														<?php if(isset($position_id) && $position_id == 2) { ?>
									

															<?php if($kanwil_staff->kode_sub_area == "") { ?>

																<select class="select sub_area" name="sub_area" id="sub_area">
																<?php foreach ($sub_area_staff as $value) { ?>
																<option value="<?php  echo $value->kode_sub_area;?>"><?php  echo $value->nm_sub_area;?></option>
																
																<?php } ?>
																</select>
																
															<?php } else { ?>

																<input type="hidden" name="sub_area" value="<?php  echo $kanwil_staff->kode_sub_area;?>">

																<?php  echo $kanwil_staff->nm_sub_area;?>
															<?php } ?>

														<?php } else { ?>
														<select class="select sub_area" name="sub_area" id="sub_area">
														
														<?php foreach ($sub_area as $value) {?>

														<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->kode_sub_area == $value->kode_sub_area) { echo "selected"; } ?> value="<?php  echo $value->kode_sub_area;?>">
														<?php  echo $value->nm_sub_area;?>
															
														</option>
														
														<?php } ?>
														</select>
														<?php } ?>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>No Gambar:</label>
														 <input type="text" class="form-control" name="no_gambar" id="no_gambar" placeholder="Nomor Gambar" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->no_gambar; } ?>" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Tanggal Gambar:</label>
														<input name="tgl_gambar" class="form-control daterange-single" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->tgl_gambar; } ?>">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Asosiasi No Gambar:</label>
														<select class="select" name="asosiasi" id="asosiasi" required>
															
														<?php foreach ($asosiasi as $value) {?>
																<option value="<?php if(isset($detail_pelanggan) && $detail_pelanggan->id_asosiasi == $value->id_asosiasi) { echo $value->id_asosiasi?>" selected <?php } ?><?php  echo $value->id_asosiasi;?>"><?php  echo $value->nm_asosiasi;?></option>
																
														<?php } ?>
														

														</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>No Agenda Pln:</label>
														<?php if(isset($detail_pelanggan)) {?>
														 <input type="text" class="form-control" name="no_reg" id="no_reg" placeholder="No Reg PLN" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->noreg_pln; } ?>" required>
														<?php }elseif(isset($calon_pelanggan)){ ?>
															<input type="text" class="form-control" name="no_reg" id="no_reg" placeholder="No Reg PLN" value="<?php if(isset($calon_pelanggan)) { echo $calon_pelanggan->noreg_pln; } ?>" required>
														<?php } else { ?>
															<input type="text" class="form-control" name="no_reg" id="no_reg" placeholder="No Reg PLN" required>
														<?php } ?>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Tanggal Reg Pln:</label>
														<?php if(isset($detail_pelanggan)) {?>
														 <input type="text" class="form-control daterange-single" name="tgl_rek" id="tgl_rek" placeholder="Tanggal Reg Pln" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->tgl_reg_pln; } ?>" required>
														 <?php }elseif(isset($calon_pelanggan)){ ?>
														 	<input type="text" class="form-control daterange-single" name="tgl_rek" id="tgl_rek" placeholder="Tanggal Reg Pln" value="<?php if(isset($calon_pelanggan)) { echo $calon_pelanggan->created_at; } ?>" required>
														 <?php } else { ?>
														 	<input type="text" class="form-control daterange-single" name="tgl_rek" id="tgl_rek" placeholder="Tanggal Reg Pln" required>
														 <?php } ?>
													</div>
												</div>

												<!--
												<div class="col-md-6">
													<div class="form-group">
														<label>Nama Instalir:</label>
														 <input type="text" class="form-control" name="nm_instalir" id="nm_instalir" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nama_instalir; } ?>" placeholder="Nama Instalir" required>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Telpon Instalir:</label>
														 <input type="text" class="form-control" name="telp_instalir" id="telp_instalir" placeholder="Telepon Instalir" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->telp_instalir; } ?>" required>
													</div>
												</div>
												-->
					
											</div>

									
									

										</fieldset>
									</div>
									<br>
									
								<input type="hidden" name="tipe_pelanggan" value="<?php echo $tipe_pelanggan ?>">
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();
         
	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
