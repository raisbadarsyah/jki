<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
	
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	

	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	
	<!-- /theme JS files -->
	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>
	<style type="text/css">

		.dt-buttons {
		    position: relative;
		    display: block;
		    float: right;
		    margin: 0 0 20px 20px;
		}

	</style>

	<style type="text/css">
		
		/* Table */
		.display {
			background: white;
			border-collapse: collapse;
			width: 100%;
			border: 1px #D0E3EF solid;
		}
		.display th, 
		.display td {
			font-size: 0.9em;
			border: 1px white solid;
			padding: 0.3em;
		}
		.display .title {
			caption-side: bottom;
			margin-top: 12px;
		}
		
		/* Table Header */
		.display thead th {
			background-color: #508abb;
			color: #FFFFFF;
			border-color: #6ea1cc !important;
			
		}
		
		/* Table Body */
		.display tbody td {
			color: #353535;
			background:url(down.gif) right center no-repeat;
		padding-right: 10px;
		}
		.display tbody td:first-child,
		.display tbody td:last-child,
		.display tbody td:nth-child(4) {
			text-align: left;
		}
		.display tbody tr:nth-child(odd) td {
			background-color: #f4fbff;
		}
		.display tbody tr:hover td {
			background-color: #ffffa2;
			border-color: #ffff0f;
			transition: all .2s;
		}
		
		/* Table Footer */
		.display tfoot th {
			background-color: #e5f5ff;
		}
		.display tfoot th:first-child {
			text-align: left;
		}
		

	</style>

	<style type="text/css">
	
	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}

</style>
<!--
<script type="text/javascript">
						

						$(document).ready(function() {
 								
 								

 								var dataTable =  $('#sudah_sertifikasi').DataTable( {
							    "bDestroy": true
							 
							  });
							  
							    $("#sudah_sertifikasi_filter").css("display","none");  // hiding global search box
							 
							    $('.employee-search-input').on('keyup click change', function () {
							        var i =$(this).attr('id');  // getting column index
							        var v =$(this).val();  // getting search input value
							        dataTable.columns(i).search(v).draw();
							    });

							   

						});

					</script>
-->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Dashboard</a></li>
							<li><a>Pelanggan</a></li>
							<li class="active">Calon Pelanggan</li>

						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Calon Pelanggan PLN</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
						<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success alert-styled-left"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

							<?php 
					$position_id = $this->session->userdata('position_id');
					//if($position_id == 9){
				?>
  				
						</div>

						
						<table class="display" id="sudah_sertifikasi">
							 <thead>
					<tr>
						<th>ID Pelanggan</th>
						<th>No Agenda</th>						
						<th>Nama Pelanggan</th>
						<th>Alamat Pelanggan</th>
						<th>Tarif</th>
						<th>Daya</th>
						<th>Nama UP</th>
						<th>Kabupaten</th>
						<th>Tgl Bayar</th>
						<th>No Registrasi PLN</th>
						<th width="30%" align="center">Action</th>
					</tr>
					<!--
					<tr>
				        <td><input type="text" id="0" class="form-control employee-search-input"></td>
				        <td><input type="text" id="1" class="form-control employee-search-input"></td>
				        <td><input type="text" id="2" class="form-control employee-search-input" ></td>
				        <td><input type="text" id="3" class="form-control employee-search-input" ></td>
				        <td><input type="text" id="4" class="form-control employee-search-input" ></td>
				        <td align="middle"><input  readonly="readonly" type="text" id="5" class="form-control employee-search-input datepicker" ></td>
				        <td><input type="text" id="6" class="form-control employee-search-input" ></td>
				        <td><input type="text" id="7" class="form-control employee-search-input"></td>
				        <td><input type="text" id="8" class="form-control employee-search-input"></td>
				        <td><input type="text" id="9" class="form-control employee-search-input daterange-single" name="tgl_terbit"></td>
				        <td><input type="text" id="10" class="form-control employee-search-input" ></td>
				        <td></td>
				        
				    </tr>
					-->

				  </thead>
				  
						</table>
					</div>
					</div>
				
					<!--
					<script>

						$("#9").bind("change paste keyup", function() {
					      
					       var email = $(this).val();

					       var monthNames = [
							    "January", "February", "March",
							    "April", "May", "June", "July",
							    "August", "September", "October",
							    "November", "December"
							  ];

							  var date = new Date(email);
							  var day = date.getDate();
							  var monthIndex = date.getMonth();
							  var year = date.getFullYear();

							  var date1 =  day + ' ' + monthNames[monthIndex] + ' ' + year;


							$("#9").val(date1);
					    });
						</script>
						-->
					
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


<!-- Primary modal -->
          <div class="modal fade" id="popUp">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header bg-primary">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h6 class="modal-title">Konfirmasi Kesiapan Instalasi</h6>
                </div>
			<form id="bayar" method="get" action="<?php echo base_url().'pemeriksaan/add'; ?>">
                <div class="modal-body">
                  <div class="fetched-data"></div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Lakukan Pemeriksaan</button>
                
                </div>


            </form>
              </div>
            </div>
          </div>
          <!-- /primary modal -->

<script type="text/javascript">
    	
        $('#popUp').on('show.bs.modal', function (e) {
        	
            var no_pendaftaran = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
         
            $.ajax({
                type : 'post',
                url : "<?php echo site_url();?>pemeriksaan/detail_pelanggan",
                data :  'no_pendaftaran='+ no_pendaftaran,
                dataType: "html",
                success : function(data){
                  
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
               
            });
         });
   
  </script>

  <!-- Horizontal form modal -->
					<div class="modal fade" id="popUpReject">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Alasan Belum Siap</h5>
								</div>

								<form action="<?php echo site_url();?>management_fdk/send" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Nomor Pendaftaran</label>
											<div class="col-sm-9">
												<input type="text" name="no_pendaftaran" id="showid" class="form-control" readonly>
												
												
											</div>
										</div>
										

										<div class="form-group">
											<label class="control-label col-sm-3">Alasan Belum Siap</label>
											<div class="col-sm-9">
												<textarea class="form-control" placeholder="Sudah kadaluarsa" name="reject_reason"></textarea>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							
										<input type="submit" class="btn btn-primary" name="reject" id="reject" value="Submit">
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

					<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpJadikanPelanggan">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Input Data Pelanggan</h5>
								</div>

								<form action="<?php echo site_url();?>pelanggan/add" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Tipe Pelanggan</label>
											<div class="col-sm-9">
												<select class="form-control select" name="tipe_pelanggan" id="tipe_pelanggan" onchange="showDiv()">
													
													<option value="1">Pelanggan Baru</option>
													<option value="2">Tambah Daya</option>
													
												</select>
												
											</div>
										</div>
										
										<input type="text" name="calon_pelanggan_id" id="calon_pelanggan_id">
										<input type="text" name="tipe_calon_pelanggan_id" id="tipe_calon_pelanggan_id" value="pln">
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

<script type="text/javascript">
	
	function showDiv()
	{
		var tipe_pelanggan = $('#tipe_pelanggan :selected').val();

		 if(tipe_pelanggan==2){
		 	
		    document.getElementById('div_no_slo').style.display = "block";
		   
			$("#no_slo").prop('disabled',false);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==1){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==3){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

	

	}
</script>


  <script type="text/javascript">
 
var sudah_sertifikasi;
 
$(document).ready(function() {
 	 
 	 var dark = $("table.display").parent();

           $(dark).block({
            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.85,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none',
                color: '#1B2024'
            }
        });

     sudah_sertifikasi = $('#sudah_sertifikasi').DataTable({ 
 		
   
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('pelanggan/querypagingcalonpelangganpln')?>",
            "type": "POST",
        },
 		
 		"bDestroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 		"pageLength": 5,
 		"initComplete": function( settings, json ) {
		    $(dark).unblock();
		  },

 		//"dom": '<"datatable-header"flr><"datatable-scroll-wrap"t><"datatable-footer"ip>',
		//"oLanguage": {"sProcessing":"<div id='loader'></div>"},
		"dom": '<"datatable-header"fB><"datatable-scroll-wrap"rt><"datatable-footer"ip>',
		"language": {
                    "processing": '<i class="icon-spinner spinner"></i>Silahkan tunggu'
                },
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print'
					],

        //Set column definition initialisation properties.
        "columnDefs": [

        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            "autoWidth": false,

        },
        ],
 
    });

});

</script>
<script type="text/javascript">
	
	 $('#popUpJadikanPelanggan').on('show.bs.modal', function(e) {
	    var id_calon_pelanggan = $(e.relatedTarget).data('calon-pelanggan-id');
	    $(e.currentTarget).find('input[name="calon_pelanggan_id"]').val(id_calon_pelanggan);
	});

</script>
</body>
</html>
