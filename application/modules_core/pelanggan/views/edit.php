
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<!-- untuk set alamat -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location.js"></script>
	<!-- untuk set alamat map-->

	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Pelanggan</li>
							<li class="active">Buat Pelanggan Baru</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'pelanggan/proses_edit'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Membuat Data Pelanggan Baru</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Membuat Pelanggan</legend>

											<div class="row">
												

												<div class="col-md-12">
													<div class="form-group">
														<label>Nama Lengkap:</label>
														 <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php echo $detail_pelanggan->Nama;?>" required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Alamat:</label>
														 <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap" value="<?php echo $detail_pelanggan->alamat;?>" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Kota:</label>
														 <select class="select" name="kota" id="kota" required>
																<option value="">-- Pilih Kota --</option>
																<?php foreach ($kota as $key => $value) {?>

																<option <?php if($detail_pelanggan->kd_kota== $value->id_kota) {echo "selected";} ?> value="<?php echo $value->id_kota;?>"><?php  echo $value->nm_kota;?></option>
																<?php } ?>
																
															</select>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>No KTP:</label>
														 <input type="text" class="form-control" name="no_ktp" id="no_ktp" placeholder="Nomor Ktp" value="<?php echo $detail_pelanggan->no_ktp;?>" required>
													</div>
												</div>
												
											
												<div class="col-md-6">
													<div class="form-group">
														<label>Telp:</label>
														<input type="text" class="form-control" name="telp" id="telp" placeholder="Telp" value="<?php echo $detail_pelanggan->telp;?>" required>
													</div>
												</div>

												<div class="col-md-6" <?php if(isset($detail_pelanggan) && $detail_pelanggan->tipe_pelanggan=="2") { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
													<div class="form-group">
														<label>Tarif(Lama):</label>
														 <select class="select" name="tarif_lama" id="tarif_lama">
																<option value="">-- Pilih Tarif --</option>
																<?php foreach ($tarif as $key => $value) {?>

																<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->tarif_lama == $value->id_tarif) { echo "selected"; }?> value="<?php echo $value->id_tarif;?>" selected><?php  echo $value->nm_tarif;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6" <?php if(isset($detail_pelanggan) && $detail_pelanggan->tipe_pelanggan=="2") { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>
													<div class="form-group">
														<label>Daya (Lama)</label>
														 <select class="select" name="daya_lama" id="daya_lama">
																<option value="">-- Pilih Daya --</option>
																<?php foreach ($daya as $key => $value) {?>

																<option <?php if(isset($detail_pelanggan) && $detail_pelanggan->daya_lama == $value->id_daya) { echo "selected"; }?> value="<?php echo $value->id_daya;?>"><?php  echo $value->daya;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Tarif:</label>
														 <select class="select" name="tarif" id="tarif">
																<option value="">-- Pilih Tarif --</option>
																<?php foreach ($tarif as $key => $value) {?>

																<option <?php if($detail_pelanggan->id_tarif == $value->id_tarif) { echo "selected";} ?> value="<?php echo $value->id_tarif;?>"><?php  echo $value->nm_tarif;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Daya</label>
														 <select class="select" name="daya" id="daya">
																<option value="">-- Pilih Daya --</option>
																<?php foreach ($daya as $key => $value) {?>

																<option value="<?php if($detail_pelanggan->id_daya == $value->id_daya) { echo $value->id_daya ?>" selected <?php } ?> <?php echo $value->id_daya;?>"><?php  echo $value->daya;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												
												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Nama BTL:</label>
														<select class="select" name="btl" id="btl">
																<?php foreach ($btl as $key => $value) {?>
																<option value="<?php if($detail_pelanggan->btl_id == $value->btl_id) { echo $value->btl_id?>" selected <?php } ?> <?php echo $value->btl_id;?>"><?php  echo $value->nm_btl;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_atasan1">
													<div class="form-group">
														<label>Bangunan:</label>
														<select class="select" name="bangunan" id="bangunan">
																<option> -- Pilih Bangunan --</option>
																<?php foreach ($bangunan as $key => $value) {?>
																<option value="<?php if($detail_pelanggan->id_bangunan == $value->id_bangunan) { echo $value->id_bangunan?>" selected <?php } ?> <?php echo $value->id_bangunan;?>"><?php  echo $value->nm_bangunan;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_atasan2">
													<div class="form-group">
														<label>Penyedia Tenaga Listrik:</label>
														 <div class="multi-select-full">
															<select class="select" name="ptl" id="ptl" required>
																<option value=""> -- Pilih Penyedia Tenaga Listrik --</option>
																<?php foreach ($ptl as $key => $value) {?>
																<option value="<?php if($detail_pelanggan->id_ptl == $value->id_ptl) { echo $value->id_ptl?>" selected <?php } ?> <?php echo $value->id_ptl;?>"><?php  echo $value->nm_ptl;?></option>
																<?php } ?>
													
															</select>
														</div>

													</div>
												</div>

												<div class="col-md-4" id="div_kanwil">
													<div class="form-group">
														<label>Kantor Wilayah:</label>
														<div class="col-lg-9">
															<label class="col-lg-4 control-label"><?php echo $detail_pelanggan->nm_kanwil?></label>
														</div>
															
														</select>
														
													</div>
												</div>

											<div id="div_area">
												<div class="col-md-4">
													<div class="form-group">
														<label>Area:</label>
														
														<select name="area" id="area" onChange="javascript:get_sub_area()" class="select area" required="required">

														<?php 
														

														foreach ($area as $value) {?>

																<option 

																<?php if($detail_pelanggan->kd_area == $value->kode_area) {?>

																value="<?php echo $value->kode_area;?>" selected
																
																<?php } else { ?>
																value="<?php  echo $value->kode_area;?>"
																<?php } ?>
																>
																<?php  echo $value->nm_area;?></option>


														<?php } ?>

														</select>

													
													</div>
												</div>
											</div>

											
												<div class="col-md-4" id="div_sub_area">
													<div class="form-group">
														<label>Sub Area:</label>
														
														<select class="select sub_area" name="sub_area" id="sub_area">
														
														<?php foreach ($sub_area as $value) {?>
														<option 

																<?php if($detail_pelanggan->kode_sub_area == $value->kode_sub_area) {?>

																value="<?php echo $value->kode_sub_area;?>" selected
																
																<?php } else { ?>
																value="<?php  echo $value->kode_sub_area;?>"
																<?php } ?>
																>
																<?php  echo $value->nm_sub_area;?></option>
														
														<?php } ?>
														</select>
													
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>No Gambar:</label>
														 <input type="text" class="form-control" name="no_gambar" id="no_gambar" placeholder="Telepon" value="<?php echo $detail_pelanggan->no_gambar;?>" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Tanggal Gambar:</label>
														<input name="tgl_gambar" class="form-control daterange-single" value="<?php echo $detail_pelanggan->tgl_gambar;?>">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Asosiasi No Gambar:</label>
														<select class="select" name="asosiasi" id="asosiasi" required>
															
														<?php foreach ($asosiasi as $value) {?>
																<option value="<?php if($detail_pelanggan->id_asosiasi == $value->id_asosiasi) { echo $value->id_asosiasi?>" selected <?php } ?><?php  echo $value->id_asosiasi;?>"><?php  echo $value->nm_asosiasi;?></option>
																
														<?php } ?>
														

														</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>No Reg Pln:</label>
														 <input type="text" class="form-control" name="no_reg" id="no_reg" placeholder="Telepon" value="<?php echo $detail_pelanggan->noreg_pln;?>" required>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Tanggal Reg Pln:</label>
														 <input type="text" class="form-control daterange-single" name="tgl_rek" id="tgl_rek" placeholder="Tanggal Reg Pln" value="<?php  echo $detail_pelanggan->tgl_reg_pln;?>" required>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Nama Instalir:</label>
														 <input type="text" class="form-control" name="nm_instalir" id="nm_instalir" value="<?php echo $detail_pelanggan->nama_instalir;?>" placeholder="Telepon" required>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Telpon Instalir:</label>
														 <input type="text" class="form-control" name="telp_instalir" id="telp_instalir" placeholder="Telepon Instalir" value="<?php echo $detail_pelanggan->telp_instalir;?>" required>
													</div>
												</div>
					
											</div>

									
									

										</fieldset>
									</div>
									<br>
									
								<input type="hidden" name="no_pendaftaran" value="<?php echo $detail_pelanggan->no_pendaftaran ?>">
								<div class="text-right">
									<button type="submit" class="btn btn-primary">EDIT</button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();
          
	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
