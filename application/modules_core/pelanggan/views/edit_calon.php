
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='http://https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location.js"></script>


	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Pelanggan</li>
							<li class="active">Buat Pelanggan Baru</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'pelanggan/proses_edit_calon_pelanggan'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Membuat Data Pelanggan Baru</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Membuat Pelanggan</legend>

											<div class="row">
												<input type="hidden" name="calon_pelanggan_id" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->id; } ?>">
												<div class="col-md-12">
													<div class="form-group">
														<label>Wilayah:</label>
														 <input type="text" class="form-control" name="nm_kanwil" id="nm_kanwil" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nm_kanwil; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Area:</label>
														 <input type="text" class="form-control" name="nm_area" id="nm_area" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nm_area; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Nama:</label>
														 <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nama; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Alamat:</label>
														 <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->alamat; } ?>" required>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label>Provinsi:</label>
														 <input type="text" class="form-control" name="nm_provinsi" id="nm_provinsi" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nm_provinsi; } ?>" readonly required>
													</div>
												</div>

												
												<div class="col-md-12">
													<div class="form-group">
														<label>Kota:</label>
														 <input type="text" class="form-control" name="nm_kota" id="nm_kota" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nm_kota; } ?>" readonly required>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label>Wilayah:</label>
														 <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->nama; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Telepon:</label>
														 <input type="text" class="form-control" name="telepon" id="telepon" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan)) { echo $detail_pelanggan->telepon; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>BTL:</label>
														 <input type="text" class="form-control" name="nm_btl" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan->nm_btl)) { echo $detail_pelanggan->nm_btl; } ?>" readonly required>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label>Tarif/daya:</label>
														 <input type="text" class="form-control" name="nm_btl" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan->nm_tarif)) { echo "".$detail_pelanggan->nm_tarif." ".$detail_pelanggan->daya.""; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Tanggal Daftar:</label>
														 <input type="text" class="form-control" name="nm_btl" id="nama" placeholder="Nama Lengkap" value="<?php if(isset($detail_pelanggan->tgl_daftar)) { echo $detail_pelanggan->tgl_daftar; } ?>" readonly required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Status:</label>
														<select name="status" class="select2">
															<option <?php if(isset($detail_pelanggan->status_calon_pelanggan) && $detail_pelanggan->status_calon_pelanggan == "1") { echo "selected"; }?> value="1"> Baru </option>
															<option <?php if(isset($detail_pelanggan->status_calon_pelanggan) && $detail_pelanggan->status_calon_pelanggan == "2") { echo "selected"; }?> value="2"> Dalam Proses </option>
															<option <?php if(isset($detail_pelanggan->status_calon_pelanggan) && $detail_pelanggan->status_calon_pelanggan == "3") { echo "selected"; }?> value="3"> Terdaftar </option>
															<option <?php if(isset($detail_pelanggan->status_calon_pelanggan) && $detail_pelanggan->status_calon_pelanggan == "4") { echo "selected"; }?> value="4"> Batal </option>
														</select>
													</div>
												</div>
												
												<div class="col-md-12">
													<div class="form-group">
														<label>Alasan:</label>
														<textarea name="alasan"></textarea>
													</div>
												</div>

					
											</div>

									
									

										</fieldset>
									</div>
									<br>
								
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->

					

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();
         
	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
