<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MPI - Discount Modifier</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/noty.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/switch.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/components_notifications_other.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_checkboxes_radios.js"></script>

        <!-- include bootstrap files -->
      
	<script>


function validate_fileupload(file)
{
	
	
	var fileSize = file.files[0].size / 1024 / 1024;
	var fileName = file.files[0].name;

    var allowed_extensions = new Array("xlsx","xls","csv");
    var file_extension = fileName.split('.').pop().toLowerCase(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.

   
        if (fileSize > 2) {
            //alert('File tidak boleh lebih dari 2 MB');

            // Top center
		   
		        $('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File tidak boleh lebih dari 2 MB', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
		  $('#uploadBar').attr('disabled', 'disabled');


            return false;
        } 


    for(var i = 0; i <= allowed_extensions.length; i++)
    {
        if(allowed_extensions[i]==file_extension)
        {
        	$('#uploadBar').removeAttr('disabled');
        	//alert(FileSize);
            return true; // valid file extension

        }
    }

    			$('body').find('.jGrowl').attr('class', '').attr('id', '').hide();
		        $.jGrowl('File ekstensi salah', {
		            position: 'top-center',
		            theme: 'bg-danger',
		            header: 'Attention!'
		        });
    $('#uploadBar').attr('disabled', 'disabled');
    return false;
}


function uploadFile() {
    // membaca data file yg akan diupload, dari komponen 'fileku'

	    				
     				$('.myprogress').css('width', '0');
                    $('.msg').text('');
       
                    var myfile = $('#csv').val();
                
                    if (myfile == '') {
                        alert('Please select file');
                        return;
                    }

                    var formData = new FormData();

                    formData.append('myfile', $('#csv')[0].files[0]);
                  
                    $('#uploadBar').attr('disabled', 'disabled');
                     $('.msg').text('Uploading in progress...');
                    $.ajax({
                        url: '<?php echo site_url('pelanggan/uploadCsvBar') ?>',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress').text(percentComplete + '%');
                                    $('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        beforeSubmit: function() {
		                     $('.msg').text(data);

		                },
                        success: function (data) {
                            $('.msg').html(data);
                            $('#uploadBar').removeAttr('disabled');
                        }
                    });
}

function progressHandler(event){
    // hitung prosentase
   // alert(event.loaded);
    var percent = (event.loaded / event.total) * 100;
    // menampilkan prosentase ke komponen id 'progressBar'
    document.getElementById("progressBar").value = Math.round(percent);
    // menampilkan prosentase ke komponen id 'status'
    document.getElementById("status").innerHTML = Math.round(percent)+"% telah terupload";
    // menampilkan file size yg tlh terupload dan totalnya ke komponen id 'total'
    document.getElementById("total").innerHTML = "Telah terupload "+event.loaded+" bytes dari "+event.total;
}

</script>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main Sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Method</span> - Upload Excel</h4>
						</div>

						
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Upload Pelanggan</li>
							
							<li class="active">Upload Pelanggan</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

				<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Upload Excel</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>
			
						<div class="panel-body">
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-danger alert-styled-left alert-bordered"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

							<form class="form-horizontal" action="<?php echo site_url('pelanggan/uploadCsv') ?>" method="post" enctype="multipart/form-data">
							
							<fieldset class="content-group">
							<legend class="text-bold">Upload Excel</legend>

								<div class="form-group">
										<label class="control-label col-lg-3">Upload File <span class="text-danger">*</span></label>
										<div class="col-lg-9">
											<input type="file" name="csv" class="file-styled" required="required" id="csv" onchange="validate_fileupload(this)">
										</div>
									</div>

								
								<div class="progress progress-rounded">
									<div class="progress-bar progress-bar-warning myprogress" role="progressbar" style="width: 0%">
										<span>0% Complete</span>
									</div>
								</div>
								<div class="msg"></div>
							</fieldset>

								<div class="text-right">
									<!--
									<button type="submit" class="btn btn-primary">Upload <i class="icon-arrow-right14 position-right"></i></button>
								-->
								
									<input type="button" class="btn btn-primary" value="Upload File" id="uploadBar" onclick="uploadFile()">
   						

								</div>

							</form>
						</div>
					</div>
					<!-- /form horizontal -->

			
						
						
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
