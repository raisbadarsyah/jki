<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	function __construct()
	{
		ini_set('memory_limit', '-1');
		parent::__construct();
		/*
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		*/
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Pelanggan_Model','staff/Staff_model','Kota/Kota_model','Ptl/Ptl_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		
		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		

		//	$data['pelanggan'] = $this->Pelanggan_Model->get_all($params);
		

		$this->load->view('index');
	}

	function pln()
	{
		$this->load->view('calonpelangganpln');
	}

	function list_pelanggan()
	{
		$this->load->view('list_pelanggan');
	}

	function querypaging()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
	 	$list = $this->Pelanggan_Model->get_datatables($params);

	 	//var_dump($list);
	 	//exit();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
            if(isset($data_pelanggan) && $data_pelanggan->tipe_pelanggan == 1) 
            { 
					$tipe_pelanggan = "Baru";
			} 
			elseif (isset($data_pelanggan) &&  $data_pelanggan->tipe_pelanggan == 2) 
			{
					$tipe_pelanggan = "Rubah Daya";
			} 
			elseif (isset($data_pelanggan) &&  $data_pelanggan->tipe_pelanggan == 3) 
			{
					$tipe_pelanggan = "Instalasi Lama";
			}
			else
			{
				$tipe_pelanggan = "";
			}

			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='".base_url()."pelanggan/add_agenda/".$data_pelanggan->no_pendaftaran."' title='Buat No Agenda'><i class='fa fa-plus'></i></a>

															 <a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>
															
															  <a href='".base_url()."pelanggan/view_map/".$data_pelanggan->no_pendaftaran."'  title='Lihat Peta Lokasi'><i class='icon-map5'></i></a>
															  <br>
															  <a  href='".base_url()."pelanggan/edit/".$data_pelanggan->no_pendaftaran."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete/".$data_pelanggan->no_pendaftaran."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}
			else if($id_departemen == 12)
			{
				$button = "<a href='".base_url()."pelanggan/add_agenda/".$data_pelanggan->no_pendaftaran."' title='Buat No Agenda'><i class='fa fa-plus'></i></a>

															 <a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>
															
															  <a href='".base_url()."pelanggan/view_map/".$data_pelanggan->no_pendaftaran."'  title='Lihat Peta Lokasi'><i class='icon-map5'></i></a>
															  <br>";

				if($data_pelanggan->status_pemeriksaan == 2)
				{

				$button .= "<a  href='".base_url()."pelanggan/edit/".$data_pelanggan->no_pendaftaran."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete/".$data_pelanggan->no_pendaftaran."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
				}

			}

			else if($id_departemen == 7)
			{
				$button = "<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>
															
															  <a href='".base_url()."pelanggan/view_map/".$data_pelanggan->no_pendaftaran."'  title='Lihat Peta Lokasi'><i class='icon-map5'></i></a>
															  <br>";

			}

			else if($id_departemen == 6)
			{
				$button = "<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>
															
															  <a href='".base_url()."pelanggan/view_map/".$data_pelanggan->no_pendaftaran."'  title='Lihat Peta Lokasi'><i class='icon-map5'></i></a>
															  <br>";

			}

			else
			{
				$button = "<a class='btn bg-slate btn-icon' href='".base_url()."pelanggan/add_agenda/".$data_pelanggan->no_pendaftaran."' title='Buat No Agenda'><i class='fa fa-plus'></i></a>

															 <a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."' class='btn btn-primary'' title='View'><i class='icon-search4'></i></a>
															
															  <a href='".base_url()."pelanggan/view_map/".$data_pelanggan->no_pendaftaran."' class='btn btn-primary'' title='Lihat Peta Lokasi'><i class='icon-map5'></i></a>";
			}

			
            $row[] = $data_pelanggan->no_pendaftaran;
            $row[] = $data_pelanggan->no_agenda;
            $row[] = $data_pelanggan->Nama;
            $row[] = $data_pelanggan->alamat;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $sub_area;
            $row[] = $data_pelanggan->nm_btl;
            $row[] = $data_pelanggan->nm_bangunan;
            $row[] = "".$data_pelanggan->nm_tarif."/".$data_pelanggan->daya."";
           // $row[] = $data_pelanggan->no_gambar;
            $row[] = $data_pelanggan->nm_asosiasi;
            $row[] = date('d F Y H:i:s', strtotime($data_pelanggan->created_at));
            $row[] = $tipe_pelanggan;
            $row[] = $data_pelanggan->Name;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 		

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pelanggan_Model->count_all($params),
                        "recordsFiltered" => count($list),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function querypagingcalonpelangganpln()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));
		
	 	$list = $this->Pelanggan_Model->get_datatables_calon_pelanggan_pln($params);

	 	/*
	 	echo "<pre>";
	 	print_r($list);
	 	echo "</pre>";
	 	exit();
	 	*/
        $data = array();
        $no = $_POST['start'];


        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
          
			
			if($position_id == 1)
			{
				 $button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->idpel."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 
				if(isset($data_pelanggan->status_calon_pelanggan) && $data_pelanggan->status_calon_pelanggan == 3) 
				{
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}

				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->idpel."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->idpel."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}else
			{
				$button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->idpel."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 if(isset($detail_pelanggan->status) && $detail_pelanggan->status == 3) {
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}
													
				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->idpel."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->idpel."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}


			
            $row[] = $data_pelanggan->idpel;
            $row[] = $data_pelanggan->noagenda;
            $row[] = $data_pelanggan->namaplg;
            $row[] = $data_pelanggan->alamatplg;
            $row[] = $data_pelanggan->tarif;
            $row[] = $data_pelanggan->daya;
            $row[] = $data_pelanggan->namaup;
            $row[] = $data_pelanggan->namakab;
      		$row[] = $data_pelanggan->tglbayar;
            $row[] = $data_pelanggan->noregpln;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pelanggan_Model->count_all_calon_pelanggan_pln($params),
                        "recordsFiltered" => $this->Pelanggan_Model->count_filtered_calon_pelanggan_pln($params),
                        "data" => $data,
                );

        //output to json format
        echo json_encode($output);
	}

	function querypagingcalonpelangganweb()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Pelanggan_Model->get_datatables_calon_pelanggan_web($params);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
            if($data_pelanggan->status_calon_pelanggan =="1") 
            { 
					$status = "Baru";
			} 
			else if($data_pelanggan->status_calon_pelanggan =="2") 
			{
					$status = "Dalam Proses";
			} 
			else if(isset($data_pelanggan->status_calon_pelanggan) && $data_pelanggan->status_calon_pelanggan == "3") 
			{
					$status = "Terdaftar";
			} 
			else if($data_pelanggan->status_calon_pelanggan =="4") 
			{
					$status = "Batal";
			} 
			else
			{
					$status = "";
			}
			

			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->id."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 
				if(isset($data_pelanggan->status_calon_pelanggan) && $data_pelanggan->status_calon_pelanggan == 3) 
				{
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}

				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->id."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->id."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}else
			{
				$button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->id."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 if(isset($detail_pelanggan->status) && $detail_pelanggan->status == 3) {
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}
													
				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->id."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->id."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}

			
            $row[] = "".wordwrap($data_pelanggan->nm_kanwil,9,"<br>\n")."";
            $row[] = $data_pelanggan->nm_area;
            $row[] = "".wordwrap($data_pelanggan->nama,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->alamat,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_provinsi,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_kota,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->telepon,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_tarif,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->daya,9,"<br>\n")."";
            $row[] = "".wordwrap(date('d F Y H:i:s', strtotime($data_pelanggan->tgl_daftar)),9,"<br>\n")."";
            $row[] = "WEB";
            $row[] = $status;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pelanggan_Model->count_all_calon_pelanggan_web(),
                        "recordsFiltered" => $this->Pelanggan_Model->count_filtered_calon_pelanggan_web($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}


	function querypagingcalonpelanggandjk()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Pelanggan_Model->get_datatables_calon_pelanggan_djk($params);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
            if($data_pelanggan->status_calon_pelanggan =="1") 
            { 
					$status = "Baru";
			} 
			else if($data_pelanggan->status_calon_pelanggan =="2") 
			{
					$status = "Dalam Proses";
			} 
			else if(isset($data_pelanggan->status_calon_pelanggan) && $data_pelanggan->status_calon_pelanggan == "3") 
			{
					$status = "Terdaftar";
			} 
			else if($data_pelanggan->status_calon_pelanggan =="4") 
			{
					$status = "Batal";
			} 
			else
			{
					$status = "";
			}
			

			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->id."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 
				if(isset($data_pelanggan->status_calon_pelanggan) && $data_pelanggan->status_calon_pelanggan == 3) 
				{
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}

				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->id."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->id."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}else
			{
				$button = "<a href='#popUpJadikanPelanggan' data-calon-pelanggan-id='".$data_pelanggan->id."' title='Jadikan Pelanggan' data-toggle='modal' class='popUpJadikanPelanggan'><i class='icon-person'></i></a>";

				 if(isset($detail_pelanggan->status) && $detail_pelanggan->status == 3) {
				$button.="<a href='".base_url()."pelanggan/view_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
				}
													
				$button.="<br>
															  <a  href='".base_url()."pelanggan/edit_calon/".$data_pelanggan->id."' title='Edit'><i class='fa fa-edit'></i></a>

															  <a href='".base_url()."pelanggan/delete_calon/".$data_pelanggan->id."' title='Delete' onclick=\"return confirm('Apakah anda yakin menghapus data ini?')\"><i class='icon-cross2'></i></a>";
			}

			
            $row[] = "".wordwrap($data_pelanggan->nm_kanwil,9,"<br>\n")."";
            $row[] = $data_pelanggan->nm_area;
            $row[] = "".wordwrap($data_pelanggan->nama,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->alamat,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_provinsi,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_kota,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->telepon,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->nm_tarif,9,"<br>\n")."";
            $row[] = "".wordwrap($data_pelanggan->daya,9,"<br>\n")."";
            $row[] = "".wordwrap(date('d F Y H:i:s', strtotime($data_pelanggan->tgl_daftar)),9,"<br>\n")."";
            $row[] = "DJK";
            $row[] = $status;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pelanggan_Model->count_all_calon_pelanggan_djk(),
                        "recordsFiltered" => $this->Pelanggan_Model->count_filtered_calon_pelanggan_djk($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}


	function ajaxpaging()
	{
		
		$this->load->view('ajaxpaging');
	}

	function upload()
	{
		$user_id = $this->session->userdata('user_id');
		
		$this->load->view('v_upload');
	}

	function uploadCsvBar()
	{
		$created_at = Date("YmdHis");
		error_reporting(0);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$user_id = $this->session->userdata('user_id');
			
		    $path = "./uploads/"; //set your folder path
		    //set the valid file extensions 
		    $valid_formats = array("xls", "xlsx", "csv"); //add the formats you want to upload

		    $name = $_FILES['myfile']['name']; //get the name of the file
		    
		    $size = $_FILES['myfile']['size']; //get the size of the file

		  
		    if (strlen($name)) { //check if the file is selected or cancelled after pressing the browse button.
		        list($txt, $ext) = explode(".", $name); //extract the name and extension of the file
		        if (in_array($ext, $valid_formats)) { //if the file is valid go on.
		            if ($size < 2098888) { // check if the file size is more than 2 mb
		                $file_name = $created_at; //get the file name
		                $file = $file_name.'.'.$ext;
		                $tmp = $_FILES['myfile']['tmp_name'];
		                if (move_uploaded_file($tmp, $path . $file_name.'.'.$ext)) { //check if it the file move successfully.


		                	//$this->checkFile($file);
		                	$uploadId = date('YmdHis');
							$upload_pelanggan = array('upload_id' => $uploadId,
													  'file' => $file,
													  'status' => 'Uploaded'
												);
						
							$upload = $this->Pelanggan_Model->upload_pelanggan($upload_pelanggan);
							
			   					/*
			   					echo "<pre>";
							    print_r($data_excel);
							    echo "</pre>";
							    */
							  	if($upload == 1)
							  	{
							  		echo "Upload Berhasil..<a href='".base_url()."pelanggan/checkFile/".$file_name."'>Cek File</a>";
							  	}
							  	else{
							  		echo "Upload Gagal";
							  	}

		                    
		                } else {
		                    echo "failed";
		                }
		            } else {
		                echo "File size max 2 MB";
		            }
		        } else {
		            echo "Invalid file format..";
		        }
		    } else {
		        echo "Please select a file..!";
		    }
		    exit;
		}
	}


	function checkFile()
	{

			$created_at = date("Y-m-d");
			$user_id = $this->session->userdata('user_id');
			$file = $this->uri->segment(3).'.xls';
			//$file_path =  './uploads/20180427164156.xls';
			//$file = "20180427164156.xls";
 			$file_path =  './uploads/'.$file.'';
			//load the excel library
			$this->load->library('PHPExcel');
			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($file_path);
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

			$sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
         	
         
         	$sheetData = $objPHPExcel->getActiveSheet()->rangeToArray("A1:".$highestColumn.$highestRow,
                                                NULL,
                                                TRUE,
                                               FALSE);
          	$no_pendaftaran = 'jki-'.''.date('YmdHis').'';
         	$kode_pelanggan = date('YmdHis');
			$newArr = array();
			$cell = array();
			$message_error = "";
			$data_excel = array();
			$kd_kota = "";
			$kode_sub_area = "";
			$checkError = "";

			foreach ($sheetData as $key=>$val) {
				if($key==0)
				{
					continue;
				}

				if($val[0]== '' && $val[1] == '' && $val[2] == '' && $val[3] == '' && $val[4] == '' && $val[5] == '' && $val[6] == '' && $val[7] == '' && $val[8] == '' && $val[9] == '' && $val[10] == '' && $val[11] == '' && $val[12] == '' && $val[13] == '' && $val[14] == '' && $val[15] == '' && $val[16] == '')
				{
					continue;
				}

				$length = count($val); 	
				
				$cek_btl = $this->Pelanggan_Model->cek_btl_nama($val[4]);
				$cek_kota = $this->Pelanggan_Model->cek_kota_nama($val[2]);
				$cek_tarif = $this->Pelanggan_Model->cek_tarif_nama($val[5]);
				$cek_daya = $this->Pelanggan_Model->cek_daya_nama($val[6]);
				$cek_bangunan = $this->Pelanggan_Model->cek_bangunan_nama($val[7]);
				$cek_pjt = $this->Pelanggan_Model->cek_pjt_nama($val[8]);
				$cek_area = $this->Pelanggan_Model->cek_area($val[9]);
				$cek_sub_area = $this->Pelanggan_Model->cek_sub_area($val[10]);
				

				if($length != 17)
                {
                	echo "Format Template Baris dan Kolom Salah";
            		exit();
  
                }

				if($val[0]==NULL)
				{
					$valid_nama = "Nama Tidak Boleh Kosong<br>";
					//echo "Nama Tidak Boleh Kosong";
					//exit();
					
				}
				else{

					$valid_nama = "";

				}

				
				if($val[1]=='')
				{
					$valid_alamat = "Alamat Tidak Boleh Kosong<br>";
					//echo "Alamat Tidak Boleh Kosong";
					
            		//exit();
				}
				else{

					$valid_alamat = "";

				}


				if($val[2]=='')
				{	
					$valid_kota = "Kota Tidak Boleh Kosong<br>";
					//echo "Kota Tidak Boleh Kosong";
            		//exit();
				}
				else
				{
					$valid_kota = "";
				}

				if($val[11]=='')
				{
					$valid_no_jil = "No JIL Tidak Boleh Kosong<br>";
					//echo "NO Jil Tidak Boleh Kosong";
            		//exit();
				}
				else
				{
					$valid_no_jil = "";
				}
				
				if(!isset($cek_kota->nm_kota)) 
   				{
   					
   					$valid_cek_kota = "Kota tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_kota ="";
   					$kd_kota = $cek_kota->id_kota;
   					
   				}

   				if(!isset($cek_sub_area->nm_sub_area))
   				{
   					$valid_cek_sub_area = "Sub area tidak ditemukan di database<br>";

   				}else
   				{
   					$kode_sub_area = $cek_sub_area->kode_sub_area;
   				}

   				if(!isset($cek_area->nm_area))
   				{
   					$valid_cek_area = "Area tidak ditemukan di database<br>";

   				}else
   				{
   					$kode_area = $cek_area->kode_area;
   				}

   				if(!isset($cek_btl->nm_btl)) 
   				{
   					
   					$valid_cek_btl = "Nama BTL tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_btl = "";
   				}

   				if(!isset($cek_tarif->nm_tarif)) 
   				{
   					
   					$valid_cek_tarif = "Tarif tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_tarif = "";
   				}

   				if(!isset($cek_daya->daya)) 
   				{
   					
   					$valid_cek_daya = "Daya tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_daya = "";
   				}

   				if(!isset($cek_tarif->nm_tarif)) 
   				{
   					
   					$valid_cek_tarif = "Tarif tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_tarif = "";
   				}

   				if(!isset($cek_bangunan->nm_bangunan)) 
   				{
   					
   					$valid_cek_bangunan = "Jenis bangunan tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_bangunan = "";
   				}

   				if(!isset($cek_pjt->nm_ptl)) 
   				{
   					
   					$valid_cek_pjt = "UPJ tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_pjt = "";
   				}

   				if(!isset($cek_sub_area->nm_sub_area)) 
   				{
   					
   					$valid_cek_sub_area = "Sub area tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_sub_area = "";
   				}

   				if(!isset($cek_area->nm_area)) 
   				{
   					
   					$valid_cek_area = "Area tidak ditemukan di database<br>";
            	
   				}
   				else
   				{
   					$valid_cek_area = "";
   				}
				
   				$message_error = "".$valid_nama.$valid_alamat.$valid_kota.$valid_no_jil.$valid_cek_kota.$valid_cek_pjt.$valid_cek_bangunan.$valid_cek_tarif.$valid_cek_daya.$valid_cek_btl.$valid_cek_sub_area.$valid_cek_area."";

   				$data_excel[] = array('Nama' => $val[0],
									'alamat' => $val[1],
									'nm_kota' => $val[2],
									'telp' => $val[3],
									'nm_btl' => $val[4],
									'tarif' => $val[5],
									'daya' => $val[6],
									'nm_bangunan' => $val[7],
									'nm_ptl' => $val[8],
									'kode_area' => $val[9],
									'kode_sub_area' => $val[10],
									'no_gambar' => $val[11],
									'tgl_gambar' => $val[12],
									'no_sip' => $val[13],
									'tgl_sip' => $val[14],
									'nm_instalir' => $val[15],
									'telp_instalir' => $val[16],
									'message' => $message_error,
									);


   				/*
		   		$data_pelanggan = array(
							"kode_pelanggan"=> $kode_pelanggan,
							"no_pendaftaran"=> $no_pendaftaran,
							"Nama"=> $val[0],
							"alamat"=> $val[1],
							"kd_kota"=> $cek_kota->id_kota,
							"no_ktp"=> '',
							"lattitude"=> '',
							"longitude"=> '',
							"telp"=> $val[3],
							"id_tarif"=> $val[5],
							"id_daya"=> $val[6],
							"id_btl"=> $cek_btl->btl_id,
							"id_bangunan"=> $cek_bangunan->id_bangunan,
							"id_ptl"=> $cek_pjt->id_ptl,
							"kode_sub_area"=> $kode_sub_area,
							"no_gambar"=> $val[10],
							"tgl_gambar"=> $val[11],
							"id_asosiasi"=> '',
							"noreg_pln"=> $val[12],
							"tgl_reg_pln"=> $val[13],
							"nama_instalir"=> $val[14],
							"telp_instalir"=> $val[15],
							"tipe_pelanggan"=> 1
				);	
				*/
			}

				
   					/*
   					echo "<pre>";
				    print_r($data_excel);
				    echo "</pre>";
				    */
				 
				  	$data['checkFile'] = $data_excel;
				  	$data['file'] = $file;
				  	$this->load->view('checkFile',$data);

	}

	function upload_pelanggan()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = 'jki-'.''.date('YmdHis').'';
         	$kode_pelanggan = date('YmdHis');
			$file = $this->input->post('file');
			$user_id = $this->session->userdata('user_id');
			$created_at = Date("Y-m-d H:i:s");

			try {
				
				$file_path =  './uploads/'.$file.'';
				//load the excel library
				$this->load->library('PHPExcel');
				//read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($file_path);
				//get only the Cell Collection
				$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

				$sheet = $objPHPExcel->getSheet(0);
	            $highestRow = $sheet->getHighestRow();
	            $highestColumn = $sheet->getHighestColumn();
	         	
	         
	         	$sheetData = $objPHPExcel->getActiveSheet()->rangeToArray("A1:".$highestColumn.$highestRow,
	                                                NULL,
	                                                TRUE,
	                                               FALSE);
	         	$no = 1;
				foreach ($sheetData as $key=>$val) {
					if($key==0)
					{
						continue;
					}

					if($val[0]== '' && $val[1] == '' && $val[2] == '' && $val[3] == '' && $val[4] == '' && $val[5] == '' && $val[6] == '' && $val[7] == '' && $val[8] == '' && $val[9] == '' && $val[10] == '' && $val[11] == '' && $val[12] == '' && $val[13] == '' && $val[14] == '' && $val[15] == '' && $val[16] == '')
					{
						continue;
					}

					$length = count($val); 	
					
					$cek_btl = $this->Pelanggan_Model->cek_btl_nama($val[4]);
					$cek_kota = $this->Pelanggan_Model->cek_kota_nama($val[2]);
					$cek_tarif = $this->Pelanggan_Model->cek_tarif_nama($val[5]);
					$cek_daya = $this->Pelanggan_Model->cek_daya_nama($val[6]);
					$cek_bangunan = $this->Pelanggan_Model->cek_bangunan_nama($val[7]);
					$cek_pjt = $this->Pelanggan_Model->cek_pjt_nama($val[8]);
					$cek_area = $this->Pelanggan_Model->cek_area($val[9]);
					$cek_sub_area = $this->Pelanggan_Model->cek_sub_area($val[10]);
				

					$user_id = $this->session->userdata('user_id');
					$position_id = $this->session->userdata('position_id');
					$id_departemen = $this->session->userdata('id_departemen');

					$created_at = Date("Y-m-d H:i:s");

					$params_staff = array_filter(array(
			            'position_id' => $position_id,
			            'id_departemen' => $id_departemen,
			      	));



					$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);
					
					$kode_area = "";

					if(isset($get_detail_staff))
					{
						$kode_area = $get_detail_staff->kode_area;
					}

				
					$params_input = array_filter(array(
			            'position_id' => $position_id,
			            'id_departemen' => $id_departemen,
			        	'kode_sub_area' =>$cek_sub_area->kode_sub_area,
			        	'kode_area' =>$kode_area));

					$get_max_kode_pelanggan = $this->Pelanggan_Model->get_max_kode_pelanggan($cek_kota->id_kota);
					$good_total = $get_max_kode_pelanggan->ExtractString;
					
					if($good_total=='') { // bila data kosong
		                $good_total ="0001";
		            }else {
		                $MaksID = $good_total;
		                $MaksID++;
		              
		                if($MaksID < 1) $good_total = "0000".$MaksID; // nilai kurang dari 1000
		                else if($MaksID < 10) $good_total = "000".$MaksID; // nilai kurang dari 10000
		                else if($MaksID < 100) $good_total = "00".$MaksID; // nilai kurang dari 100000
		                else if($MaksID < 1000) $good_total = "0".$MaksID; // nilai kurang dari 10000
		                else $good_total = $MaksID; // lebih dari 10000
		            }


					$current_month = date("m");
					$current_year = date("y");
					
					$no_pendaftaran = "JKI-".$current_month.$current_year."-".$cek_kota->id_kota."-".$good_total."";
					if($no_pendaftaran == ""){
						echo "error";
						die;
					}

			   		$data_pelanggan = array(
								"kode_pelanggan"=> $kode_pelanggan.$no++,
								"no_pendaftaran"=> $no_pendaftaran,
								"Nama"=> $val[0],
								"alamat"=> $val[1],
								"kd_kota"=> $cek_kota->id_kota,
								"no_ktp"=> '',
								"lattitude"=> '',
								"longitude"=> '',
								"telp"=> $val[3],
								"id_tarif"=> $cek_tarif->id_tarif,
								"id_daya"=> $cek_daya->id_daya,
								"id_btl"=> $cek_btl->btl_id,
								"id_bangunan"=> $cek_bangunan->id_bangunan,
								"id_ptl"=> $cek_pjt->id_ptl,
								"kode_sub_area"=> $cek_sub_area->kode_sub_area,
								"kd_area"=> $cek_area->kode_area,
								"no_gambar"=> $val[11],
								"tgl_gambar"=> $val[12],
								"id_asosiasi"=> '',
								"no_sip"=> $val[13],
								"tgl_sip"=> $val[14],
								"nama_instalir"=> $val[15],
								"telp_instalir"=> $val[16],
								"tipe_pelanggan"=> 1,
								"created_at"=> $created_at,
								"created_by"=> $user_id
					);	
			   	
			   		//var_dump($data_pelanggan);
					$insert_pelanggan = $this->Pelanggan_Model->insert($data_pelanggan);
				}

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pelanggan','refresh');
				}

		}
	}

	function edit_Calon()
	{
		$calon_pelanggan_id = $this->uri->segment(3);
		$detail_pelanggan = $this->Pelanggan_Model->get_detail_calon_pelanggan($calon_pelanggan_id);

		$data['detail_pelanggan'] = $detail_pelanggan;
		$this->load->view('edit_calon',$data);
	}

	function proses_edit_Calon_pelanggan()
	{

		$calon_pelanggan_id = $this->input->post('calon_pelanggan_id');
		$status = $this->input->post('status');
		$alasan = $this->input->post('alasan');

					$data_calon_pelanggan = array('status' => $status,
												  'alasan' => $alasan);

					$update_calon_pelanggan = $this->Pelanggan_Model->update_calon_pelanggan($data_calon_pelanggan,$calon_pelanggan_id);

		if($update_calon_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pelanggan','refresh');
				}


	}

	function add()
	{	
		$kode_area = "";
		$kode_kanwil = "";
		$area = "";
		$sub_area = "";
		$tipe_pelanggan = "";
		$detail_pelanggan = "";

		$tipe_pelanggan = $this->input->post('tipe_pelanggan');
		$no_slo = $this->input->post('no_slo');
		$calon_pelanggan_id = $this->input->post('calon_pelanggan_id');
		$tipe_calon_pelanggan_id = $this->input->post('tipe_calon_pelanggan_id');


		if(isset($tipe_pelanggan))
		{
			$tipe_pelanggan = $this->input->post('tipe_pelanggan');
		}

		
		$calon_pelanggan= new stdClass;
		if(isset($calon_pelanggan_id))
		{
			if($calon_pelanggan_id != "")
			{
				//$detail_pelanggan = $this->Pelanggan_Model->get_detail_calon_pelanggan($calon_pelanggan_id);

				if($tipe_calon_pelanggan_id == 'pln')
				{
					$detail_pelanggan = $this->Pelanggan_Model->get_detail_calon_pelanggan_pln($calon_pelanggan_id);
					
					$cek_kota = $this->Kota_model->get_detail_kota_prov($detail_pelanggan->kodekab);
					$cek_ptl = $this->Ptl_model->get_by_kode_unit_pln($detail_pelanggan->unitup);
					
					$data['cek_kota'] = $cek_kota;
					$data['cek_ptl'] = $cek_ptl;

				
					$area = $this->Kantor_Sub_Area_Model->get_area($cek_ptl->kode_kanwil);
					$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($cek_ptl->kode_area);
					$data['area'] = $area;
					$data['sub_area'] = $sub_area;

				}

				else
				{
					$detail_pelanggan = $this->Pelanggan_Model->get_detail_calon_pelanggan($calon_pelanggan_id);

					$area = $this->Kantor_Sub_Area_Model->get_area($detail_pelanggan->id_wilayah);
					$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($detail_pelanggan->id_area);
					$data['area'] = $area;
					$data['sub_area'] = $sub_area;

				}
				//var_dump($detail_pelanggan);
				if($detail_pelanggan == "")
				{
					$this->session->set_flashdata('error', 'ID Calon Pelanggan Tidak Ditemukan');
					redirect('pelanggan','refresh');
					exit();
				}
				else
				{	
					//echo $detail_pelanggan->id_wilayah;

					$data['calon_pelanggan'] = $detail_pelanggan;
				}

			}
		}

		if(isset($no_slo))
		{
			if($no_slo != "")
			{

				$detail_pelanggan = $this->Pelanggan_Model->get_detail_pelanggan($no_slo);

				if($detail_pelanggan == "")
				{
					$this->session->set_flashdata('error', 'Nomor SLO tidak ditemukan');
					redirect('pelanggan','refresh');
					exit();
				}
				else
				{
					$area = $this->Kantor_Sub_Area_Model->get_area($detail_pelanggan->kode_kanwil);
					$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($detail_pelanggan->kode_area);
					$data['area'] = $area;
					$data['sub_area'] = $sub_area;
					$data['detail_pelanggan'] = $detail_pelanggan;
				}
			}
			
		}

	

		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));


		$user_id = $this->session->userdata('user_id');
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);
		

		if(isset($get_detail_staff))
		{
			$kode_area = $get_detail_staff->kode_area;
			$kode_kanwil = $get_detail_staff->kode_kanwil;
		}
		

		$params = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
        	'kode_area' =>$kode_area,
        	'kode_kanwil' =>$kode_kanwil));

		if($position_id == 2)
		{

			$data['kanwil_staff'] = $this->Kanwil_Model->get_by_user_id($user_id,$params);

			if(!isset($data['kanwil_staff']->kode_sub_area) or $data['kanwil_staff']->kode_sub_area == "")
			{
				$data['sub_area_staff'] = $this->Kantor_Sub_Area_Model->get_sub_area($data['kanwil_staff']->kode_area);
			}

			if(!isset($data['kanwil_staff']->kode_area) or $data['kanwil_staff']->kode_area == "")
			{
				$data['area_staff'] = $this->Kantor_Sub_Area_Model->get_area($data['kanwil_staff']->kode_kanwil);
			}

			

		}
		

		$data['calon_pelanggan_id'] = $calon_pelanggan_id;
		$data['position_id'] = $position_id;
		
		$data['tipe_pelanggan'] = $tipe_pelanggan;
		
		$data['kanwil'] = $this->Kanwil_Model->get_kanwil_by_user($params);
		$data['kota'] = $this->Pelanggan_Model->get_kota($params);
		$data['tarif'] = $this->Pelanggan_Model->get_tarif();
		$data['daya'] = $this->Pelanggan_Model->get_daya();
		$data['btl'] = $this->Pelanggan_Model->get_btl($params);
		$data['bangunan'] = $this->Pelanggan_Model->get_bangunan();
		$data['ptl'] = $this->Pelanggan_Model->get_ptl($params);
		$data['asosiasi'] = $this->Pelanggan_Model->get_asosiasi();

		
		//var_dump($detail_pelanggan);
		//exit();
		$this->load->view('add',$data);

		
	
	}

	function save_pelanggan()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$user_id = $this->session->userdata('user_id');
			$position_id = $this->session->userdata('position_id');
			$id_departemen = $this->session->userdata('id_departemen');
			
			$created_at = Date("Y-m-d H:i:s");

			
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$no_ktp = $this->input->post('no_ktp');
			$telp = $this->input->post('telp');
			$tarif = $this->input->post('tarif');
			$daya = $this->input->post('daya');
			$calon_pelanggan_id = $this->input->post('calon_pelanggan_id');

			$tarif_lama = $this->input->post('tarif_lama');
			$daya_lama = $this->input->post('daya_lama');

			$btl = $this->input->post('btl');
			$bangunan = $this->input->post('bangunan');
			$ptl = $this->input->post('ptl');
			$sub_area = $this->input->post('sub_area');
			$area = $this->input->post('area');
			$kanwil = $this->input->post('kanwil');
			$no_gambar = $this->input->post('no_gambar');
			$asosiasi = $this->input->post('asosiasi');
			$no_reg = $this->input->post('no_reg');
			$tgl_rek = $this->input->post('tgl_rek');
			$tgl_gambar = $this->input->post('tgl_gambar');
			$nm_instalir = $this->input->post('nm_instalir');
			$telp_instalir = $this->input->post('telp_instalir');
			$lattitude = $this->input->post('lattitude');
			$longitude = $this->input->post('longitude');
			$tipe_pelanggan = $this->input->post('tipe_pelanggan');

		
			$params_input = array_filter(array(
	            'position_id' => $position_id,
	            'id_departemen' => $id_departemen,
	        	'kode_sub_area' =>$sub_area,
	        	'kode_area' =>$area,
	        	'kode_kanwil' => $kanwil));

			$get_max_kode_pelanggan = $this->Pelanggan_Model->get_max_kode_pelanggan($kota);
			$good_total = $get_max_kode_pelanggan->ExtractString;
			
			if($good_total=='') { // bila data kosong
                $good_total ="0001";
            }else {
                $MaksID = $good_total;
                $MaksID++;
              
                if($MaksID < 1) $good_total = "0000".$MaksID; // nilai kurang dari 1000
                else if($MaksID < 10) $good_total = "000".$MaksID; // nilai kurang dari 10000
                else if($MaksID < 100) $good_total = "00".$MaksID; // nilai kurang dari 100000
                else if($MaksID < 1000) $good_total = "0".$MaksID; // nilai kurang dari 10000
                else $good_total = $MaksID; // lebih dari 10000
            }

           

			$current_month = date("m");
			$current_year = date("y");
			
			$no_pendaftaran = "JKI-".$current_month.$current_year."-".$kota."-".$good_total."";
			$kode_pelanggan = date('YmdHis');
			$created_at = date('Y:m:d H:i:s');
			$location = $this->input->post('location');

			try {
				
				$data_pelanggan = array(
					"kode_pelanggan"=> $no_pendaftaran,
					"no_pendaftaran"=> $no_pendaftaran,
					"Nama"=> $nama,
					"alamat"=> $alamat,
					"kd_kota"=> $kota,
					"no_ktp"=> $no_ktp,
					"lattitude"=> $lattitude,
					"longitude"=> $longitude,
					"telp"=> $telp,
					"id_tarif"=> $tarif,
					"id_daya"=> $daya,
					"tarif_lama"=> $tarif_lama,
					"daya_lama"=> $daya_lama,
					"id_btl"=> $btl,
					"id_bangunan"=> $bangunan,
					"id_ptl"=> $ptl,
					"kode_sub_area"=> $sub_area,
					"kd_area"=> $area,
					"no_gambar"=> $no_gambar,
					"tgl_gambar"=> $tgl_gambar,
					"id_asosiasi"=> $asosiasi,
					"noreg_pln"=> $no_reg,
					"tgl_reg_pln"=> $tgl_rek,
					"nama_instalir"=> $nm_instalir,
					"telp_instalir"=> $telp_instalir,
					"tipe_pelanggan"=> $tipe_pelanggan,
					"created_at"=> $created_at,
					"created_by"=> $user_id,
					"location"=>$location,
					"no_urut_pelanggan"=>$good_total
				);
				

				/*
				echo "<pre>";
				print_r($data_pelanggan);
				echo "</pre>";

			
				exit();
				*/

				$insert_pelanggan = $this->Pelanggan_Model->insert($data_pelanggan);

				if($calon_pelanggan_id != "")
				{
					$data_calon_pelanggan = array('status' => 3,
												  'no_pendaftaran' => $no_pendaftaran);

					$update_calon_pelanggan = $this->Pelanggan_Model->update_calon_pelanggan($data_calon_pelanggan,$calon_pelanggan_id);
				}

				
			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}


				if($insert_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pelanggan','refresh');
				}


		}
	}

	function edit()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);

		$kode_area = "";
		$kode_kanwil = "";
		$area = "";
		$sub_area = "";
		$tipe_pelanggan = "";
		$detail_pelanggan = "";

			$detail_pelanggan = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
			//var_dump($detail_pelanggan);
			if($detail_pelanggan == NULL)
			{
				$this->session->set_flashdata('error', 'Nomor SLO tidak ditemukan');
				redirect('pelanggan','refresh');
				exit();
			}else
			{
				$area = $this->Kantor_Sub_Area_Model->get_area($detail_pelanggan->kode_kanwil);
				$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($detail_pelanggan->kode_area);

			}


		$area = $this->Kantor_Sub_Area_Model->get_area($detail_pelanggan->kode_kanwil);
		$sub_area = $this->Kantor_Sub_Area_Model->get_sub_area($detail_pelanggan->kode_area);
		$position_id = $this->session->userdata('position_id');
		
		$user_id = $this->session->userdata('user_id');

		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);
		

		if(isset($get_detail_staff))
		{
			$kode_area = $get_detail_staff->kode_area;
			$kode_kanwil = $get_detail_staff->kode_kanwil;
		}
		

		$params = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
        	'kode_area' =>$kode_area,
        	'kode_kanwil' =>$kode_kanwil));

		
		if($position_id == 2)
		{

			$data['kanwil_staff'] = $this->Kanwil_Model->get_by_user_id($user_id,$params);

		}
		
		$data['position_id'] = $position_id;
		$data['area'] = $area;
		$data['sub_area'] = $sub_area;
		$data['tipe_pelanggan'] = $tipe_pelanggan;
		$data['detail_pelanggan'] = $detail_pelanggan;
		$data['kanwil'] = $this->Kanwil_Model->get_kanwil_by_user($params);
		$data['kota'] = $this->Pelanggan_Model->get_kota($params);

		$data['tarif'] = $this->Pelanggan_Model->get_tarif();
		$data['daya'] = $this->Pelanggan_Model->get_daya();
		$data['btl'] = $this->Pelanggan_Model->get_btl($params);
		$data['bangunan'] = $this->Pelanggan_Model->get_bangunan();
		$data['ptl'] = $this->Pelanggan_Model->get_ptl($params);
		$data['asosiasi'] = $this->Pelanggan_Model->get_asosiasi();


		$this->load->view('edit',$data);


	}

	function get_sbu($kode_djp_kota)
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
			'kota_id' => $kode_djp_kota//optional
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'https://slodjk.esdm.go.id/api/ref/sbu'); 
		//curl_setopt ($ch, CURLOPT_URL, 'https://localhost/slo/new/api/ref/sbu'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;
	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$user_id = $this->session->userdata('user_id');
			$created_at = Date("Y-m-d H:i:s");

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$kota = $this->input->post('kota');
			$no_ktp = $this->input->post('no_ktp');
			$telp = $this->input->post('telp');
			$tarif_lama = $this->input->post('tarif_lama');
			$daya_lama = $this->input->post('daya_lama');
			$tarif = $this->input->post('tarif');
			$daya = $this->input->post('daya');
			$btl = $this->input->post('btl');
			$bangunan = $this->input->post('bangunan');
			$ptl = $this->input->post('ptl');
			$sub_area = $this->input->post('sub_area');
			$area = $this->input->post('area');
			$no_gambar = $this->input->post('no_gambar');
			$asosiasi = $this->input->post('asosiasi');
			$no_reg = $this->input->post('no_reg');
			$tgl_rek = $this->input->post('tgl_rek');
			$tgl_gambar = $this->input->post('tgl_gambar');
			$nm_instalir = $this->input->post('nm_instalir');
			$telp_instalir = $this->input->post('telp_instalir');
			$tipe_pelanggan = $this->input->post('tipe_pelanggan');


			try {
				
				$data_pelanggan = array(
					"no_pendaftaran"=> $no_pendaftaran,
					"Nama"=> $nama,
					"alamat"=> $alamat,
					"kd_kota"=> $kota,
					"no_ktp"=> $no_ktp,
					"telp"=> $telp,
					"tarif_lama"=> $tarif_lama,
					"daya_lama"=> $daya_lama,
					"id_tarif"=> $tarif,
					"id_daya"=> $daya,
					"id_btl"=> $btl,
					"id_bangunan"=> $bangunan,
					"id_ptl"=> $ptl,
					"kode_sub_area"=> $sub_area,
					"kd_area"=> $area,
					"no_gambar"=> $no_gambar,
					"tgl_gambar"=> $tgl_gambar,
					"id_asosiasi"=> $asosiasi,
					"noreg_pln"=> $no_reg,
					"tgl_reg_pln"=> $tgl_rek,
					"nama_instalir"=> $nm_instalir,
					"telp_instalir"=> $telp_instalir
				);
				/*
				echo "<pre>";
				print_r($data_pelanggan);
				echo "</pre>";
				exit();
				*/
				$update_pelanggan = $this->Pelanggan_Model->update($data_pelanggan,$no_pendaftaran);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pelanggan');

			}

			if($update_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data berhasil di edit');
						redirect('pelanggan','refresh');
				}
		}
	}


	function kirim_permohonan_djk($data_pelanggan)
	{
		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
			'nama' => $data_pelanggan['nama'],
			'email' => $data_pelanggan['email'],
			'alamat' => $data_pelanggan['alamat'],
			'kota_id' => $data_pelanggan['kota_id'],
			'no_telpon' => $data_pelanggan['no_telpon'],
			'daya' => $data_pelanggan['daya'],
			'unit_pelayanan_id' => $data_pelanggan['unit_pelayanan_id'],
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'https://slodjk.esdm.go.id/api/tr/permohonan/create'); 
		//curl_setopt ($ch, CURLOPT_URL, 'https://localhost/slo/new/api/tr/permohonan/create'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;
	}

	function view_pelanggan()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
		$this->load->view('view_pelanggan',$data);
	}

	function view_calon_pelanggan()
	{
		$id = $this->uri->segment(3);
		$data['detail_calon_pelanggan'] = $this->Pelanggan_Model->get_detail_calon_pelanggan($id);
		$this->load->view('view_calon_pelanggan',$data);
	}

	function view_map()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
		$this->load->view('view_map',$data);
	}

	function delete()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$delete_pelanggan = $this->Pelanggan_Model->delete($no_pendaftaran);

		if($delete_pelanggan == 1) 
		{
				$this->session->set_flashdata('success', 'Data berhasil di hapus');
				redirect('pelanggan','refresh');
		}
	}

	function delete_calon()
	{
		$calon_pelanggan_id = $this->uri->segment(3);
		$delete_calon_pelanggan = $this->Pelanggan_Model->delete_calon_pelanggan($calon_pelanggan_id);

		if($delete_calon_pelanggan == 1) 
		{
				$this->session->set_flashdata('success', 'Data berhasil di hapus');
				redirect('pelanggan','refresh');
		}
	}

	function add_agenda()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
		//var_dump($data['detail_pelanggan']);
		$this->load->view('add_agenda',$data);


		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$no_pendaftaran = $this->input->post('no_pendaftaran');
			$detail_pelanggan = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);

			$no_telp = $this->input->post('no_telp');
			$email = $this->input->post('email');

			try {

				$data_pelanggan = array(
					"nama"=> $detail_pelanggan->Nama,
					"email"=> $email,
					"alamat"=> $detail_pelanggan->alamat,
					"kota_id"=> $detail_pelanggan->kode_djp_kota,
					"no_telpon"=> $no_telp,
					"daya"=> $detail_pelanggan->daya,
					"unit_pelayanan_id"=> $detail_pelanggan->kode_unit_pln
				);
				

				$response = $this->kirim_permohonan_djk($data_pelanggan);
			
				$data_permohonan = json_decode($response,TRUE);

				if($data_permohonan['metadata']['code'] == 1)
				{
					$no_agenda = $data_permohonan['response']['no_agenda'];
				}
				else
				{
					$this->session->set_flashdata('error', ''.$data_permohonan['metadata']['message'].'');
					redirect('pelanggan','refresh');
					exit();
				}

				
				$data_no_agenda = array(
					"no_agenda"=> $no_agenda,
					"email_pelanggan"=> $email,
				);

				$update_pelanggan = $this->Pelanggan_Model->update($data_no_agenda,$no_pendaftaran);

				
			} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal Kirim Ke DJK');
					redirect('pelanggan','refresh');

			}


			if($update_pelanggan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('pelanggan','refresh');
				}

		}

	}

	
}
