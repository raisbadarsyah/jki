<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); //error_reporting(0);

class Management_user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_user_model'));
		$this->load->helper(array('check_auth_menu','email_content'));
		$this->load->library('upload');
		//check_authority_url();		
	}

	function index()
	{
		$position_id = $this->session->userdata('position_id');
		$config['title'] = 'List User Aktive';
		$config['page_title'] = 'List User Aktive';
		$config['page_subtitle'] = 'User List';

		
	   	$config['super_user_list'] = $this->management_user_model->get_all();
	   

        $config['dep_list'] = $this->management_user_model->get_all_dep();
        $config['pos_list'] = $this->management_user_model->get_all_pos();

       
		$this->load->view('v_list', $config);
	}
	
	function user_verifikasi()
	{
		$config['title'] = 'List New User';
		$config['page_title'] = 'List New User';
		$config['page_subtitle'] = 'User List';
		
		$group = $this->session->userdata('dep_id');
		$user_id   = $this->session->userdata('user_id');
		
		if($group !=3){
			$principal_list = $this->menu_model->get_principal_list($user_id);
		} 
		else {
			$principal_list = $this->management_user_model->get_all_principal();
		}
				
		if(count($principal_list) > 1){
			foreach($principal_list as $row){
				$field[] = $row->suplierId;
			}

			$field_output = implode(',', $field);
		} 
		else {
			$principal_list = $this->menu_model->get_principal_list($user_id);
			foreach($principal_list as $row ){
				$principal_id = $row->suplierId;
			}
			
			$field_output = $principal_id;
		}
		
		$config['user_set_principal'] = $principal_list;
		
		$cabang_set    = $this->input->post('cabang');
		$principal_set = $this->input->post('principal_set');
		if(!empty($cabang_set) && !empty($principal_set)){
			$config['user_list']          = $this->management_user_model->get_all_new_user_by_set($cabang_set, $principal_set);  
			$config['cabang_set_view']    = $cabang_set;
			$config['principal_set_view'] = $principal_set;
		} else {
			$config['user_list'] = $this->management_user_model->get_all_new_user($field_output);   
            //$config['user_list'] = NULL;			
        }			
        //echo $this->db->last_query();die();
        $config['cabang'] = $this->management_user_model->get_branch();

		$this->load->view('v_new_user', $config);
	}
	
	function profile_by_id()
	{
		$id     = $this->uri->segment(3);

		$position_id = $this->session->userdata('position_id');
		$level = $this->session->userdata('level');

		$config['profile']     = $this->management_user_model->get_super_user_by_id($id);

		$this->load->view('v_profile', $config);

		
	}

	function email_verifikasi($user_id, $principal_id, $cabang_id){
		//$email       = $this->management_user_model->get_email_user($user_id);
		//$token       = $this->management_user_model->get_token_user($user_id);
		$status_user           = $this->management_user_model->get_status_user($user_id);
		$cek_exist_user        = $this->management_user_model->count_user_principal_on_set($user_id, $principal_id);
		$cek_exist_user_cabang = $this->management_user_model->count_user_cabang_on_set($user_id, $cabang_id); 
	
		if($status_user == 3){			
			$data = array('coreUserActive' => 1);
			$this->management_user_model->update_user($data, $user_id);			
			
			if(empty($cek_exist_user)){
				//set user principal
				$data = array(
					'coreUserUserId'      => $user_id,
					'coreUserPrincipalId' => $principal_id
				);
			
			    $this->management_user_model->insert_user_principal($data);
			}
			
			if(empty($cek_exist_user_cabang)){
				//set user cabang
				$data_cabang = array(
					'coreUserUserId'      => $user_id,
					'coreUserBranchId' => $cabang_id
				);
				
				$action = $this->management_user_model->insert_user_cabang($data_cabang);
			}
				
			if($action){
				$this->session->set_flashdata('message','User berhasil diaktivasi.');
			} else {
				$this->session->set_flashdata('message','User gagal diaktivasi.');
			}
		} 
		else {		
			if(empty($cek_exist_user)){
				//set user principal
				$data = array(
					'coreUserUserId'      => $user_id,
					'coreUserPrincipalId' => $principal_id
				);
			
			    $this->management_user_model->insert_user_principal($data);
			}
			
			if(empty($cek_exist_user_cabang)){
				//set user cabang
				$data_cabang = array(
					'coreUserUserId'      => $user_id,
					'coreUserBranchId' => $cabang_id
				);
				
				$action = $this->management_user_model->insert_user_cabang($data_cabang);
			}
			
			if($action){
				$this->session->set_flashdata('message','User berhasil diaktivasi.');
			} else {
				$this->session->set_flashdata('message','User gagal diaktivasi.');
			}
		}
		
		redirect(base_url().'management_user/user_verifikasi','refresh');
	}
	
	function email_verifikasi_multi_user(){
		$coreUserId   = $this->input->post('coreUserId');
		$principal_id = $this->input->post('principal_set_id_check');
		$cabang_id    = $this->input->post('cabang_set_id_check');
		
		if(!empty($coreUserId)){
			for($i=0; $i<count($coreUserId); $i++){
				//$email = $this->management_user_model->get_email_user($coreUserId[$i]);
				//$token = $this->management_user_model->get_token_user($coreUserId[$i]);
				$status_user = $this->management_user_model->get_status_user($coreUserId[$i]);
				$cek_exist_user = $this->management_user_model->count_user_principal_on_set($coreUserId[$i], $principal_id[$i]);
				$cek_exist_user_cabang = $this->management_user_model->count_user_cabang_on_set($coreUserId[$i], $cabang_id[$i]); 
				
				if($status_user == 3){				
					$data = array('coreUserActive' => 1);
					$this->management_user_model->update_user($data, $coreUserId[$i]);
					
					if(empty($cek_exist_user)){
						//set user principal
						$data = array(
							'coreUserUserId'      => $coreUserId[$i],
							'coreUserPrincipalId' => $principal_id[$i]
						);
						
						$this->management_user_model->insert_user_principal($data);
					}
					
					if(empty($cek_exist_user_cabang)){
						//set user cabang
						$data_cabang = array(
							'coreUserUserId'      => $coreUserId[$i],
							'coreUserBranchId' => $cabang_id[$i]
						);
					
						$action = $this->management_user_model->insert_user_cabang($data_cabang);	
					}
				} 
				else {
					if(empty($cek_exist_user)){
						//set user principal
						$data = array(
							'coreUserUserId'      => $coreUserId[$i],
							'coreUserPrincipalId' => $principal_id[$i]
						);
						
						$this->management_user_model->insert_user_principal($data);
					}
					
					if(empty($cek_exist_user_cabang)){					
						//set user cabang
						$data_cabang = array(
							'coreUserUserId'      => $coreUserId[$i],
							'coreUserBranchId' => $cabang_id[$i]
						);
						
						$action = $this->management_user_model->insert_user_cabang($data_cabang);	
					}
				}
			}
			
			if($action){
				$this->session->set_flashdata('message','User berhasil diaktivasi.');
			} else {
				$this->session->set_flashdata('message','User berhasil diaktivasi.');
			}
		} else {
			 $this->session->set_flashdata('message','Silahkan pilih user yang akan diaktivasi.');
		}
		redirect(base_url().'management_user/user_verifikasi','refresh');		
	}
	
	function ubah_password()
	{
		$user_id    = $this->input->post('user_id');
		$current_pass    = $this->input->post('current_pass');
		$new_pass    = $this->input->post('new_pass');
		$repeat_pass    = $this->input->post('repeat_pass');
		$username    = $this->input->post('username');
		$email    = $this->input->post('email');

		if(empty($user_id))
		{
			$this->session->set_flashdata('error', 'user id tidak terbaca');
			redirect(base_url() . 'management_user/profile_by_id/'.$this->session->userdata('user_id').'', 'refresh');
			exit();
		}

		$cek_password = $this->management_user_model->check_pass(md5($current_pass),$user_id);

		if(empty($cek_password))
		{
			$this->session->set_flashdata('error', 'Password Lama Anda Salah');
			redirect(base_url() . 'management_user/profile_by_id/'.$user_id.'', 'refresh');
			exit();
		}

		if($new_pass != $repeat_pass)
		{
			$this->session->set_flashdata('error', 'New Password tidak sesuai dengan repeat password');
			redirect(base_url() . 'management_user/profile_by_id/'.$user_id.'', 'refresh');
			exit();
		}

		if($new_pass == $current_pass)
		{
			$this->session->set_flashdata('error', 'Password baru tidak boleh sama dengan password lama');
			redirect(base_url() . 'management_user/profile_by_id/'.$user_id.'', 'refresh');
			exit();
		}

		try {
			
			$data = array(
				"coreUserPassword" 		=> md5($new_pass),
				"coreUserPasswordTxt" 		=> $new_pass
			);

			$action = $this->management_user_model->update_super_user($data, $user_id);

		} catch (Exception $e) {

			 $this->db->trans_rollback();
			 throw new Exception('Model whats_its_name has nothing to process');
			 $this->session->set_flashdata('error', 'Failed to update data');
			 redirect(base_url() . 'management_user/profile_by_id/'.$user_id.'', 'refresh');

		}

	
		$this->session->set_flashdata('success', 'Password berhasil dirubah');
		redirect(base_url() . 'management_user/profile_by_id/'.$user_id.'', 'refresh');

	}

	function add(){
		$config['title'] = 'Management User';
		$config['page_title'] = 'Management User';
		$config['page_subtitle'] = 'User List';
	   	
	   	$config['position_list'] = $this->management_user_model->get_su_position();
	   	$config['branch'] = $this->master_branch_model->get_data();

		$this->load->view('v_add', $config);
	}

	function insert_user(){
		$coreUsername  = $this->input->post('username');
		$corePassword  = md5($this->input->post('password'));
		$coreUserPositionId  = $this->input->post('position');
		$branchId  = $this->input->post('branch');		
		
		$data = array(
			"coreUserPassword" 		=> $corePassword,
			"coreUserPasswordTxt" 	=> $this->input->post('password'),
			"coreUserName"    		=> $coreUsername,
			"coreUserPositionId"	=> $coreUserPositionId,
			"branchId"				=> $branchId,
			"coreUserCodeActivate"  => 1,
			"coreUserActive"     	=> 1
		);

	
		$action = $this->management_user_model->insert_user($data);
			
		
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been added successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to add user');
		}

		redirect('management_user','refresh');
	}

	function checkUsername($uname){
		$this->management_user_model->check_username($uname);
	}
	
	function edit($id){
		$config['title'] = 'Management User';
		$config['page_title'] = 'Management User';
		$config['page_subtitle'] = 'User List';
	    $id = $this->uri->segment(3);
        $config['dep_list'] = $this->management_user_model->get_all_dep();
        $config['pos_list'] = $this->management_user_model->get_all_pos();
        $config['user']     = $this->management_user_model->get_user_by_id($id);
		$config['principal_list'] = $this->management_user_model->get_all_principal();
       
		$this->load->view('v_edit', $config);
	}
	
	function update(){	
		$coreUserId    = $this->input->post('userId');
		$coreUsername  = $this->input->post('username');
		$pass          = $this->input->post('password');
		$corePassword  = md5($this->input->post('password'));
		$coreRealName  = $this->input->post('realName');
		$coreUserEmail = $this->input->post('email');
		$coreUserGroup = $this->input->post('group');
		
		if(!empty($pass)){
			$data = array(
				"coreUserUserName" 		=> $coreRealName,
				"coreUserPassword" 		=> $corePassword,
				//"coreUserName"    		=> $coreUsername,
				"coreUserEmail"     	=> $coreUserEmail,
				"coreUserActive"     	=> 1
			);
		}else{
			$data = array(
				"coreUserUserName" 		=> $coreRealName,
				//"coreUserName"    		=> $coreUsername,
				"coreUserEmail"     	=> $coreUserEmail,
				"coreUserActive"     	=> 1
			);
		}
		
		$action = $this->management_user_model->update_user($data, $coreUserId);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update user');
		}	
		redirect(base_url() . 'management_user', 'refresh');
	}
	
	function delete($id){
		$id     = $this->uri->segment(3);
		$action = $this->management_user_model->delete_user($id);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been delete successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to delete user');
		}	
		redirect(base_url() . 'management_user', 'refresh');
	}
	
	function active($id){
		$data = array(
			'coreUserActive' => '1'
		);
		
		$action = $this->management_user_model->active_user($id, $data);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been active successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to active user');
		}	
		redirect(base_url() . 'management_user', 'refresh');
	}
	
	function inactive($id){
		$data = array(
			'coreUserActive' => '0'
		);
		
		$action = $this->management_user_model->active_user($id, $data);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'User has been in active successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to in active user');
		}	
		redirect(base_url() . 'management_user', 'refresh');
	}
	
	function download(){			
		$data = $this->management_user_model->get_all();
		
		$this->load->library("PHPExcel");
		$PHPExcel = new PHPExcel();
		$PHPExcel->getProperties()->setTitle('title')->setDescription('description');
		$PHPExcel->setActiveSheetIndex(0);
		
		
		$kolom = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O');
		$header = array('No','Principal Name','Username','Email');
						
		$jml_kolom = count($header); 
					
		//Creating Border -------------------------------------------------------------------
		$styleArray = array(
			   'borders' => array(
					 'outline' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => '000000'),
					 ),
					 'inside' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => '000000'),
					 ),
			   ),
			   'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'size'  => 12,
					'name'  => 'Calibri'
				),
		);		
		
		//Column Header
		for($i=0;$i<$jml_kolom;$i++){
			$PHPExcel->getActiveSheet()->setCellValue("$kolom[$i]2", $header[$i]);
		}
		
		//width colum
		$PHPExcel->getActiveSheet()->getColumnDimension("A")->setWidth(5);
		$PHPExcel->getActiveSheet()->getColumnDimension("B")->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension("C")->setWidth(25);
		$PHPExcel->getActiveSheet()->getColumnDimension("D")->setWidth(35);
		$PHPExcel->getActiveSheet()->getColumnDimension("E")->setWidth(25);
		
		//Header Style
		$tmp = $jml_kolom-1;
		$PHPExcel->getActiveSheet()->getStyle("A2:$kolom[$tmp]2")->getFont()->setBold(true);
		$PHPExcel->getActiveSheet()->getStyle("A2:$kolom[$tmp]2")->getAlignment()->setHorizontal('center');
		$PHPExcel->getActiveSheet()->getStyle("A2:$kolom[$tmp]2")->getFill()->setFillType('solid')->getStartColor()->setRGB('E0E0E0');
		$PHPExcel->getActiveSheet()->getStyle("A2:$kolom[$tmp]2")->applyFromArray($styleArray);
	
		
		$set_kolom = 3;
		$total_row = count($data);
		
		$no=0;
		$ar=0;			
		$grand_total =0;

		
		foreach ($data as $val) { 
		    if(empty($val->supplierPrincipal)){
				$principalname = "All";
			} else {
				$principalname = $val->supplierPrincipal;
			}
			$PHPExcel->getActiveSheet()->setCellValue($kolom[0].$set_kolom, ++$no);			
			$PHPExcel->getActiveSheet()->setCellValue($kolom[1].$set_kolom, $principalname);
			$PHPExcel->getActiveSheet()->setCellValue($kolom[2].$set_kolom, $val->coreUserName);
			$PHPExcel->getActiveSheet()->setCellValue($kolom[3].$set_kolom, $val->coreUserEmail);
		
				
			$set_kolom++;									
		}
		
	
		
		for($box=1; $box<=$total_row+3; $box++){
			$PHPExcel->getActiveSheet()->getStyle($kolom[0].$box.':'.$kolom[3].$box)->applyFromArray($styleArray);				
		}
				
		// Save it as file ------------------------------------------------------------------
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="daftar_user.xls"');
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
		$objWriter->save('php://output');
		//-----------------------------------------------------------------------------------
	}
	
	function import()
	{

		$config['title'] = 'Import User';
		$config['page_title'] = 'Import Excel User';
		$config['page_subtitle'] = 'Import User';
        $config['file']   = base_url().'uploads/format/import.xlsx';

		$this->load->view('v_import', $config);
	}

	function import_excel(){
		$PHPExcel = $this->load->library("PHPExcel"); 
		$fileName = $_FILES['file_excel']['name'];
		$inputFileName = ($_FILES['file_excel']['tmp_name']);
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		$ObjReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel5');
		$ObjPHPExcel = $ObjReader->load($inputFileName);
		$ObjWorksheet = $ObjPHPExcel->setActiveSheetIndex(0);

		$data = array();

		foreach ($ObjWorksheet->getRowIterator() as $row) {
			$row_data = array();
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			foreach ($cellIterator as $cell) {
				if (!is_null($cell)) {
					if(strstr($cell,'=')==true)
					{
						$row_data[] = trim($cell->getOldCalculatedValue());
					} else {
						$row_data[] = trim($cell->getCalculatedValue());
					}
				}  
			}
			
			$data[] = $row_data;
		}
		
		if ($data){
                unset($data[0]);
		}
		
		//loop data from excel
        foreach ($data as $key => $val) {
			$coreUserUserName  	= $val['1'];
			$coreUserName       = $val['2'];
			$coreUserPassword  	= md5($val['3']);
			$coreUserEmail 	    = $val['4'];
			
			
			$data = array(
				"coreUserUserName" 		=> $coreUserUserName,
				"coreUserPassword" 		=> $coreUserPassword,
				"coreUserName"          => $coreUserName,
				"coreUserDepId"     	=> '1',
				"coreUserPositionId"    => '5',
				"coreUserEmail"     	=> $coreUserEmail
			);

		    $action = $this->management_user_model->insert_user($data);
		}

		if($action) {
			$this->session->set_flashdata('message', 'register has been added successfully');
		}else{
			$this->session->set_flashdata('error', 'Failed to add');
		    redirect('management_user/import','refresh');
		}
		
		redirect(base_url() . 'management_user', 'refresh');
	}
	
	/* function test_mail(){
        $this->load->library('PHPMailerAutoload');
        
        $mail = new PHPMailer();
        $mail->SMTPDebug = 2;
        $mail->Debugoutput = 'html';
        
        $mail->isSMTP();
        $mail->Host = 'ssl://smtp.googlemail.com';
        $mail->Port = '465';
        $mail->SMTPAuth = true;
		$mail->IsHTML(true);
        $mail->Username = 'mpi17sppt@gmail.com';
        $mail->Password = 'support2017';
        $mail->WordWrap = 50;  
        $mail->setFrom('mpi17sppt@gmail.com', 'Wahyu Ganteng');
        $mail->addAddress('wahyu.hdy@gmail.com');
        $mail->Subject = 'Test PHPMailer';
		
		$content = "Verifikasi";
		
		$mail->Body = email_body($content);
        
        $mail->send();
	} */
	
}