<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title"><h4><i class="glyphicon glyphicon-th-list"></i> <?php echo $page_title;?></h4></div>	
					<?php if($this->session->flashdata('message') != ""){ ?> 
						<br /><br />				
						<div class="alert alert-info" role="alert">
						  <i class="glyphicon glyphicon-cog"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?>
						</div>
					<?php } ?>
				</div>
				
				<div class="panel-body">
                <div>
				    <form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'management_user/user_verifikasi'; ?>" enctype="multipart/form-data">
					<table  width="100%">
						<tr>
							<td>
							    <div class="col-md-3">							
									<p>
										<div id="cabang" class="bfh-selectbox" data-name="cabang" data-value="<?php if(!empty($cabang_set_view)){ echo $cabang_set_view; } else { echo"0"; } ?>" data-filter="true" style="width: 180px;">
										  <div data-value="0">Cabang</div>
										  <?php foreach($cabang as $row): ?>
											 <div data-value="<?php echo $row->branchId; ?>"><?php echo $row->branchName; ?></div>
										  <?php endforeach; ?>
										</div>
									</p>
								</div>
								
								<div class="col-md-3">
									<p>
									    <div id="principal_set" class="bfh-selectbox" data-name="principal_set" data-value="<?php if(!empty($principal_set_view)){ echo $principal_set_view; } else { echo"0"; } ?>" data-filter="true" style="width: 180px;">
										  <div data-value="0">Principal</div>
										  <?php foreach($user_set_principal as $row): ?>
											 <div data-value="<?php echo $row->suplierId; ?>"><?php echo $row->supplierPrincipal; ?></div>
										  <?php endforeach; ?>
										</div>
									</p>
								</div>								
							</td>
							<td style="text-align:left;">
								<p>
									<button class="btn btn-primary" onclick="email_view()">Load</button>
								</p>
							</td>
						</tr>
					</table> 
                    </form>					
				</div><hr />
				<form id="form-add" method="POST" action="<?php echo base_url().'management_user/email_verifikasi_multi_user'; ?>" enctype="multipart/form-data">				
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				  <thead>
					<tr>
						<th>No</th>
						<th width="8%" align="center"><input type="checkbox" id="checkAll"></th>
						<th>Email</th>
						<th>Cabang</th>
						<th>Principal</th>
						<th>Status</th>
						<th width="10%" align="center">Action</th>
						 </tr>
				  </thead>
				  <tbody>
				    <?php 
					     $no=0; 
						 $username = $this->session->userdata('uname');
				         if(!empty($user_list)){ 
				            foreach($user_list as $row){
								
							if(!empty($principal_set_view)){
								$principal_set_id = $principal_set_view;
							} else {
								$principal_set_id = $row->suplierId;
							}
							
							if(!empty($cabang_set_view)){
								$cabang_set_id = $cabang_set_view;
							} else {
								$cabang_set_id = $row->branchId;
							}
						 
							if($row->coreUserName != $username){  
					?>
						<tr>
							<td><?php echo ++$no;?></td>
							<td width="4%">
								<input type="hidden" name="principal_set_id_check[]" value="<?php if(!empty($principal_set_view)){ echo $principal_set_view; } else { echo $principal_set_id; } ?>" />
								<input type="hidden" name="cabang_set_id_check[]" value="<?php if(!empty($cabang_set_view)){ echo $cabang_set_view; } else { echo $cabang_set_id; } ?>" />
								<?php cek_set_checkbox($row->coreUserId, $principal_set_id, $cabang_set_id); ?>
							</td>							
							<td><?php echo $row->coreUserName;?></td>
							<td><?php if(!empty($row->branchName)){ echo $row->branchName; } ?></td>
							<td><?php if(!empty($row->supplierPrincipal)){ echo $row->supplierPrincipal; }?></td>
							<td><?php cek_status_user($row->coreUserId, $principal_set_id, $cabang_set_id); ?></td>
							<td><?php cek_email_verifikasi($row->coreUserId, $principal_set_id, $cabang_set_id); ?></td>					  
						</tr>
				    <?php 
						      }
						   }
						}
					?>					
				  </tbody>
				</table>
				<div class="form-group" style="padding-right: 980px;">
					<div class="col-sm-offset-2 col-sm-10">
					  <button type="submit" class="btn btn-primary">Activate All</button>
					</div>
				</div>
				</form>
				</div>
  			</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>
<script type="text/javascript">
	$("#checkAll").click(function () {
		 $('input:checkbox').not(this).prop('checked', this.checked);
	});
</script>