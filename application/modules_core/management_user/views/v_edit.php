<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
							     <?php 
									$group = $this->session->userdata('dep_id');
									if($group == 3 || $group == 2){
								?>
							    <a href="<?php echo base_url(); ?>management_user"><button class="btn btn-warning"><i class="glyphicon glyphicon-arrow-left"></i> Back</button></a><hr />
								<?php } ?>
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'Management_user/update'; ?>" enctype="multipart/form-data">
									  <?php foreach($user as $row): ?>
									  <input name="userId" type="hidden" value="<?php echo $row->coreUserId; ?>">
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Username</label>
										<div class="col-sm-8" id="unameInput">
										  <input type="text" class="form-control" id="username" name="username" value="<?php echo $row->coreUserName; ?>" <?php $group = $this->session->userdata('dep_id'); if($group !=3){ echo"disabled"; }?>>
										  <span id="usernameAvailResult"></span>
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Real Name</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control" name="realName" id="realname" value="<?php echo $row->coreUserUserName; ?>">
										</div>
									  </div>									  
									  <?php 
										$group = $this->session->userdata('dep_id');
										if($group ==3){
									  ?>
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Department</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="group" id="group" required>
											<option value="">--Select--</option>
											<?php foreach($dep_list as $rows){?>
											  <option value="<?php echo $rows->coreDepId;?>" <?php if($row->coreUserDepId == $rows->coreDepId){ echo"selected='selected'"; }?>><?php echo $rows->coreDepName;?></option>
											<?php };?>
										  </select>
										</div>
									  </div>
										<?php } ?>
										
										<?php /*
										<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Principal</label>
										<div class="col-sm-8">
										  <select class="form-control select2" style="width: 100%;" name="principal" id="principal" required>
											<option value="">--Select--</option>
											<?php foreach($principal_list as $rows){?>
											  <option value="<?php echo $rows->suplierId;?>" <?php if($row->corePrincipalId == $rows->suplierId){ echo"selected='selected'"; }?>><?php echo $rows->supplierPrincipal;?></option>
											<?php };?>
										  </select>
										</div>
									  </div> */ ?>
									  
									  <div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
										<div class="col-sm-8">
										  <input type="email" class="form-control" id="email" name="email" value="<?php echo $row->coreUserEmail; ?>" required>
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
										<div class="col-sm-8">
										  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
										  *) Jika password tidak akan diubah silahkan dikosongkan saja.
										  <span id="noticePassword" style="color:#3C8DBC;"></span>
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-2 control-label" id="active-label" style="display:none;">Status</label>
										  <div class="col-sm-8" id="active-div">
										</div>
									  </div>
									 <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									  </div>
									<?php endforeach; ?>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>