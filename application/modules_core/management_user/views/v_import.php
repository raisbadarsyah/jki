<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">

  				<div class="col-md-10">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title"><?php echo $page_title;?></div>
								<?php if($this->session->flashdata('message') != ""){ ?>  
								  <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?>
								
					            <div class="panel-options">
					              <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
					              <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
					            </div>
					        </div>
			  				<div class="panel-body">
			  					<form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'Management_user/import_excel'; ?>" enctype="multipart/form-data">
									<div class="form-group">
									  <a href="<?php echo $file; ?>">Silahkan Download Format Excel Untuk Upload Data Disini</a>
									  <input type="file" class="file-styled" name="file_excel">
									</div>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										  <button type="submit" class="btn btn-primary">Save</button>
										</div>
									</div>
								</form>
								  
			  				</div>
			  			</div>
	  				</div>

		  </div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>