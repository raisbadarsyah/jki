<?php

class Management_user_model extends CI_Model {

    function get_by_id($user_id) {
        $this->db->select('*');
        $this->db->from('core_user');
	//	$this->db->join('core_department','core_user.coreUserdepId=core_department.coredepId','left');
	//	$this->db->join('core_position','core_user.coreUserPositionId=core_position.corePositionId','left');
		//$this->db->join('m_supplier','core_user.corePrincipalId=m_supplier.suplierId','left');
		$this->db->where('coreUserId', $user_id);
		$this->db->order_by('core_user.coreUserName','asc');
        $query = $this->db->get();
       // return $query->result();
        return $query->row();
    }

   
    function get_all() {
        $this->db->select('*');
        $this->db->from('core_super_user');
		//$this->db->join('m_supplier','core_user.supplierId=m_supplier.suplierId','left');
		//$this->db->join('core_position','core_user.coreUserPositionId=core_position.corePositionId','left');
		//$this->db->join('m_supplier','core_user.corePrincipalId=m_supplier.suplierId','left');
		//$this->db->join('m_branch','core_user.coreBranchId=m_branch.branchId','left');
		$this->db->where('core_super_user.coreUserActive !=','2');
		$this->db->or_where('core_super_user.coreUserActive !=','3');
		$this->db->order_by('core_super_user.coreUserName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_new_user($field_output) {
		$query = "SELECT 
				  `core_user`.`coreUserId`,
				  `core_user`.`coreUserName`,
				  `core_user`.`coreUserActive`,
				  `m_branch`.`branchName`,
                  `m_supplier`.`supplierPrincipal`,
				  `m_branch`.`branchId`,
                  `m_supplier`.`suplierId` 	 				  
				FROM
				  `core_user` 
				  LEFT JOIN `ref_email` 
					ON `core_user`.`coreUserName` = `ref_email`.`refEmail` 
				  LEFT JOIN `m_branch` 
                        ON `ref_email`.`refBranchId` = `m_branch`.`branchId` 
				  LEFT JOIN `m_supplier` 
						ON `ref_email`.`refPrincipalId` = `m_supplier`.`suplierId`		
				WHERE `ref_email`.`refPrincipalId` IN ($field_output)
                AND `core_user`.`coreUserDepId` = '1'
                GROUP BY `m_supplier`.`suplierId`, `core_user`.`coreUserName`, `m_branch`.`branchId`				
				";
		
		$qry = $this->db->query($query);
        //$data = 				
        return $qry->result();
    }
	
	function get_all_new_user_by_set($cabang_set, $principal_set) {
		if($cabang_set != 00){
			$query = "SELECT 
					  `core_user`.`coreUserId`,
					  `core_user`.`coreUserName`,
					  `core_user`.`coreUserActive`,
					  `m_branch`.`branchName`,
					  `m_branch`.`branchId`,
					  `m_supplier`.`supplierPrincipal`  
					FROM
					  `core_user` 
					  LEFT JOIN `ref_email` 
						ON `core_user`.`coreUserName` = `ref_email`.`refEmail` 
					 LEFT JOIN `m_branch` 
                        ON `ref_email`.`refBranchId` = `m_branch`.`branchId` 
					 LEFT JOIN `m_supplier` 
						ON `ref_email`.`refPrincipalId` = `m_supplier`.`suplierId`
					WHERE `ref_email`.`refBranchId` = '$cabang_set'
					AND `ref_email`.`refPrincipalId` = '$principal_set'
					AND `core_user`.`coreUserDepId` = '1'
					GROUP BY `m_supplier`.`suplierId`, `core_user`.`coreUserName`, `m_branch`.`branchId`";
		} 
		else { 
			$query = "SELECT 
				  `core_user`.`coreUserId`,
				  `core_user`.`coreUserName`,
				  `core_user`.`coreUserActive`,
				  `m_branch`.`branchName`,
				  `m_branch`.`branchId`,
                  `m_supplier`.`supplierPrincipal`  
				FROM
				  `core_user` 
				  LEFT JOIN `ref_email` 
					ON `core_user`.`coreUserName` = `ref_email`.`refEmail` 
				  LEFT JOIN `m_branch` 
                    ON `ref_email`.`refBranchId` = `m_branch`.`branchId` 
				  LEFT JOIN `m_supplier` 
                    ON `ref_email`.`refPrincipalId` = `m_supplier`.`suplierId`
				WHERE `ref_email`.`refPrincipalId` = '$principal_set'
				AND `core_user`.`coreUserDepId` = '1'
				GROUP BY `m_supplier`.`suplierId`, `core_user`.`coreUserName`, `m_branch`.`branchId`";
		}
		
		$qry = $this->db->query($query);
        //$data = 				
        return $qry->result();
    }

    function get_all_dep() {
        $this->db->select('*');
        $this->db->from('core_department');
		$this->db->order_by('coredepName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
    function get_all_pos() {
        $this->db->select('*');
        $this->db->from('core_position');
		$this->db->order_by('corePositionName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_all_principal() {
        $this->db->select('*');
        $this->db->from('m_supplier');
		$this->db->order_by('supplierPrincipal','asc');
        $query = $this->db->get();
        return $query->result();
    }

    function get_all_position() {
        $this->db->select('*');
        $this->db->from('core_position');
		$this->db->order_by('corePositionName','asc');
        $query = $this->db->get();
        return $query->result();
    }

     function get_su_position() {
        $this->db->select('*');
        $this->db->from('core_position');
        $this->db->where('corePositionId =','9');
        $this->db->or_where('corePositionId =','14');
        $this->db->or_where('corePositionId =','15');
        $this->db->order_by('corePositionName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_branch($params) {
        $this->db->select('*');
        $this->db->from('m_branch');

        if (isset($params['level']) && $params['level'] == 4)
        {
           
            $this->db->join('core_super_user','core_super_user.branchId=m_branch.branchId');
            $this->db->where('core_super_user.coreUserId', $params['user_id']);
        }

		$this->db->order_by('m_branch.branchId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_user_by_id($id) {
        $this->db->select('*');
        $this->db->from('core_user');
		$this->db->where('coreUserId', $id);
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_super_user_by_id($id) {
        $this->db->select('*');
        $this->db->from('core_user');
		$this->db->where('coreUserId', $id);
        $query = $this->db->get();
        return $query->result();
    }

	function insert_user($data){
		$this->db->trans_begin();
        $this->db->insert('core_super_user', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update_user($data, $userId){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $userId);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function update_super_user($data, $userId){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $userId);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function active_user($id, $data){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $id);
		$this->db->update('core_super_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function active_user_by_code($kode, $data){
        $this->db->trans_begin();
		$this->db->where('coreUserCodeActivate', $kode);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function delete_user($id) {
        $query = $this->db->delete('core_super_user', array('coreUserId' => $id));

		 if ($query) { 
			return 1;
	    } 
	    else { 
	    	return 0; 
	    } 
	}
	
	function check_pass($current_pass, $user_id) {
        $this->db->select('*');
        $this->db->from('core_user');
        $this->db->where('core_user.coreUserId', $user_id);
        $this->db->where('core_user.coreUserPassword', $current_pass);
        $query = $this->db->get();
       // return $query->result();
        return $query->row();
    }

	function check_username($username){
		$this->db->select('coreUsername');
        $this->db->from('core_user');
		$this->db->where('coreUsername', $username);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            echo 1;
        }else {
            echo 0;
        }	
	}

	function get_file_name($userId){
		$this->db->select('coreUserPic');        
		$this->db->where('coreUserId', $userId);
        $query = $this->db->get('core_user');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
            	return $row->coreUserPic;
            }
        }else {
            echo 0;
        }
	}
	
	function get_email_verifikasi($email_user) {
        $this->db->select('refEmail');
        $this->db->from('ref_email');
        $this->db->where('refEmail', $email_user);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $refEmail = $row->refEmail;
            }

            return $refEmail;
        } else {
            return FALSE;
        }
    }
	
	function get_email_user($user_id) {
        $this->db->select('coreUserName');
        $this->db->from('core_user');
        $this->db->where('coreUserId', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserName = $row->coreUserName;
            }

            return $coreUserName;
        } else {
            return FALSE;
        }
    }
	
	function get_token_user($user_id) {
        $this->db->select('coreUserCodeActivate');
        $this->db->from('core_user');
        $this->db->where('coreUserId', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserCodeActivate = $row->coreUserCodeActivate;
            }

            return $coreUserCodeActivate;
        } else {
            return FALSE;
        }
    }
	
	function cek_token_user($kode) {
        $this->db->select('coreUserCodeActivate');
        $this->db->from('core_user');
        $this->db->where('coreUserCodeActivate', $kode);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserCodeActivate = $row->coreUserCodeActivate;
            }

            return $coreUserCodeActivate;
        } else {
            return FALSE;
        }
    }
	
	function get_status_user($user_id) {
        $this->db->select('coreUserActive');
        $this->db->from('core_user');
        $this->db->where('coreUserId', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserActive = $row->coreUserActive;
            }

            return $coreUserActive;
        } else {
            return FALSE;
        }
    }
	
	function get_principal_user($email) {
        $this->db->select('*');
        $this->db->from('ref_email');
		$this->db->where('refEmail', $email);
		$this->db->where('refEmailStatus', '1');
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert_user_principal($data){
		$this->db->trans_begin();
        $this->db->insert('core_user_principal', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function insert_user_cabang($data_cabang){
		$this->db->trans_begin();
        $this->db->insert('core_user_cabang', $data_cabang);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function count_user_principal_on_set($userId, $principal_id) {
        $this->db->select('coreUserPrincipalId');
        $this->db->from('core_user_principal');
        $this->db->where('coreUserUserId', $userId);
		$this->db->where('coreUserPrincipalId', $principal_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserPrincipalId = $row->coreUserPrincipalId;
            }

            return $coreUserPrincipalId;
        } else {
            return FALSE;
        }
    }
	
	function count_user_cabang_on_set($userId, $id_cabang) {
        $this->db->select('coreUserBranchId');
        $this->db->from('core_user_cabang');
        $this->db->where('coreUserUserId', $userId);
		$this->db->where('coreUserBranchId', $id_cabang);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserBranchId = $row->coreUserBranchId;
            }

            return $coreUserBranchId;
        } else {
            return FALSE;
        }
    }

}

