
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<!-- /main navbar -->

	<!-- Memanggil file .js untuk proses autocomplete -->
  
    <script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.autocomplete.js'></script>

    <!-- Memanggil file .css untuk style saat data dicari dalam filed -->
    <link href='<?php echo base_url();?>assets/js/jquery.autocomplete.css' rel='stylesheet' />

    <!-- Memanggil file .css autocomplete_ci/assets/css/default.css -->
    <link href='<?php echo base_url();?>assets/css/default.css' rel='stylesheet' />

    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><i class="icon-home2 position-left"></i>Dashboard</li>
							<li>Staff</li>
						
							<li class="active">ADD</li>
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<!-- 2 columns form -->
					<form action="<?php echo base_url().'staff/add'; ?>" enctype="multipart/form-data" method="post">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Buat Staff</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>
							<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<fieldset class="text-semibold">
											<legend><i class="icon-reading position-left"></i>Buat Staff</legend>

											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label>Username: <span id="user-result"></span></label>
														 <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
													</div>
												</div>

											
												<div class="col-md-3">
													<div class="form-group">
														<label>Password:</label>
														<input type="password" class="form-control" name="password" required="">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label>Nama Lengkap:</label>
														 <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" required>
													</div>
												</div>


												<div class="col-md-6">
													<div class="form-group">
														<label>Nip:</label>
														 <input type="text" class="form-control" name="nip" id="nip" placeholder="nip" required>
													</div>
												</div>
												
												
												<div class="col-md-3">
													<div class="form-group">
														<label>Email:</label>
														<input type="text" class="form-control" name="email" id="email" placeholder="email" required>
													</div>
												</div>

												<div class="col-md-3">
													<div class="form-group">
														<label>Jabatan:</label>
														<input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="jabatan" required>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label>Wewenang:</label>
														 <div class="multi-select-full">
															<select class="select" name="wewenang" id="wewenang" onchange="showDiv()">
																<option value="">-- Pilih Wewenang --</option>
																<?php foreach ($wewenang as $key => $value) {?>

																<option value="<?php echo $value->corePositionId;?>"><?php  echo $value->corePositionName;?></option>
																<?php } ?>
													
															</select>
														</div>

													</div>
												</div>
												
												<div class="col-md-12" id="div_departemen">
													<div class="form-group">
														<label>Departemen:</label>
														<select class="select" name="departemen" id="departemen" onchange="showDivDepartemen()">
																<?php foreach ($departemen as $key => $value) {?>
																<option value="<?php echo $value->id_departemen;?>"><?php  echo $value->nm_departemen;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_atasan1">
													<div class="form-group">
														<label>Atasan 1:</label>
														<select class="select" name="atasan1" id="atasan1">
																<option> -- Pilih Atasan Pertama --</option>
																<?php foreach ($atasan as $key => $value) {?>
																<option value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
													</div>
												</div>

												<div class="col-md-12" id="div_atasan2">
													<div class="form-group">
														<label>Atasan 2:</label>
														 <div class="multi-select-full">
															<select class="select" name="atasan2" id="atasan2">
																<option> -- Pilih Atasan Kedua --</option>
																<?php foreach ($atasan as $key => $value) {?>
																<option value="<?php echo $value->coreUserId;?>"><?php  echo $value->Name;?></option>
																<?php } ?>
													
															</select>
														</div>

													</div>
												</div>

												<div class="col-md-4" id="div_kanwil">
													<div class="form-group">
														<label>Kantor Wilayah:</label>
														 <select name="kanwil" id="kanwil" onChange="javascript:get_area()" class="select" required="required">
														<option value=""> -- Pilih Kantor Wilayah -- </option>

														<?php foreach ($kanwil as $value) {?>
																<option value="<?php  echo $value->kode_kanwil;?>"><?php  echo $value->nm_kanwil;?> - <?php  echo $value->alias;?></option>
																
														<?php } ?>
															
														</select>
													</div>
												</div>

											<div id="div_area">
												<div class="col-md-4">
													<div class="form-group">
														<label>Area:</label>
														<select name="area" id="area" onChange="javascript:get_sub_area()" class="select area" required="required">
															
														<option value=""> Pilih Kantor Wilayah Terlebih Dulu</option>
														
														</select>
													</div>
												</div>
											</div>

											
												<div class="col-md-4" id="div_sub_area">
													<div class="form-group">
														<label>Sub Area:</label>
														<select class="select sub_area" name="sub_area" id="sub_area">
															
														<option value=""> Pilih Kantor Wilayah Terlebih Dulu</option>
														

														</select>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Telepon:</label>
														 <input type="text" class="form-control" name="telp" id="telp" placeholder="Telepon" required>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label>Notifikasi:</label>
														<textarea name="notifikasi" class="form-control"></textarea>
													</div>
												</div>


												<div class="col-md-6">
													<div class="form-group">
														<label>Status:</label>
														 <div class="multi-select-full">
															<select class="select" name="status" id="status">
																
																<option value="1">Aktif</option>
																<option value="2">Tidak Aktif</option>
													
															</select>
														</div>

													</div>
												</div>




											</div>

									
									

										</fieldset>
									</div>
									<br>
									

								<div class="text-right">
									<button type="submit" class="btn btn-primary" id="submit">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					<!-- /2 columns form -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

  	<script type="text/javascript">
		function showDiv(){

		var wewenang = $('#wewenang :selected').val();

		   if(wewenang==1){
		    document.getElementById('div_departemen').style.display = "none";
		    document.getElementById('div_atasan1').style.display = "none";
		    document.getElementById('div_atasan2').style.display = "none";
		    document.getElementById('div_kanwil').style.display = "none";
		    document.getElementById('div_area').style.display = "none";
		    document.getElementById('div_sub_area').style.display = "none";

			$("#departemen").prop('disabled',true);
			$("#atasan1").prop('disabled',true);
			$("#atasan2").prop('disabled',true);
			$("#kanwil").prop('disabled',true);
			$("#area").prop('disabled',true);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();
		   }
		    if(wewenang==2){
		    document.getElementById('div_departemen').style.display = "block";
		    document.getElementById('div_atasan1').style.display = "block";
		    document.getElementById('div_atasan2').style.display = "block";
		    document.getElementById('div_kanwil').style.display = "block";
		    document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#departemen").prop('disabled',false);
			$("#atasan1").prop('disabled',false);
			$("#atasan2").prop('disabled',false);
			$("#kanwil").prop('disabled',false);
			$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',false);

			event.preventDefault();

		   }

		   if(wewenang==3){
		    document.getElementById('div_departemen').style.display = "block";
		    document.getElementById('div_atasan1').style.display = "block";
		    document.getElementById('div_atasan2').style.display = "block";
		    document.getElementById('div_kanwil').style.display = "block";
		    document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#departemen").prop('disabled',false);
			$("#atasan1").prop('disabled',false);
			$("#atasan2").prop('disabled',false);
			$("#kanwil").prop('disabled',false);
			$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',false);

			event.preventDefault();

		   }
		   if(wewenang==4){
		    document.getElementById('div_departemen').style.display = "none";
		    document.getElementById('div_atasan1').style.display = "block";
		    document.getElementById('div_atasan2').style.display = "block";
		    document.getElementById('div_kanwil').style.display = "block";
		    document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#departemen").prop('disabled',true);
			$("#atasan1").prop('disabled',false);
			$("#atasan2").prop('disabled',false);
			$("#kanwil").prop('disabled',false);
			$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',false);

		    event.preventDefault();

		   }
		   if(wewenang==5){
		    document.getElementById('div_departemen').style.display = "none";
		    document.getElementById('div_atasan1').style.display = "none";
		    document.getElementById('div_atasan2').style.display = "none";
		    document.getElementById('div_kanwil').style.display = "block";
		    document.getElementById('div_area').style.display = "none";
		    document.getElementById('div_sub_area').style.display = "none";

		    $("#departemen").prop('disabled',true);
			$("#atasan1").prop('disabled',true);
			$("#atasan2").prop('disabled',true);
			$("#kanwil").prop('disabled',false);
			$("#area").prop('disabled',true);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();

		   }
		   if(wewenang==6){
		    document.getElementById('div_departemen').style.display = "none";
		    document.getElementById('div_atasan1').style.display = "none";
		    document.getElementById('div_atasan2').style.display = "none";
		    document.getElementById('div_kanwil').style.display = "none";
		    document.getElementById('div_area').style.display = "none";
		    document.getElementById('div_sub_area').style.display = "none";

		    $("#departemen").prop('disabled',true);
			$("#atasan1").prop('disabled',true);
			$("#atasan2").prop('disabled',true);
			$("#area").prop('disabled',true);
			$("#sub_area").prop('disabled',true);
			$("#kanwil").prop('disabled',true);

	    	event.preventDefault();


		   } 
		   if(wewenang==7){
		    document.getElementById('div_departemen').style.display = "none";
		    document.getElementById('div_atasan1').style.display = "none";
		    document.getElementById('div_atasan2').style.display = "none";
		    document.getElementById('div_kanwil').style.display = "none";
		    document.getElementById('div_area').style.display = "none";
		    document.getElementById('div_sub_area').style.display = "none";

		    $("#departemen").prop('disabled',true);
			$("#atasan1").prop('disabled',true);
			$("#atasan2").prop('disabled',true);
			$("#kanwil").prop('disabled',true);
			$("#area").prop('disabled',true);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();


		   }else{
		    //document.getElementById('departemen').style.display = "none";
		   }
		} 

		function showDivDepartemen(){

		var departemen = $('#departemen :selected').val();

			if(departemen==12){
		    document.getElementById('div_area').style.display = "none";
		    document.getElementById('div_sub_area').style.display = "none";

			$("#area").prop('disabled',true);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();


		   }
		   else if(departemen==11){
		   	document.getElementById('div_kanwil').style.display = "block";
		   	document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "none";
		  	
		  	$("#kanwil").prop('disabled',false);
		  	$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();


		   }
		   else if(departemen==7){
		   	
		    document.getElementById('div_sub_area').style.display = "none";
		  	
		  	$("#kanwil").prop('disabled',false);
		  	$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();


		   }

		   else if(departemen==3){
		   	
		    document.getElementById('div_sub_area').style.display = "none";
		  	
		  	$("#kanwil").prop('disabled',false);
		  	$("#area").prop('disabled',false);
			$("#sub_area").prop('disabled',true);

	    	event.preventDefault();


		   }

		   else if(departemen==9){
		   	
		   	document.getElementById('div_kanwil').style.display = "block";
		   	document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#kanwil").prop('disabled',false);
			$("#sub_area").prop('disabled',false);
			$("#area").prop('disabled',false);

	    	event.preventDefault();

	    	}
	    	else if(departemen==8){
		   	
		   	document.getElementById('div_kanwil').style.display = "block";
		   	document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#kanwil").prop('disabled',false);
			$("#sub_area").prop('disabled',false);
			$("#area").prop('disabled',false);

	    	event.preventDefault();


		   }else{
		    //document.getElementById('departemen').style.display = "none";
		    document.getElementById('div_kanwil').style.display = "block";
		   	document.getElementById('div_area').style.display = "block";
		    document.getElementById('div_sub_area').style.display = "block";

		    $("#kanwil").prop('disabled',false);
			$("#sub_area").prop('disabled',false);
			$("#area").prop('disabled',false);

		    event.preventDefault();
		   }


		}
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
		    var x_timer;    
		    $("#username").keyup(function (e){

		        clearTimeout(x_timer);
		        var user_name = $(this).val();
		        x_timer = setTimeout(function(){
		            check_username_ajax(user_name);
		        }, 1000);
		    }); 

		function check_username_ajax(username){
		    $("#user-result").html('<img src="<?php echo base_url();?>template/assets/images/ajax-loader.gif" />');
		    $.post('<?php echo base_url('staff/checkusername')?>', {'username':username}, function(data) {

		    	//alert(data);
		    	if(data == 1)
		    	{
		    		$("#user-result").html('<img src="<?php echo base_url();?>template/assets/images/not-available.png" />');
		    		document.getElementById("submit").disabled = true;
		    	}
		    	else
		    	{
		    		$("#user-result").html('<img src="<?php echo base_url();?>template/assets/images/available.png" />');
		    		document.getElementById("submit").disabled = false;
		    	}
		      //$("#user-result").html(data);
		    });
		}
		});
	</script>

</body>
</html>
