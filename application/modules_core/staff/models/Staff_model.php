<?php

class Staff_model extends CI_Model {

   function get_all($params){
		$this->db->select('*');
        $this->db->from('core_user');
        $this->db->join('core_position','core_user.coreUserPositionId=core_position.corePositionId');
        $this->db->join('departemen','departemen.id_departemen=core_user.id_departemen','left');
        $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','core_user.kode_sub_area=kantor_sub_area.kode_sub_area','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_detail_staff($coreUserId,$params){
        $this->db->select('*');
        $this->db->from('core_user');
        $this->db->join('core_position','core_user.coreUserPositionId=core_position.corePositionId');
        $this->db->join('departemen','departemen.id_departemen=core_user.id_departemen','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
            $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 6)
        {
            //$this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
            $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area');
            $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == 7)
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
            $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            $this->db->join('kantor_wilayah','core_user.kode_kanwil=kantor_wilayah.kode_kanwil','left');
            $this->db->join('kantor_area','core_user.kode_area=kantor_area.kode_area','left');
            $this->db->join('kantor_sub_area','core_user.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        
        }

        
        $this->db->where('coreUserId',$coreUserId);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_atasan(){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserPositionId',6);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function get_username($username){
        $this->db->select('*');
        $this->db->from('core_user');
         $this->db->where('coreUserName',$username);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

   
	function insert($data_staff) {
       $query = $this->db->insert('core_user', $data_staff);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

  
    function update($data, $coreUserId){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $coreUserId);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

    function delete($coreUserId) {
        $query = $this->db->delete('core_user', array('coreUserId' => $coreUserId));

         if ($query) { 
            return 1; 
        } 
        else { 
            return 0; 
        } 
    }

}

