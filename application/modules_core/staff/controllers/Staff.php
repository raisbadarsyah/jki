<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_model','Kanwil/Kanwil_model','Provinsi/Provinsi_model','kantor_sub_area/Kantor_sub_area_model','kantor_area/Kantor_area_model','Staff_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}

	function checkusername()
	{
		$username = $this->input->post('username');
		$user = $this->Staff_model->get_username($username);

		if(isset($user)){
	        //die('<img src="'.base_url().'template/assets/images/not-available.png" />');
	        die('1');
	    }else{
	       // die('<img src="'.base_url().'template/assets/images/available.png" />');
	    	die('2');
	    }

	}

	function index()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		if(isset($get_detail_staff->kode_kanwil))
		{
			$kode_kanwil = $get_detail_staff->kode_kanwil;
		}
		else
		{
			$kode_kanwil = "";
		}

		if(isset($get_detail_staff->kode_area))
		{
			$kode_area = $get_detail_staff->kode_area;
		}
		else
		{
			$kode_area = "";
		}

		if(isset($get_detail_staff->kode_sub_area))
		{
			$kode_sub_area = $get_detail_staff->kode_sub_area;
		}
		else
		{
			$kode_sub_area = "";
		}

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $kode_kanwil,
            'kode_area' => $kode_area,
            'kode_sub_area' => $kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$data['position_id'] = $position_id;
		$data['departemen_id'] = $id_departemen;
		$data['staff'] = $this->Staff_model->get_all($params);


		$this->load->view('index',$data);
	}

	function add()
	{	
		$data['wewenang'] = $this->management_position_model->get_all();
		$data['departemen'] = $this->Departemen_model->get_all();
		$data['kanwil'] = $this->Kanwil_model->get_all();
		$data['atasan'] = $this->Staff_model->get_atasan();

		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$nama = $this->input->post('nama');
			$nip = $this->input->post('nip');
			$email = $this->input->post('email');
			$jabatan = $this->input->post('jabatan');
			$wewenang = $this->input->post('wewenang');
			$departemen = $this->input->post('departemen');
			$atasan1 = $this->input->post('atasan1');
			$atasan2 = $this->input->post('atasan2');
			$kanwil = $this->input->post('kanwil');
			$area = $this->input->post('area');
			$sub_area = $this->input->post('sub_area');
			$telp = $this->input->post('telp');
			$notifikasi = $this->input->post('notifikasi');
			$status = $this->input->post('status');


			try {
				
				$data_staff = array(
					"nip"=> $nip,
					"Name"=> $nama,
					"coreUserName"=> $username,
					"coreUserPassword"=> md5($password),
					"coreUserPasswordTxt"=> $password,
					"coreUserActive"=> $status,
					"coreUserEmail"=> $email,
					"coreUserPositionId"=> $wewenang,
					"jabatan"=> $jabatan,
					"id_departemen"=> $departemen,
					"atasan1"=> $atasan1,
					"atasan2"=> $atasan2,
					"kode_kanwil"=> $kanwil,
					"kode_area"=> $area,
					"kode_sub_area"=> $sub_area,
					"telp"=> $telp,
					"notifikasi"=> $notifikasi
				);
				
				$insert_staff = $this->Staff_model->insert($data_staff);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_staff == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('staff','refresh');
				}


		}
	
	}

	function edit()
	{	
		$data['wewenang'] = $this->management_position_model->get_all();
		$data['departemen'] = $this->Departemen_model->get_all();
		$data['kanwil'] = $this->Kanwil_model->get_all();
		$data['atasan'] = $this->Staff_model->get_atasan();

		$user_id = $this->uri->segment(3);
		$id_departemen = $this->session->userdata('id_departemen');
		$position_id = $this->session->userdata('position_id');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));

		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$data['detail_staff'] = $get_detail_staff;

		if(isset($data['detail_staff']->kode_kanwil))
		{
			$data['area'] = $this->Kantor_area_model->area_by_kanwil($data['detail_staff']->kode_kanwil);
		}
		if(isset($data['detail_staff']->kode_area))
		{
			$data['sub_area'] = $this->Kantor_area_model->sub_area_by_area($data['detail_staff']->kode_area);
		}
		/*
		echo "<pre>";
		print_r($get_detail_staff);
		echo "</pre>";
		*/
		
		$this->load->view('edit',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$coreUserId = $this->input->post('coreUserId');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$nama = $this->input->post('nama');
			$nip = $this->input->post('nip');
			$email = $this->input->post('email');
			$jabatan = $this->input->post('jabatan');
			$wewenang = $this->input->post('wewenang');
			$departemen = $this->input->post('departemen');
			$atasan1 = $this->input->post('atasan1');
			$atasan2 = $this->input->post('atasan2');
			$kanwil = $this->input->post('kanwil');
			$area = $this->input->post('area');
			$sub_area = $this->input->post('sub_area');
			$telp = $this->input->post('telp');
			$notifikasi = $this->input->post('notifikasi');
			$status = $this->input->post('status');


			try {
				
				$data_staff = array(
					"nip"=> $nip,
					"Name"=> $nama,
					"coreUserName"=> $username,
					"coreUserPassword"=> md5($password),
					"coreUserPasswordTxt"=> $password,
					"coreUserActive"=> $status,
					"coreUserEmail"=> $email,
					"coreUserPositionId"=> $wewenang,
					"jabatan"=> $jabatan,
					"id_departemen"=> $departemen,
					"atasan1"=> $atasan1,
					"atasan2"=> $atasan2,
					"kode_kanwil"=> $kanwil,
					"kode_area"=> $area,
					"kode_sub_area"=> $sub_area,
					"telp"=> $telp,
					"notifikasi"=> $notifikasi
				);
				
				$update_staff = $this->Staff_model->update($data_staff,$coreUserId);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal edit staff');

			}


				if($update_staff == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('staff','refresh');
				}


		}
	
	}

	function delete()
	{
		$user_id = $this->uri->segment(3);

		try {
			
			$delete_staff = $this->Staff_model->delete($user_id);

			if($delete_staff == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('staff','refresh');
			}else
			{
				$this->session->set_flashdata('error', 'Gagal edit staff');
				redirect('staff','refresh');
			}

		} catch (Exception $e) {
			
			$this->session->set_flashdata('error', 'Gagal edit staff');
			redirect('staff','refresh');

		}

		

	}

}
