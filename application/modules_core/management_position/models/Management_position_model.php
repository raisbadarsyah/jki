<?php

class Management_position_model extends CI_Model {

    function get_all() {
        $this->db->select('*');
        $this->db->from('core_position');
		$this->db->order_by('corePositionName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_akses_position($id){
		$this->db->select('*');
        $this->db->from('core_role');
		$this->db->where('core_role.coreRolePosition', $id);
        $query = $this->db->get();
        return $query->result();
	}
	
	function cek_akses_position($id){
		$this->db->select('*');
        $this->db->from('core_role');
		$this->db->where('core_role.coreRolePosition', $id);
		$query = $this->db->get();
        return $query->num_rows();
	}

	function get_item_by_id($id) {
        $this->db->select('*');
        $this->db->from('core_position');
		$this->db->where('corePositionId', $id);
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert_item($data){
		$this->db->trans_begin();
        $this->db->insert('core_position', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function update_item($data, $itemId){
        $this->db->trans_begin();
		$this->db->where('corePositionId', $itemId);
		$this->db->update('core_position', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function delete_item($id) {
        $query = $this->db->delete('core_position', array('corePositionId' => $id));

		 if ($query) { 
			return 1; 
	    } 
	    else { 
	    	return 0; 
	    } 
	}
	
	function check_menuUrl($url){
		$this->db->select('coreMenuUrl');
        $this->db->from('core_menu');
		$this->db->where('coreMenuUrl', $url);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            echo 1;
        }
        else {
            echo 0;
        }	
	}

	/*
	function get_parent_menu() {
        $this->db->select('*');
        $this->db->from('core_menu');
        //$this->db->where('coreParentId',0);
		$this->db->order_by('coreParentId','asc');
		$this->db->order_by('coreMenuName','asc');
        $query = $this->db->get();
        return $query->result();
    }
	*/
    function insert_roles($roles){
		$this->db->trans_begin();
        $this->db->insert('core_role', $roles);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
    
    function update_roles($removed_roles,$id_group,$id_menu){
        $this->db->trans_begin();
		$this->db->where('coreRolePosition', $id_group);
		$this->db->where('coreRoleMenu', $id_menu);
		$this->db->update('core_role', $removed_roles);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
        
    function check_roles_by_group($id_grup,$id_menu){
        $this->db->select('*');
        $this->db->from('core_role');
        $this->db->where('coreRolePosition',$id_grup);
        $this->db->where('coreRoleMenu',$id_menu);
        return $this->db->get();
    }

    function get_parent_menu($parentId){
		$this->db->select('core_menu.*, Deriv1.Count');
        $this->db->from('core_menu');
        $this->db->join('(SELECT coreParentId, COUNT(*) AS COUNT FROM `core_menu` GROUP BY coreParentId) as Deriv1', 'core_menu.coreMenuId = Deriv1.coreParentId','LEFT');
		$this->db->where('core_menu.coreParentId', $parentId);
		$this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        
        return $query->result();
	}
	
    function get_parent_menu_name($id_parent){
		$this->db->select('coreMenuName');
		$this->db->where('coreMenuId',$id_parent);
		$query = $this->db->get('core_menu');

		foreach ($query->result() as $row) {
			return $row->coreMenuName;
		}
	}
}

