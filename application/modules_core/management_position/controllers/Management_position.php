<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); error_reporting(E_ALL);

class Management_position extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
			
	}

	function management_position_role($id){
		$config['title'] 	 = 'HMVCFW | Manage Department';	
		$config['page_title'] = 'Management Department';
		$config['page_subtitle'] = 'Manage Role';
        $config['positionId']   = $id;

		$cek_akses_group     = $this->management_position_model->cek_akses_position($config['positionId']);

		if($cek_akses_group > 0){
			$config['roles'] = $this->management_position_model->get_akses_position($config['positionId']);
			$config['status']='edit';
			$config['link'] ='management_position/update_roles/';
		}else{
			$config['status']='new';
			$config['link'] ='management_position/insert_roles/';
		}
		
		$this->load->view('v_manage', $config);
	}

	function index()
	{
		$config['title'] 	 = 'Koperasi Syariah Duasatudua | Manage Positions';
		$config['page_title'] = 'Management Positions';
		$config['page_subtitle'] = 'Positions List';
	    $config['item_list'] = $this->management_position_model->get_all();
	    
       
		$this->load->view('v_list', $config);
	}
	
	function insert_roles(){
		$id_menu       = $this->input->post('id_menu');
		$id_grup       = $this->input->post('groupId');
		$jumlah_menu   = count($id_menu);
        
		if($jumlah_menu>0){   
			for($i=0;$i<$jumlah_menu;$i++){
				$menu_id = $id_menu[$i];
			    $active = $this->input->post('active_'.$menu_id);
				
				if($active !== '1'){
					 $active = '0';
				}
				
				$roles = array(
					"coreRolePosition" => $id_grup,
					"coreRoleMenu" => $menu_id,
					"coreRoleActive" => $active
				);
				
				$action = $this->management_position_model->insert_roles($roles);
			}  
		}
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Group roles have been updated');
		}
		else {
			$this->session->set_flashdata('error', 'Group roles have been updated');
		}	
		
        redirect(base_url() . 'management_position', 'refresh');	
	}
	
	function update_roles(){
		 $id_menu    = $this->input->post('id_menu');
		 $id_group    = $this->input->post('groupId');
		 $jumlah     = count($id_menu);
		 $id_menu_removed = $this->input->post('remove');
		 $jumlah_hilang = count($id_menu_removed);
		 
		if($jumlah_hilang>0)  {			
			for($i=0;$i<$jumlah_hilang;$i++){         
				$id_removed = $id_menu_removed[$i];
				$removed_roles = array(
					"coreRoleActive" => '0'
				);
				
				$action = $this->management_position_model->update_roles($removed_roles,$id_group,$id_removed);    
			}  
		}
		  
		if($jumlah>0){   
			for($i=0;$i<$jumlah;$i++){         
				$menu_id = $id_menu[$i];
				$active = $this->input->post('active_'.$menu_id);
				 
				if($active !== '1'){
					$active = '0';
				}
				 
				$menu = $this->management_position_model->check_roles_by_group($id_group,$menu_id);
				if($menu->num_rows()>0){
					$roles = array(
						"coreRoleActive" => $active
					 );
					 
					$action = $this->management_position_model->update_roles($roles,$id_group,$menu_id); 
				}
				else{
					
					$roles = array(
						"coreRolePosition" => $id_group,
						"coreRoleMenu" => $menu_id,
						"coreRoleActive" => $active
					);
						
					$action = $this->management_position_model->insert_roles($roles);
				}     
			}  
		}
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Group roles have been updated');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update group roles');
		}
		
		redirect(base_url() . 'management_position', 'refresh');	
	}

	function insert_item(){
		$itemName  = $this->input->post('itemName');
		
		if(isset($itemName) && $itemName == "")
		{
			$this->session->set_flashdata('message', 'Position Name harus diisi');
			redirect(base_url() . 'management_position', 'refresh');
		}

		$data = array(
			"corePositionName" => $itemName
			
		);	
		$action = $this->management_position_model->insert_item($data);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'Positions has been added successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to add Positions');
		}	
		redirect(base_url() . 'management_position', 'refresh');	
	}

	function edit_item($id){
			$item = $this->management_position_model->get_item_by_id($id);			
			echo json_encode($item);
	}

	function update_item(){	
		$itemName  = $this->input->post('itemName');
		$itemId  = $this->input->post('itemId');
		
		
		
		$data = array(
			"refItemName" => $itemName
			
		);

		$action = $this->management_position_model->update_item($data, $itemId);
		
		if($action == 1) {
			$this->session->set_flashdata('message', 'item has been updated successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to update item');
		}	
		redirect(base_url() . 'management_item', 'refresh');
	}
	
	function delete($id){
		$action = $this->management_position_model->delete_item($id);

		if($action == 1) {
			$this->session->set_flashdata('message', 'item has been deleted successfully');
		}
		else {
			$this->session->set_flashdata('error', 'Failed to deleted item');
		}

		redirect(base_url() . 'management_position', 'refresh');
	}

	function checkURL($url){
		$this->management_position_model->check_menuUrl($url);
	}
}