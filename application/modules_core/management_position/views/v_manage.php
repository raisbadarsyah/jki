<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PT. JKI</title>

  
  <!-- Global stylesheets -->

  <link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
  <!-- /theme JS files -->

</head>

<body>

  <!-- Main navbar -->
  <?php
  $this->load->view('template/main_navbar');
  ?>
  <!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
  
  
  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main sidebar -->
       <?php $this->load->view('template/sidebar'); ?>
      <!-- /main sidebar -->


      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
          
          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i> Admin</a></li>
              <li><a>Management Position</a></li>
              <li>Position Access Menu</li>
            </ul>

            <ul class="breadcrumb-elements">
              <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear position-left"></i>
                  Settings
                  <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                  <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                  <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                  <li class="divider"></li>
                  <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

          <!-- Basic datatable -->
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Position Access Menu</h5>
              <div class="heading-elements">
                <ul class="icons-list">
                          <li><a data-action="collapse"></a></li>
                          <li><a data-action="reload"></a></li>
                          <li><a data-action="close"></a></li>
                        </ul>
                      </div>
            </div>

            <div class="panel-body">
             <?php if($this->session->flashdata('message') != ""){ ?>  
            <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
            <?php } ?>
            </div>
           
         
           <form id="fm_hak_akses" action="<?php echo base_url().$link;?>" method="POST">
          <input type="hidden" name="groupId" value="<?php echo $positionId; ?>" />
          <table class="table table-bordered table-hover">
            <!-- <tr>
            <th style="width: 10px">#</th>
            <th>Task</th>
            <th>Progress</th>
            <th style="width: 40px">Label</th>
            </tr> -->
            <?php
            if (isset($roles)){
            get_list_module($status,$roles,0);
            }else{
            get_list_module($status,null,0);
            }
            ?>
          </table>
          <button type="submit" class="btn btn-primary">Save</button> 
          </form>  
          
          </div>
          <!-- /basic datatable -->


          <!-- Pagination types -->
          
          <!-- /pagination types -->


          <!-- State saving -->
          
          <!-- /state saving -->


          <!-- Scrollable datatable -->
          
          <!-- /scrollable datatable -->
 
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
  

    <script src="<?php echo base_url(); ?>assets/vendors/datatables/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/vendors/datatables/dataTables.bootstrap.js"></script>


   
   
  <script src="<?php echo base_url(); ?>assets/js/tables.js"></script>
  


<link rel="stylesheet" href="<?php echo base_url('assets/iCheck/all.css');?>">
  <script src="<?php echo base_url('assets/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<script type="text/javascript">

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
      
      $(function(){
        $('.set_role').on('click', function () {
          if ($(this).is(':checked'))
          {
            var menu_id = $(this).attr('id');
            var menu_num = $(this).val();
            if($('#remove_'+menu_num).length > 0){$('#remove_'+menu_num).removeAttr('checked')}
            $('#menu_num_'+menu_num).append('<td class="'+menu_id+'" width="4%">>></td>');
            $('#menu_num_'+menu_num).append('<td class="'+menu_id+'" colspan="3"><input name="active_'+menu_num+'" type="checkbox" checked="checked" value="1"/> <b>Active</b></td>');
            /*if($('.parent_menu').is(':checked')){
              $('.child_'+menu_num).prop('checked',true);
            }*/
          } else {
            var menu_id = $(this).attr('id');
              $('.'+menu_id).detach();
            var id = $(this).val();
            if($('#remove_'+id).length > 0){$('#remove_'+id).attr('checked', 'checked')}
            /*if($('.parent_menu').length > 0){
              $('.child_'+menu_num).prop('checked',false);
            }*/
          }
        });
      });
  
</script>

          <!-- Footer -->
          <?php $this->load->view('template/footer'); ?>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

</body>
</html>

   