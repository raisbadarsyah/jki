<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PT. JKI</title>

  
  <!-- Global stylesheets -->

  <link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

  <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
  
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
  <!-- /theme JS files -->

</head>

<script>
  function dodelete()
  {
      job=confirm("Apakah anda yakin menghapus permanen user ini? data transaksi terkait user ini akan ikut terhapus");
      if(job!=true)
      {
          return false;
      }
  }
  </script>

<body>

  <!-- Main navbar -->
  <?php
  $this->load->view('template/main_navbar');
  ?>
  <!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
  
  
  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main sidebar -->
       <?php $this->load->view('template/sidebar'); ?>
      <!-- /main sidebar -->


      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
          
          <div class="breadcrumb-line">
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i> Admin</a></li>
              <li class="active"><a>Management Position</a></li>
            </ul>

            <ul class="breadcrumb-elements">
              <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-gear position-left"></i>
                  Settings
                  <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                  <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                  <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                  <li class="divider"></li>
                  <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

          <!-- Basic datatable -->
          <div class="panel panel-flat">
            <div class="panel-heading">
              <h5 class="panel-title">Management Master Data Position</h5>
              <div class="heading-elements">
                <ul class="icons-list">
                          <li><a data-action="collapse"></a></li>
                          <li><a data-action="reload"></a></li>
                          <li><a data-action="close"></a></li>
                        </ul>
                      </div>
            </div>

            <div class="panel-body">
             <?php if($this->session->flashdata('message') != ""){ ?>  
                <h6 class="warning"><i class="fa fa-check"></i>&nbsp;<?php echo $this->session->flashdata('message'); ?></h6>
              <?php } ?>

               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popUp">
                Add New
              </button>
            </div>
           
         
            <table class="table datatable-tools-basic">
               <thead>
                  <tr>
                    <th>No</th>
                    <th>Position Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no=0;foreach($item_list as $row){?>
                  <tr>
                    <td><?php echo ++$no;?></td>
                    <td><?php echo $row->corePositionName;?></td>
                    
                    <td>
                          <a href="<?php echo base_url().'management_position/management_position_role/'.$row->corePositionId;?>" title="Manage" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></a>

                          <a class="btn bg-slate btn-icon" href="<?php echo base_url(); ?>management_position/delete/<?php echo $row->corePositionId;?>" title="Delete" onClick="return dodelete();"><i class="icon-trash"></i></a>
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
            </table>
          
          </div>
          <!-- /basic datatable -->


          <!-- Pagination types -->
          
          <!-- /pagination types -->


          <!-- State saving -->
          
          <!-- /state saving -->


          <!-- Scrollable datatable -->
          
          <!-- /scrollable datatable -->


          <!-- Footer -->
          <?php $this->load->view('template/footer'); ?>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

</body>
</html>


<?php if(!empty($this->session->flashdata('message'))){ 
                  echo "<script type='text/javascript'>
                  var base_url = window.location.origin;
                    PNotify.desktop.permission();
        (new PNotify({
            title: 'Success notification',
            type: 'success',
            text: 'Your Data has been Added Successfully.',
            desktop: {
                desktop: true,
                icon: '". base_url()."assets/images/pnotify/success.png'
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
            alert('Hey! You clicked the desktop notification!');
        });  </script>";}elseif(!empty($this->session->flashdata('error'))){ 
                  echo "<script type='text/javascript'>
                  var base_url = window.location.origin;
                    PNotify.desktop.permission();
        (new PNotify({
            title: 'Failed notification',
            type: 'danger',
            text: 'Failed to add data',
            desktop: {
                desktop: true,
                icon: '". base_url()."assets/images/pnotify/danger.png'
            }
        })
        ).get().click(function(e) {
            if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
            alert('Hey! You clicked the desktop notification!');
        });  </script>";}  ?>

<div class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" id="popUp" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="close_popup()" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="title_popup">Add Position</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-8">
          
            <form class="form-horizontal form-validate-jquery" id="form-add" method="POST" action="<?php echo base_url().'management_position/insert_item'; ?>">
              <input name="itemId" type="hidden">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Position Name</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="itemName" id="itemName" placeholder="Position Name" required>
                </div>
              </div>
              
            </form>
          
          </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="button_save"  onclick="$('#form-add').submit()">Save</button>
      </div>
   
<script type="text/javascript">
 $(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});

    function edititem(id){
      $.getJSON("<?php echo base_url().'management_position/edit_item/';?>"+id, function(json){
        $('input[name="itemName"]').val(json['0'].corePositionName);
        $('input[name="itemId"]').val(json['0'].corePositionId);
        $('#title_popup').text(' Edit Position');
        $('#form-add').attr('action', "<?php echo base_url().'management_position/update_item'; ?>");
        
      });
    };

    function close_popup(){
      $('#title_popup').text(' Add Position');
      var hidden = $('input[name="itemId"]').val();
      $('#parent option[value="'+hidden+'"]').css('display','block');
      $('#form-add')[0].reset();
      $('#form-add').attr('action', "<?php echo base_url().'management_position/insert_item'; ?>");
      
      
    }
    function deletedata(rowdata)
    {
      var okcancel = confirm("Are you sure to delete the data?");

      if (okcancel) {
        $.ajax({
          type: "post",
          url: "<?php echo base_url().'management_position/delete/'; ?>"+rowdata,
          success: function(response) {
            if (response) { 
              $(".rowdata_"+rowdata).fadeOut("fast");
              alert("Success delete data");
            } else {
              alert("Sory, failed delete your data.");
            };
          }
        });
      };
    };

    function isNumberKey(evt)
    {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
      return true;
    };
</script>