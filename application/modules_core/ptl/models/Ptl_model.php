<?php

class Ptl_model extends CI_Model {

   function get_all($params){
		$this->db->select('*,penyedia_listrik.status as status_ptl');
        $this->db->from('penyedia_listrik');
        $this->db->join('kantor_area','penyedia_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
  
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                
                 $this->db->where('penyedia_listrik.kode_area', $params['kode_area']);
            }
        }

        $query = $this->db->get();
       
        return $query->result();
       
	}

    function get_fasa(){
        $this->db->select('*');
        $this->db->from('fasa');
       
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($id_ptl){
		$this->db->select('*');
        $this->db->from('penyedia_listrik');
        $this->db->join('kantor_area','penyedia_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->where('id_ptl',$id_ptl);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
	}

    function get_by_kode_unit_pln($kode_unit_pln){
        $this->db->select('*');
        $this->db->from('penyedia_listrik');
        $this->db->join('kantor_area','penyedia_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->where('kode_unit_pln',$kode_unit_pln);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

	function insert($data_ptl) {
       $query = $this->db->insert('penyedia_listrik', $data_ptl);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update_ptl($data, $id_ptl){
        $this->db->trans_begin();
        $this->db->where('id_ptl', $id_ptl);
        $this->db->update('penyedia_listrik', $data);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return 1;
        }       
    }

    function delete($id_ptl) {
        $query = $this->db->delete('penyedia_listrik', array('id_ptl' => $id_ptl));
         if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
        
    }

}

