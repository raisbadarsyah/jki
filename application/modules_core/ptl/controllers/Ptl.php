<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ptl extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Ptl_Model','Staff/Staff_model','Kanwil/Kanwil_model','Kantor_Sub_Area/Kantor_Sub_Area_Model','Staff/Staff_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$data['position_id'] = $position_id;
		$data['ptl'] = $this->Ptl_Model->get_all($params);


		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$data['kanwil'] = $this->Kanwil_model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$nm_ptl = $this->input->post('nm_ptl');
			$kd_unit_pln = $this->input->post('kd_unit_pln');
			$alamat = $this->input->post('alamat');
			$area = $this->input->post('area');
			$status = $this->input->post('status');

			try {
				
				$data_ptl = array(
					"kode_area"=> $area,
					"nm_ptl"=> $nm_ptl,
					"kode_unit_pln"=> $kd_unit_pln,
					"alamat_ptl"=> $alamat,
					"status"=> $status
				);
				
				$insert_ptl = $this->Ptl_Model->insert($data_ptl);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($insert_ptl == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('ptl','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_ptl = $this->uri->segment(3);

		$data['kanwil'] = $this->Kanwil_model->get_all();
		$data['detail_ptl'] = $this->Ptl_Model->get_by_id($id_ptl);

		$data['area'] = $this->Kantor_Sub_Area_Model->get_area($data['detail_ptl']->kode_kanwil);

		$this->load->view('edit',$data);


	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_ptl = $this->input->post('id_ptl');
			$nm_ptl = $this->input->post('nm_ptl');
			$kd_unit_pln = $this->input->post('kd_unit_pln');
			$alamat = $this->input->post('alamat');
			$area = $this->input->post('area');
			$status = $this->input->post('status');
			
			try {
				
				$data_ptl = array(
					"kode_area"=> $area,
					"nm_ptl"=> $nm_ptl,
					"kode_unit_pln"=> $kd_unit_pln,
					"alamat_ptl"=> $alamat,
					"status"=> $status
				);
				
				$update_ptl = $this->Ptl_Model->update_ptl($data_ptl,$id_ptl);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pemeriksa');

			}


				if($update_ptl == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('ptl','refresh');
				}



		}
	}


	function delete()
	{
		$id_ptl = $this->uri->segment(3);
		$delete_ptl = $this->Ptl_Model->delete($id_ptl);

		if($delete_ptl == 1) 
		{
				$this->session->set_flashdata('success', 'Data berhasil di hapus');
				redirect('ptl','refresh');
		}
	}


}
