<?php

class Register_model extends CI_Model {

    function get_principal() {
        $this->db->select('*');
        $this->db->from('m_supplier');
		$this->db->order_by('supplierPrincipal','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_branch() {
        $this->db->select('*');
        $this->db->from('m_branch');
		$this->db->order_by('branchId','asc');
        $query = $this->db->get();
        return $query->result();
    }
	
	function insert($data){
		$this->db->trans_begin();
        $this->db->insert('core_user', $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
    }
	
	function cek_user($user) {
        $this->db->select('coreUserName');
        $this->db->from('core_user');
        $this->db->where('coreUserName', $user);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserName = $row->coreUserName;
            }

            return $coreUserName;
        } else {
            return FALSE;
        }
    }
	
	function cek_email_available($user) {
        $this->db->select('refEmail');
        $this->db->from('ref_email');
        $this->db->where('refEmail', $user);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $refEmail = $row->refEmail;
            }

            return $refEmail;
        } else {
            return FALSE;
        }
    }
	
	function get_user_id($username) {
        $this->db->select('coreUserId');
        $this->db->from('core_user');
        $this->db->where('coreUserName', $username);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $coreUserId = $row->coreUserId;
            }

            return $coreUserId;
        } else {
            return FALSE;
        }
    }
	
	function ubah_pass($userId, $data){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $userId);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
	
	function aktivasi($id, $data){
        $this->db->trans_begin();
		$this->db->where('coreUserId', $id);
		$this->db->update('core_user', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }
}

