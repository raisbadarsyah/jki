<?php
defined('BASEPATH') OR exit('No direct script access allowed');error_reporting(E_ALL);

class Register extends MX_Controller {
    function __construct(){
        parent::__construct();
		$this->load->model(array('register_model'));
    }

	function index()
	{
		$this->load->helper('captcha');
		$random_number = substr(number_format(time() * rand(),0,'',''),0,6);
		$vals = array(
		 'word' => $random_number,
		 'img_path' => './captcha/',
		 'img_url' => base_url().'captcha/',
		 'img_width' => 140,
		 'img_height' => 50,
		 'expiration' => 7200
		);

		$cap                  = create_captcha($vals);
		$config['captcha']    = $cap['image'];
		$this->session->set_userdata('captchaUser', $cap['word']);
		
		$config['list_principal'] = $this->register_model->get_principal();
		$config['list_branch']    = $this->register_model->get_branch();
		$config['title'] = "Registrasi User";
		$config['title_box'] = "Registrasi User ";
		$this->load->view('register', $config);
	}

	function insert()
	{
		$string = $this->input->post("captcha");
		if($string==$this->session->userdata('captchaUser')) {   
		
			$user 		= $this->input->post('user');
			//$branch     = $this->input->post('branch');
			$email      = $this->input->post('email');
		    $pass 	    = md5($this->input->post('password'));
			$code       = $this->generate_random_string();
			
			$coreUserDepId = 1;
			
			
			$cek_email_in  = $this->register_model->cek_email_available($user);
			if(!empty($cek_email_in)){			
				$cek_user   = $this->register_model->cek_user($user);
				if(empty($cek_user)){
					$data = array(
						'coreUserDepId' => $coreUserDepId,
						'coreUserPositionId' => 5,
						'coreUserName' => $user,
						//'coreBranchId' => $branch,
						'coreUserPassword' => $pass,
						'coreUserActive' => '3',
						'coreUserEmail' => $user,
						'coreUserCodeActivate' => $code
					);
					
					$action = $this->register_model->insert($data);
					
					if($action){
						$this->session->set_flashdata('message','Data pendaftaran berhasil disimpan, aktivasi akun menunggu persetujuan admin HO.');
						redirect('auth','refresh');
					} else {
						$this->session->set_flashdata('message','Data pendaftaran gagal disimpan');
						redirect('register','refresh');
					}
					
				} else {				
					$this->session->set_flashdata('message','Username sudah digunakan');
					$this->session->set_flashdata('username_reg', $user);
					//$this->session->set_flashdata('principal_reg', $principal);
					//$this->session->set_flashdata('branch_reg', $branch);
					$this->session->set_flashdata('email_reg', $email);
					
					redirect('register','refresh');
				}
			} else {
				$this->session->set_flashdata('message','Email anda belum terdaftar sebagai email inventaris web report, mohon segera hubungi admin.');
				redirect('register','refresh');
			}
			
		} else {
			$this->session->set_flashdata('message','Code Verification is Invalid');
			redirect('register','refresh');
		}			
	}
	
	function aktivasi($id){
		$data = array(
			'coreUserActive' => '1'
		);
		
		$action = $this->register_model->aktivasi($id, $data);
		
		if($action){
			$this->session->set_flashdata('message','Akun anda sudah bisa digunakan.');
		}
		else {
			$this->session->set_flashdata('message','Gagal melakukan aktivasi');			
		}
		
		redirect('auth','refresh');
	}
	
	public function create_captcha() {
        $this->load->helper('captcha');
        $random_number = substr(number_format(time() * rand(),0,'',''),0,6);
        $vals = array(
         'word' => $random_number,
         'img_path' => './captcha/',
         'img_url' => base_url().'captcha/',
         'img_width' => 140,
         'img_height' => 50,
         'expiration' => 7200
        );

        $cap    = create_captcha($vals);
        $image  = $cap['image'];
        $this->session->set_userdata('captchaUser', $cap['word']);

        print $image;
    }
	
	function generate_random_string($name_length = 8) {
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		return substr(str_shuffle($alpha_numeric), 0, $name_length);
	}
        
 
}
