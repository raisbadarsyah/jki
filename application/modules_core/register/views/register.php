<!DOCTYPE html>
<html>
  <head>
    <title>PT. MILLENNIUM PHARMACON INTERNATIONAL. Tbk</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/favicon-16x16.png">
    <?php //echo $script_captcha; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<!--<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <div class="logo">
	                 <h1><a href="#">PT. MILLENNIUM PHARMACON INTERNATIONAL. Tbk</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>-->

	<div class="page-content container">
		<div class="row"><br />
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
				<form action="<?php echo site_url('register/insert') ?>" method="post" class="form-validate-jquery">
			        <div class="box">
			            <div class="content-wrap">
			                <div class="social">
	                           <img src="<?php echo base_url();?>assets/images/logo.png" width="120px" />  <br />   
                                <span style="font-size: 11px;"><b>PT. MILLENNIUM PHARMACON INTERNATIONAL Tbk</b></span>	
								<span style="font-size: 11px;"><b>Formulir Registrasi User Web Report</b></span>	
	                        </div>
							
							<br />							
			                <input class="form-control" name="user" type="text"  value="<?php if($this->session->flashdata('username_reg') != ""){ echo $this->session->flashdata('username_reg'); }?>" placeholder="Email">		
							
			                <input class="form-control" name="password" type="password" placeholder="Password">
							
							<form method="GET" id="fmCaptcha">
                            <div style="margin-top:15px;">
                              <div id="captcha">
                                <?php echo $captcha; ?>
                              </div>
                            
                              <a href="javascript:;" onclick="create_captcha();" id="change-image">Tidak terbaca? Ganti text.</a>
                              <br/><br/>
                             </div>
                            <input class="form-control" type="text" name="captcha" id="captcha-form" placeholder="Masukan Kode Verifikasi" />
                          </form>
							
			                <div class="action">
								<?php if($this->session->flashdata('message') != ""){ ?>  
									<h6 class="warning"><i class="fa fa-external-link"></i> <?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?> 
								<div class="form-group">
									<button type="submit" class="btn btn-lg btn-block btn-success">Buat Akun</button>
								</div>
														
			                </div>                
			            </div>
			        </div>
                </form>
			        <div class="already">
			            <!--<p>Don't have an account yet?</p>
			            <a href="signup.html">Sign Up</a>-->
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
	<script type="text/javascript">
		function create_captcha() {
          $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>register/create_captcha",
                cache: false,
                success: function(result) {
                   $("#captcha").html(result);
                }
            });
        }
		
		$(document).ready(function () {
		  $("#principal").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			    return false;
			 }
		   });
		});
	
	</script>
  </body>
</html>