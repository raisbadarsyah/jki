<?php
defined('BASEPATH') OR exit('No direct script access allowed');error_reporting(E_ALL);

class User_aktivasi_akun extends MX_Controller {
    function __construct(){
        parent::__construct();
		$this->load->model(array('management_user/management_user_model'));
    }

	function index()
	{
		$config['title'] = 'Aktivasi Akun';
		$config['page_title'] = 'Aktivasi Akun';
		$config['page_subtitle'] = 'Aktivasi Akun';
       
		$this->load->view('v_aktivasi_akun', $config);
	}

	function activate_account(){
		$kode  = $this->input->post('kode');

		$cek_token = $this->management_user_model->cek_token_user($kode);
		//echo $this->db->last_query(); die();
		if(!empty($cek_token)){
			$data = array(
				'coreUserActive' => '1',
				'coreUserCodeActivate' => ''
			);
			
			$this->management_user_model->active_user_by_code($cek_token, $data);
			$this->session->set_flashdata('message','Akun Anda Sudah Dapat Digunakan Untuk Login');
		} else {
			$this->session->set_flashdata('message','Maaf Kode Tidak Terdaftar');
		}	
		
		redirect(base_url().'auth','refresh');
	}
	
}
