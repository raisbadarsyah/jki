<!DOCTYPE html>
<html>
  <head>
    <title>PT. MILLENNIUM PHARMACON INTERNATIONAL. Tbk</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/favicon-16x16.png">
    <?php //echo $script_captcha; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<!--<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <div class="logo">
	                 <h1><a href="#">PT. MILLENNIUM PHARMACON INTERNATIONAL. Tbk</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>-->

	<div class="page-content container">
		<div class="row"><br />
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
				<form action="<?php echo site_url('lupa_password/rest_pass') ?>" method="post" class="form-validate-jquery">
			        <div class="box">
			            <div class="content-wrap">
			                <div class="social">
	                           <img src="<?php echo base_url();?>assets/images/logo.png" width="120px" />  <br />   
                                <span style="font-size: 11px;"><b>PT. MILLENNIUM PHARMACON INTERNATIONAL Tbk</b></span>	
								<span style="font-size: 11px;"><b>Lupa Password</b></span>	
	                        </div>							
							<br />							
			                <input class="form-control" name="username" value="<?php if($this->session->flashdata('username_reg') != ""){ echo $this->session->flashdata('username_reg'); }?>" type="text" placeholder="Email">		
			                <!--<input class="form-control" id="password1" name="password1" type="password" placeholder="Password">
							<input class="form-control" id="password2" name="password2" type="password" placeholder="Ketik Ulang Password">
							<p id="validate-status"></p>-->
			                <div class="action">
								<?php if($this->session->flashdata('message') != ""){ ?>  
									<h6><?php echo $this->session->flashdata('message'); ?></h6>
								<?php } ?> 
								<div class="form-group">
									<button type="submit" class="btn btn-lg btn-block btn-success">Submit</button>
								</div>
														
			                </div>                
			            </div>
			        </div>
                </form>
			        <div class="already">
			            <!--<p>Don't have an account yet?</p>
			            <a href="signup.html">Sign Up</a>-->
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
	<script type="text/javascript">
	    /* $(document).ready(function() {
		  $("#password2").keyup(validate);
		});

		function validate() {
		  var password1 = $("#password1").val();
		  var password2 = $("#password2").val();

			
		 
			if(password1 == password2) {
			   $("#validate-status").text("password valid");        
			}
			else {
				$("#validate-status").text("password invalid");  
			}
			
		} */
	</script>
  </body>
</html>