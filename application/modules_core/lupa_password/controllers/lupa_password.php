<?php
defined('BASEPATH') OR exit('No direct script access allowed');error_reporting(E_ALL);

class Lupa_password extends MX_Controller {
    function __construct(){
        parent::__construct();
		$this->load->model(array('register/register_model'));
		$this->load->helper(array('email_content'));
    }

	function index()
	{
		$config['title'] = 'Management User | Lupa Password';
		$config['page_title'] = 'Management User | Lupa Password';
		$config['page_subtitle'] = 'Lupa Password';
       
		$this->load->view('v_lupa_password', $config);
	}

	function rest_pass(){
		$username = $this->input->post('username');
		if(!empty($username)){
			$this->session->set_flashdata('username_reg', $username);
		}
		
		$new_pass = $this->generate_random_string();
		
		$data = array(
			'coreUserPassword' => md5($new_pass)
		);
		
		$user_id   =  $this->register_model->get_user_id($username);
		$ubah_pass =  $this->register_model->ubah_pass($user_id, $data);
		
		$cek_user = $this->register_model->cek_user($username);
		
		if(empty($cek_user)){
			$this->session->set_flashdata('message','Maaf Username Tidak Terdaftar');
		} else {
			/* $config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' => 'mpi17sppt@gmail.com',
				'smtp_pass' => 'support2017',
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1'
			);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			$mail = $this->email;

			$mail->from('mpi17sppt@gmail.com', 'MIS-Admin');
			$mail->to($username); 
			
			$data = array(
				'password'=> $new_pass
			);
			
			$mail->subject('Reset Password');
			$body = $this->load->view('template/email_lupa_password', $data, TRUE);
			$this->email->message($body);   

			$mail->send(); */
			
			$this->load->library('PHPMailerAutoload');
        
			$mail = new PHPMailer();
			//$mail->SMTPDebug = 2;
			$mail->Debugoutput = 'html';
			
			$mail->isSMTP();
			$mail->Host = 'mail.mpi-indonesia.co.id';
			$mail->Port = 587; 
			$mail->SMTPAuth = true;
			$mail->IsHTML(true);
			$mail->Username = 'missupport@mpi-indonesia.co.id';
			$mail->Password = 'support2017';
			$mail->WordWrap = 50;  
			$mail->setFrom('missupport@mpi-indonesia.co.id', 'Generate New Password');
			$mail->addAddress($username);
			$mail->Subject = 'Generate New Password';
			
			$content = "Generate password anda : <b>".$new_pass."</b> <br />Untuk keamanan silahkan login dan ubah password anda.";
			
			$mail->Body = email_body($content);
			
			$mail->send();
			
			$this->session->set_flashdata('message','Password berhasil dirubah silahkan cek email, mohon untuk mengubah password setelah berhasil login.');
		}
		
		redirect(base_url().'auth','refresh');
	}
	
	function generate_random_string($name_length = 8) {
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		return substr(str_shuffle($alpha_numeric), 0, $name_length);
	}
}
