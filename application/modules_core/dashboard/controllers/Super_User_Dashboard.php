<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_User_Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','sales_nasional/sales_nasional_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();
		//var_dump(check_authority_url());		
	}


	function index()
	{
		$user_id = $this->session->userdata('user_id');
		$group = $this->session->userdata('dep_id');
		//var_dump($group);
		$config['title'] = 'Dashboard';
		$config['page_title'] = 'Dashboard';
		$config['page_subtitle'] = 'Dashboard';	
		
		if($group !=3){
			$config['principal_list'] = $this->menu_model->get_principal_list($user_id);
		} 
		else {
			$config['principal_list'] = $this->sales_nasional_model->get_principal();
		}
		
		$this->load->view('super_user_dashboard',$config);
	}

	

}
