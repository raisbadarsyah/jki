<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		
		$this->load->model(array('menu_model','dashboard_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();
		
		//var_dump(check_authority_menu());		
	}


	function index()
	{
		$user_id = $this->session->userdata('user_id');
		$level = $this->session->userdata('level');

		$config['title'] = 'Dashboard';
		$config['page_title'] = 'Dashboard';
		$config['page_subtitle'] = 'Dashboard';	
		
	
		$this->load->view('dashboard',$config);
	}

	

}
