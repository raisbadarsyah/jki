
	
<?php
$this->load->view('template/header');
?>


	<!-- Page container -->
	<div class="page-container">
		
		<!-- Page content -->
		<div class="page-content">
			
			<!-- Main Sidebar -->

			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">


				<!-- Page header -->
				<!-- 
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
						</div>

						
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						
					</div>
				</div>
				-->
				<!-- /page header -->

				
				<!-- Content area -->
				<div class="content">

					
					<!-- Quick stats boxes -->
					
							<div class="row">
								<div class="col-lg-12">

									<!-- Custom text -->
							<div class="panel panel-white">
								<div class="panel-heading">
									<h6 class="panel-title">Selamat Datang di Aplikasi SLO Online PT.JKI</h6>
									<div class="heading-elements">
										<span class="heading-text"><i class="icon-github4"></i>&nbsp; Klik <b><a href="<?php echo base_url();?>uploads/format/pelanggan_import.xls">disini </a></b>untuk format excel import pelanggan</span>
									</div>
								</div>
								
								<div class="panel-body">
									<h1>Visi</h1>
					<h3 style="line-height:22px;">Menjadi suatu perusahaan yang bergerak di bidang ketenagalistrikan yang mampu menjadi pelopor untuk terwujudnya "Keselamatan Ketenagalistrikan" pada Instalasi Pemanfaat Tenaga Listrik Tegangan Rendah.</h2>
					<br />
					<h1>Misi</h1>
					<h4>
					<ol>
						<li style="line-height:22px;">Mendukung dan melaksanakan program Keselamatan Ketenagalistrikan untuk  pemeriksaan dan pengujian instalasi pemanfaatan tenaga listrik tegangan rendah;</li>
					<br />
						<li style="line-height:22px;">Memastikan kegiatan usaha ketenagalistrikan memenuhi syarat  Aman, Andal dan Akrab Lingkungan;</li>
					<br />
						<li style="line-height:22px;">Mendukung  dan melaksanakan  program sertifikasi di bidang usaha pemanfaatan tenaga listrik dan usaha penunjang tenaga listrik;</li>
					<br />
						<li style="line-height:22px;">Mendukung upaya menjadikan tenaga listrik sebagai media untuk  meningkatkan kualitas kehidupan masyarakat;</li>
					<br />
						<li style="line-height:22px;">Memenuhi tuntutan pasar terhadap kepuasan pelanggan dengan memanfaatkan kemajuan teknologi.</li>
					</ol>
					</h3>
					<br />

								</div>
							</div>
							<!-- /custom text -->

								</div>

								
							</div>

					<!-- Dashboard content -->
					<div class="row">
						<div class="col-lg-12">

						
							
							<!-- /quick stats boxes -->


							<!-- Support tickets -->
							
							<!-- /support tickets -->


							<!-- Latest posts -->
							
							<!-- /latest posts -->

						</div>

						
						
					<!-- /dashboard content -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->
					
				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
