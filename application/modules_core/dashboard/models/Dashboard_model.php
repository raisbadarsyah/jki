<?php

class Dashboard_model extends CI_Model {
		
		function total_principal()
		{
			$this->db->select('COUNT(suplierId) as jumlah_principal');
	        $this->db->from('m_supplier');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_principal_user_principal($user_id)
		{
			$this->db->select('COUNT(suplierId) as jumlah_principal');
	        $this->db->from('m_supplier');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId');
	        $this->db->where('coreUserId',$user_id);
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_fdk()
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_fdk_user_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->join('m_supplier','m_supplier.suplierId=fdk.id_principal');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId');
	        $this->db->where('coreUserId',$user_id);
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_ho_approve()
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->where('id_approval','11');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_ho_approve_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	         $this->db->join('m_supplier','m_supplier.suplierId=fdk.id_principal','left');
	         $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId','left');
	        $this->db->where('coreUserId',$user_id);
	        $this->db->where('id_approval','11');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_all_approve()
		{
			$this->db->select('COUNT(number) as jumlah_fdk,nm_approval');
	        $this->db->from('fdk');
	        $this->db->join('m_approval_status','fdk.id_approval=m_approval_status.id_approval','right');
	        $this->db->where('m_approval_status.id_approval','4');
	        $this->db->or_where('m_approval_status.id_approval','9');
	        $this->db->or_where('m_approval_status.id_approval','10');
	        $this->db->or_where('m_approval_status.id_approval','11');
	        $this->db->group_by('m_approval_status.id_approval');
	        $query = $this->db->get();
	     
	        return $query->result();
		}

		function total_all_approve_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk,nm_approval');
	        $this->db->from('fdk');
	        $this->db->join('m_supplier','m_supplier.suplierId=fdk.id_principal');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId');
	        $this->db->join('m_approval_status','fdk.id_approval=m_approval_status.id_approval','right');
	        $this->db->where('coreUserId',$user_id);
	        $this->db->where('(m_approval_status.id_approval','4');
	        $this->db->or_where('m_approval_status.id_approval','9');
	        $this->db->or_where('m_approval_status.id_approval','10');
	        $this->db->or_where('m_approval_status.id_approval','5');
	        $this->db->or_where('m_approval_status.id_approval = 11)');
	        $this->db->group_by('m_approval_status.id_approval');
	        $query = $this->db->get();
	     
	        return $query->result();
		}

		function total_fdk_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk,suplierId,supplierPrincipal,supplierNickName');
	        $this->db->from('m_supplier');
	        $this->db->join('fdk','m_supplier.suplierId=fdk.id_principal','left');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId');
	        $this->db->where('coreUserId',$user_id);
	        $this->db->group_by('m_supplier.suplierId');
	        $query = $this->db->get();
	     
	        return $query->result();
		}

		function total_all_fdk_principal()
		{
			$this->db->select('COUNT(number) as jumlah_fdk,suplierId,supplierPrincipal,supplierNickName');
	        $this->db->from('m_supplier');
	        $this->db->join('fdk','m_supplier.suplierId=fdk.id_principal','left');
	        $this->db->group_by('m_supplier.suplierId');
	        $query = $this->db->get();
	     
	        return $query->result();
		}

		function total_all_principal_approve()
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->where('id_approval','4');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_all_principal_approve_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->join('m_supplier','m_supplier.suplierId=fdk.id_principal','left');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId','left');
	        $this->db->where('coreUserId',$user_id);
	        $this->db->where('id_approval','4');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_fdk_created_approve()
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->where('id_approval','9');
	        $query = $this->db->get();
	     
	        return $query->row();
		}

		function total_fdk_created_approve_principal($user_id)
		{
			$this->db->select('COUNT(number) as jumlah_fdk');
	        $this->db->from('fdk');
	        $this->db->join('m_supplier','m_supplier.suplierId=fdk.id_principal','left');
	        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId','left');
	        $this->db->where('coreUserId',$user_id);
	        $this->db->where('id_approval','9');
	        $query = $this->db->get();
	     
	        return $query->row();
		}
}