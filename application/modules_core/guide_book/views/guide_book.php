<?php
$this->load->view('template/header');
?>

    <div class="page-content">
    	<div class="row">
		  <?php $this->load->view('template/sidebar'); ?>
		  <div class="col-md-10">  
            <div class="content-box-large">		  
				<iframe id="fred" title="PDF in an i-Frame" src="<?php echo base_url(); ?>uploads/GuideBookWebReport.pdf" frameborder="0" scrolling="auto" height="500" width="1050"></iframe>
			</div>
		</div>
    </div>
<?php $this->load->view('template/footer');?>
