<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	

	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	
	<script type="text/javascript" class="init">
	

	$(document).ready(function() {

				
			load_data();

			 function load_data(tgl_terbit_from,tgl_terbit_to)
			 {


				sertifikasi = $('#sertifikasi').DataTable({ 
			
			        "processing": true, //Feature control the processing indicator.
			        "serverSide": true, //Feature control DataTables' server-side processing mode.
			        "order": [], //Initial no order.
			        "lengthMenu": [
			            [ 10, 25, 50, -1 ],
			            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
			        ],
			        // Load data for the table's content from an Ajax source
			        "ajax": {
			            "url": "<?php echo base_url('laporan/laporan_sertifikasi')?>",
			            "type": "POST",
			            "data": function(data){
			                data.tgl_terbit_from = $('#tgl_terbit_from').val();
			                data.tgl_terbit_to = $('#tgl_terbit_to').val();
			            }
			        },
			 		sRowSelect: "single",
			 		//dom: 'Bfrtip',
			 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
					],

			        //Set column definition initialisation properties.
			        "columnDefs": [
			        { 
			            "targets": [11,12], //first column / numbering column
			            "orderable": false, //set not orderable
			        },
			        ],
			 
			    });

			}

		   	

			$("#tgl_terbit_to").bind("change paste keyup", function() {
					
				
					var tgl_terbit_from = $('#tgl_terbit_from').val();
					var tgl_terbit_to = $(this).val();
					
					$('#sertifikasi').DataTable().destroy();
							  if(tgl_terbit_from != '' && tgl_terbit_to == '')
							  {
							   load_data(tgl_terbit_from,'');
							  }
							  else if(tgl_terbit_to != '' && tgl_terbit_from == '')
							  {
							  	load_data('',tgl_terbit_to);
							  }
							  else if(tgl_terbit_from != '' && tgl_terbit_to != '')
							  {
							  	load_data(tgl_terbit_from,tgl_terbit_to);
							  }
							  else
							  {
							   load_data();
							  }
			});

							//$("#9").val(date1);
			


 });

	</script>

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>

	<style type="text/css">

		.dt-buttons {
		    position: relative;
		    display: block;
		    float: right;
		    margin: 0 0 20px 20px;
		}

	</style>
	<style type="text/css">
	
	.grid-view-loading
	{
		background:url(loading.gif) no-repeat;
	}

	.grid-view
	{
		padding: 4px 0;
	}

	.datatable-header>div:first-child, .datatable-footer>div:first-child {
    margin-left: 20px;
	}

	.dataTables_length {
    	float: right;
	    display: inline-block;
	    margin: 0 0 20px 20px;
	    margin-top: 0px;
	    margin-right: 20px;
	    margin-bottom: 20px;
	    margin-left: 20px;
	}

	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}

</style>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Admin</a></li>
							<li><a>Laporan</a></li>
							
						</ul>

						
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<!-- Left icons -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h6 class="panel-title">Laporan</h6>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="tabbable">
										<ul class="nav nav-tabs nav-tabs-highlight">
											<li class="active"><a href="#left-icon-tab1" data-toggle="tab"><i class="icon-menu7 position-left"></i> Pelanggan</a></li>
											<li><a href="#left-icon-tab2" data-toggle="tab"><i class="icon-menu7 position-left"></i>Pembayaran</a></li>
											<li><a href="#left-icon-tab3" data-toggle="tab"><i class="icon-menu7 position-left"></i>Pemeriksaan</a></li>
											<li><a href="#left-icon-tab4" data-toggle="tab"><i class="icon-menu7 position-left"></i>Verifikasi</a></li>
											<li><a href="#left-icon-tab5" data-toggle="tab"><i class="icon-menu7 position-left"></i>Sertifikasi</a></li>

										</ul>

										<div class="tab-content">
											<div class="tab-pane active" id="left-icon-tab1">
												
																<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Pelanggan List</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															<?php if ($this->session->flashdata('error') == TRUE): ?>
									                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
									            <?php endif; ?>
									            <?php if ($this->session->flashdata('success') == TRUE): ?>
									                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
									            <?php endif; ?>

																<?php 
														$position_id = $this->session->userdata('position_id');
														//if($position_id == 9){
													?>
													<!--
									  				<a href="<?php echo base_url(); ?>pelanggan/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>
													-->

									  			
																
															</div>

															 <div id="pelanggan-grid" class="grid-view">
																 <table id="pelanggan" class="display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
															               <th>No Pendaftaran</th>						
																			<th>Tgl Input Pelanggan</th>
																			
																			<th>Tipe</th>
																			<th>Tarif/Daya</th>
																			<th>Wilayah</th>
																			<th>Area</th>
																			<th>Sub Area</th>
																			<th>Nama</th>
																			<th>Alamat</th>

																			<th>Kota</th>
																			
																			<th>Nama Btl</th>
																			<th>Penyedia Listrik</th>
																			<th>Metode Daftar</th>
																			
																			<th>Status Pembayaran</th>
																			<th>Status Pemeriksaan</th>
																			<th>Status Verifikasi</th>
																			<th>Status Sertifikat</th>
																			<th>Diinputkan Oleh</th>
																			<th>Action</th>
															            </tr>
															        </thead>
															 
															    </table>
																</div>
														</div>
														<!-- /basic datatable -->
														<div class="text-left" class="col-md-6">
															<a href="<?php echo base_url()?>laporan/excel_pelanggan" class="btn btn-primary">Export</a>
															
														</div>
											</div>

											<div class="tab-pane" id="left-icon-tab2">

														<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Pembayaran</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															
															</div>

															<div id="pelanggan-grid" class="grid-view">
																 <table id="pembayaran" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
																			<th>No Kwitansi</th>						
																			<th>No Pendaftaran</th>
																			<th>Nama</th>
																			<th>Kota</th>
																			<th>Wilayah</th>
																			<th>Area</th>
																			<th>Sub Area</th>
																			<th>Tanggal Pembayaran</th>
																			<th>Diterima Oleh</th>
																			<th>Cetak Kwitansi</th>
																			<th>Dicetak Oleh</th>
																			
																			<th>Action</th>
																		</tr>
															        </thead>
															 
															    </table>
																</div>
														</div>
														<!-- /basic datatable -->
														<div class="text-left" class="col-md-6">
															<a href="<?php echo base_url()?>laporan/excel_pembayaran" class="btn btn-primary">Export</a>
															
														</div>
											</div>

											<div class="tab-pane" id="left-icon-tab3">
												
												<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Pemeriksaan</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															
															</div>

															<div id="pelanggan-grid" class="grid-view">
																 <table id="pemeriksaan" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															            <tr>
																		<th>No Kwitansi</th>
																		<th>No Pendaftaran</th>						
																		<th>Nama</th>
																		<th>Alamat</th>
																		<th>Daya</th>
																		<th>Kota</th>
																		<th>Wilayah</th>
																		<th>Area</th>
																		<th>Sub Area</th>
																		<th>NO LHPP</th>
																		<th>Tgl LHPP</th>
																		<th>Petugas Pemeriksa</th>
																		<th>Tgl Pembayaran</th>
																		<th>Tgl Pemeriksaan</th>
																		<th>Diperiksa</th>
																		<th>Action</th>
																		</tr>
															        </thead>
															 
															    </table>
																</div>
														</div>
														<!-- /basic datatable -->

														<div class="text-left" class="col-md-6">
															<a href="<?php echo base_url()?>laporan/excel_pemeriksaan" class="btn btn-primary">Export</a>
															
														</div>

											</div>

											<div class="tab-pane" id="left-icon-tab4">
												

												<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Verifikasi</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															
															</div>

															 <div id="pelanggan-grid" class="grid-view">
																 <table id="verifikasi" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															           <tr>
																			<th>No Pendaftaran</th>
																			<th>Nama Pelanggan</th>						
																			<th>Kota</th>
																			<th>Wilayah</th>
																			<th>Area</th>
																			<th>Sub Area</th>
																			<th>No LHPP</th>
																			<th>Tanggal Verifikasi</th>
																			<th>Hasil Verifikasi</th>
																			<th>Action</th>
																		</tr>
															        </thead>
															 
															    </table>
																</div>
														</div>
														<!-- /basic datatable -->
														<div class="text-left" class="col-md-6">
															<a href="<?php echo base_url()?>laporan/excel_verifikasi" class="btn btn-primary">Export</a>
															
														</div>

											</div>

											<div class="tab-pane" id="left-icon-tab5">
												

												<!-- Basic datatable -->
														<div class="panel panel-flat">
															<div class="panel-heading">
																<h5 class="panel-title">Sertifikasi</h5>
																<div class="heading-elements">
																	<ul class="icons-list">
												                		<li><a data-action="collapse"></a></li>
												                		<li><a data-action="reload"></a></li>
												                		<li><a data-action="close"></a></li>
												                	</ul>
											                	</div>
															</div>

															<div class="panel-body">
															
															</div>

															 <div id="pelanggan-grid" class="grid-view">
																 <table id="sertifikasi" class="table display items" cellspacing="0" cellpadding="0" border="0">
															        <thead>
															        	<tr>
																	        <td></td>
																	        <td></td>
																	        <td></td>
																	         <td></td>
																	        <td></td>
																	        <td></td>
																	        <td></td>
																	        <td align="middle"></td>
																	        <td></td>
																	        <td></td>
																	        <td></td>
																	        <td><input type="text" id="tgl_terbit_from" class="daterange-single" name="tgl_terbit_from" width="5px">
																	        To
																	        <br>
																	        <input type="text" id="tgl_terbit_to" class="daterange-single" name="tgl_terbit_to">
																	        </td>
																	        <td></td>
																	    
																	        
																	    </tr>

															            <tr>
																			<th>No Pendaftaran</th>
																			<th>Nama Pelanggan</th>
																			<th>Daya</th>
																			<th>Harga</th>						
																			<th>Kota</th>
																			<th>Wilayah</th>
																			<th>Area</th>
																			<th>Sub Area</th>
																			<th>No LHPP</th>
																			<th>BTL</th>
																			<th>No Seri</th>
																			<th>Tgl cetak</th>
																			<th>Dicetak Oleh</th>
																			
																		</tr>

																		

															        </thead>
															 
															    </table>
																</div>
														</div>
														<!-- /basic datatable -->
														<div class="text-left" class="col-md-6">
															<a href="<?php echo base_url()?>laporan/excel_sertifikasi" class="btn btn-primary">Export</a>
															
														</div>

											</div>


										</div>
									</div>
								</div>
							</div>
						</div>

						
					</div>
					<!-- /left icons -->

					
				

					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpAdd">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Input Data Pelanggan</h5>
								</div>

								<form action="<?php echo site_url();?>pelanggan/add" class="form-horizontal" method="get">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Tipe Pelanggan</label>
											<div class="col-sm-9">
												<select class="form-control select" name="tipe_pelanggan" id="tipe_pelanggan" onchange="showDiv()">
													
													<option value="1">Baru</option>
													<option value="2">Rubah Daya</option>
													<option value="3">Instalasi Lama</option>
												</select>
												
											</div>
										</div>
										
										<div class="form-group" id="div_no_slo" style="display: none">
											<label class="control-label col-sm-3">No SLO</label>
											<div class="col-sm-9">
												<input type="text" name="no_slo" id="no_slo" class="form-control" disabled>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

<script type="text/javascript">
	
	function showDiv()
	{
		var tipe_pelanggan = $('#tipe_pelanggan :selected').val();

		 if(tipe_pelanggan==2){
		 	
		    document.getElementById('div_no_slo').style.display = "block";
		   
			$("#no_slo").prop('disabled',false);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==1){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==3){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

	

	}
</script>

<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>

<script type="text/javascript">

var pelanggan;
 
$(document).ready(function() {
 	 
 	  var dark = $("table.items").parent();

           $(dark).block({
            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.85,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none',
                color: '#1B2024'
            }
        });


     pelanggan = $('#pelanggan').DataTable({ 
 		
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('laporan/laporan_pelanggan')?>",
            "type": "POST"
        },
 		
 		"initComplete": function( settings, json ) {
		    $(dark).unblock();
		  },
		  
 		"lengthMenu": [
			            [ 10, 25, 50, -1 ],
			            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
			        ],
			        
 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
					],

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 18 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 
});

var pembayaran;
$(document).ready(function() {

pembayaran = $('#pembayaran').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('laporan/laporan_pembayaran')?>",
            "type": "POST",
        },
 		
 		"lengthMenu": [
			            [ 10, 25, 50, -1 ],
			            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
			        ],
			        
 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
					],

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 });

var pemeriksaan;
$(document).ready(function() {

pemeriksaan = $('#pemeriksaan').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('laporan/laporan_pemeriksaan')?>",
            "type": "POST",
        },
 		
 		"lengthMenu": [
			            [ 10, 25, 50, -1 ],
			            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
			        ],
			        
 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
					],

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 });

var verifikasi;
$(document).ready(function() {

verifikasi = $('#verifikasi').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 		
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('laporan/laporan_verifikasi')?>",
            "type": "POST",
        },
 		
 		 "lengthMenu": [
			            [ 10, 25, 50, -1 ],
			            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
			        ],

 		"dom": '<"datatable-header"frB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
					buttons: [
						'copy', 'csv', 'excel', 'pdf', 'print', 'pageLength'
					],

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });
 });

</script>

</body>
</html>
