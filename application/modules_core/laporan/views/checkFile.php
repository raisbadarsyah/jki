<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>
	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Admin</a></li>
							<li><a>Pelanggan</a></li>
							<li class="active">Pelanggan</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Upload Pelanggan</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
						
						<?php 
						$total_error = 0;
						foreach($checkFile as $row){
							if($row['message'] !="") { 

							$error = 'Error';
							$total_error = count($error);

							}
						}
						?>
						<?php if($total_error > 0 ) { ?>
						
									<div class="alert bg-danger">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold">Warning!</span> Ada kesalahan data, Silahkan cek kembali file. Pastikan semua isian sesuai
								    </div>
									</div>

						
						
						<?php } ?>

				<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>Nama</th>						
										<th>Alamat</th>
										<th>Kota</th>
										<th>Telp</th>
										<th>BTL</th>
										<th>Tarif</th>
										<th>Daya</th>
										<th>Bangunan</th>
										<th>PJT</th>
										<th>Sub Area</th>
										<th>No Gambar</th>
										<th>Tgl Gambar</th>
										<th>No Sip</th>
										<th>Tgl Sip</th>
										<th>Nama Instalir</th>
										<th>Telp Instalir</th>

										<th width="15%" align="center">Message</th>
									</tr>
								</thead>
								<tbody>
									<?php 
					$total_error = 0;
					foreach($checkFile as $row){?>
					<?php if($row['message'] !="") { 

						$error = 'Error';
						$total_error = count($error);
					?>
					<tr class="warning">
					<?php } else { ?>
					<tr>
					<?php } ?>
					
						<td><?php echo $row['Nama'];?></td>
						<td><?php echo $row['alamat'];?></td>
						<td><?php echo $row['nm_kota'];?></td>
						<td><?php echo $row['telp'];?></td>
						<td><?php echo $row['nm_btl'];?></td>
						<td><?php echo $row['tarif'];?></td>
						<td><?php echo $row['daya'];?></td>
						<td><?php echo $row['nm_bangunan'];?></td>
						<td><?php echo $row['nm_ptl'];?></td>
						<td><?php echo $row['kode_sub_area'];?></td>
						<td><?php echo $row['no_gambar'];?></td>
						<td><?php echo $row['tgl_gambar'];?></td>
						<td><?php echo $row['no_sip'];?></td>
						<td><?php echo $row['tgl_sip'];?></td>
						<td><?php echo $row['nm_instalir'];?></td>
						<td><?php echo $row['telp_instalir'];?></td>
						<td><?php echo $row['message'];?></td>
						
				    </tr>
				    <?php }?>
				  
								</tbody>
							</table>
						</div>
					</div>		

					

					<div class="text-right">
									<?php if($total_error > 0) { ?>

									<a href="<?php echo base_url(); ?>pelanggan/upload"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Upload Ulang</button></a>

									<button type="submit" class="btn btn-primary" disabled>Submit form <i class="icon-arrow-right14 position-right"></i></button>

									<?php } ?>
								</div>
					

					</div>
					<form action="<?php echo base_url().'pelanggan/upload_pelanggan'; ?>" method="post">
								<input type="hidden" name="file" value="<?php echo $file ?>">
								
								<div class="text-right">
									<?php if($total_error == 0) { ?>

									
									<button type="submit" class="btn btn-primary">Prosess Input <i class="icon-arrow-right14 position-right"></i></button>
									<?php } ?>
								</div>
								
					</form>
					<!-- /basic table -->

					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<!-- Horizontal form modal -->
					<div class="modal fade" id="popUpAdd">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Input Data Pelanggan</h5>
								</div>

								<form action="<?php echo site_url();?>pelanggan/add" class="form-horizontal" method="get">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Tipe Pelanggan</label>
											<div class="col-sm-9">
												<select class="form-control select" name="tipe_pelanggan" id="tipe_pelanggan" onchange="showDiv()">
													
													<option value="1">Baru</option>
													<option value="2">Rubah Daya</option>
													<option value="3">Instalasi Lama</option>
												</select>
												
											</div>
										</div>
										
										<div class="form-group" id="div_no_slo" style="display: none">
											<label class="control-label col-sm-3">No SLO</label>
											<div class="col-sm-9">
												<input type="text" name="no_slo" id="no_slo" class="form-control" disabled>
												
											</div>
										</div>
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->

<script type="text/javascript">
	
	function showDiv()
	{
		var tipe_pelanggan = $('#tipe_pelanggan :selected').val();

		 if(tipe_pelanggan==2){
		 	
		    document.getElementById('div_no_slo').style.display = "block";
		   
			$("#no_slo").prop('disabled',false);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==1){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

		  if(tipe_pelanggan==3){
		 	
		    document.getElementById('div_no_slo').style.display = "none";
		   
			$("#no_slo").prop('disabled',true);

	    	event.preventDefault();
		  }

	

	}
</script>

<script type="text/javascript">
	
	$(function () {      
      $('#datatable').DataTable();
    });
 $('.datatable-tools-basic').DataTable({
  autoWidth: true,
  dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
    },
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});



</script>
</body>
</html>
