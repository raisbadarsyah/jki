
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT. JKI</title>

		<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_layouts.js"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_date.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_advanced.js"></script>


	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/media/fancybox.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>

	<!-- /theme JS files -->

	
    
    <script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/form_multiselect.js"></script>


	<!-- Theme JS files -->
	<script type="text/javascript" src='https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyDK0sv7tzzzK7eqMR18xQmsiIIkB1fw3Do&libraries=places'></script>

	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery_ui/autocomplete.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/typeahead_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/autocomplete_addresspicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/pickers/location/location.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/prism.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/gallery_library.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/picker_location_edit.js"></script>

	

	
</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Vertical form options -->
			
					<!-- /vertical form options -->


					<!-- Centered forms -->
				
					<!-- /form centered -->


					<!-- Fieldset legend -->
					
					<!-- /fieldset legend -->


					<div class="row">

							<div class="col-md-12">

							<!-- Basic layout-->
							<form action="" enctype="multipart/form-data" method="post" class="form-horizontal">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Lokasi Pemeriksaan</h5>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">
										
										<div class="form-group">
											<label class="col-lg-3 control-label">No Pendaftaran:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="no_pendaftaran" value="<?php echo $detail_pemeriksaan->no_pendaftaran ?>" readonly>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Tipe Pelanggan:</label>
											<div class="col-lg-9">
												<?php if($detail_pemeriksaan->tipe_pelanggan == 1) { ?>
													<label class="col-lg-4 control-label"> Baru </label>
												<?php } elseif ($detail_pemeriksaan->tipe_pelanggan == 2) { ?>
													<label class="col-lg-4 control-label"> Rubah Daya </label>
												<?php } elseif ($detail_pemeriksaan->tipe_pelanggan == 3) { ?>
													<label class="col-lg-4 control-label"> Instalasi Lama </label>
												<?php } ?>
											</div>
										</div>

										
										<div class="form-group">
											<label class="col-lg-3 control-label">Nama:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->Nama ?></label>
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Alamat:</label>
											<div class="col-lg-9">
												<label class="col-lg-4 control-label"><?php echo $detail_pemeriksaan->alamat ?></label>
											</div>
										</div>

										

										<div class="col-md-12">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Koordinat Lokasi</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<div class="form-group" style="display: none">
										<label>Location:</label>
										<input type="hidden" class="form-control" id="us3-address">
									</div>

									

									<div class="form-group">
										<div id="us3" class="map-wrapper"></div>
									</div>

									<div class="form-group">		
										<label>Latitude:</label>
										<input type="text" class="form-control" id="us3-lat" name="lattitude" required="" value="<?php echo $detail_pemeriksaan->lattitude ?>">
									</div>

									<div class="form-group">
										<label>Longitude:</label>
										<input type="text" class="form-control" id="us3-lon" name="longitude" required="" value="<?php echo $detail_pemeriksaan->longitude ?>">
									</div>
								</div>
							</div>
						</div>
										
										
									</div>
								</div>
							</form>
							<!-- /basic layout -->

						</div>

					<!-- 2 columns form -->
			
				

							<!-- /basic layout -->
				</div>
		

						

					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<script>
 		
 		function get_area(){
           var kanwil = $('#kanwil :selected').val();
           var dark = $("select.area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "kanwil="+kanwil,
               url: '<?php echo base_url('kantor_sub_area/get_area/' )?>',
               success: function(result) {
                result;
                 
                $('#area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
        
       }


       function get_sub_area(){
       		
			var area = $('#area :selected').val();
            var dark = $("select.sub_area").parent();

	           $(dark).block({
	            message: '<i class="icon-spinner spinner"></i>Silahkan tunggu',
	            overlayCSS: {
	                backgroundColor: '#1B2024',
	                opacity: 0.85,
	                cursor: 'wait'
	            },
	            css: {
	                border: 0,
	                padding: 0,
	                backgroundColor: 'none',
	                color: '#fff'
	            }
	        });
       

             // alert(id_branch);
              $.ajax({
               type: 'POST',
               data: "area="+area,
               url: '<?php echo base_url('kantor_sub_area/get_sub_area/' )?>',
               success: function(result) {
                result;
                 
               
                $('#sub_area').html(result);  

                window.setTimeout(function () {
		            $(dark).unblock();
		        }, 20);


                }
              });
         
        
       }

  </script>

</body>
</html>
