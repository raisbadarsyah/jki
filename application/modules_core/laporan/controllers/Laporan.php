<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 require_once APPPATH.'/libraries/spout/src/Spout/Autoloader/autoload.php';
        //lets Use the Spout Namespaces
 use Box\Spout\Writer\WriterFactory;
 use Box\Spout\Common\Type;

class Laporan extends CI_Controller {

	function __construct()
	{
		ini_set('memory_limit', '-1');
		set_time_limit(0);
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','management_position/management_position_model','departemen/Departemen_Model','Kanwil/Kanwil_Model','Provinsi/Provinsi_Model','kantor_sub_area/Kantor_Sub_Area_Model','kantor_area/Kantor_Area_Model','Laporan_Model','staff/Staff_model','Pelanggan/Pelanggan_Model','Sertifikasi/Sertifikasi_Model','Pembayaran/Pembayaran_Model','Verifikasi/Verifikasi_Model'));
		$this->load->helper('check_auth_menu');
		//$this->load->library('csvimport');
		$this->load->library('PHPExcel');
		check_authority_url();		

	}


	function index()
	{
		

		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		//$data['pelanggan'] = $this->Laporan_Model->get_all($params);
		
		$this->load->view('index');
	}

	function pembayaran()
	{
		$this->load->view('pembayaran');
	}

	function pemeriksaan()
	{
		$this->load->view('pemeriksaan');
	}

	function verifikasi()
	{
		$this->load->view('verifikasi');
	}

	function sertifikasi()
	{
		$this->load->view('sertifikasi');
	}

	function pelanggan()
	{
		
		
		//$data['pelanggan'] = $this->Laporan_Model->get_all($params);
		
		$this->load->view('pelanggan');
	}

	
	function laporan_pelanggan()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Laporan_Model->get_datatables($params);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
            if($data_pelanggan->tipe_pelanggan == 1) 
            { 
					$tipe_pelanggan = "Baru";
			} 
			elseif ($data_pelanggan->tipe_pelanggan == 2) 
			{
					$tipe_pelanggan = "Rubah Daya";
			} 
			elseif ($data_pelanggan->tipe_pelanggan == 3) 
			{
					$tipe_pelanggan = "Instalasi Lama";
			} 
			else
			{
				$tipe_pelanggan = "";
			}

			//
			if($data_pelanggan->status_pembayaran == 1) 
            { 
					$status_pembayaran = "Sudah Bayar";
			} 
			elseif ($data_pelanggan->tipe_pelanggan == 2) 
			{
					$status_pembayaran = "Belum Bayar";
			}
			else
			{
				$status_pembayaran = "";
			}

			if($data_pelanggan->status_pemeriksaan == 1) 
            { 
					$status_pemeriksaan = "Sudah Diperiksa";
			} 
			elseif ($data_pelanggan->status_pemeriksaan == 2) 
			{
					$status_pemeriksaan = "Belum Diperiksa";
			} 
			elseif ($data_pelanggan->status_pemeriksaan == 3) 
			{
					$status_pemeriksaan = "Instalasi Belum Siap";
			} 
			else
			{
					$status_pemeriksaan = "";
			}

			if($data_pelanggan->status_verifikasi == 1) 
            { 
					$status_verifikasi = "Sudah diverifikasi";
			} 
			elseif ($data_pelanggan->status_verifikasi == 2) 
			{
					$status_verifikasi = "Belum Diverifikasi";
			}
			else
			{
					$status_verifikasi = "";
			}

			if($data_pelanggan->status_sertifikasi == 1) 
            { 
					$status_sertifikasi = "Sudah Terbit";
			} 
			elseif ($data_pelanggan->status_sertifikasi == 2) 
			{
					$status_sertifikasi = "Belum Terbit";
			}
			else
			{
				$status_sertifikasi = "";
			}

			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='".base_url()."laporan/view_laporan_pelanggan/".$data_pelanggan->no_pendaftaran."'  title='View' target='_blank'><i class='icon-search4'></i></a>";
			}else
			{
				$button = "<a href='".base_url()."laporan/view_laporan_pelanggan/".$data_pelanggan->no_pendaftaran."' target='_blank' class='btn btn-primary'' title='View'><i class='icon-search4'></i></a>";
			}


			
            $row[] = $data_pelanggan->no_pendaftaran;
           // $row[] = "".date('d F Y H:i:s', strtotime($data_pelanggan->created_at))."";
            $row[] = $data_pelanggan->created_at;
            $row[] = $tipe_pelanggan;
            $row[] = "".$data_pelanggan->nm_tarif."/".$data_pelanggan->daya."";
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $sub_area;
            $row[] = $data_pelanggan->Name;
            $row[] = $data_pelanggan->alamat;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_btl;
            $row[] = $data_pelanggan->nm_ptl;
            $row[] = $data_pelanggan->metode_pendaftaran;
            $row[] = $status_pembayaran;
            $row[] = $status_pemeriksaan;
            $row[] = $status_verifikasi;
            $row[] = $status_sertifikasi;
            $row[] = $data_pelanggan->Name;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Laporan_Model->count_all($params),
                        "recordsFiltered" => $this->Laporan_Model->count_filtered($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}


	function laporan_pembayaran()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Laporan_Model->get_datatables_pembayaran($params);


        $data = array();
        //$dicetak_oleh = array();
        $no = $_POST['start'];

        /*
        foreach ($list as $value) {
				
				$dicetak_oleh[$value->no_pendaftaran] = $this->Pelanggan_Model->get_dicetak_oleh($value->dicetak_oleh);
			}

		*/

        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
           
            
            
			if($data_pelanggan->status_cetak == 1) 
			{
					$status_cetak = "Sudah dicetak ".$data_pelanggan->total_cetak."";
			} else {
					$status_cetak = "Belum dicetak";
			} 

			/*
			if(isset($dicetak_oleh[$data_pelanggan->no_pendaftaran]->Name)) 
			{ 
					$pencetak = $dicetak_oleh[$data_pelanggan->no_pendaftaran]->Name;
			}
			else
			{
				 $pencetak = "";
			}
			*/
			if($position_id == 1)
			{
				 $button = "<a href='".base_url()."laporan/view_laporan_pembayaran/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
			}else
			{
				$button = "<a href='".base_url()."laporan/view_laporan_pembayaran/".$data_pelanggan->no_pendaftaran."' class='btn btn-primary'' title='View'><i class='icon-search4'></i></a>";
			}


		
            $row[] = $data_pelanggan->no_kwitansi;
            $row[] = $data_pelanggan->no_pendaftaran;
            $row[] = $data_pelanggan->Nama;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $data_pelanggan->nm_sub_area;
            $row[] = $data_pelanggan->tgl_bayar;
            $row[] = $data_pelanggan->Name;
            $row[] = $status_cetak;
            $row[] = $data_pelanggan->nm_sub_area;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Laporan_Model->count_all_pembayaran($params),
                        "recordsFiltered" => count($data),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}


	function laporan_pemeriksaan()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Laporan_Model->get_datatables_pemeriksaan($params);

	 	
        $data = array();
        $dicetak_oleh = array();
        $no = $_POST['start'];

        foreach ($list as $value) {
				
				$dicetak_oleh[$value->no_pendaftaran] = $this->Pelanggan_Model->get_dicetak_oleh($value->dicetak_oleh);
			}


        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
           
            

			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}


			if($data_pelanggan->status_cetak == 1) 
			{
					$status_cetak = "Sudah dicetak ".$data_pelanggan->total_cetak."";
			} else {
					$status_cetak = "Belum dicetak";
			} 

			if(isset($dicetak_oleh[$data_pelanggan->no_pendaftaran]->Name)) 
			{ 
					$pencetak = $dicetak_oleh[$data_pelanggan->no_pendaftaran]->Name;
			}
			else
			{
				 $pencetak = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='".base_url()."laporan/view_laporan_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>

				 	<a href='".base_url()."laporan/view_map_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='Lokasi Pemeriksaan'><i class='icon-map5'></i></a>

				 	<a href='".base_url()."laporan/view_foto_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='Foto Pemeriksaan'><i class='icon-stack-picture'></i></a>

				 	";
			}else
			{
				$button = "<a href='".base_url()."laporan/view_laporan_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>

				 	<a href='".base_url()."laporan/view_map_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='Lokasi Pemeriksaan'><i class='icon-map5'></i></a>

				 	<a href='".base_url()."laporan/view_foto_pemeriksaan/".$data_pelanggan->no_pendaftaran."'  title='Foto Pemeriksaan'><i class='icon-stack-picture'></i></a>";
			}


		
            $row[] = $data_pelanggan->no_kwitansi;
            $row[] = $data_pelanggan->no_pendaftaran;
            $row[] = $data_pelanggan->Nama;
            $row[] = $data_pelanggan->alamat;
            $row[] = $data_pelanggan->daya;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $data_pelanggan->nm_sub_area;
            $row[] = $data_pelanggan->no_lhpp;
            $row[] = $data_pelanggan->tgl_lhpp;
            $row[] = "".$data_pelanggan->nm_pemeriksa1." ".$data_pelanggan->nm_pemeriksa2."";
            $row[] = $data_pelanggan->tgl_bayar;
            $row[] = $data_pelanggan->created_at;
            $row[] = "".$data_pelanggan->jumlah_pemeriksaan." Kali";
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Laporan_Model->count_all_pemeriksaan($params),
                        "recordsFiltered" => '65000',
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}


	function laporan_verifikasi()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Laporan_Model->get_datatables_verifikasi($params);

        $data = array();
        $no = $_POST['start'];

       
        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
           
          
			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			if($position_id == 1)
			{
				 $button = "<a href='".base_url()."laporan/view_laporan_verifikasi/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
			}else
			{
				$button = "<a href='".base_url()."laporan/view_laporan_verifikasi/".$data_pelanggan->no_pendaftaran."'  title='View'><i class='icon-search4'></i></a>";
			}


		
            $row[] = $data_pelanggan->no_pendaftaran;
            $row[] = $data_pelanggan->Nama;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $data_pelanggan->nm_sub_area;
            $row[] = $data_pelanggan->no_lhpp;
            $row[] = date('d F Y', strtotime($data_pelanggan->verifikasi_at));
            $row[] = $data_pelanggan->hasil_pemeriksaan;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Laporan_Model->count_all_verifikasi(),
                        "recordsFiltered" => $this->Laporan_Model->count_filtered_verifikasi($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function laporan_sertifikasi()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

	 	$list = $this->Laporan_Model->get_datatables_sertifikasi($params);
        $data = array();
        $no = $_POST['start'];

       	$data_sertifikat = array();

		foreach ($list as $key => $value) {
			$data_sertifikat[$value->no_pendaftaran] = $this->Sertifikasi_Model->data_sertifikasi_last($value->no_pendaftaran);
		}


        foreach ($list as $data_pelanggan) {
            $no++;
            $row = array();
            //$row[] = $no;
           
          	
			if(isset($data_pelanggan->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}

			
            $row[] = $data_pelanggan->no_pendaftaran;
            $row[] = $data_pelanggan->Nama;
            $row[] = $data_pelanggan->daya;
            $row[] = $data_pelanggan->harga;
            $row[] = $data_pelanggan->nm_kota;
            $row[] = $data_pelanggan->nm_kanwil;
            $row[] = $data_pelanggan->nm_area;
            $row[] = $data_pelanggan->nm_sub_area;
            $row[] = $data_pelanggan->no_lhpp;
            $row[] = $data_pelanggan->nm_btl;
            $row[] = $data_sertifikat[$data_pelanggan->no_pendaftaran]->no_seri;
            $row[] = date('d F Y H:i:s', strtotime($data_sertifikat[$data_pelanggan->no_pendaftaran]->dtcreate));
            $row[] = $data_sertifikat[$data_pelanggan->no_pendaftaran]->Name;
       
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Laporan_Model->count_all_sertifikasi(),
                        "recordsFiltered" => $this->Laporan_Model->count_filtered_sertifikasi($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function excel_pelanggan()
	{	 
		
		 $now = Date("Y-m-d H:i:s");
		// $fdk_info =$this->fdk_row_model->exportExcelAll($params);


		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		$data_pelanggan = $this->Pelanggan_Model->get_all($params);

		//$excel_pelanggan = array();


		foreach ($data_pelanggan as $key => $value) {
			
			if($value->tipe_pelanggan == 1) 
            { 
					$tipe_pelanggan = "Baru";
			} 
			elseif($value->tipe_pelanggan == 2) 
			{
					$tipe_pelanggan = "Rubah Daya";
			} 
			elseif($value->tipe_pelanggan == 3) 
			{
					$tipe_pelanggan = "Instalasi Lama";
			}

		
			$excel_pelanggan[] = array('no_pendaftaran' =>$value->no_pendaftaran,
									   'no_agenda' =>$value->no_agenda,
									   'nama' =>$value->Nama,
									   'alamat' =>$value->alamat,
									   'nm_kota' =>$value->nm_kota,
									   'nm_wilayah' =>$value->nm_kanwil,
									   'nm_area' =>$value->nm_area,
									   'nm_sub_area' =>$value->nm_sub_area,
									   'nm_btl' =>$value->nm_btl,
									   'nm_bangunan' =>$value->nm_bangunan,
									   'nm_tarif' =>$value->nm_tarif,
									   'daya' =>$value->daya,
									   'asosiasi' =>$value->nm_asosiasi,
									   'created_at' =>$value->created_at,
									   'tipe_pelanggan' =>$tipe_pelanggan,
									   'created_by' =>$value->Name,
									  );
			
		
		}

		
        if(isset($excel_pelanggan)){
            
             $writer = WriterFactory::create(Type::XLSX);
        //$writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
	        $writer->openToBrowser("PelangganExport".date("Y-m-d H:i:s").".xlsx");

	        $val = array("No Pendaftaran","No Agenda", "Nama","Alamat","Kota","Wilayah","Area","Sub Area","Nama BTL","Jenis Bangunan","Tarif","Daya","Asosiasi No Gambar","Tgl Daftar","Tipe","Diinputkan Oleh");

	        $writer->addRow($val); // add a row at a time

	        $writer->addRows($excel_pelanggan); // add multiple rows at a time

	        $writer->close();


        }else{
            redirect('Excel');
        }
	}

	function excel_pembayaran()
	{	 
		
		 $now = Date("Y-m-d H:i:s");
		// $fdk_info =$this->fdk_row_model->exportExcelAll($params);


		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		$data_pelanggan = $this->Pelanggan_Model->get_sudah_bayar($params);

		$dicetak_oleh = array();

			foreach ($data_pelanggan as $value) {
				
				$dicetak_oleh[$value->no_pendaftaran] = $this->Pelanggan_Model->get_dicetak_oleh($value->dicetak_oleh);
			}

		foreach ($data_pelanggan as $key => $value) {
		
			if(isset($value->nm_sub_area))
			{
				$sub_area = $data_pelanggan->nm_sub_area;
			}
			else
			{
				$sub_area = "";
			}


			if($value->status_cetak == 1) 
			{
					$status_cetak = "Sudah dicetak ".$value->total_cetak."";
			} else {
					$status_cetak = "Belum dicetak";
			} 

			if(isset($dicetak_oleh[$value->no_pendaftaran]->Name)) 
			{ 
					$pencetak = $dicetak_oleh[$value->no_pendaftaran]->Name;
			}
			else
			{
				 $pencetak = "";
			}

			$excel_pelanggan[] = array('no_kwitansi' =>$value->no_kwitansi,
									   'no_pendaftaran' =>$value->no_pendaftaran,
									   'nama' =>$value->Nama,
									   'nm_kota' =>$value->nm_kota,
									   'nm_wilayah' =>$value->nm_kanwil,
									   'nm_area' =>$value->nm_area,
									   'nm_sub_area' =>$value->nm_sub_area,
									   'tgl_bayar' =>$value->tgl_bayar,
									   'diterima_oleh' =>$value->Name,
									   'status_cetak' =>$status_cetak,
									   'print_by' =>$pencetak,
									  );
			
		
		}

		/*
		echo "<pre>";
		print_r($excel_pelanggan);
		echo "</pre>";
		exit();
		*/
        if(isset($excel_pelanggan)){
            
             $writer = WriterFactory::create(Type::XLSX);
        //$writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
	        $writer->openToBrowser("PembayaranExport".date("Y-m-d H:i:s").".xlsx");

	        $val = array("No Kwitansi","No Pendaftaran", "Nama","Kota","Wilayah","Area","Sub Area","Tanggal Pembayaran","Diterima Oleh","Cetak Kwitansi","Dicetak Oleh");

	        $writer->addRow($val); // add a row at a time

	        $writer->addRows($excel_pelanggan); // add multiple rows at a time

	        $writer->close();


        }else{
            redirect('Excel');
        }
	}


	function excel_pemeriksaan()
	{	 
		
		 $now = Date("Y-m-d H:i:s");
		// $fdk_info =$this->fdk_row_model->exportExcelAll($params);


		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		$data_pelanggan = $this->Pelanggan_Model->get_sudah_diperiksa($params);


		foreach ($data_pelanggan as $key => $value) {
		
			$excel_pelanggan[] = array('no_kwitansi' =>$value->no_kwitansi,
									   'no_pendaftaran' =>$value->no_pendaftaran,
									   'nama' =>$value->Nama,
									   'alamat' =>$value->alamat,
									   'daya' =>$value->daya,
									   'nm_kota' =>$value->nm_kota,
									   'nm_wilayah' =>$value->nm_kanwil,
									   'nm_area' =>$value->nm_area,
									   'nm_sub_area' =>$value->nm_sub_area,
									   'no_lhpp' =>$value->no_lhpp,
									   'tgl_lhpp' =>$value->tgl_lhpp,
									   'pemeriksa' => "".$value->nm_pemeriksa1." ".$value->nm_pemeriksa2."",
									   'tgl_bayar' =>$value->tgl_bayar,
									   'tgl_pemeriksaan' =>$value->created_at,
									   'jumlah_pemeriksaan' =>$value->jumlah_pemeriksaan,
									  );
			
		
		}

		/*
		echo "<pre>";
		print_r($excel_pelanggan);
		echo "</pre>";
		exit();
		*/
        if(isset($excel_pelanggan)){
            
             $writer = WriterFactory::create(Type::XLSX);
        //$writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
	        $writer->openToBrowser("PemeriksaanExport".date("Y-m-d H:i:s").".xlsx");

	        $val = array("No Kwitansi","No Pendaftaran", "Nama","Alamat","Daya","Kota","Wilayah","Area","Sub Area","No LHPP","Tgl LHPP","Pemeriksa","Tgl Bayar","Tgl Pemeriksaan","Jumlah pemeriksaan");

	        $writer->addRow($val); // add a row at a time

	        $writer->addRows($excel_pelanggan); // add multiple rows at a time

	        $writer->close();


        }else{
            redirect('Excel');
        }
	}

	function excel_verifikasi()
	{	 
		
		 $now = Date("Y-m-d H:i:s");
		// $fdk_info =$this->fdk_row_model->exportExcelAll($params);


		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		$data_pelanggan = $this->Pelanggan_Model->get_sudah_diverifikasi($params);


		foreach ($data_pelanggan as $key => $value) {
		
			$excel_pelanggan[] = array('no_pendaftaran' =>$value->no_pendaftaran,
									   'nm_kota' =>$value->nm_kota,
									   'nm_wilayah' =>$value->nm_kanwil,
									   'nm_area' =>$value->nm_area,
									   'nm_sub_area' =>$value->nm_sub_area,
									   'no_lhpp' =>$value->no_lhpp,
									   'tgl_verifikasi' =>$value->verifikasi_at,
									   'hasil_pemeriksaan' =>$value->hasil_pemeriksaan,
									  );
			
		
		}

		/*
		echo "<pre>";
		print_r($excel_pelanggan);
		echo "</pre>";
		exit();
		*/

        if(isset($excel_pelanggan)){
            
             $writer = WriterFactory::create(Type::XLSX);
        //$writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
	        $writer->openToBrowser("VerifikasiExport".date("Y-m-d H:i:s").".xlsx");

	        $val = array("No Pendaftaran","Kota","Wilayah","Area","Sub Area","No LHPP","Tgl Verifikasi","Hasil Pemeriksaan");

	        $writer->addRow($val); // add a row at a time

	        $writer->addRows($excel_pelanggan); // add multiple rows at a time

	        $writer->close();


        }else{
            redirect('Excel');
        }
	}

	function excel_sertifikasi()
	{	 
		
		 $now = Date("Y-m-d H:i:s");
		// $fdk_info =$this->fdk_row_model->exportExcelAll($params);


		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		
		$data_pelanggan = $this->Sertifikasi_Model->get_laporan_sertifikasi_sudah_terbit($params);

		$data_sertifikat = array();

		foreach ($data_pelanggan as $key => $value) {
			$data_sertifikat[$value->no_pendaftaran] = $this->Sertifikasi_Model->data_sertifikasi_last($value->no_pendaftaran);
		}

		foreach ($data_pelanggan as $key => $value) {

			$data_sertifikat_sebelumnya = $this->Sertifikasi_Model->data_sertifikasi_sebelumnya($value->no_pendaftaran,$data_sertifikat[$value->no_pendaftaran]->nomor_sertifikat);
			
			$output = array_map(function ($data_sertifikat_sebelumnya) { return $data_sertifikat_sebelumnya->nomor_sertifikat; }, $data_sertifikat_sebelumnya);

			$nomor_sertifikat_sebelumnya = implode(', ', $output);

			
			$dt = new DateTime($data_sertifikat[$value->no_pendaftaran]->dtcreate);
			$tgl_terbit = $dt->format('Y-m-d H:i:s');

			$masa_berlaku = date('Y-m-d H:i:s',strtotime('+15 years', strtotime($tgl_terbit)));

			$awal  = new DateTime($value->created_at);
			$akhir = $dt; // waktu sekarang
			$diff  = date_diff($awal, $akhir );
			
			if($diff->d > 3)
			{
				$status = "Lebih dari 3 hari";
			}
			else
			{
				$status = "Kurang dari 3 hari";
			}

			 if($value->tipe_pelanggan == 1) 
            { 
					$tipe_pelanggan = "Baru";
			} 
			elseif ($value->tipe_pelanggan == 2) 
			{
					$tipe_pelanggan = "Rubah Daya";
			} 
			elseif ($value->tipe_pelanggan == 3) 
			{
					$tipe_pelanggan = "Instalasi Lama";
			}
			else
			{
				$tipe_pelanggan = "";
			}

			$excel_pelanggan[] = array('nomor_sertifikat' => $data_sertifikat[$value->no_pendaftaran]->nomor_sertifikat,
									   'no_seri' => $data_sertifikat[$value->no_pendaftaran]->no_seri,
									   'no_pendaftaran' => $value->no_pendaftaran,
									   'tgl_bayar' => $value->tgl_bayar,
									   'no_gambar' => $value->no_gambar,
									   'no_sip' => $value->no_sip,
									   'nm_asosiasi' => $value->nm_asosiasi,
									   'no_lhpp' => $value->no_lhpp,
									   'tipe_pelanggan' => $tipe_pelanggan,
									   'metode_pendaftaran' => $value->metode_pendaftaran,
									   'nm_tarif' => $value->nm_tarif,
									   'daya' => $value->daya,
									   'harga' => $value->harga,
									   'nm_wilayah' => $value->nm_kanwil,
									   'nm_area' => $value->nm_area,
									   'nm_sub_area' => $value->nm_sub_area,
									   'nama' => $value->Nama,
									   'alamat' => $value->alamat,
									   'nm_btl' => $value->nm_btl,
									   'nm_ptl' => $value->nm_ptl,
									   'dtcreate' => $data_sertifikat[$value->no_pendaftaran]->dtcreate,
									   'durasi_cetak' => "".$diff->d." Hari ".$diff->h." Jam ".$diff->m." Menit",
									   'status' => $status,
									   'alasan_lebih_tiga_hari' =>'',
									   'dicetak_oleh' => $data_sertifikat[$value->no_pendaftaran]->Name,
									   'nm_bangunan' => $value->nm_bangunan,
									   'no_agenda' => $value->no_agenda,
									   'telp' => $value->telp,
									   'tgl_siap_periksa' =>$value->tgl_lhpp,
									   'nilai' => '1',
									   'sumber' => $value->metode_pendaftaran,
									   'no_seri_sebelumnya' => $nomor_sertifikat_sebelumnya,
									  );
			
		
		}

		/*
		echo "<pre>";
		print_r($excel_pelanggan);
		echo "</pre>";
		exit();
		*/
		

        if(isset($excel_pelanggan)){
            
          //   $writer = WriterFactory::create(Type::XLSX);
        $writer = WriterFactory::create(Type::CSV); // for CSV files
        //$writer = WriterFactory::create(Type::ODS); // for ODS files

        //stream to browser
	        $writer->openToBrowser("SertifikasiExport".date("Y-m-d H:i:s").".xlsx");

	        $val = array("Nomor Sertifikat","No Seri","Nomor Pendaftaran","Tanggal Bayar","No Gambar","No Sip","Asosiasi","Nomor LHPP","Tipe Pelanggan","Jenis Konsumen","Tarif","Daya","Harga","Wilayah","Area","Sub Area","Nama Pelanggan","Alamat","BTL","PTL","Tanggal Cetak","Durasi Cetak","Status","Alasan Lebih 3 Hari","Dicetak Oleh","Jenis Bangunan","No Registrasi DJK","telp","Tanggal Siap Periksa","Nilai","Sumber","No Seri Sebelumnya");

	        $writer->addRow($val); // add a row at a time

	        $writer->addRows($excel_pelanggan); // add multiple rows at a time

	        $writer->close();


        }else{
            redirect('Excel');
        }
	}

	function view_laporan_pelanggan()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);
		$data['detail_pembayaran'] = $this->Laporan_Model->get_detail_pembayaran($no_pendaftaran);

		$params = array_filter(array(
            'kode_area' => $data['detail_pelanggan']->kode_area));

		
		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);

		$data['detail_verifikasi'] = $this->Laporan_Model->get_detail_verifikasi($no_pendaftaran);
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		
		$this->load->view('view_laporan_pelanggan',$data);
	}

	function view_laporan_pembayaran()
	{
		$no_pendaftaran = $this->uri->segment(3);
		
		$data['detail_pembayaran'] = $this->Laporan_Model->get_detail_pembayaran($no_pendaftaran);
		$this->load->view('view_laporan_pembayaran',$data);
	}

	function view_laporan_pemeriksaan()
	{
		$no_pendaftaran = $this->uri->segment(3);
		
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		$this->load->view('view_laporan_pemeriksaan',$data);
	}

	function view_map_pemeriksaan()
	{
		$no_pendaftaran = $this->uri->segment(3);
		
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		$this->load->view('view_map_pemeriksaan',$data);
	}

	function view_foto_pemeriksaan()
	{
		$no_pendaftaran = $this->uri->segment(3);
		
		$data['detail_pemeriksaan'] = $this->Laporan_Model->get_detail_pemeriksaan($no_pendaftaran);
		$this->load->view('view_foto_pemeriksaan',$data);
	}

	function view_laporan_verifikasi()
	{
		$no_pendaftaran = $this->uri->segment(3);
		$data['detail_pelanggan'] = $this->Pelanggan_Model->get_detail_pelanggan($no_pendaftaran);

		$params = array_filter(array(
            'kode_area' => $data['detail_pelanggan']->kode_area));

		
		$data['verifikator'] = $this->Verifikasi_Model->get_verifikator($params);
		$data['detail_verifikasi'] = $this->Laporan_Model->get_detail_verifikasi($no_pendaftaran);
		$this->load->view('view_laporan_verifikasi',$data);
	}

}
