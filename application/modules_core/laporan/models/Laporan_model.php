<?php

class Laporan_model extends CI_Model {

    /* Pelanggan */
    var $column_order = array('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe_pelanggan','nm_tarif','nm_kanwil','nm_area','nm_sub_area','Nama','alamat','nm_kota','nm_btl','nm_ptl','metode_pendaftaran','status_pembayaran','status_pemeriksaan','status_verifikasi','status_sertifikasi','Name'); //set column field database for datatable orderable
    var $column_search = array('pelanggan.no_pendaftaran','pelanggan.created_at','pelanggan.tipe_pelanggan','nm_tarif','nm_kanwil','nm_area','nm_sub_area','Nama','alamat','nm_kota','nm_btl','nm_ptl','metode_pendaftaran','status_pembayaran','status_pemeriksaan','status_verifikasi','status_sertifikasi','Name'); //set column field database for datatable searchable 
    var $order = array('pelanggan.created_at' => 'desc'); // default order 


    /* Pembayaran */

    var $column_order_pembayaran = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','tgl_bayar','Name','diterima_oleh','dicetak_oleh'); //set column field database for datatable orderable
    var $column_search_pembayaran = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','tgl_bayar','Name','diterima_oleh','dicetak_oleh'); //set column field database for datatable searchable 
    var $order_pembayaran = array('pelanggan.created_at' => 'desc'); // default order

    /* pemeriksaan */

    var $column_order_pemeriksaan = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat','daya','nm_kota','nm_kanwil','nm_area','nm_sub_area','pemeriksaan.no_lhpp','tgl_lhpp','pemeriksa1','tgl_bayar','pemeriksaan.created_at','jumlah_pemeriksaan'); //set column field database for datatable orderable
    var $column_search_pemeriksaan = array('no_kwitansi','pelanggan.no_pendaftaran','Nama','alamat','daya','nm_kota','nm_kanwil','nm_area','nm_sub_area','pemeriksaan.no_lhpp','tgl_lhpp','pemeriksa1','tgl_bayar','pemeriksaan.created_at','jumlah_pemeriksaan'); //set column field database for datatable searchable 
    var $order_pemeriksaan = array('no_kwitansi' => 'asc'); // default order


    /* verifikasi */

    var $column_order_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','pemeriksaan.no_lhpp','verifikasi_at','hasil_pemeriksaan'); //set column field database for datatable orderable
    var $column_search_verifikasi = array('pelanggan.no_pendaftaran','Nama','nm_kota','nm_kanwil','nm_area','nm_sub_area','pemeriksaan.no_lhpp','verifikasi_at','hasil_pemeriksaan'); //set column field database for datatable searchable 
    var $order_verifikasi = array('verifikasi.no_pendaftaran' => 'asc'); // default order


     /* verifikasi */

    var $column_order_sertifikasi = array('pelanggan.no_pendaftaran','Nama','daya','harga','nm_kota','nm_kanwil','nm_area','nm_sub_area','verifikasi.no_lhpp','nm_btl','sertifikasi.no_seri'); //set column field database for datatable orderable
    var $column_search_sertifikasi = array('pelanggan.no_pendaftaran','Nama','daya','harga','nm_kota','nm_kanwil','nm_area','nm_sub_area','verifikasi.no_lhpp','nm_btl','sertifikasi.no_seri'); //set column field database for datatable searchable 
    var $order_sertifikasi = array('pelanggan.no_pendaftaran' => 'asc'); // default order

   function get_all($params){
		$this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_sub_area.kode_area=kantor_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');

        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi');
        $this->db->join('core_user','pelanggan.created_by=core_user.coreUserId','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $this->db->order_by('pelanggan.created_at','desc');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
      //  return $this->db->last_query();
	}

    private function _get_datatables_query($params)
     {
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        /*
        $this->db->join('kantor_sub_area','pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        */
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('core_user','pelanggan.created_by=core_user.coreUserId','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    private function _get_datatables_query_pembayaran($params)
     {
        $this->db->select('*');
        $this->db->from('pelanggan');
        
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('core_user','pembayaran.diterima_oleh=core_user.coreUserId','left');

        if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_pembayaran as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_pembayaran) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_pembayaran[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_pembayaran))
        {
            $order = $this->order_pembayaran;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


     private function _get_datatables_query_pemeriksaan($params)
     {
       $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_pemeriksaan as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_pemeriksaan) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_pemeriksaan[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_pemeriksaan))
        {
            $order = $this->order_pemeriksaan;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_query_verifikasi($params)
     {
       $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.id_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.id_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
         $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');

          if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }

        $i = 0;
     
        foreach ($this->column_search_verifikasi as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_verifikasi) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_verifikasi[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_verifikasi))
        {
            $order = $this->order_verifikasi;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    private function _get_datatables_query_sertifikasi($params)
     {
         $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('verifikasi','pelanggan.no_pendaftaran=verifikasi.no_pendaftaran');
        $this->db->join('sertifikasi','pelanggan.no_pendaftaran=sertifikasi.no_pendaftaran');
        $this->db->join('core_user','sertifikasi.user_id=core_user.coreUserId');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area AND pelanggan.kode_sub_area=kantor_sub_area.kode_sub_area','left');

        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi');

         if(isset($params['id_departemen']) && $params['id_departemen'] == 12)
        {
            
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }

         if(isset($params['id_departemen']) && $params['id_departemen'] == 11)
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        
        }

        if(isset($params['id_departemen']) && $params['id_departemen'] == '9')
        {
            if(isset($params['kode_sub_area']))
            {
                $this->db->where('kantor_sub_area.kode_sub_area', $params['kode_sub_area']);
            }else
            {
                 $this->db->where('kantor_sub_area.kode_sub_area', '0');
            }
            
        
        }

        if(isset($params['position_id']) && $params['position_id'] == '5')
        {
             if(isset($params['kode_kanwil']))
            {
                $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);
            }else
            {
                 $this->db->where('kantor_wilayah.kode_kanwil', '0');
            }
            
        
        }


        if(isset($params['position_id']) && $params['position_id'] == '4')
        {
            
            if(isset($params['kode_area']))
            {
                $this->db->where('kantor_area.kode_area', $params['kode_area']);
            }else
            {
                 $this->db->where('kantor_area.kode_area', '0');
            }
            
        }
        
        $this->db->where('status_pembayaran =', 1);
        $this->db->where('status_pemeriksaan =', 1);
        $this->db->where('status_verifikasi =', 1);
        $this->db->where('status_sertifikasi =', 1);

       
        
        //$this->db->where('tgl_terbit BETWEEN "'. date('Y-m-d', strtotime('2018-06-01')). '" and "'. date('Y-m-d', strtotime('2018-06-28')).'"');

        $i = 0;
     
        foreach ($this->column_search_sertifikasi as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_sertifikasi) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
    
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_sertifikasi[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order_sertifikasi))
        {
            $order = $this->order_sertifikasi;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if($this->input->post('no_pendaftaran'))
        {
            $this->db->like('pelanggan.no_pendaftaran', $this->input->post('no_pendaftaran'));
        }

       
        if($this->input->post('tgl_terbit_from') && $this->input->post('tgl_terbit_to'))
        {
           // $this->db->like('tgl_terbit', $this->input->post('tgl_terbit_from'));
            $this->db->where('tgl_terbit BETWEEN "'. date('Y-m-d', strtotime($this->input->post('tgl_terbit_from'))). '" and "'. date('Y-m-d', strtotime($this->input->post('tgl_terbit_to'))).'"');
            $this->db->order_by('tgl_terbit');
        }

        $this->db->group_by('sertifikasi.no_pendaftaran');

    }

    function get_datatables($params)
    {
        $this->_get_datatables_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
        
    }
    
    function count_filtered($params)
    {
        $this->_get_datatables_query($params);
        return $this->db->count_all_results();
    }
 
    public function count_all($params)
    {
        return $this->db->count_all("pelanggan");
    }


    function get_datatables_pembayaran($params)
    {
        $this->_get_datatables_query_pembayaran($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        // $this->db->limit('5', '1');
        $query = $this->db->get();
        return $query->result();
       // return $this->db->last_query();
    }


    function count_filtered_pembayaran($params)
    {
        $this->_get_datatables_query_pembayaran($params);
        return $this->db->count_all_results();
        //return $this->db->last_query();
    }
 
    public function count_all_pembayaran($params)
    {
        return $this->db->count_all("pembayaran");
    }



    function get_datatables_pemeriksaan($params)
    {
        $this->_get_datatables_query_pemeriksaan($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered_pemeriksaan($params)
    {
        /*
        $this->_get_datatables_query_pemeriksaan($params);
        $this->db->count_all_results();
        */
        
        return $this->db->count_all("pemeriksaan");
       // return $this->db->last_query();
    }
 
    public function count_all_pemeriksaan()
    {
        return $this->db->count_all("pemeriksaan");
    }

    function get_datatables_verifikasi($params)
    {
        $this->_get_datatables_query_verifikasi($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered_verifikasi($params)
    {
        $this->_get_datatables_query_verifikasi($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_verifikasi()
    {
        $this->db->select('*');
        $this->db->from('verifikasi');
        return $this->db->count_all_results();
    }


    function get_datatables_sertifikasi($params)
    {
        $this->_get_datatables_query_sertifikasi($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }


    function count_filtered_sertifikasi($params)
    {
        $this->_get_datatables_query_sertifikasi($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_sertifikasi()
    {
        $this->db->select('*');
        $this->db->from('sertifikasi');
        return $this->db->count_all_results();
    }

  
    function get_detail_pembayaran($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pembayaran.no_pendaftaran=pelanggan.no_pendaftaran');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('provinsi','provinsi.id_provinsi=kota.id_provinsi');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
        $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pembayaran.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_verifikasi($no_pendaftaran){
        $this->db->select('*');
        $this->db->from('verifikasi');
        $this->db->join('pelanggan','verifikasi.no_pendaftaran=pelanggan.no_pendaftaran');
        $this->db->where('pelanggan.no_pendaftaran',$no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
    }

    function get_detail_pemeriksaan($no_pendaftaran)
    {
        $this->db->select('*,pelanggan.no_pendaftaran as no_pendaftaran_pelanggan,pemeriksa.nm_pemeriksa as nm_pemeriksa1,pemeriksa2.nm_pemeriksa as nm_pemeriksa2');
        $this->db->from('pelanggan');
        $this->db->join('pembayaran','pelanggan.no_pendaftaran=pembayaran.no_pendaftaran');
        $this->db->join('pemeriksaan','pelanggan.no_pendaftaran=pemeriksaan.no_pendaftaran');
        $this->db->join('pemeriksa','pemeriksa.nip_pemeriksa=pemeriksaan.pemeriksa1','left');
        $this->db->join('pemeriksa as pemeriksa2','pemeriksa2.nip_pemeriksa=pemeriksaan.pemeriksa2','left');
        $this->db->join('kota','pelanggan.kd_kota=kota.id_kota');
        $this->db->join('tarif','pelanggan.id_tarif=tarif.id_tarif','left');
        $this->db->join('daya','pelanggan.id_daya=daya.id_daya','left');
        $this->db->join('biro_teknik_listrik','pelanggan.id_btl=biro_teknik_listrik.btl_id','left');
        $this->db->join('bangunan','pelanggan.id_bangunan=bangunan.id_bangunan','left');
        $this->db->join('penyedia_listrik','pelanggan.id_ptl=penyedia_listrik.id_ptl','left');
         $this->db->join('kantor_area','pelanggan.kd_area=kantor_area.kode_area','left');
        $this->db->join('kantor_sub_area','kantor_area.kode_area=kantor_sub_area.kode_area','left');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil','left');
        $this->db->join('asosiasi','pelanggan.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->where('pelanggan.no_pendaftaran =', $no_pendaftaran);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
    }

}

