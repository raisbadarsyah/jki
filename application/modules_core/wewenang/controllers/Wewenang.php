<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wewenang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Management_Position/management_position_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		
	    $config['item_list'] = $this->management_position_model->get_all();
	    
       
		$this->load->view('index', $config);
	}

	function add()
	{	
		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_kanwil = $this->input->post('kode_kanwil');
			$nm_wilayah = $this->input->post('nm_wilayah');
			$id_provinsi = $this->input->post('id_provinsi');
			$general_manager = $this->input->post('general_manager');


			try {
				
				$data_kanwil = array(
					"kode_kanwil"=> $kode_kanwil,
					"nm_kanwil"=> $nm_wilayah,
					"general_manager"=> $general_manager,
				);
				
				$insert_kanwil = $this->Kanwil_Model->insert($data_kanwil);

				foreach($id_provinsi as $provinsi){

					$cakupan_kanwil = array(
						"id_provinsi" 		=> $provinsi,
						"kode_kanwil" 		=> $kode_kanwil
					);

					$cakupan_kanwil = $this->Kanwil_Model->insert_cakupan_kanwil($cakupan_kanwil);
				   
				}

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_kanwil == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('kanwil','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_kanwil = $this->uri->segment(3);

		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_by_id($kode_kanwil);

		foreach ($data['provinsi'] as $key => $value) {
			$cakupan_provinsi[$value->id_provinsi][$kode_kanwil] = $this->Kanwil_Model->detail_cakupan_provinsi($kode_kanwil,$value->id_provinsi);

		}

		$data['cakupan_provinsi'] = $cakupan_provinsi;


		$this->load->view('edit',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_provinsi = $this->input->post('id_provinsi');
			$nm_provinsi = $this->input->post('nm_provinsi');
			$kode_djk = $this->input->post('kode_djk');


				try {
					
					$data_provinsi = array(
						"nm_provinsi"=> $nm_provinsi,
						"kd_djk"=> $kode_djk
					);
					
					$update = $this->Provinsi_Model->update($data_provinsi,$id_provinsi);


				} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal input provinsi');

				}


			if($update == 1) 
			{
							$this->session->set_flashdata('success', 'Sukses ubah data');
							redirect('provinsi','refresh');
			}


		}

	}



}
