<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departemen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Departemen_Model','Provinsi/Provinsi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$config['title'] = 'Guide Book';
		$config['page_title'] = 'Guide Book';
		$config['page_subtitle'] = 'Guide Book';
		$data['departemen'] = $this->Departemen_Model->get_all();

		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$this->load->view('add');

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kode_departemen = $this->input->post('kode_departemen');
			$nm_departemen = $this->input->post('nm_departemen');
		
			try {
				
				$data_departemen = array(
					"kode_departemen"=> $kode_departemen,
					"nm_departemen"=> $nm_departemen,
				);
				
				$insert_departemen = $this->Departemen_Model->insert($data_departemen);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($insert_departemen == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('departemen','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_departemen = $this->uri->segment(3);
		$data_departemen = $this->Departemen_Model->get_by_id($id_departemen);
		$data['data_departemen'] = $data_departemen;

		$this->load->view('edit',$data);


	}

	function delete()
	{
		$id_departemen = $this->uri->segment(3);
		$delete_departemen = $this->Departemen_Model->delete($id_departemen);

		if($delete_departemen == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('departemen','refresh');
				}
	}

	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_departemen = $this->input->post('id_departemen');
			$kode_departemen = $this->input->post('kode_departemen');
			$nm_departemen = $this->input->post('nm_departemen');
		
			try {
				
				$data_departemen = array(
					"kode_departemen"=> $kode_departemen,
					"nm_departemen"=> $nm_departemen,
				);
				
				$update_departemen = $this->Departemen_Model->update($data_departemen,$id_departemen);


			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input provinsi');

			}


				if($update_departemen == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('departemen','refresh');
				}


		}
	}

}
