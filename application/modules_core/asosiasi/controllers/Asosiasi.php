<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asosiasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Asosiasi_Model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		
		$data['asosiasi'] = $this->Asosiasi_Model->get_all();
		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$this->load->view('add');

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$jenis_bangunan = $this->input->post('jenis_bangunan');
			$tampil = $this->input->post('tampil');
			
			try {
				
				$data_bangunan = array(
					"nm_bangunan"=> $jenis_bangunan,
					"Active"=> $tampil
				);
				
				$insert_bangunan = $this->Bangunan_Model->insert($data_bangunan);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($insert_bangunan == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('bangunan','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$kode_kanwil = $this->uri->segment(3);

		$data['provinsi'] = $this->Provinsi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_by_id($kode_kanwil);

		foreach ($data['provinsi'] as $key => $value) {
			$cakupan_provinsi[$value->id_provinsi][$kode_kanwil] = $this->Kanwil_Model->detail_cakupan_provinsi($kode_kanwil,$value->id_provinsi);

		}

		$data['cakupan_provinsi'] = $cakupan_provinsi;


		$this->load->view('edit',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_provinsi = $this->input->post('id_provinsi');
			$nm_provinsi = $this->input->post('nm_provinsi');
			$kode_djk = $this->input->post('kode_djk');


				try {
					
					$data_provinsi = array(
						"nm_provinsi"=> $nm_provinsi,
						"kd_djk"=> $kode_djk
					);
					
					$update = $this->Provinsi_Model->update($data_provinsi,$id_provinsi);


				} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal input provinsi');

				}


			if($update == 1) 
			{
							$this->session->set_flashdata('success', 'Sukses ubah data');
							redirect('provinsi','refresh');
			}


		}

	}



}
