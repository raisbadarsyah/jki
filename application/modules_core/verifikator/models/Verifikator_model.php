<?php

class Verifikator_model extends CI_Model {

   function get_all($params){
		$this->db->select('*');
        $this->db->from('verifikator');
        $this->db->join('kantor_area','verifikator.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
  
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                
                 $this->db->where('verifikator.kode_area', $params['kode_area']);
            }
        }

        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_fasa(){
        $this->db->select('*');
        $this->db->from('fasa');
       
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($id_verifikator){
		$this->db->select('*');
        $this->db->from('verifikator');
        $this->db->join('kantor_area','verifikator.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');
        $this->db->where('id_verifikator',$id_verifikator);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_verifikator) {
       $query = $this->db->insert('verifikator', $data_verifikator);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update_verifikator($data, $id_verifikator){
        $this->db->trans_begin();
		$this->db->where('id_verifikator', $id_verifikator);
		$this->db->update('verifikator', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

}

