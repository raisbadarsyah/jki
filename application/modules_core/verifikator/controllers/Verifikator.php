<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikator extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Verifikator_model','Provinsi/provinsi_model','Kanwil/Kanwil_model','Kantor_Sub_Area/Kantor_Sub_Area_Model','Staff/Staff_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);


		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$data['verifikator'] = $this->Verifikator_model->get_all($params);
		$data['position_id'] = $position_id;

		$this->load->view('index',$data);
	}

	function add()
	{	
		
		$data['kanwil'] = $this->Kanwil_model->get_all();
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			
			$nm_verifikator = $this->input->post('nm_verifikator');
			$nip_verifikator = $this->input->post('nip_verifikator');
			$area = $this->input->post('area');
			$status = $this->input->post('status');
			
			try {
				
				$data_verifikator = array(
					"kode_area"=> $area,
					"nm_verifikator"=> $nm_verifikator,
					"nip_verifikator"=> $nip_verifikator,
					"status"=> $status
				);
				
				$insert_verifikator = $this->Verifikator_model->insert($data_verifikator);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pemeriksa');

			}


				if($insert_verifikator == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('verifikator','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$id_verifikator = $this->uri->segment(3);

		$data['kanwil'] = $this->Kanwil_model->get_all();
		$data['detail_verifikator'] = $this->Verifikator_model->get_by_id($id_verifikator);

		$data['area'] = $this->Kantor_Sub_Area_Model->get_area($data['detail_verifikator']->kode_kanwil);

		$this->load->view('edit',$data);
	}


	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$id_verifikator = $this->input->post('id_verifikator');
			$nm_verifikator = $this->input->post('nm_verifikator');
			$nip_verifikator = $this->input->post('nip_verifikator');
			$area = $this->input->post('area');
			$status = $this->input->post('status');
			
			try {
				
				$data_verifikator = array(
					"kode_area"=> $area,
					"nm_verifikator"=> $nm_verifikator,
					"nip_verifikator"=> $nip_verifikator,
					"status"=> $status
				);
				
				$update_verifikator = $this->Verifikator_model->update_verifikator($data_verifikator,$id_verifikator);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input pemeriksa');

			}


				if($update_verifikator == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('verifikator','refresh');
				}



		}
	}

}
