<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT. JKI</title>

	
	<!-- Global stylesheets -->

	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/datatables_responsive.js"></script>

	<!-- /theme JS files -->

	<script>
	function dodelete()
	{
	    job=confirm("Apakah anda yakin menghapus permanen user ini? approval data terkait user ini akan ikut terhapus");
	    if(job!=true)
	    {
	        return false;
	    }
	}
	</script>
	<style type="text/css">
	
	.grid-view-loading
	{
		background:url(loading.gif) no-repeat;
	}

	.grid-view
	{
		padding: 40px 0;
	}

	.datatable-header>div:first-child, .datatable-footer>div:first-child {
    margin-left: 20px;
	}

	.dataTables_length {
    	float: right;
	    display: inline-block;
	    margin: 0 0 20px 20px;
	    margin-top: 0px;
	    margin-right: 20px;
	    margin-bottom: 20px;
	    margin-left: 20px;
	}

	.grid-view table.display
	{
		background: white;
		border-collapse: collapse;
		width: 100%;
		border: 1px #D0E3EF solid;
	}

	.grid-view table.display th, .grid-view table.display td
	{
		font-size: 0.9em;
		border: 1px white solid;
		padding: 0.3em;
	}

	.grid-view table.display th
	{
		color: white;
		background: url("<?php echo base_url();?>template/cleandream/gridview/bg.gif") repeat-x scroll left top white;
		text-align: center;
	}
	.grid-view table.items th a
	{
		color: #EEE;
		font-weight: bold;
		text-decoration: none;
	}

	.grid-view table.items th a:hover
	{
		color: #FFF;
	}

	.grid-view table.items th a.asc
	{
		background:url(up.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items th a.desc
	{
		background:url(down.gif) right center no-repeat;
		padding-right: 10px;
	}

	.grid-view table.items tr.even
	{
		background: #F8F8F8;
	}

	.grid-view table.items tr.odd
	{
		background: #E5F1F4;
	}

	.grid-view table.items tr.selected
	{
		background: #BCE774;
	}

	.grid-view table.items tr:hover
	{
		background: #ECFBD4;
	}



</style>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

<!-- Theme JS files -->
<!-- Core JS files -->
	
	
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			 <?php $this->load->view('template/sidebar'); ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url().'dashboard'; ?>"><i class="icon-home2 position-left"></i>Admin</a></li>
							<li><a>Setting</a></li>
							<li class="active">Bangunan</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Biro Teknik Listrik</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
						<?php if ($this->session->flashdata('error') == TRUE): ?>
                <div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('success') == TRUE): ?>
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php endif; ?>

				<?php if($position_id==1) { ?>
  				<a href="<?php echo base_url(); ?>btl/add"><button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Add</button></a>

  				<a href='#popUp' id='add' data-toggle='modal' ><button class="btn btn-primary"> Referensi BTL DJK</button></a>
				<?php } ?>
				
						</div>
						<div id="pelanggan-grid" class="grid-view">
						 <table id="btl" class="table display items" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th>ID</th>						
									<th>Kodefikasi</th>
									<th>Nama BTL</th>				
									<th>Wilayah</th>
									<th>Area</th>
									<th>Alamat</th>						
									<th>Nama Direktur</th>
									<th>Nama Ahli Teknik</th>
									<th>Asosiasi</th>						
									<th>Status</th>
									<th width="15%" align="center">Action</th>
								</tr>
				  			</thead>
						</table>
					</div>
					</div>
					<!-- /basic datatable -->


					<!-- Pagination types -->
					
					<!-- /pagination types -->


					<!-- State saving -->
					
					<!-- /state saving -->


					<!-- Scrollable datatable -->
					
					<!-- /scrollable datatable -->


					<!-- Footer -->
					<?php $this->load->view('template/footer'); ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

<!-- Horizontal form modal -->
					<div class="modal fade" id="popUp">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header bg-primary">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title">Cari Data BTL DJK</h5>
								</div>

								<form action="<?php echo site_url();?>btl/btl_djk" class="form-horizontal" method="post">
									<div class="modal-body">
										
										<div class="form-group">
											<label class="control-label col-sm-3">Silahkan Pilih kota</label>
											<div class="col-sm-9">
												<select class="form-control select" name="kota" id="kota">
													
													<?php foreach($kota as $row){?>
													<option value=<?php echo $row->kode_djp_kota;?>><?php echo $row->nm_kota;?></option>
												
													<?php } ?>
												</select>
												
											</div>
										</div>
										
									
	
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
										
										<button type="submit" class="btn btn-primary" >Submit</button>

									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- /horizontal form modal -->
<script type="text/javascript">
 
var btl;
 
$(document).ready(function() {
 
    //datatables
   
     btl = $('#btl').DataTable({ 
 		
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 	
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('btl/querypaging')?>",
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [

        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
            "autoWidth": false,

        },
        ],
 
    });
 
});

</script>

</body>
</html>
