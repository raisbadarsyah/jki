<?php

class Btl_model extends CI_Model {

    var $column_order = array('btl_id','kodefikasi','nm_btl','nm_kanwil','nm_area','alamat_btl','nm_direktur','nm_ahli_teknik','nm_asosiasi','status_btl',); //set column field database for datatable orderable
    var $column_search = array('btl_id','kodefikasi','nm_btl','nm_kanwil','nm_area','alamat_btl','nm_direktur','nm_ahli_teknik','nm_asosiasi','biro_teknik_listrik.status'); //set column field database for datatable 
    var $order = array('btl_id' => 'desc'); // default order 

   function get_all($params){
		$this->db->select('*,biro_teknik_listrik.status as status_btl');
        $this->db->from('biro_teknik_listrik');
        $this->db->join('asosiasi','biro_teknik_listrik.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
            
             if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0)
                    {    
                    
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);

                    }

            /*
            if (isset($params['kode_area']) and $params['kode_area'] !=0)
            {
                
                 $this->db->where('biro_teknik_listrik.kode_area', $params['kode_area']);
            }
            */
        }


        $query = $this->db->get();
       
        return $query->result();
      
	}

     private function _get_datatables_query($params)
     {
        $this->db->select('*,biro_teknik_listrik.status as status_btl');
        $this->db->from('biro_teknik_listrik');
        $this->db->join('asosiasi','biro_teknik_listrik.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
            
             if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0)
                    {    
                    
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);

                    }

        }

        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($params)
    {
        $this->_get_datatables_query($params);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
       // $this->db->limit('10', '1');
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($params)
    {
        $this->_get_datatables_query($params);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($params)
    {
        $this->db->select('*,biro_teknik_listrik.status as status_btl');
        $this->db->from('biro_teknik_listrik');
        $this->db->join('asosiasi','biro_teknik_listrik.id_asosiasi=asosiasi.id_asosiasi','left');
        $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_area.kode_kanwil=kantor_wilayah.kode_kanwil');

        if (isset($params['position_id']) and $params['position_id'] !=1)
        {    
            
             if (isset($params['kode_kanwil']) and $params['kode_kanwil'] !=0)
                    {    
                    
                         $this->db->where('kantor_wilayah.kode_kanwil', $params['kode_kanwil']);

                    }
        }
        return $this->db->count_all_results();
    }

    function get_btl_by_id($btl_id){
        $this->db->select('*,biro_teknik_listrik.status as status');
        $this->db->from('biro_teknik_listrik');
        $this->db->join('kantor_area','biro_teknik_listrik.kode_area=kantor_area.kode_area');
        $this->db->join('kantor_wilayah','kantor_wilayah.kode_kanwil=kantor_area.kode_kanwil');
        $this->db->where('btl_id',$btl_id);
        $query = $this->db->get();
       
        return $query->row();
      
    }

    function get_fasa(){
        $this->db->select('*');
        $this->db->from('fasa');
       
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

    function cakupan_provinsi($kode_kanwil){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $query = $this->db->get();
       
        return $query->result();
       
    }

    function detail_cakupan_provinsi($kode_kanwil,$id_provinsi){
        $this->db->select('*');
        $this->db->from('cakupan_kanwil');
        $this->db->join('provinsi','cakupan_kanwil.id_provinsi=provinsi.id_provinsi');
        $this->db->where('kode_kanwil',$kode_kanwil);
        $this->db->where('cakupan_kanwil.id_provinsi',$id_provinsi);
        $query = $this->db->get();
       
        return $query->row();
       
    }

	function get_by_id($kode_kanwil){
		$this->db->select('*');
        $this->db->from('kantor_wilayah');
        $this->db->where('kode_kanwil', $kode_kanwil);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->row();
       // return $this->db->last_query();
	}

	function insert($data_btl) {
       $query = $this->db->insert('biro_teknik_listrik', $data_btl);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function insert_cakupan_kanwil($cakupan_kanwil) {
       $query = $this->db->insert('cakupan_kanwil', $cakupan_kanwil);
        if ($query) { 
            $a = 1; 
        } 
        else { 
            $a = 0; 
        } 

        return $a;
    }

    function update($data, $btl_id){
        $this->db->trans_begin();
		$this->db->where('btl_id', $btl_id);
		$this->db->update('biro_teknik_listrik', $data);
        
        if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}		
    }

}

