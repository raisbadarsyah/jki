<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Btl extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('is_login')){
			redirect('auth?location='.urlencode($_SERVER['REQUEST_URI']));
		}
		$this->load->model(array('menu_model','Btl_Model','Asosiasi/Asosiasi_Model','Kanwil/Kanwil_Model','Kantor_area/Kantor_area_model','Kota/Kota_Model','Staff/Staff_model'));
		$this->load->helper('check_auth_menu');
		check_authority_url();		
	}


	function index()
	{
		
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));

		$data['btl'] = $this->Btl_Model->get_all($params);
		$data['kota'] = $this->Kota_Model->get_all();
		$data['position_id'] = $position_id;
		$this->load->view('index',$data);
	}

	function querypaging()
	{
		$user_id = $this->session->userdata('user_id');
		$position_id = $this->session->userdata('position_id');
		$id_departemen = $this->session->userdata('id_departemen');
		
		$params_staff = array_filter(array(
            'position_id' => $position_id,
            'id_departemen' => $id_departemen,
      	));
		
		$get_detail_staff = $this->Staff_model->get_detail_staff($user_id,$params_staff);

		$params = array_filter(array(
            'position_id' => $position_id,
            'kode_kanwil' => $get_detail_staff->kode_kanwil,
            'kode_area' => $get_detail_staff->kode_area,
            'kode_sub_area' => $get_detail_staff->kode_sub_area,
            'id_departemen' => $id_departemen,
      	));


	 	$list = $this->Btl_Model->get_datatables($params);
	 
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_btl) {
            $no++;
            $row = array();
            //$row[] = $no;
          

			if($position_id == 1)
			{
				 $button = "<a class='btn bg-slate btn-icon' href='".base_url()."btl/edit/".$data_btl->btl_id."' title=Edit'><i class='fa fa-edit'></i></a>

						
						 <a class='btn bg-slate btn-icon' href='".base_url()."btl/delete/".$data_btl->btl_id."' title='Delete' onClick='return dodelete();'><i class='icon-trash'></i></a>";
			}
			else
			{
				$button = "";

			}
			
			if($data_btl->status_btl == 1)
			{
				$status_btl = "Aktif";
			}
			else
			{
				$status_btl = "Tidak Aktif";
			}

            $row[] = $data_btl->btl_id;
            $row[] = $data_btl->kodefikasi;
            $row[] = $data_btl->nm_btl;
            $row[] = $data_btl->nm_kanwil;
            $row[] = $data_btl->nm_area;
            $row[] = $data_btl->alamat_btl;
            $row[] = $data_btl->nm_direktur;
            $row[] = $data_btl->nm_ahli_teknik;
            $row[] = $data_btl->nm_asosiasi;
            $row[] = $status_btl;
            $row[] = $button;
           
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Btl_Model->count_all($params),
                        "recordsFiltered" => $this->Btl_Model->count_filtered($params),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);

	}

	function btl_djk()
	{
		$kota = $this->input->post('kota');
		$data['detail_kota'] = $this->Kota_Model->get_detail_kodefikasi($kota);

		$btl = $this->api_btl($kota);
		$data['btl'] = json_decode($btl,TRUE);
		$data['kota'] = $this->Kota_Model->get_all();
		
		$this->load->view('btl_djk',$data);
	}

	function api_btl($kota)
	{

		$consumerId = "jki";
		$secretKey = "X1oRO5yB";

		//Computes timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		//Computes signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $consumerId."&".$tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		$header[] = "X-cons-id:".$consumerId;
		$header[] = "X-timestamp:".$tStamp;
		$header[] = "X-signature:".$encodedSignature;

		$postData = [
			'kota_id' => $kota //optional
		];

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, 'http://103.87.161.97/slo/api/ref/sbu'); 
		//curl_setopt ($ch, CURLOPT_URL, 'http://localhost/slo/new/api/ref/sbu'); 
		curl_setopt ($ch, CURLOPT_HEADER, 0); 
		curl_setopt ($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($postData));
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch); 
		curl_close ($ch);
		return $tmp;

	}

	function add()
	{	
		
		$data['asosiasi'] = $this->Asosiasi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		
		$this->load->view('add',$data);

		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$kodefikasi = $this->input->post('kodefikasi');
			$asosiasi = $this->input->post('asosiasi');
			$area = $this->input->post('area');
			$nm_btl = $this->input->post('nm_btl');
			$nm_direktur = $this->input->post('nm_direktur');
			$nm_ahli_teknik = $this->input->post('nm_ahli_teknik');
			$alamat = $this->input->post('alamat');
			$status = $this->input->post('status');

			try {
				
				$data_btl = array(
					"kodefikasi"=> $kodefikasi,
					"id_asosiasi"=> $asosiasi,
					"kode_area"=> $area,
					"nm_btl"=> $nm_btl,
					"nm_direktur"=> $nm_direktur,
					"nm_ahli_teknik"=> $nm_ahli_teknik,
					"alamat_btl"=> $alamat,
					"status"=> $status,
				);
				
			
				$insert_btl = $this->Btl_Model->insert($data_btl);

			} catch (Exception $e) {
				
				$this->session->set_flashdata('error', 'Gagal input bangunan');

			}


				if($insert_btl == 1) {
						$this->session->set_flashdata('success', 'Data has been submitted successfully');
						redirect('btl','refresh');
				}


		}
	
	}
	
	function edit()
	{
		$btl_id = $this->uri->segment(3);

		$data['detail_btl'] = $this->Btl_Model->get_btl_by_id($btl_id);
		$data['asosiasi'] = $this->Asosiasi_Model->get_all();
		$data['kanwil'] = $this->Kanwil_Model->get_all();
		$data['area'] = $this->Kantor_area_model->area_by_kanwil($data['detail_btl']->kode_kanwil);

		$this->load->view('edit',$data);

	}


	function proses_edit()
	{
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") {

			$btl_id = $this->input->post('btl_id');
			$kodefikasi = $this->input->post('kodefikasi');
			$asosiasi = $this->input->post('asosiasi');
			$area = $this->input->post('area');
			$nm_btl = $this->input->post('nm_btl');
			$nm_direktur = $this->input->post('nm_direktur');
			$nm_ahli_teknik = $this->input->post('nm_ahli_teknik');
			$alamat = $this->input->post('alamat');
			$status = $this->input->post('status');


				try {
					
						$data_btl = array(
							"kodefikasi"=> $kodefikasi,
							"id_asosiasi"=> $asosiasi,
							"kode_area"=> $area,
							"nm_btl"=> $nm_btl,
							"nm_direktur"=> $nm_direktur,
							"nm_ahli_teknik"=> $nm_ahli_teknik,
							"alamat_btl"=> $alamat,
							"status"=> $status,
						);
					

					$update = $this->Btl_Model->update($data_btl,$btl_id);


				} catch (Exception $e) {
					
					$this->session->set_flashdata('error', 'Gagal input provinsi');

				}


			if($update == 1) 
			{
							$this->session->set_flashdata('success', 'Sukses ubah data');
							redirect('btl','refresh');
			}


		}
		
	}

}
