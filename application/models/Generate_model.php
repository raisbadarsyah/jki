<?php
	class Generate_model extends CI_Model {
		
	function count_nomor() {
        $this->db->select('*');
        $this->db->from('user_registration');
        return $this->db->get()->num_rows();
    }

    function get_last_number_code() {
        $this->db->select_max('userNoRegister');
        $this->db->from('user_registration');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $userNoRegister = $row->userNoRegister;
            }
            return $userNoRegister;
        } else {
            return 0;
        }
    }

	}
?>