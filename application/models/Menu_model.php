<?php

class Menu_model extends CI_Model {

	function get_parent_menu($parentId, $id_user){
		$this->db->select('core_menu.coreMenuId, core_menu.coreMenuName, core_menu.coreMenuUrl, core_menu.coreMenuIcon, core_menu.coreMenuTitle, core_menu.coreParentId, Deriv1.Count');
        $this->db->from('core_role');
        $this->db->join('core_menu', 'core_role.coreRoleMenu = core_menu.coreMenuId');
        $this->db->join('core_user', 'core_role.coreRolePosition = core_user.coreUserPositionId');
        $this->db->join('(SELECT coreParentId, COUNT(*) AS COUNT FROM `core_menu` GROUP BY coreParentId) as Deriv1', 'core_menu.coreMenuId = Deriv1.coreParentId','LEFT');
		$this->db->where('core_menu.coreParentId', $parentId);
		// $this->db->where('core_menu.coreMenuHidden', '0');
        //$this->db->where('core_role.coreRolePosition', $position_id);
		$this->db->where('core_user.coreUserId', $id_user);
		$this->db->where('core_role.coreRoleActive','1');
        $this->db->group_by('core_menu.coreMenuId');
		$this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
	}

    function get_mpi_user_parent_menu($parentId, $id_user, $position_id){
        $this->db->select('core_menu.coreMenuId, core_menu.coreMenuName, core_menu.coreMenuUrl, core_menu.coreMenuIcon, core_menu.coreMenuTitle, core_menu.coreParentId, Deriv1.Count');
        $this->db->from('core_role');
        $this->db->join('core_mpi_user', 'core_role.coreRolePosition = core_mpi_user.coreUserPositionId');
        $this->db->join('core_menu', 'core_role.coreRoleMenu = core_menu.coreMenuId');
        $this->db->join('(SELECT coreParentId, COUNT(*) AS COUNT FROM `core_menu` GROUP BY coreParentId) as Deriv1', 'core_menu.coreMenuId = Deriv1.coreParentId','LEFT');
        $this->db->where('core_menu.coreParentId', $parentId);
        // $this->db->where('core_menu.coreMenuHidden', '0');
        $this->db->where('core_role.coreRolePosition', $position_id);
        $this->db->where('core_mpi_user.coreUserId', $id_user);
        $this->db->where('core_role.coreRoleActive','1');
        $this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
    }

    function get_super_user_parent_menu($parentId, $id_user, $position_id){
       $this->db->select('core_menu.coreMenuId, core_menu.coreMenuName, core_menu.coreMenuUrl, core_menu.coreMenuIcon, core_menu.coreMenuTitle, core_menu.coreParentId, Deriv1.Count');
        $this->db->from('core_role');
        $this->db->join('core_menu', 'core_role.coreRoleMenu = core_menu.coreMenuId');
        $this->db->join('core_super_user', 'core_role.coreRolePosition = core_super_user.coreUserPositionId');
        $this->db->join('(SELECT coreParentId, COUNT(*) AS COUNT FROM `core_menu` GROUP BY coreParentId) as Deriv1', 'core_menu.coreMenuId = Deriv1.coreParentId','LEFT');
        $this->db->where('core_menu.coreParentId', $parentId);
        // $this->db->where('core_menu.coreMenuHidden', '0');
        //$this->db->where('core_role.coreRolePosition', $position_id);
        $this->db->where('core_super_user.coreUserId', $id_user);
        $this->db->where('core_role.coreRoleActive','1');
        $this->db->group_by('core_menu.coreMenuId');
        $this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
       // return $this->db->last_query();
    }

	function get_menu_id($menuUri){
		$query = $this->db->get_where('core_menu',array('coreMenuUrl' => $menuUri));
		if($query->num_rows() > 0){
			foreach ($query->result() as $row) {
				return $row->coreMenuId;
			}
		}else{
				return FALSE;
		}
	}

	function get_authority_grup($id_menu, $id_grup, $id_user){
		$this->db->select('core_menu.coreMenuId, core_menu.coreMenuName, core_menu.coreMenuUrl');
        $this->db->from('core_role');
        $this->db->join('core_user', 'core_role.coreRolePosition = core_user.coreUserPositionId');
		$this->db->join('core_menu', 'core_role.coreRoleMenu = core_menu.coreMenuId');
        $this->db->where('core_role.coreRolePosition', $id_grup);
		$this->db->where('core_user.coreUserId', $id_user);
		$this->db->where('core_role.coreRoleActive','1');
		$this->db->where('core_menu.coreMenuId', $id_menu);
		$this->db->order_by('core_menu.coreMenuSort');
        $query = $this->db->get();
        
        if($query->num_rows() > 0){
			return TRUE;
		}else{
			return $this->db->last_query();
		}

	}

	//function for breadcrumb
	function get_parent_menu_breadcrumb($uri){
		$query = $this->db->get_where('core_menu',array('coreMenuUrl' => $uri));
		return $query->result();
	}

	function get_parent_menu_id_breadcrumb($id){
		$query = $this->db->get_where('core_menu',array('coreMenuId' => $id));
		return $query->result();
	}
	
	function get_principal_list($user_id) {
        $this->db->select('suplierId, supplierPrincipal');
        $this->db->from('m_supplier');
        $this->db->join('core_user','m_supplier.suplierId=core_user.supplierId','left');
        $this->db->where('core_user.coreUserId', $user_id);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
    }
	
	function get_cabang_list($user_id) {
        $this->db->select('branchId, branchName');
        $this->db->from('m_branch');
		$this->db->join('core_user_cabang','m_branch.branchId=core_user_cabang.coreUserBranchId','left');
        $this->db->where('core_user_cabang.coreUserUserId', $user_id);
        $query = $this->db->get();
        //echo "<pre>".$this->db->last_query()."</pre>";
        return $query->result();
    }
	
	function get_nama_principal($principal_id) {
        $this->db->select('supplierPrincipal');
        $this->db->from('m_supplier');
        $this->db->where('suplierId', $principal_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $supplierPrincipal = $row->supplierPrincipal;
            }

            return $supplierPrincipal;
        } else {
            return FALSE;
        }
    }
	
	function get_nama_cabang($cabang_id) {
        $this->db->select('branchName');
        $this->db->from('m_branch');
        $this->db->where('branchId', $cabang_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $branchName = $row->branchName;
            }

            return $branchName;
        } else {
            return FALSE;
        }
    }
}