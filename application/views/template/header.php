
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PT.JKI</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>template/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script async="async" type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/pace.min.js"></script>
	<script async="async" type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/jquery.min.js"></script>
	<script async="async" type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/libraries/bootstrap.min.js"></script>
	<script async="async" type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/plugins/visualization/echarts/echarts.js"></script>

	<script async="async" type="text/javascript" src="<?php echo base_url();?>template/assets/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>template/assets/js/pages/user_pages_profile.js"></script>
	<!-- /theme JS files -->

	<style type="text/css">
		
		@import url('<?php echo base_url();?>template/cleandream/css/style_text.css');
	
		@import url('<?php echo base_url();?>template/cleandream/css/menu.css');
	
	</style>

</head>

<body>

	<!-- Main navbar -->
	<?php
	$this->load->view('template/main_navbar');
	?>
	<!-- /main navbar -->

