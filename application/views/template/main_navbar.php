
<div class="navbar navbar-inverse">
		<div class="navbar-header">
			
			<a class="navbar-brand">
			<span style="color:#FFFFFF";">PT.JKI</span>
			</a>
			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
				<li><a class="sidebar-mobile-secondary-toggle"><i class="icon-more"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				<li>
					<a class="sidebar-control sidebar-secondary-hide hidden-xs">
						<i class="icon-transmission"></i>
					</a>
				</li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				
				<li>
					
					

				</li>
				
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url();?>template/assets/images/placeholder.jpg" alt="">
						<span><?php echo $this->session->userdata('uname');  ?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?php echo base_url(); ?>management_user/profile_by_id/<?php echo $this->session->userdata('user_id'); ?>"><i class="icon-user-plus"></i> My profile</a></li>
						<li class="divider"></li>
					
						<li><a href="<?php echo base_url().'auth/logout'; ?>"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>