	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/icons/icomoon/styles.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/core.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/components.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/colors.css') ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/icons/fontawesome/styles.min.css') ?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->
	<!-- mask money and wa-mediabox -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/wa-mediabox/wa-mediabox.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/wa-mediabox/wa-mediabox.min.css" />
	
	<!-- mask money and wa-mediabox -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/pace.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/bootstrap.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/blockui.min.js') ?>"></script>
	<!-- <script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
	 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/all.css');?>">
	<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
	<!-- /core JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/notifications/pnotify.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/components_notifications_pnotify.js')?>"></script>
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/visualization/d3/d3.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/visualization/d3/d3_tooltip.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/styling/switchery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/selects/bootstrap_multiselect.js') ?>"></script>
	
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/ui/moment/moment.min.js') ?>"></script>
	
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/notifications/bootbox.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/notifications/sweet_alert.min.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/components_modals.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/core/app.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/picker_date.js')?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/datatables_basic.js')?>"></script>
	
	
	<!-- DATATABLES and select2-->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/tables/datatables/datatables.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/selects/select2.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/datatables_extension_autofill.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/jquery_ui/interactions.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/tables/datatables/extensions/select.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/datatables_extension_buttons_print.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/tables/datatables/extensions/buttons.min.js') ?>"></script>
	<!-- DATATABLES -->
	<!-- form validation -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/validation/validate.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/inputs/touchspin.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/styling/switch.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/pages/form_validation.js') ?>"></script>
	<!-- Form validation -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/notifications/jgrowl.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/anytime.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/pickadate/picker.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/pickadate/picker.date.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/pickadate/picker.time.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/daterangepicker.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/plugins/pickers/pickadate/legacy.js') ?>"></script>